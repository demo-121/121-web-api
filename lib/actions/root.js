"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.save = exports.cleanClientData = undefined;

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * cleanClientData
 * @description update quotation
 * @reducer client
 * @reducer appSummary
 * @reducer quotation
 * @param {function} dispatch - redux dispatch function
 * */
var cleanClientData = exports.cleanClientData = function cleanClientData(_ref) {
  var dispatch = _ref.dispatch;

  dispatch({
    type: _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA
  });
};

var save = exports.save = function save() {
  return {};
};