"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.updateRecommendationDetails = exports.updateRecommendationSelectedQuotation = exports.updateBudgetData = exports.validateBudget = exports.updateRecommendationData = exports.validateRecommendation = exports.updateRecommendationStep = exports.closeRecommendation = exports.getRecommendation = undefined;

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * saga actions
 */

/**
 * getRecommendation
 * @description get data when open Recommendation page
 * @reducer recommendation
 * @param {function} dispatch - redux dispatch function
 * */
var getRecommendation = exports.getRecommendation = function getRecommendation(_ref) {
  var dispatch = _ref.dispatch;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].SAGA_GET_RECOMMENDATION
  });
};

/**
 * closeRecommendation
 * @description save data when leave Recommendation page
 * @reducer recommendation
 * @param {function} dispatch - redux dispatch function
 * */
var closeRecommendation = exports.closeRecommendation = function closeRecommendation(_ref2) {
  var dispatch = _ref2.dispatch;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].SAGA_CLOSE_RECOMMENDATION
  });
};

/**
 * updateRecommendationStep
 * @description update step info when move to next step
 * @reducer recommendation.recommendation
 * @reducer recommendation.budget
 * @reducer recommendation.acceptance
 * @reducer recommendation.component
 * @param {function} dispatch - redux dispatch function
 * @param {number} newCurrentStep - next step index
 * */
var updateRecommendationStep = exports.updateRecommendationStep = function updateRecommendationStep(_ref3) {
  var dispatch = _ref3.dispatch,
      newCurrentStep = _ref3.newCurrentStep;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].SAGA_UPDATE_RECOMMENDATION_STEP,
    newCurrentStep: newCurrentStep
  });
};

/**
 * validateRecommendation
 * @description perform whole page validation in recommendation step
 * @reducer recommendation.error
 * @reducer recommendation.component
 * @param {function} dispatch - redux dispatch function
 * @param {boolean} init - indicate the action is called when init component
 * */
var validateRecommendation = exports.validateRecommendation = function validateRecommendation(_ref4) {
  var dispatch = _ref4.dispatch,
      init = _ref4.init;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].SAGA_VALIDATE_RECOMMENDATION,
    init: init
  });
};

/**
 * updateRecommendationData
 * @description update data, error and component when user input value in recommendation step
 * @reducer recommendation.recommendation
 * @reducer recommendation.error
 * @reducer recommendation.component
 * @param {function} dispatch - redux dispatch function
 * @param {string} quotationId - quotation Id of new data for recommendation
 * @param {object} newValue - new data for recommendation
 * */
var updateRecommendationData = exports.updateRecommendationData = function updateRecommendationData(_ref5) {
  var dispatch = _ref5.dispatch,
      quotationId = _ref5.quotationId,
      newValue = _ref5.newValue;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].SAGA_UPDATE_RECOMMENDATION_DATA,
    quotationId: quotationId,
    newValue: newValue
  });
};

/**
 * validateBudget
 * @description perform whole page validation in budget step
 * @reducer recommendation.error
 * @reducer recommendation.component
 * @param {function} dispatch - redux dispatch function
 * */
var validateBudget = exports.validateBudget = function validateBudget(_ref6) {
  var dispatch = _ref6.dispatch;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].SAGA_VALIDATE_BUDGET
  });
};

/**
 * updateBudgetData
 * @description update data, error and component when user input value in budget step
 * @reducer recommendation.budget
 * @reducer recommendation.error
 * @reducer recommendation.component
 * @param {function} dispatch - redux dispatch function
 * @param {object} newValue - new data for budget
 * @param {object} newDataError - new error object for budget
 * */
var updateBudgetData = exports.updateBudgetData = function updateBudgetData(_ref7) {
  var dispatch = _ref7.dispatch,
      newValue = _ref7.newValue;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].SAGA_UPDATE_BUDGET_DATA,
    newValue: newValue
  });
};

/**
 * normal actions
 */

/**
 * updateRecommendationSelectedQuotation
 * @description update selected quotation id when click section list in Recommendation step
 * @reducer recommendation.component
 * @param {function} dispatch - redux dispatch function
 * @param {string} newSelectedQuotId - quotation id for selected quotation
 * */
var updateRecommendationSelectedQuotation = exports.updateRecommendationSelectedQuotation = function updateRecommendationSelectedQuotation(_ref8) {
  var dispatch = _ref8.dispatch,
      newSelectedQuotId = _ref8.newSelectedQuotId;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_RECOMMENDATION_SELECTED_QUOT,
    newSelectedQuotId: newSelectedQuotId
  });
};

/**
 * updateRecommendationDetails
 * @description update step when user click to next step
 * @reducer recommendation.component
 * @param {function} dispatch - redux dispatch function
 * @param {boolean} newShowDetails - show details flag
 * */
var updateRecommendationDetails = exports.updateRecommendationDetails = function updateRecommendationDetails(_ref9) {
  var dispatch = _ref9.dispatch,
      newShowDetails = _ref9.newShowDetails;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_RECOMMENDATION_DETAILS,
    newShowDetails: newShowDetails
  });
};