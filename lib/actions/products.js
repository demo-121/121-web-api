"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.insuredOnChange = exports.currencyOnChange = exports.getProductList = exports.getDependants = undefined;

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * getDependants
 * @description get products required dependants data
 * @param {function} dispatch - redux dispatch function
 * */
var getDependants = exports.getDependants = function getDependants(_ref) {
  var dispatch = _ref.dispatch;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.PRODUCTS].GET_DEPENDANTS
  });
};
/**
 * getProductList
 * @description get product list
 * @param {function} dispatch - redux dispatch function
 * @param {function} callback - callback of this dispatch
 * */
var getProductList = exports.getProductList = function getProductList(_ref2) {
  var dispatch = _ref2.dispatch,
      _ref2$callback = _ref2.callback,
      callback = _ref2$callback === undefined ? function () {} : _ref2$callback;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.PRODUCTS].GET_PRODUCT_LIST,
    callback: callback
  });
};
/**
 * currencyOnChange
 * @description get product list
 * @param {function} dispatch - redux dispatch function
 * @param {string} currencyData - new currency data
 * */
var currencyOnChange = exports.currencyOnChange = function currencyOnChange(_ref3) {
  var dispatch = _ref3.dispatch,
      currencyData = _ref3.currencyData;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.PRODUCTS].UPDATE_CURRENCY,
    currencyData: currencyData
  });
};
/**
 * insuredOnChange
 * @description get product list
 * @param {function} dispatch - redux dispatch function
 * @param {string} insuredCidData - new insured data
 * */
var insuredOnChange = exports.insuredOnChange = function insuredOnChange(_ref4) {
  var dispatch = _ref4.dispatch,
      insuredCidData = _ref4.insuredCidData;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.PRODUCTS].UPDATE_INSURED_CID,
    insuredCidData: insuredCidData
  });
};