"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.updateUiViewPlanDetails = exports.continueApplicationForm = undefined;

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * actions
 */

/**
 * continueApplicationForm
 * @description continue application form
 * @reducer application
 * @param {function} dispatch - redux dispatch function
 * @param {string} [applicationId] - application id to continue
 * @param {string} [quotType] - "SHIELD" or "" (NON-SHIELD)
 * */
var continueApplicationForm = exports.continueApplicationForm = function continueApplicationForm(_ref) {
  var dispatch = _ref.dispatch,
      applicationId = _ref.applicationId,
      quotType = _ref.quotType,
      callback = _ref.callback;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].SAGA_CONTINUE_APPLICATION_FORM,
    applicationId: applicationId,
    quotType: quotType,
    callback: callback
  });
};

/**
 * updateUiViewPlanDetails
 * @description update UI to show / hide plan details
 * @reducer application.component
 * @param {function} dispatch - redux dispatch function
 * @param {boolean} isPlanDetailsView - show plan details flag
 * */
var updateUiViewPlanDetails = exports.updateUiViewPlanDetails = function updateUiViewPlanDetails(_ref2) {
  var dispatch = _ref2.dispatch,
      isPlanDetailsView = _ref2.isPlanDetailsView;

  dispatch({
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_UI_VIEW_PLAN_DETAILS,
    isPlanDetailsView: isPlanDetailsView
  });
};