"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.utilities = exports.REDUCER_TYPE_CHECK = exports.reducers = exports.ACTION_LIST = exports.PAYMENT_BANK = exports.EAPP = exports.DYNAMIC_VIEWS = exports.ROP_TABLE = exports.USER_TAB_TYPES = exports.LANGUAGE_TYPES = exports.TEXT_STORE = exports.saga = exports.NUMBER_FORMAT = exports.PROGRESS_TABS = exports.SYSTEM = exports.NEEDS = exports.DEPENDANT = exports.TOLERANCE_LEVEL = exports.SECTION_KEYS = exports.PRODUCT_TYPES = exports.DIALOG_TYPES = exports.DOCUMENT_TYPES = exports.FIELD_TYPES = exports.FILTER_TYPES = exports.ACTION_TYPES = exports.REDUCER_TYPES = exports.TIME_FORMAT = undefined;

var _ACTION_LIST, _reducers, _REDUCER_TYPE_CHECK;

var _client = require("./actions/client");

var clientActions = _interopRequireWildcard(_client);

var _clientForm = require("./actions/clientForm");

var clientFormActions = _interopRequireWildcard(_clientForm);

var _fna = require("./actions/fna");

var fnaActions = _interopRequireWildcard(_fna);

var _preApplication = require("./actions/preApplication");

var preApplicationActions = _interopRequireWildcard(_preApplication);

var _products = require("./actions/products");

var productsActions = _interopRequireWildcard(_products);

var _quotation = require("./actions/quotation");

var quotationActions = _interopRequireWildcard(_quotation);

var _recommendation = require("./actions/recommendation");

var recommendationActions = _interopRequireWildcard(_recommendation);

var _application = require("./actions/application");

var applicationActions = _interopRequireWildcard(_application);

var _TIME_FORMAT = require("./constants/TIME_FORMAT");

var TIME_FORMAT = _interopRequireWildcard(_TIME_FORMAT);

var _root = require("./actions/root");

var root = _interopRequireWildcard(_root);

var _ACTION_TYPES = require("./constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _FIELD_TYPES = require("./constants/FIELD_TYPES");

var FIELD_TYPES = _interopRequireWildcard(_FIELD_TYPES);

var _FILTER_TYPES = require("./constants/FILTER_TYPES");

var _FILTER_TYPES2 = _interopRequireDefault(_FILTER_TYPES);

var _PAYMENT_BANK = require("./constants/PAYMENT_BANK");

var _PAYMENT_BANK2 = _interopRequireDefault(_PAYMENT_BANK);

var _SYSTEM = require("./constants/SYSTEM");

var _SYSTEM2 = _interopRequireDefault(_SYSTEM);

var _PROGRESS_TABS = require("./constants/PROGRESS_TABS");

var _PROGRESS_TABS2 = _interopRequireDefault(_PROGRESS_TABS);

var _NUMBER_FORMAT = require("./constants/NUMBER_FORMAT");

var NUMBER_FORMAT = _interopRequireWildcard(_NUMBER_FORMAT);

var _SECTION_KEYS = require("./constants/SECTION_KEYS");

var _SECTION_KEYS2 = _interopRequireDefault(_SECTION_KEYS);

var _client2 = require("./constants/REDUCER_TYPE_CHECK/client");

var clientTypeCheck = _interopRequireWildcard(_client2);

var _clientForm2 = require("./constants/REDUCER_TYPE_CHECK/clientForm");

var clientFormTypeCheck = _interopRequireWildcard(_clientForm2);

var _common = require("./constants/REDUCER_TYPE_CHECK/common");

var commonTypeCheck = _interopRequireWildcard(_common);

var _optionsMap = require("./constants/REDUCER_TYPE_CHECK/optionsMap");

var _optionsMap2 = _interopRequireDefault(_optionsMap);

var _preApplication2 = require("./constants/REDUCER_TYPE_CHECK/preApplication");

var preApplicationTypeCheck = _interopRequireWildcard(_preApplication2);

var _application2 = require("./constants/REDUCER_TYPE_CHECK/application");

var applicationTypeCheck = _interopRequireWildcard(_application2);

var _supportingDocument = require("./constants/REDUCER_TYPE_CHECK/supportingDocument");

var supportingDocumentTypeCheck = _interopRequireWildcard(_supportingDocument);

var _products2 = require("./constants/REDUCER_TYPE_CHECK/products");

var productsTypeCheck = _interopRequireWildcard(_products2);

var _proposal = require("./constants/REDUCER_TYPE_CHECK/proposal");

var proposalTypeCheck = _interopRequireWildcard(_proposal);

var _quotation2 = require("./constants/REDUCER_TYPE_CHECK/quotation");

var quotationTypeCheck = _interopRequireWildcard(_quotation2);

var _recommendation2 = require("./constants/REDUCER_TYPE_CHECK/recommendation");

var recommendationTypeCheck = _interopRequireWildcard(_recommendation2);

var _paymentAndSubmission = require("./constants/REDUCER_TYPE_CHECK/paymentAndSubmission");

var paymentAndSubmissionTypeCheck = _interopRequireWildcard(_paymentAndSubmission);

var _signature = require("./constants/REDUCER_TYPE_CHECK/signature");

var signatureTypeCheck = _interopRequireWildcard(_signature);

var _DOCUMENT_TYPES = require("./constants/DOCUMENT_TYPES");

var DOCUMENT_TYPES = _interopRequireWildcard(_DOCUMENT_TYPES);

var _DIALOG_TYPES = require("./constants/DIALOG_TYPES");

var DIALOG_TYPES = _interopRequireWildcard(_DIALOG_TYPES);

var _PRODUCT_TYPES = require("./constants/PRODUCT_TYPES");

var PRODUCT_TYPES = _interopRequireWildcard(_PRODUCT_TYPES);

var _REDUCER_TYPES = require("./constants/REDUCER_TYPES");

var REDUCER_TYPES = _interopRequireWildcard(_REDUCER_TYPES);

var _config = require("./constants/REDUCER_TYPE_CHECK/config");

var configTypeCheck = _interopRequireWildcard(_config);

var _fna2 = require("./constants/REDUCER_TYPE_CHECK/fna");

var fnaTypeCheck = _interopRequireWildcard(_fna2);

var _TOLERANCE_LEVEL = require("./constants/TOLERANCE_LEVEL");

var TOLERANCE_LEVEL = _interopRequireWildcard(_TOLERANCE_LEVEL);

var _DEPENDANT = require("./constants/DEPENDANT");

var DEPENDANT = _interopRequireWildcard(_DEPENDANT);

var _NEEDS = require("./constants/NEEDS");

var NEEDS = _interopRequireWildcard(_NEEDS);

var _USER_TAB_TYPES = require("./constants/USER_TAB_TYPES");

var USER_TAB_TYPES = _interopRequireWildcard(_USER_TAB_TYPES);

var _ROP_TABLE = require("./constants/ROP_TABLE");

var ROP_TABLE = _interopRequireWildcard(_ROP_TABLE);

var _DYNAMIC_VIEWS = require("./constants/DYNAMIC_VIEWS");

var DYNAMIC_VIEWS = _interopRequireWildcard(_DYNAMIC_VIEWS);

var _EAPP = require("./constants/EAPP");

var _EAPP2 = _interopRequireDefault(_EAPP);

var _locales = require("./locales");

var _locales2 = _interopRequireDefault(_locales);

var _client3 = require("./reducers/client");

var _client4 = _interopRequireDefault(_client3);

var _clientForm3 = require("./reducers/clientForm");

var _clientForm4 = _interopRequireDefault(_clientForm3);

var _fna3 = require("./reducers/fna");

var _fna4 = _interopRequireDefault(_fna3);

var _optionsMap3 = require("./reducers/optionsMap");

var _optionsMap4 = _interopRequireDefault(_optionsMap3);

var _preApplication3 = require("./reducers/preApplication");

var _preApplication4 = _interopRequireDefault(_preApplication3);

var _products3 = require("./reducers/products");

var _products4 = _interopRequireDefault(_products3);

var _proposal2 = require("./reducers/proposal");

var _proposal3 = _interopRequireDefault(_proposal2);

var _quotation3 = require("./reducers/quotation");

var _quotation4 = _interopRequireDefault(_quotation3);

var _recommendation3 = require("./reducers/recommendation");

var _recommendation4 = _interopRequireDefault(_recommendation3);

var _agent = require("./reducers/agent");

var _agent2 = _interopRequireDefault(_agent);

var _agentUX = require("./reducers/agentUX");

var _agentUX2 = _interopRequireDefault(_agentUX);

var _application3 = require("./reducers/application");

var _application4 = _interopRequireDefault(_application3);

var _supportingDocument2 = require("./reducers/supportingDocument");

var _supportingDocument3 = _interopRequireDefault(_supportingDocument2);

var _paymentAndSubmission2 = require("./reducers/paymentAndSubmission");

var _paymentAndSubmission3 = _interopRequireDefault(_paymentAndSubmission2);

var _signature2 = require("./reducers/signature");

var _signature3 = _interopRequireDefault(_signature2);

var _saga = require("./saga");

var _saga2 = _interopRequireDefault(_saga);

var _common2 = require("./utilities/common");

var common = _interopRequireWildcard(_common2);

var _application5 = require("./utilities/application");

var applicationUtil = _interopRequireWildcard(_application5);

var _fundChecker = require("./utilities/fundChecker");

var _fundChecker2 = _interopRequireDefault(_fundChecker);

var _getProductId = require("./utilities/getProductId");

var _getProductId2 = _interopRequireDefault(_getProductId);

var _trigger = require("./utilities/trigger");

var trigger = _interopRequireWildcard(_trigger);

var _validation = require("./utilities/validation");

var validation = _interopRequireWildcard(_validation);

var _dataMapping = require("./utilities/dataMapping");

var dataMapping = _interopRequireWildcard(_dataMapping);

var _numberFormatter = require("./utilities/numberFormatter");

var numberFormatter = _interopRequireWildcard(_numberFormatter);

var _naAnalysis = require("./utilities/naAnalysis");

var naAnalysis = _interopRequireWildcard(_naAnalysis);

var _needsUtil = require("./utilities/needsUtil");

var needsUtil = _interopRequireWildcard(_needsUtil);

var _preApplication5 = require("./utilities/preApplication");

var preApplicationUtil = _interopRequireWildcard(_preApplication5);

var _dynamicApplicationForm = require("./utilities/dynamicApplicationForm");

var dynamicApplicationForm = _interopRequireWildcard(_dynamicApplicationForm);

var _genProfileMandatory = require("./utilities/genProfileMandatory");

var _genProfileMandatory2 = _interopRequireDefault(_genProfileMandatory);

var _searchClient = require("./utilities/searchClient");

var _searchClient2 = _interopRequireDefault(_searchClient);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
// TODO: will be remove fnaRA in the future version
// import fnaRA from "./reducers/fnaRA";


exports.TIME_FORMAT = TIME_FORMAT;
exports.REDUCER_TYPES = REDUCER_TYPES;
exports.ACTION_TYPES = _ACTION_TYPES2.default;
exports.FILTER_TYPES = _FILTER_TYPES2.default;
exports.FIELD_TYPES = FIELD_TYPES;
exports.DOCUMENT_TYPES = DOCUMENT_TYPES;
exports.DIALOG_TYPES = DIALOG_TYPES;
exports.PRODUCT_TYPES = PRODUCT_TYPES;
exports.SECTION_KEYS = _SECTION_KEYS2.default;
exports.TOLERANCE_LEVEL = TOLERANCE_LEVEL;
exports.DEPENDANT = DEPENDANT;
exports.NEEDS = NEEDS;
exports.SYSTEM = _SYSTEM2.default;
exports.PROGRESS_TABS = _PROGRESS_TABS2.default;
exports.NUMBER_FORMAT = NUMBER_FORMAT;
exports.saga = _saga2.default;
exports.TEXT_STORE = _locales2.default;
exports.LANGUAGE_TYPES = _locales.LANGUAGE_TYPES;
exports.USER_TAB_TYPES = USER_TAB_TYPES;
exports.ROP_TABLE = ROP_TABLE;
exports.DYNAMIC_VIEWS = DYNAMIC_VIEWS;
exports.EAPP = _EAPP2.default;
exports.PAYMENT_BANK = _PAYMENT_BANK2.default;
var ACTION_LIST = exports.ACTION_LIST = (_ACTION_LIST = {
  root: root
}, _defineProperty(_ACTION_LIST, REDUCER_TYPES.CLIENT_FORM, clientFormActions), _defineProperty(_ACTION_LIST, REDUCER_TYPES.PRE_APPLICATION, preApplicationActions), _defineProperty(_ACTION_LIST, REDUCER_TYPES.RECOMMENDATION, recommendationActions), _defineProperty(_ACTION_LIST, REDUCER_TYPES.QUOTATION, quotationActions), _defineProperty(_ACTION_LIST, REDUCER_TYPES.FNA, fnaActions), _defineProperty(_ACTION_LIST, REDUCER_TYPES.PRODUCTS, productsActions), _defineProperty(_ACTION_LIST, REDUCER_TYPES.CLIENT, clientActions), _defineProperty(_ACTION_LIST, REDUCER_TYPES.APPLICATION, applicationActions), _ACTION_LIST);

var reducers = exports.reducers = (_reducers = {}, _defineProperty(_reducers, REDUCER_TYPES.AGENT, _agent2.default), _defineProperty(_reducers, REDUCER_TYPES.AGENTUX, _agentUX2.default), _defineProperty(_reducers, REDUCER_TYPES.CLIENT, _client4.default), _defineProperty(_reducers, REDUCER_TYPES.CLIENT_FORM, _clientForm4.default), _defineProperty(_reducers, REDUCER_TYPES.FNA, _fna4.default), _defineProperty(_reducers, REDUCER_TYPES.PROPOSAL, _proposal3.default), _defineProperty(_reducers, REDUCER_TYPES.QUOTATION, _quotation4.default), _defineProperty(_reducers, REDUCER_TYPES.RECOMMENDATION, _recommendation4.default), _defineProperty(_reducers, REDUCER_TYPES.PRE_APPLICATION, _preApplication4.default), _defineProperty(_reducers, REDUCER_TYPES.PRODUCTS, _products4.default), _defineProperty(_reducers, REDUCER_TYPES.OPTIONS_MAP, _optionsMap4.default), _defineProperty(_reducers, REDUCER_TYPES.APPLICATION, _application4.default), _defineProperty(_reducers, REDUCER_TYPES.SUPPORTING_DOCUMENT, _supportingDocument3.default), _defineProperty(_reducers, REDUCER_TYPES.PAYMENT_AND_SUBMISSION, _paymentAndSubmission3.default), _defineProperty(_reducers, REDUCER_TYPES.SIGNATURE, _signature3.default), _reducers);

var REDUCER_TYPE_CHECK = exports.REDUCER_TYPE_CHECK = (_REDUCER_TYPE_CHECK = {}, _defineProperty(_REDUCER_TYPE_CHECK, REDUCER_TYPES.CLIENT, clientTypeCheck), _defineProperty(_REDUCER_TYPE_CHECK, REDUCER_TYPES.PROPOSAL, proposalTypeCheck), _defineProperty(_REDUCER_TYPE_CHECK, REDUCER_TYPES.QUOTATION, quotationTypeCheck), _defineProperty(_REDUCER_TYPE_CHECK, REDUCER_TYPES.PRE_APPLICATION, preApplicationTypeCheck), _defineProperty(_REDUCER_TYPE_CHECK, REDUCER_TYPES.APPLICATION, applicationTypeCheck), _defineProperty(_REDUCER_TYPE_CHECK, REDUCER_TYPES.RECOMMENDATION, recommendationTypeCheck), _defineProperty(_REDUCER_TYPE_CHECK, REDUCER_TYPES.PRODUCTS, productsTypeCheck), _defineProperty(_REDUCER_TYPE_CHECK, REDUCER_TYPES.OPTIONS_MAP, _optionsMap2.default), _defineProperty(_REDUCER_TYPE_CHECK, REDUCER_TYPES.CLIENT_FORM, clientFormTypeCheck), _defineProperty(_REDUCER_TYPE_CHECK, REDUCER_TYPES.CONFIG, configTypeCheck), _defineProperty(_REDUCER_TYPE_CHECK, REDUCER_TYPES.FNA, fnaTypeCheck), _defineProperty(_REDUCER_TYPE_CHECK, REDUCER_TYPES.COMMON, commonTypeCheck), _defineProperty(_REDUCER_TYPE_CHECK, REDUCER_TYPES.SUPPORTING_DOCUMENT, supportingDocumentTypeCheck), _defineProperty(_REDUCER_TYPE_CHECK, REDUCER_TYPES.PAYMENT_AND_SUBMISSION, paymentAndSubmissionTypeCheck), _defineProperty(_REDUCER_TYPE_CHECK, REDUCER_TYPES.SIGNATURE, signatureTypeCheck), _REDUCER_TYPE_CHECK);

var utilities = exports.utilities = {
  common: common,
  preApplication: preApplicationUtil,
  application: applicationUtil,
  validation: validation,
  getProductId: _getProductId2.default,
  trigger: trigger,
  dataMapping: dataMapping,
  numberFormatter: numberFormatter,
  naAnalysis: naAnalysis,
  needsUtil: needsUtil,
  genProfileMandatory: _genProfileMandatory2.default,
  searchClient: _searchClient2.default,
  fundChecker: _fundChecker2.default,
  dynamicApplicationForm: dynamicApplicationForm
};