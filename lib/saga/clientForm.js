"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.multiClientFormValidation = multiClientFormValidation;
exports.availableInsuredProfileValidation = availableInsuredProfileValidation;
exports.getProfileData = getProfileData;
exports.unlinkRelationship = unlinkRelationship;
exports.deleteClient = deleteClient;
exports.validateRelationship = validateRelationship;
exports.saveClient = saveClient;
exports.saveTrustedIndividual = saveTrustedIndividual;
exports.saveClientEmail = saveClientEmail;
exports.initialValidateSaga = initialValidateSaga;
exports.passSaga = passSaga;
exports.relationShipOtherSaga = relationShipOtherSaga;
exports.validateGivenName = validateGivenName;
exports.validateSmoking = validateSmoking;
exports.validateIndustry = validateIndustry;
exports.genderSaga = genderSaga;
exports.validateGender = validateGender;
exports.validateEmployStatus = validateEmployStatus;
exports.validateEmployStatusOther = validateEmployStatusOther;
exports.singaporePRStatusValidation = singaporePRStatusValidation;
exports.validateIDDocumentTypeOther = validateIDDocumentTypeOther;
exports.validateIDDocumentType = validateIDDocumentType;
exports.maritalStatusSaga = maritalStatusSaga;
exports.validateMaritalStatus = validateMaritalStatus;
exports.autoFillName = autoFillName;
exports.nameSaga = nameSaga;
exports.validateTitle = validateTitle;
exports.employStatusSaga = employStatusSaga;
exports.employStatusOtherSaga = employStatusOtherSaga;
exports.occupationSaga = occupationSaga;
exports.validateOccupation = validateOccupation;
exports.languageOtherSaga = languageOtherSaga;
exports.occupationOtherSaga = occupationOtherSaga;
exports.industrySaga = industrySaga;
exports.nameOfEmployBusinessSchoolSaga = nameOfEmployBusinessSchoolSaga;
exports.countryOfEmployBusinessSchoolSaga = countryOfEmployBusinessSchoolSaga;
exports.validateTypeOfPass = validateTypeOfPass;
exports.validateTypeOfPassOther = validateTypeOfPassOther;
exports.TypeOfPassOtherSaga = TypeOfPassOtherSaga;
exports.singaporePRStatusSaga = singaporePRStatusSaga;
exports.validateNationality = validateNationality;
exports.countrySaga = countrySaga;
exports.postalCodeSaga = postalCodeSaga;
exports.isCityOfResidenceErrorSaga = isCityOfResidenceErrorSaga;
exports.cityOfResidenceSaga = cityOfResidenceSaga;
exports.validateOtherCityOfResidence = validateOtherCityOfResidence;
exports.validateID = validateID;
exports.validateBirthday = validateBirthday;
exports.cityStateSaga = cityStateSaga;
exports.mobileNoASaga = mobileNoASaga;
exports.mobileNoBSaga = mobileNoBSaga;
exports.validateLanguageOther = validateLanguageOther;
exports.documentTypeSaga = documentTypeSaga;
exports.validateIncome = validateIncome;
exports.validateEducation = validateEducation;
exports.validateMobileNoA = validateMobileNoA;
exports.validatePrefixA = validatePrefixA;
exports.validateEmail = validateEmail;
exports.validateLanguage = validateLanguage;
exports.updateClientProfileAndValidate = updateClientProfileAndValidate;
exports.loadClientProfileAndInitValidate = loadClientProfileAndInitValidate;
exports.checkInsuredList = checkInsuredList;
exports.removeCompleteProfileFromList = removeCompleteProfileFromList;

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _effects = require("redux-saga/effects");

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

var _common = require("../utilities/common");

var _dataMapping = require("../utilities/dataMapping");

var _trigger = require("../utilities/trigger");

var _validation = require("../utilities/validation");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var _marked = /*#__PURE__*/regeneratorRuntime.mark(multiClientFormValidation),
    _marked2 = /*#__PURE__*/regeneratorRuntime.mark(availableInsuredProfileValidation),
    _marked3 = /*#__PURE__*/regeneratorRuntime.mark(getProfileData),
    _marked4 = /*#__PURE__*/regeneratorRuntime.mark(unlinkRelationship),
    _marked5 = /*#__PURE__*/regeneratorRuntime.mark(deleteClient),
    _marked6 = /*#__PURE__*/regeneratorRuntime.mark(validateRelationship),
    _marked7 = /*#__PURE__*/regeneratorRuntime.mark(saveClient),
    _marked8 = /*#__PURE__*/regeneratorRuntime.mark(saveTrustedIndividual),
    _marked9 = /*#__PURE__*/regeneratorRuntime.mark(saveClientEmail),
    _marked10 = /*#__PURE__*/regeneratorRuntime.mark(initialValidateSaga),
    _marked11 = /*#__PURE__*/regeneratorRuntime.mark(passSaga),
    _marked12 = /*#__PURE__*/regeneratorRuntime.mark(relationShipOtherSaga),
    _marked13 = /*#__PURE__*/regeneratorRuntime.mark(validateGivenName),
    _marked14 = /*#__PURE__*/regeneratorRuntime.mark(validateSmoking),
    _marked15 = /*#__PURE__*/regeneratorRuntime.mark(validateIndustry),
    _marked16 = /*#__PURE__*/regeneratorRuntime.mark(genderSaga),
    _marked17 = /*#__PURE__*/regeneratorRuntime.mark(validateGender),
    _marked18 = /*#__PURE__*/regeneratorRuntime.mark(validateEmployStatus),
    _marked19 = /*#__PURE__*/regeneratorRuntime.mark(validateEmployStatusOther),
    _marked20 = /*#__PURE__*/regeneratorRuntime.mark(singaporePRStatusValidation),
    _marked21 = /*#__PURE__*/regeneratorRuntime.mark(validateIDDocumentTypeOther),
    _marked22 = /*#__PURE__*/regeneratorRuntime.mark(validateIDDocumentType),
    _marked23 = /*#__PURE__*/regeneratorRuntime.mark(maritalStatusSaga),
    _marked24 = /*#__PURE__*/regeneratorRuntime.mark(validateMaritalStatus),
    _marked25 = /*#__PURE__*/regeneratorRuntime.mark(autoFillName),
    _marked26 = /*#__PURE__*/regeneratorRuntime.mark(nameSaga),
    _marked27 = /*#__PURE__*/regeneratorRuntime.mark(validateTitle),
    _marked28 = /*#__PURE__*/regeneratorRuntime.mark(employStatusSaga),
    _marked29 = /*#__PURE__*/regeneratorRuntime.mark(employStatusOtherSaga),
    _marked30 = /*#__PURE__*/regeneratorRuntime.mark(occupationSaga),
    _marked31 = /*#__PURE__*/regeneratorRuntime.mark(validateOccupation),
    _marked32 = /*#__PURE__*/regeneratorRuntime.mark(languageOtherSaga),
    _marked33 = /*#__PURE__*/regeneratorRuntime.mark(occupationOtherSaga),
    _marked34 = /*#__PURE__*/regeneratorRuntime.mark(industrySaga),
    _marked35 = /*#__PURE__*/regeneratorRuntime.mark(nameOfEmployBusinessSchoolSaga),
    _marked36 = /*#__PURE__*/regeneratorRuntime.mark(countryOfEmployBusinessSchoolSaga),
    _marked37 = /*#__PURE__*/regeneratorRuntime.mark(validateTypeOfPass),
    _marked38 = /*#__PURE__*/regeneratorRuntime.mark(validateTypeOfPassOther),
    _marked39 = /*#__PURE__*/regeneratorRuntime.mark(TypeOfPassOtherSaga),
    _marked40 = /*#__PURE__*/regeneratorRuntime.mark(singaporePRStatusSaga),
    _marked41 = /*#__PURE__*/regeneratorRuntime.mark(validateNationality),
    _marked42 = /*#__PURE__*/regeneratorRuntime.mark(countrySaga),
    _marked43 = /*#__PURE__*/regeneratorRuntime.mark(postalCodeSaga),
    _marked44 = /*#__PURE__*/regeneratorRuntime.mark(isCityOfResidenceErrorSaga),
    _marked45 = /*#__PURE__*/regeneratorRuntime.mark(cityOfResidenceSaga),
    _marked46 = /*#__PURE__*/regeneratorRuntime.mark(validateOtherCityOfResidence),
    _marked47 = /*#__PURE__*/regeneratorRuntime.mark(validateID),
    _marked48 = /*#__PURE__*/regeneratorRuntime.mark(validateBirthday),
    _marked49 = /*#__PURE__*/regeneratorRuntime.mark(cityStateSaga),
    _marked50 = /*#__PURE__*/regeneratorRuntime.mark(mobileNoASaga),
    _marked51 = /*#__PURE__*/regeneratorRuntime.mark(mobileNoBSaga),
    _marked52 = /*#__PURE__*/regeneratorRuntime.mark(validateLanguageOther),
    _marked53 = /*#__PURE__*/regeneratorRuntime.mark(documentTypeSaga),
    _marked54 = /*#__PURE__*/regeneratorRuntime.mark(validateIncome),
    _marked55 = /*#__PURE__*/regeneratorRuntime.mark(validateEducation),
    _marked56 = /*#__PURE__*/regeneratorRuntime.mark(validateMobileNoA),
    _marked57 = /*#__PURE__*/regeneratorRuntime.mark(validatePrefixA),
    _marked58 = /*#__PURE__*/regeneratorRuntime.mark(validateEmail),
    _marked59 = /*#__PURE__*/regeneratorRuntime.mark(validateLanguage),
    _marked60 = /*#__PURE__*/regeneratorRuntime.mark(updateClientProfileAndValidate),
    _marked61 = /*#__PURE__*/regeneratorRuntime.mark(loadClientProfileAndInitValidate),
    _marked62 = /*#__PURE__*/regeneratorRuntime.mark(checkInsuredList),
    _marked63 = /*#__PURE__*/regeneratorRuntime.mark(removeCompleteProfileFromList);

// =============================================================================
// variables
// =============================================================================
var occupationCheckList = ["O674", "O675", "O1450", "O1132", "O1321", "O1322"];
var occupationCheckListWithOutStudent = ["O674", "O675", "O1450", "O1132"];
var employStatusCheckListWithOutStudent = ["hh", "hw", "rt", "ue"];
// =============================================================================
// saga function
// =============================================================================

function multiClientFormValidation() {
  var clientForm, hasErrorList, newHasErrorList, _isClientFormHasError, hasError, hasErrorIndex;

  return regeneratorRuntime.wrap(function multiClientFormValidation$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM];
          });

        case 3:
          clientForm = _context.sent;
          _context.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.PRE_APPLICATION].multiClientProfile.hasErrorList;
          });

        case 6:
          hasErrorList = _context.sent;
          newHasErrorList = _.cloneDeep(hasErrorList);
          _isClientFormHasError = (0, _validation.isClientFormHasError)(clientForm), hasError = _isClientFormHasError.hasError;
          hasErrorIndex = hasErrorList.findIndex(function (clientHasError) {
            return clientHasError.cid === clientForm.profileBackUp.cid;
          });

          if (!(hasErrorIndex >= 0)) {
            _context.next = 14;
            break;
          }

          newHasErrorList[hasErrorIndex].hasError = hasError;
          _context.next = 14;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].UPDATE_MULTI_CLIENT_PROFILE_HAS_ERROR,
            newHasErrorList: newHasErrorList
          });

        case 14:
          _context.next = 18;
          break;

        case 16:
          _context.prev = 16;
          _context.t0 = _context["catch"](0);

        case 18:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, this, [[0, 16]]);
}

function availableInsuredProfileValidation() {
  var clientForm, hasErrorList, newHasErrorList, _isClientFormHasError2, hasError, hasErrorIndex;

  return regeneratorRuntime.wrap(function availableInsuredProfileValidation$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          _context2.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM];
          });

        case 3:
          clientForm = _context2.sent;
          _context2.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].availableInsuredProfile.hasErrorList;
          });

        case 6:
          hasErrorList = _context2.sent;
          newHasErrorList = _.cloneDeep(hasErrorList);
          _isClientFormHasError2 = (0, _validation.isClientFormHasError)(clientForm), hasError = _isClientFormHasError2.hasError;
          hasErrorIndex = hasErrorList.findIndex(function (clientHasError) {
            return clientHasError.cid === clientForm.profileBackUp.cid;
          });

          if (!(hasErrorIndex >= 0)) {
            _context2.next = 14;
            break;
          }

          newHasErrorList[hasErrorIndex].hasError = hasError;
          _context2.next = 14;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT].UPDATE_AVAILABLE_INSURED_PROFILE_HAS_ERROR,
            newHasErrorList: newHasErrorList
          });

        case 14:
          _context2.next = 18;
          break;

        case 16:
          _context2.prev = 16;
          _context2.t0 = _context2["catch"](0);

        case 18:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2, this, [[0, 16]]);
}

function getProfileData(action) {
  var callServer, cid, profileResponse, profile;
  return regeneratorRuntime.wrap(function getProfileData$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          _context3.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context3.sent;
          cid = action.cid;
          _context3.next = 7;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CONFIG].START_LOADING
          });

        case 7:
          _context3.next = 9;
          return (0, _effects.call)(callServer, {
            url: "client",
            data: {
              action: "getProfile",
              docId: cid
            }
          });

        case 9:
          profileResponse = _context3.sent;

          if (!profileResponse.success) {
            _context3.next = 17;
            break;
          }

          profile = (0, _dataMapping.profileMapForm)(_.cloneDeep(profileResponse.profile));

          profile.type = _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT;

          _context3.next = 15;
          return (0, _effects.put)(profile);

        case 15:
          _context3.next = 17;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_IS_FNA_INVALID,
            newIsFnaInvalid: !!profileResponse.showFnaInvalidFlag
          });

        case 17:
          _context3.next = 19;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CONFIG].FINISH_LOADING
          });

        case 19:

          if (_.isFunction(action.callback)) {
            action.callback();
          }
          _context3.next = 24;
          break;

        case 22:
          _context3.prev = 22;
          _context3.t0 = _context3["catch"](0);

        case 24:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3, this, [[0, 22]]);
}

function unlinkRelationship(action) {
  var callServer, client, clientForm, saveClientAPI;
  return regeneratorRuntime.wrap(function unlinkRelationship$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.prev = 0;
          _context4.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context4.sent;
          _context4.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT];
          });

        case 6:
          client = _context4.sent;
          _context4.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM];
          });

        case 9:
          clientForm = _context4.sent;
          _context4.next = 12;
          return (0, _effects.call)(callServer, {
            url: "client",
            data: {
              action: "unlinkRelationship",
              cid: client.profile.cid,
              fid: clientForm.profileBackUp.cid,
              confirm: true
            }
          });

        case 12:
          saveClientAPI = _context4.sent;

          if (!saveClientAPI.success) {
            _context4.next = 22;
            break;
          }

          _context4.next = 16;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_IS_FNA_INVALID,
            newIsFnaInvalid: !!saveClientAPI.showFnaInvalidFlag
          });

        case 16:
          _context4.next = 18;
          return (0, _effects.call)(action.callback);

        case 18:
          _context4.next = 20;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM
          });

        case 20:
          _context4.next = 22;
          break;

        case 22:
          _context4.next = 26;
          break;

        case 24:
          _context4.prev = 24;
          _context4.t0 = _context4["catch"](0);

        case 26:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4, this, [[0, 24]]);
}

function deleteClient(action) {
  var callServer, cid, saveClientAPI;
  return regeneratorRuntime.wrap(function deleteClient$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.prev = 0;
          _context5.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context5.sent;
          _context5.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].profileBackUp.cid;
          });

        case 6:
          cid = _context5.sent;
          _context5.next = 9;
          return (0, _effects.call)(callServer, {
            url: "client",
            data: {
              action: "deleteProfile",
              cid: cid,
              confirm: true
            }
          });

        case 9:
          saveClientAPI = _context5.sent;

          if (!saveClientAPI.success) {
            _context5.next = 17;
            break;
          }

          _context5.next = 13;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM
          });

        case 13:
          _context5.next = 15;
          return (0, _effects.call)(action.callback);

        case 15:
          _context5.next = 17;
          break;

        case 17:
          _context5.next = 21;
          break;

        case 19:
          _context5.prev = 19;
          _context5.t0 = _context5["catch"](0);

        case 21:
        case "end":
          return _context5.stop();
      }
    }
  }, _marked5, this, [[0, 19]]);
}

function validateRelationship() {
  var relationshipData, genderData, profileData, configData, isPDA, isAPP, isFamilyMemberData, newCid;
  return regeneratorRuntime.wrap(function validateRelationship$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.prev = 0;
          _context6.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].relationship;
          });

        case 3:
          relationshipData = _context6.sent;
          _context6.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].gender;
          });

        case 6:
          genderData = _context6.sent;
          _context6.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile;
          });

        case 9:
          profileData = _context6.sent;
          _context6.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config;
          });

        case 12:
          configData = _context6.sent;
          _context6.next = 15;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isPDA;
          });

        case 15:
          isPDA = _context6.sent;
          _context6.next = 18;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isAPP;
          });

        case 18:
          isAPP = _context6.sent;
          _context6.next = 21;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isFamilyMember;
          });

        case 21:
          isFamilyMemberData = _context6.sent;
          _context6.next = 24;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].profileBackUp.cid;
          });

        case 24:
          newCid = _context6.sent;
          _context6.next = 27;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_RELATIONSHIP,
            relationshipData: relationshipData,
            genderData: genderData,
            profileData: profileData,
            configData: configData,
            isFamilyMemberData: isFamilyMemberData,
            isPDA: isPDA,
            newCid: newCid
          });

        case 27:
          if (!isAPP) {
            _context6.next = 30;
            break;
          }

          _context6.next = 30;
          return multiClientFormValidation();

        case 30:
          _context6.next = 34;
          break;

        case 32:
          _context6.prev = 32;
          _context6.t0 = _context6["catch"](0);

        case 34:
        case "end":
          return _context6.stop();
      }
    }
  }, _marked6, this, [[0, 32]]);
}

function saveClient(action) {
  var callServer, clientForm, clientProfile, confirm, callback, actionName, profile, saveClientAPI;
  return regeneratorRuntime.wrap(function saveClient$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          _context7.prev = 0;
          _context7.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context7.sent;
          _context7.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM];
          });

        case 6:
          clientForm = _context7.sent;
          _context7.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile;
          });

        case 9:
          clientProfile = _context7.sent;
          confirm = action.confirm, callback = action.callback;

          /* get action */

          actionName = "";

          if (clientForm.config.isFamilyMember) {
            actionName = "saveFamilyMember";
          } else if (clientForm.config.isCreate) {
            actionName = "addProfile";
          } else {
            actionName = "saveProfile";
          }

          /* map redux data to server structure data */
          // let profile = formMapProfile(clientForm);
          profile = Object.assign({}, (0, _dataMapping.formMapProfile)(clientForm), clientForm.profileBackUp);

          if (!(actionName !== "addProfile")) {
            _context7.next = 26;
            break;
          }

          _context7.t0 = Object;
          _context7.t1 = {};
          _context7.t2 = profile;
          _context7.next = 20;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.AGENT].agentCode;
          });

        case 20:
          _context7.t3 = _context7.sent;
          _context7.next = 23;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.AGENT].agentCode;
          });

        case 23:
          _context7.t4 = _context7.sent;
          _context7.t5 = {
            agentCode: _context7.t3,
            agentId: _context7.t4
          };
          profile = _context7.t0.assign.call(_context7.t0, _context7.t1, _context7.t2, _context7.t5);

        case 26:
          _context7.next = 28;
          return (0, _effects.call)(callServer, {
            url: "client",
            data: {
              action: actionName,
              profile: profile,
              photo: clientForm.photo,
              fid: clientForm.config.isFamilyMember ? profile.cid : undefined,
              cid: clientForm.config.isFamilyMember ? clientProfile.cid : undefined,
              tiPhoto: clientForm.config.isFamilyMember ? undefined : "",
              confirm: confirm
            }
          });

        case 28:
          saveClientAPI = _context7.sent;

          if (!saveClientAPI.success) {
            _context7.next = 52;
            break;
          }

          if (!saveClientAPI.code) {
            _context7.next = 35;
            break;
          }

          _context7.next = 33;
          return (0, _effects.call)(callback, saveClientAPI.code);

        case 33:
          _context7.next = 52;
          break;

        case 35:
          if (!(clientForm.config.isCreate && !clientForm.config.isFamilyMember)) {
            _context7.next = 46;
            break;
          }

          _context7.next = 38;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT].UPDATE_PROFILE,
            profileData: saveClientAPI.profile
          });

        case 38:
          _context7.next = 40;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT].GET_CONTACT_LIST,
            callback: callback
          });

        case 40:
          _context7.next = 42;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_IS_FNA_INVALID,
            newIsFnaInvalid: !!saveClientAPI.showFnaInvalidFlag
          });

        case 42:
          _context7.next = 44;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM
          });

        case 44:
          _context7.next = 52;
          break;

        case 46:
          _context7.next = 48;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT].GET_PROFILE,
            cid: clientProfile.cid,
            callback: callback
          });

        case 48:
          _context7.next = 50;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_IS_FNA_INVALID,
            newIsFnaInvalid: !!saveClientAPI.showFnaInvalidFlag
          });

        case 50:
          _context7.next = 52;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM
          });

        case 52:
          _context7.next = 56;
          break;

        case 54:
          _context7.prev = 54;
          _context7.t6 = _context7["catch"](0);

        case 56:
        case "end":
          return _context7.stop();
      }
    }
  }, _marked7, this, [[0, 54]]);
}

function saveTrustedIndividual(action) {
  var callServer, clientForm, cid, currentPage, tiPhoto, base64Data, response;
  return regeneratorRuntime.wrap(function saveTrustedIndividual$(_context8) {
    while (1) {
      switch (_context8.prev = _context8.next) {
        case 0:
          _context8.prev = 0;
          _context8.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context8.sent;
          _context8.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM];
          });

        case 6:
          clientForm = _context8.sent;
          _context8.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile.cid;
          });

        case 9:
          cid = _context8.sent;
          _context8.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].currentPage;
          });

        case 12:
          currentPage = _context8.sent;


          // get tiPhoto
          tiPhoto = void 0;

          if (clientForm.photo.uri) {
            base64Data = clientForm.photo.uri.replace("data:image/jpeg;base64,", "");


            tiPhoto = {
              type: "data:image/jpeg;base64,",
              value: base64Data
            };
          }

          _context8.next = 17;
          return (0, _effects.call)(callServer, {
            url: "client",
            data: {
              action: "saveTrustedIndividual",
              cid: cid,
              tiPhoto: tiPhoto,
              tiInfo: (0, _dataMapping.formMapTrustedIndividuel)(clientForm),
              confirm: action.confirm
            }
          });

        case 17:
          response = _context8.sent;

          if (!response.success) {
            _context8.next = 31;
            break;
          }

          if (!response.code) {
            _context8.next = 23;
            break;
          }

          action.alert(response.code);
          _context8.next = 31;
          break;

        case 23:
          _context8.next = 25;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT].UPDATE_PROFILE,
            profileData: response.profile
          });

        case 25:
          _context8.next = 27;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_IS_FNA_INVALID,
            newIsFnaInvalid: !!response.showFnaInvalidFlag
          });

        case 27:
          action.callback();

          if (!(currentPage !== "PDA")) {
            _context8.next = 31;
            break;
          }

          _context8.next = 31;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CLEAN_CLIENT_FORM
          });

        case 31:
          _context8.next = 35;
          break;

        case 33:
          _context8.prev = 33;
          _context8.t0 = _context8["catch"](0);

        case 35:
        case "end":
          return _context8.stop();
      }
    }
  }, _marked8, this, [[0, 33]]);
}

function saveClientEmail(action) {
  var callServer, clientForm, clientProfile, profile, saveClientAPI;
  return regeneratorRuntime.wrap(function saveClientEmail$(_context9) {
    while (1) {
      switch (_context9.prev = _context9.next) {
        case 0:
          _context9.prev = 0;
          _context9.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context9.sent;
          _context9.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM];
          });

        case 6:
          clientForm = _context9.sent;
          _context9.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile;
          });

        case 9:
          clientProfile = _context9.sent;
          profile = Object.assign({}, clientProfile, {
            email: clientForm.email
          });
          _context9.next = 13;
          return (0, _effects.call)(callServer, {
            url: "client",
            data: {
              action: "saveProfile",
              profile: profile,
              photo: clientProfile.photo,
              tiPhoto: "",
              confirm: true
            }
          });

        case 13:
          saveClientAPI = _context9.sent;

          if (!saveClientAPI.success) {
            _context9.next = 17;
            break;
          }

          _context9.next = 17;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT].UPDATE_PROFILE,
            profileData: saveClientAPI.profile
          });

        case 17:
          if (!_.isFunction(action.callback)) {
            _context9.next = 20;
            break;
          }

          _context9.next = 20;
          return (0, _effects.call)(action.callback);

        case 20:
          _context9.next = 24;
          break;

        case 22:
          _context9.prev = 22;
          _context9.t0 = _context9["catch"](0);

        case 24:
        case "end":
          return _context9.stop();
      }
    }
  }, _marked9, this, [[0, 22]]);
}

function initialValidateSaga(action) {
  var client, clientForm, optionsMapData;
  return regeneratorRuntime.wrap(function initialValidateSaga$(_context10) {
    while (1) {
      switch (_context10.prev = _context10.next) {
        case 0:
          _context10.prev = 0;
          _context10.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT];
          });

        case 3:
          client = _context10.sent;
          _context10.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM];
          });

        case 6:
          clientForm = _context10.sent;
          _context10.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.OPTIONS_MAP];
          });

        case 9:
          optionsMapData = _context10.sent;
          _context10.next = 12;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE,
            configData: clientForm.config,
            isPDA: clientForm.config.isPDA,
            isFNA: clientForm.config.isFNA,
            isAPP: clientForm.config.isAPP,
            relationshipData: clientForm.relationship,
            genderData: clientForm.gender,
            profileData: client.profile,
            incomeData: clientForm.income,
            maritalData: clientForm.maritalStatus,
            educationData: clientForm.education,
            isFamilyMemberData: clientForm.config.isFamilyMember,
            isProposerMissingData: clientForm.config.isProposerMissing,
            relationshipOtherData: clientForm.relationshipOther,
            givenNameData: clientForm.givenName,
            nameData: clientForm.givenName,
            titleData: clientForm.title,
            isCreateData: clientForm.config.isCreate,
            singaporePRStatusData: clientForm.singaporePRStatus,
            nationalityData: clientForm.nationality,
            cityOfResidenceData: clientForm.cityOfResidence,
            countryOfResidenceData: clientForm.countryOfResidence,
            optionsMapData: optionsMapData,
            isApplicationData: clientForm.config.isApplication,
            otherCityOfResidenceData: clientForm.otherCityOfResidence,
            IDData: clientForm.ID,
            IDDocumentTypeData: clientForm.IDDocumentType,
            IDDocumentTypeOtherData: clientForm.IDDocumentTypeOther,
            employStatusData: clientForm.employStatus,
            employStatusOtherData: clientForm.employStatusOther,
            industryData: clientForm.industry,
            smokingStatusData: clientForm.smokingStatus,
            occupationData: clientForm.occupation,
            birthdayData: clientForm.birthday,
            occupationOtherData: clientForm.occupationOther,
            languageData: clientForm.language,
            languageOtherData: clientForm.languageOther,
            emailData: clientForm.email,
            clientFormCid: clientForm.profileBackUp.cid,
            prefixAData: clientForm.prefixA,
            mobileNoAData: clientForm.mobileNoA,
            typeOfPassData: clientForm.typeOfPass,
            typeOfPassOtherData: clientForm.typeOfPassOther
          });

        case 12:
          _context10.next = 14;
          return (0, _effects.call)(action.callback);

        case 14:
          _context10.next = 18;
          break;

        case 16:
          _context10.prev = 16;
          _context10.t0 = _context10["catch"](0);

        case 18:
        case "end":
          return _context10.stop();
      }
    }
  }, _marked10, this, [[0, 16]]);
}

function passSaga() {
  var singaporePRStatus;
  return regeneratorRuntime.wrap(function passSaga$(_context11) {
    while (1) {
      switch (_context11.prev = _context11.next) {
        case 0:
          _context11.prev = 0;
          _context11.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].singaporePRStatus;
          });

        case 3:
          singaporePRStatus = _context11.sent;

          if (!(singaporePRStatus === "Y")) {
            _context11.next = 9;
            break;
          }

          _context11.next = 7;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].TYPE_OF_PASS_ON_CHANGE,
            newTypeOfPass: ""
          });

        case 7:
          _context11.next = 9;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].PASS_EXPIRY_DATE_ON_CHANGE,
            newPassExpiryDate: NaN
          });

        case 9:
          _context11.next = 13;
          break;

        case 11:
          _context11.prev = 11;
          _context11.t0 = _context11["catch"](0);

        case 13:
        case "end":
          return _context11.stop();
      }
    }
  }, _marked11, this, [[0, 11]]);
}

function relationShipOtherSaga() {
  var relationshipData, relationshipOtherData;
  return regeneratorRuntime.wrap(function relationShipOtherSaga$(_context12) {
    while (1) {
      switch (_context12.prev = _context12.next) {
        case 0:
          _context12.prev = 0;
          _context12.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].relationship;
          });

        case 3:
          relationshipData = _context12.sent;
          _context12.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].relationshipOther;
          });

        case 6:
          relationshipOtherData = _context12.sent;

          if (!(relationshipData !== "OTH" && relationshipOtherData !== "")) {
            _context12.next = 12;
            break;
          }

          _context12.next = 10;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].RELATIONSHIP_OTHER_ON_CHANGE,
            newRelationshipOther: ""
          });

        case 10:
          _context12.next = 14;
          break;

        case 12:
          _context12.next = 14;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_RELATIONSHIP_OTHER,
            relationshipOtherData: relationshipOtherData,
            relationshipData: relationshipData
          });

        case 14:
          _context12.next = 18;
          break;

        case 16:
          _context12.prev = 16;
          _context12.t0 = _context12["catch"](0);

        case 18:
        case "end":
          return _context12.stop();
      }
    }
  }, _marked12, this, [[0, 16]]);
}

function validateGivenName() {
  var isAPP, givenNameData;
  return regeneratorRuntime.wrap(function validateGivenName$(_context13) {
    while (1) {
      switch (_context13.prev = _context13.next) {
        case 0:
          _context13.prev = 0;
          _context13.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isAPP;
          });

        case 3:
          isAPP = _context13.sent;
          _context13.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].givenName;
          });

        case 6:
          givenNameData = _context13.sent;
          _context13.next = 9;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_GIVEN_NAME,
            givenNameData: givenNameData
          });

        case 9:
          if (!isAPP) {
            _context13.next = 12;
            break;
          }

          _context13.next = 12;
          return multiClientFormValidation();

        case 12:
          _context13.next = 16;
          break;

        case 14:
          _context13.prev = 14;
          _context13.t0 = _context13["catch"](0);

        case 16:
        case "end":
          return _context13.stop();
      }
    }
  }, _marked13, this, [[0, 14]]);
}

function validateSmoking() {
  var isAPP, isProposerMissingData, isInsuredMissingData, smokingStatusData, isShieldQuotation;
  return regeneratorRuntime.wrap(function validateSmoking$(_context14) {
    while (1) {
      switch (_context14.prev = _context14.next) {
        case 0:
          _context14.prev = 0;
          _context14.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isAPP;
          });

        case 3:
          isAPP = _context14.sent;
          _context14.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isProposerMissing;
          });

        case 6:
          isProposerMissingData = _context14.sent;
          _context14.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isInsuredMissing;
          });

        case 9:
          isInsuredMissingData = _context14.sent;
          _context14.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].smokingStatus;
          });

        case 12:
          smokingStatusData = _context14.sent;
          _context14.next = 15;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isShieldQuotation;
          });

        case 15:
          isShieldQuotation = _context14.sent;
          _context14.next = 18;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_SMOKING_STATUS,
            smokingStatusData: smokingStatusData,
            isProposerMissingData: isProposerMissingData || isInsuredMissingData
          });

        case 18:
          if (!isAPP) {
            _context14.next = 21;
            break;
          }

          _context14.next = 21;
          return multiClientFormValidation();

        case 21:
          if (!isShieldQuotation) {
            _context14.next = 24;
            break;
          }

          _context14.next = 24;
          return availableInsuredProfileValidation();

        case 24:
          _context14.next = 28;
          break;

        case 26:
          _context14.prev = 26;
          _context14.t0 = _context14["catch"](0);

        case 28:
        case "end":
          return _context14.stop();
      }
    }
  }, _marked14, this, [[0, 26]]);
}

function validateIndustry() {
  var isAPP, isProposerMissingData, isInsuredMissingData, industryData, isShieldQuotation;
  return regeneratorRuntime.wrap(function validateIndustry$(_context15) {
    while (1) {
      switch (_context15.prev = _context15.next) {
        case 0:
          _context15.prev = 0;
          _context15.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isAPP;
          });

        case 3:
          isAPP = _context15.sent;
          _context15.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isProposerMissing;
          });

        case 6:
          isProposerMissingData = _context15.sent;
          _context15.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isInsuredMissing;
          });

        case 9:
          isInsuredMissingData = _context15.sent;
          _context15.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].industry;
          });

        case 12:
          industryData = _context15.sent;
          _context15.next = 15;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isShieldQuotation;
          });

        case 15:
          isShieldQuotation = _context15.sent;
          _context15.next = 18;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_INDUSTRY,
            industryData: industryData,
            isProposerMissingData: isProposerMissingData || isInsuredMissingData
          });

        case 18:
          if (!isAPP) {
            _context15.next = 21;
            break;
          }

          _context15.next = 21;
          return multiClientFormValidation();

        case 21:
          if (!isShieldQuotation) {
            _context15.next = 24;
            break;
          }

          _context15.next = 24;
          return availableInsuredProfileValidation();

        case 24:
          _context15.next = 28;
          break;

        case 26:
          _context15.prev = 26;
          _context15.t0 = _context15["catch"](0);

        case 28:
        case "end":
          return _context15.stop();
      }
    }
  }, _marked15, this, [[0, 26]]);
}

function genderSaga() {
  var relationship, clientGender, newGender;
  return regeneratorRuntime.wrap(function genderSaga$(_context16) {
    while (1) {
      switch (_context16.prev = _context16.next) {
        case 0:
          _context16.prev = 0;
          _context16.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].relationship;
          });

        case 3:
          relationship = _context16.sent;
          _context16.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile.gender;
          });

        case 6:
          clientGender = _context16.sent;
          newGender = null;


          if (["GFA", "SON", "FAT", "BRO", "GSO"].indexOf(relationship) > -1) {
            newGender = "M";
          } else if (["GMO", "MOT", "DAU", "GDA", "SIS"].indexOf(relationship) > -1) {
            newGender = "F";
          } else if (relationship === "SPO") {
            if (clientGender === "M") {
              newGender = "F";
            } else {
              newGender = "M";
            }
          }

          if (!(newGender !== null)) {
            _context16.next = 12;
            break;
          }

          _context16.next = 12;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].GENDER_ON_CHANGE,
            newGender: newGender
          });

        case 12:
          _context16.next = 16;
          break;

        case 14:
          _context16.prev = 14;
          _context16.t0 = _context16["catch"](0);

        case 16:
        case "end":
          return _context16.stop();
      }
    }
  }, _marked16, this, [[0, 14]]);
}

function validateGender() {
  var isAPP, genderData, configData;
  return regeneratorRuntime.wrap(function validateGender$(_context17) {
    while (1) {
      switch (_context17.prev = _context17.next) {
        case 0:
          _context17.prev = 0;
          _context17.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isAPP;
          });

        case 3:
          isAPP = _context17.sent;
          _context17.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].gender;
          });

        case 6:
          genderData = _context17.sent;
          _context17.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config;
          });

        case 9:
          configData = _context17.sent;
          _context17.next = 12;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_GENDER,
            genderData: genderData,
            configData: configData
          });

        case 12:
          if (!isAPP) {
            _context17.next = 15;
            break;
          }

          _context17.next = 15;
          return multiClientFormValidation();

        case 15:
          _context17.next = 19;
          break;

        case 17:
          _context17.prev = 17;
          _context17.t0 = _context17["catch"](0);

        case 19:
        case "end":
          return _context17.stop();
      }
    }
  }, _marked17, this, [[0, 17]]);
}

function validateEmployStatus() {
  var genderData, employStatusData, isFNA, isAPP;
  return regeneratorRuntime.wrap(function validateEmployStatus$(_context18) {
    while (1) {
      switch (_context18.prev = _context18.next) {
        case 0:
          _context18.prev = 0;
          _context18.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].gender;
          });

        case 3:
          genderData = _context18.sent;
          _context18.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].employStatus;
          });

        case 6:
          employStatusData = _context18.sent;
          _context18.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isFNA;
          });

        case 9:
          isFNA = _context18.sent;
          _context18.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isAPP;
          });

        case 12:
          isAPP = _context18.sent;
          _context18.next = 15;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_EMPLOY_STATUS,
            employStatusData: employStatusData,
            genderData: genderData,
            isFNA: isFNA
          });

        case 15:
          if (!isAPP) {
            _context18.next = 18;
            break;
          }

          _context18.next = 18;
          return multiClientFormValidation();

        case 18:
          _context18.next = 22;
          break;

        case 20:
          _context18.prev = 20;
          _context18.t0 = _context18["catch"](0);

        case 22:
        case "end":
          return _context18.stop();
      }
    }
  }, _marked18, this, [[0, 20]]);
}

function validateEmployStatusOther() {
  var isAPP, employStatusData, employStatusOtherData;
  return regeneratorRuntime.wrap(function validateEmployStatusOther$(_context19) {
    while (1) {
      switch (_context19.prev = _context19.next) {
        case 0:
          _context19.prev = 0;
          _context19.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isAPP;
          });

        case 3:
          isAPP = _context19.sent;
          _context19.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].employStatus;
          });

        case 6:
          employStatusData = _context19.sent;
          _context19.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].employStatusOther;
          });

        case 9:
          employStatusOtherData = _context19.sent;
          _context19.next = 12;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_EMPLOY_STATUS_OTHER,
            employStatusData: employStatusData,
            employStatusOtherData: employStatusOtherData
          });

        case 12:
          if (!isAPP) {
            _context19.next = 15;
            break;
          }

          _context19.next = 15;
          return multiClientFormValidation();

        case 15:
          _context19.next = 19;
          break;

        case 17:
          _context19.prev = 17;
          _context19.t0 = _context19["catch"](0);

        case 19:
        case "end":
          return _context19.stop();
      }
    }
  }, _marked19, this, [[0, 17]]);
}

function singaporePRStatusValidation() {
  var isAPP, singaporePRStatusData, nationalityData;
  return regeneratorRuntime.wrap(function singaporePRStatusValidation$(_context20) {
    while (1) {
      switch (_context20.prev = _context20.next) {
        case 0:
          _context20.prev = 0;
          _context20.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isAPP;
          });

        case 3:
          isAPP = _context20.sent;
          _context20.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].singaporePRStatus;
          });

        case 6:
          singaporePRStatusData = _context20.sent;
          _context20.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].nationality;
          });

        case 9:
          nationalityData = _context20.sent;
          _context20.next = 12;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_SINGAPORE_PR_STATUS,
            singaporePRStatusData: singaporePRStatusData,
            nationalityData: nationalityData
          });

        case 12:
          if (!isAPP) {
            _context20.next = 15;
            break;
          }

          _context20.next = 15;
          return multiClientFormValidation();

        case 15:
          _context20.next = 19;
          break;

        case 17:
          _context20.prev = 17;
          _context20.t0 = _context20["catch"](0);

        case 19:
        case "end":
          return _context20.stop();
      }
    }
  }, _marked20, this, [[0, 17]]);
}

function validateIDDocumentTypeOther() {
  var isAPP, IDDocumentTypeOtherData, IDDocumentTypeData;
  return regeneratorRuntime.wrap(function validateIDDocumentTypeOther$(_context21) {
    while (1) {
      switch (_context21.prev = _context21.next) {
        case 0:
          _context21.prev = 0;
          _context21.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isAPP;
          });

        case 3:
          isAPP = _context21.sent;
          _context21.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].IDDocumentTypeOther;
          });

        case 6:
          IDDocumentTypeOtherData = _context21.sent;
          _context21.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].IDDocumentType;
          });

        case 9:
          IDDocumentTypeData = _context21.sent;

          if (!(IDDocumentTypeData !== "other" && IDDocumentTypeOtherData !== "")) {
            _context21.next = 15;
            break;
          }

          _context21.next = 13;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].ID_DOCUMENT_TYPE_OTHER_ON_CHANGE,
            newIDDocTypeOther: ""
          });

        case 13:
          _context21.next = 17;
          break;

        case 15:
          _context21.next = 17;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_ID_DOCUMENT_TYPE_OTHER,
            IDDocumentTypeOtherData: IDDocumentTypeOtherData,
            IDDocumentTypeData: IDDocumentTypeData
          });

        case 17:
          if (!isAPP) {
            _context21.next = 20;
            break;
          }

          _context21.next = 20;
          return multiClientFormValidation();

        case 20:
          _context21.next = 24;
          break;

        case 22:
          _context21.prev = 22;
          _context21.t0 = _context21["catch"](0);

        case 24:
        case "end":
          return _context21.stop();
      }
    }
  }, _marked21, this, [[0, 22]]);
}

function validateIDDocumentType() {
  var configData, isFNA, isPDA, isAPP, IDDocumentTypeData;
  return regeneratorRuntime.wrap(function validateIDDocumentType$(_context22) {
    while (1) {
      switch (_context22.prev = _context22.next) {
        case 0:
          _context22.prev = 0;
          _context22.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config;
          });

        case 3:
          configData = _context22.sent;
          _context22.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isFNA;
          });

        case 6:
          isFNA = _context22.sent;
          _context22.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isPDA;
          });

        case 9:
          isPDA = _context22.sent;
          _context22.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isAPP;
          });

        case 12:
          isAPP = _context22.sent;
          _context22.next = 15;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].IDDocumentType;
          });

        case 15:
          IDDocumentTypeData = _context22.sent;
          _context22.next = 18;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_ID_DOCUMENT_TYPE,
            IDDocumentTypeData: IDDocumentTypeData,
            configData: configData,
            isFNA: isFNA,
            isPDA: isPDA,
            isAPP: isAPP
          });

        case 18:
          if (!isAPP) {
            _context22.next = 21;
            break;
          }

          _context22.next = 21;
          return multiClientFormValidation();

        case 21:
          _context22.next = 25;
          break;

        case 23:
          _context22.prev = 23;
          _context22.t0 = _context22["catch"](0);

        case 25:
        case "end":
          return _context22.stop();
      }
    }
  }, _marked22, this, [[0, 23]]);
}

function maritalStatusSaga() {
  var relationshipData, newMaritalStatus;
  return regeneratorRuntime.wrap(function maritalStatusSaga$(_context23) {
    while (1) {
      switch (_context23.prev = _context23.next) {
        case 0:
          _context23.prev = 0;
          _context23.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].relationship;
          });

        case 3:
          relationshipData = _context23.sent;
          _context23.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].maritalStatus;
          });

        case 6:
          newMaritalStatus = _context23.sent;
          _context23.next = 9;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].MARITAL_STATUS_ON_CHANGE,
            newMaritalStatus: newMaritalStatus,
            relationshipData: relationshipData
          });

        case 9:
          _context23.next = 13;
          break;

        case 11:
          _context23.prev = 11;
          _context23.t0 = _context23["catch"](0);

        case 13:
        case "end":
          return _context23.stop();
      }
    }
  }, _marked23, this, [[0, 11]]);
}

function validateMaritalStatus(action) {
  var isAPP, isFNA, maritalData;
  return regeneratorRuntime.wrap(function validateMaritalStatus$(_context24) {
    while (1) {
      switch (_context24.prev = _context24.next) {
        case 0:
          _context24.prev = 0;
          _context24.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isAPP;
          });

        case 3:
          isAPP = _context24.sent;
          _context24.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isFNA;
          });

        case 6:
          isFNA = _context24.sent;
          _context24.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].maritalStatus;
          });

        case 9:
          maritalData = _context24.sent;
          _context24.next = 12;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_MARITAL_STATUS,
            maritalData: maritalData,
            isFNA: isFNA,
            isAPP: isAPP
          });

        case 12:
          if (!isAPP) {
            _context24.next = 15;
            break;
          }

          _context24.next = 15;
          return multiClientFormValidation();

        case 15:
          if (!_.isFunction(action.callback)) {
            _context24.next = 18;
            break;
          }

          _context24.next = 18;
          return (0, _effects.put)(action.callback);

        case 18:
          _context24.next = 22;
          break;

        case 20:
          _context24.prev = 20;
          _context24.t0 = _context24["catch"](0);

        case 22:
        case "end":
          return _context24.stop();
      }
    }
  }, _marked24, this, [[0, 20]]);
}

function autoFillName() {
  var givenName, surname, nameOrder, newName;
  return regeneratorRuntime.wrap(function autoFillName$(_context25) {
    while (1) {
      switch (_context25.prev = _context25.next) {
        case 0:
          _context25.prev = 0;
          _context25.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].givenName;
          });

        case 3:
          givenName = _context25.sent;
          _context25.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].surname;
          });

        case 6:
          surname = _context25.sent;
          _context25.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].nameOrder;
          });

        case 9:
          nameOrder = _context25.sent;
          newName = "";

          if (nameOrder === "L") {
            newName = surname + " " + givenName;
          } else if (nameOrder === "F") {
            newName = givenName + " " + surname;
          }

          _context25.next = 14;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].NAME_ON_CHANGE,
            newName: newName
          });

        case 14:
          _context25.next = 18;
          break;

        case 16:
          _context25.prev = 16;
          _context25.t0 = _context25["catch"](0);

        case 18:
        case "end":
          return _context25.stop();
      }
    }
  }, _marked25, this, [[0, 16]]);
}

function nameSaga() {
  var nameData, givenNameData;
  return regeneratorRuntime.wrap(function nameSaga$(_context26) {
    while (1) {
      switch (_context26.prev = _context26.next) {
        case 0:
          _context26.prev = 0;
          _context26.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].name;
          });

        case 3:
          nameData = _context26.sent;
          _context26.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].givenName;
          });

        case 6:
          givenNameData = _context26.sent;
          _context26.next = 9;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_NAME,
            nameData: nameData,
            givenNameData: givenNameData
          });

        case 9:
          _context26.next = 13;
          break;

        case 11:
          _context26.prev = 11;
          _context26.t0 = _context26["catch"](0);

        case 13:
        case "end":
          return _context26.stop();
      }
    }
  }, _marked26, this, [[0, 11]]);
}

function validateTitle() {
  var genderData, titleData, isFNA, isAPP, isTrustedIndividual;
  return regeneratorRuntime.wrap(function validateTitle$(_context27) {
    while (1) {
      switch (_context27.prev = _context27.next) {
        case 0:
          _context27.prev = 0;
          _context27.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].gender;
          });

        case 3:
          genderData = _context27.sent;
          _context27.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].title;
          });

        case 6:
          titleData = _context27.sent;
          _context27.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isFNA;
          });

        case 9:
          isFNA = _context27.sent;
          _context27.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isAPP;
          });

        case 12:
          isAPP = _context27.sent;
          _context27.next = 15;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isTrustedIndividual;
          });

        case 15:
          isTrustedIndividual = _context27.sent;
          _context27.next = 18;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_TITLE,
            titleData: titleData,
            genderData: genderData,
            isFNA: isFNA,
            isTrustedIndividual: isTrustedIndividual
          });

        case 18:
          if (!isAPP) {
            _context27.next = 21;
            break;
          }

          _context27.next = 21;
          return multiClientFormValidation();

        case 21:
          _context27.next = 25;
          break;

        case 23:
          _context27.prev = 23;
          _context27.t0 = _context27["catch"](0);

        case 25:
        case "end":
          return _context27.stop();
      }
    }
  }, _marked27, this, [[0, 23]]);
}

function employStatusSaga(action) {
  var birthday, age, occupation, occupationCurrently, newEmployStatus;
  return regeneratorRuntime.wrap(function employStatusSaga$(_context28) {
    while (1) {
      switch (_context28.prev = _context28.next) {
        case 0:
          _context28.prev = 0;

          if (action["@@redux-saga/SAGA_ACTION"]) {
            _context28.next = 31;
            break;
          }

          _context28.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].birthday;
          });

        case 4:
          birthday = _context28.sent;
          age = (0, _common.calcAge)(birthday);
          _context28.next = 8;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].occupation;
          });

        case 8:
          occupation = _context28.sent;
          occupationCurrently = action.occupationCurrently;
          newEmployStatus = null;


          if (age < 18 && action.type === _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].BIRTHDAY_ON_CHANGE) {
            newEmployStatus = "sd";
          }

          _context28.t0 = true;
          _context28.next = _context28.t0 === ["O1321", "O1322"].indexOf(occupation) > -1 ? 15 : _context28.t0 === (occupation === "O674") ? 17 : _context28.t0 === (occupation === "O675") ? 19 : _context28.t0 === (occupation === "O1450") ? 21 : _context28.t0 === (occupation === "O1132") ? 23 : _context28.t0 === (occupationCheckList.indexOf(occupationCurrently) > -1 && occupationCheckList.indexOf(occupation) === -1) ? 25 : 27;
          break;

        case 15:
          newEmployStatus = "sd";
          return _context28.abrupt("break", 28);

        case 17:
          newEmployStatus = "hh";
          return _context28.abrupt("break", 28);

        case 19:
          newEmployStatus = "hw";
          return _context28.abrupt("break", 28);

        case 21:
          newEmployStatus = "ue";
          return _context28.abrupt("break", 28);

        case 23:
          newEmployStatus = "rt";
          return _context28.abrupt("break", 28);

        case 25:
          newEmployStatus = "";
          return _context28.abrupt("break", 28);

        case 27:
          return _context28.abrupt("break", 28);

        case 28:
          if (!(newEmployStatus !== null)) {
            _context28.next = 31;
            break;
          }

          _context28.next = 31;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].EMPLOY_STATUS_ON_CHANGE,
            newEmployStatus: newEmployStatus
          });

        case 31:
          _context28.next = 35;
          break;

        case 33:
          _context28.prev = 33;
          _context28.t1 = _context28["catch"](0);

        case 35:
        case "end":
          return _context28.stop();
      }
    }
  }, _marked28, this, [[0, 33]]);
}

function employStatusOtherSaga(action) {
  var employStatus, employStatusOther;
  return regeneratorRuntime.wrap(function employStatusOtherSaga$(_context29) {
    while (1) {
      switch (_context29.prev = _context29.next) {
        case 0:
          _context29.prev = 0;

          if (action["@@redux-saga/SAGA_ACTION"]) {
            _context29.next = 11;
            break;
          }

          _context29.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].employStatus;
          });

        case 4:
          employStatus = _context29.sent;
          _context29.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].employStatusOther;
          });

        case 7:
          employStatusOther = _context29.sent;

          if (!(employStatus.indexOf("ot") === -1 && employStatusOther !== "")) {
            _context29.next = 11;
            break;
          }

          _context29.next = 11;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].EMPLOY_STATUS_OTHER_ON_CHANGE,
            newEmployStatusOther: ""
          });

        case 11:
          _context29.next = 15;
          break;

        case 13:
          _context29.prev = 13;
          _context29.t0 = _context29["catch"](0);

        case 15:
        case "end":
          return _context29.stop();
      }
    }
  }, _marked29, this, [[0, 13]]);
}

function occupationSaga(action) {
  var optionsMap, employStatus, occupation, birthday, age, newOccupation, isChangeOccupation, occupationOpt, occupationOptIndustry;
  return regeneratorRuntime.wrap(function occupationSaga$(_context30) {
    while (1) {
      switch (_context30.prev = _context30.next) {
        case 0:
          _context30.prev = 0;

          if (action["@@redux-saga/SAGA_ACTION"]) {
            _context30.next = 41;
            break;
          }

          _context30.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.OPTIONS_MAP];
          });

        case 4:
          optionsMap = _context30.sent;
          _context30.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].employStatus;
          });

        case 7:
          employStatus = _context30.sent;
          _context30.next = 10;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].occupation;
          });

        case 10:
          occupation = _context30.sent;
          _context30.next = 13;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].birthday;
          });

        case 13:
          birthday = _context30.sent;
          age = (0, _common.calcAge)(birthday);
          newOccupation = null;
          isChangeOccupation = false;

          /* employ status rule */

          _context30.t0 = employStatus;
          _context30.next = _context30.t0 === "hh" ? 20 : _context30.t0 === "hw" ? 23 : _context30.t0 === "ue" ? 26 : _context30.t0 === "rt" ? 29 : _context30.t0 === "ot" ? 32 : _context30.t0 === "se" ? 32 : _context30.t0 === "pt" ? 32 : _context30.t0 === "ft" ? 32 : _context30.t0 === "sd" ? 34 : 36;
          break;

        case 20:
          newOccupation = "O674";
          isChangeOccupation = true;
          return _context30.abrupt("break", 37);

        case 23:
          newOccupation = "O675";
          isChangeOccupation = true;
          return _context30.abrupt("break", 37);

        case 26:
          newOccupation = "O1450";
          isChangeOccupation = true;
          return _context30.abrupt("break", 37);

        case 29:
          newOccupation = "O1132";
          isChangeOccupation = true;
          return _context30.abrupt("break", 37);

        case 32:
          if (occupationCheckListWithOutStudent.indexOf(occupation) > -1) {
            newOccupation = "";
            isChangeOccupation = true;
          }
          return _context30.abrupt("break", 37);

        case 34:
          if (birthday === "") {
            newOccupation = "";
            isChangeOccupation = true;
          }
          return _context30.abrupt("break", 37);

        case 36:
          return _context30.abrupt("break", 37);

        case 37:

          if (!isChangeOccupation) {
            /* age and employ status rule */
            if (age < 6) {
              newOccupation = "O246";
            } else if (age >= 6 && age < 18 && employStatus === "sd") {
              newOccupation = "O1322";
            } else if (age >= 18 && employStatus === "sd") {
              newOccupation = "O1321";
            }

            /* industry rule */
            if (action.newIndustry) {
              occupationOpt = _.find(_.get(optionsMap, "occupation.options"), function (opt) {
                return opt.value === occupation;
              });
              occupationOptIndustry = _.get(occupationOpt, "condition", "");

              newOccupation = occupationOptIndustry === action.newIndustry ? null : "";
            }
          }

          if (!(newOccupation !== null)) {
            _context30.next = 41;
            break;
          }

          _context30.next = 41;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].OCCUPATION_ON_CHANGE,
            newOccupation: newOccupation
          });

        case 41:
          _context30.next = 45;
          break;

        case 43:
          _context30.prev = 43;
          _context30.t1 = _context30["catch"](0);

        case 45:
        case "end":
          return _context30.stop();
      }
    }
  }, _marked30, this, [[0, 43]]);
}

function validateOccupation() {
  var isAPP, isProposerMissingData, isInsuredMissingData, occupationData, birthdayData, isShieldQuotation;
  return regeneratorRuntime.wrap(function validateOccupation$(_context31) {
    while (1) {
      switch (_context31.prev = _context31.next) {
        case 0:
          _context31.prev = 0;
          _context31.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isAPP;
          });

        case 3:
          isAPP = _context31.sent;
          _context31.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isProposerMissing;
          });

        case 6:
          isProposerMissingData = _context31.sent;
          _context31.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isInsuredMissing;
          });

        case 9:
          isInsuredMissingData = _context31.sent;
          _context31.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].occupation;
          });

        case 12:
          occupationData = _context31.sent;
          _context31.next = 15;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].birthday;
          });

        case 15:
          birthdayData = _context31.sent;
          _context31.next = 18;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isShieldQuotation;
          });

        case 18:
          isShieldQuotation = _context31.sent;
          _context31.next = 21;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_OCCUPATION,
            occupationData: occupationData,
            birthdayData: birthdayData,
            isProposerMissingData: isProposerMissingData || isInsuredMissingData
          });

        case 21:
          if (!isAPP) {
            _context31.next = 24;
            break;
          }

          _context31.next = 24;
          return multiClientFormValidation();

        case 24:
          if (!isShieldQuotation) {
            _context31.next = 27;
            break;
          }

          _context31.next = 27;
          return availableInsuredProfileValidation();

        case 27:
          _context31.next = 31;
          break;

        case 29:
          _context31.prev = 29;
          _context31.t0 = _context31["catch"](0);

        case 31:
        case "end":
          return _context31.stop();
      }
    }
  }, _marked31, this, [[0, 29]]);
}

function languageOtherSaga(action) {
  var language, languageOther;
  return regeneratorRuntime.wrap(function languageOtherSaga$(_context32) {
    while (1) {
      switch (_context32.prev = _context32.next) {
        case 0:
          _context32.prev = 0;
          _context32.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].language;
          });

        case 3:
          language = _context32.sent;
          _context32.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].languageOther;
          });

        case 6:
          languageOther = _context32.sent;

          if (!(!action["@@redux-saga/SAGA_ACTION"] && language.indexOf("other") === -1 && languageOther !== "")) {
            _context32.next = 10;
            break;
          }

          _context32.next = 10;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LANGUAGE_OTHER_ON_CHANGE,
            newLanguageOther: ""
          });

        case 10:
          _context32.next = 14;
          break;

        case 12:
          _context32.prev = 12;
          _context32.t0 = _context32["catch"](0);

        case 14:
        case "end":
          return _context32.stop();
      }
    }
  }, _marked32, this, [[0, 12]]);
}

function occupationOtherSaga() {
  var occupationData, occupationOtherData;
  return regeneratorRuntime.wrap(function occupationOtherSaga$(_context33) {
    while (1) {
      switch (_context33.prev = _context33.next) {
        case 0:
          _context33.prev = 0;
          _context33.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].occupation;
          });

        case 3:
          occupationData = _context33.sent;
          _context33.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].occupationOther;
          });

        case 6:
          occupationOtherData = _context33.sent;

          if (!(occupationData !== "O921" && occupationOtherData !== "")) {
            _context33.next = 10;
            break;
          }

          _context33.next = 10;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].OCCUPATION_OTHER_ON_CHANGE,
            newOccupationOther: ""
          });

        case 10:
          _context33.next = 14;
          break;

        case 12:
          _context33.prev = 12;
          _context33.t0 = _context33["catch"](0);

        case 14:
        case "end":
          return _context33.stop();
      }
    }
  }, _marked33, this, [[0, 12]]);
}

function industrySaga(action) {
  var optionsMap, occupation, employStatus, rule, newIndustry, occupationOpt;
  return regeneratorRuntime.wrap(function industrySaga$(_context34) {
    while (1) {
      switch (_context34.prev = _context34.next) {
        case 0:
          _context34.prev = 0;
          _context34.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.OPTIONS_MAP];
          });

        case 3:
          optionsMap = _context34.sent;
          _context34.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].occupation;
          });

        case 6:
          occupation = _context34.sent;
          _context34.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].employStatus;
          });

        case 9:
          employStatus = _context34.sent;
          rule = employStatus !== "ft" || employStatus !== "pt" || employStatus !== "se" || employStatus !== "ot";
          newIndustry = null;


          if (rule && !action["@@redux-saga/SAGA_ACTION"]) {
            occupationOpt = _.find(_.get(optionsMap, "occupation.options"), function (opt) {
              return opt.value === occupation;
            });


            newIndustry = _.get(occupationOpt, "condition", "");
          }

          if (!(newIndustry !== null)) {
            _context34.next = 16;
            break;
          }

          _context34.next = 16;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INDUSTRY_ON_CHANGE,
            newIndustry: newIndustry
          });

        case 16:
          _context34.next = 20;
          break;

        case 18:
          _context34.prev = 18;
          _context34.t0 = _context34["catch"](0);

        case 20:
        case "end":
          return _context34.stop();
      }
    }
  }, _marked34, this, [[0, 18]]);
}

function nameOfEmployBusinessSchoolSaga(action) {
  var occupation, occupationCurrently, newNameOfEmployBusinessSchool, employStatusData, birthdayData, ageBelowSix;
  return regeneratorRuntime.wrap(function nameOfEmployBusinessSchoolSaga$(_context35) {
    while (1) {
      switch (_context35.prev = _context35.next) {
        case 0:
          _context35.prev = 0;
          _context35.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].occupation;
          });

        case 3:
          occupation = _context35.sent;
          occupationCurrently = action.occupationCurrently;
          _context35.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].nameOfEmployBusinessSchool;
          });

        case 7:
          newNameOfEmployBusinessSchool = _context35.sent;
          _context35.next = 10;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].employStatus;
          });

        case 10:
          employStatusData = _context35.sent;
          _context35.next = 13;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].birthday;
          });

        case 13:
          birthdayData = _context35.sent;
          ageBelowSix = (0, _common.calcAge)(birthdayData) < 6;

          /* rule of occupation change */

          if (!action["@@redux-saga/SAGA_ACTION"] && occupationCheckList.indexOf(occupation) > -1 && occupation.indexOf(occupationCurrently) === -1 && action.type !== "clientForm/BIRTHDAY_ON_CHANGE" || occupation === "") {
            newNameOfEmployBusinessSchool = "";
          }

          /* rule of employStatus change */
          if (employStatusCheckListWithOutStudent.indexOf(employStatusData) === -1) {
            newNameOfEmployBusinessSchool = newNameOfEmployBusinessSchool === "N/A" ? "" : newNameOfEmployBusinessSchool;
          }

          if (ageBelowSix) {
            newNameOfEmployBusinessSchool = "N/A";
          }

          _context35.next = 20;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].NAME_OF_EMPLOY_BUSINESS_SCHOOL_ON_CHANGE,
            newNameOfEmployBusinessSchool: newNameOfEmployBusinessSchool,
            employStatusData: employStatusData,
            birthdayData: birthdayData
          });

        case 20:
          _context35.next = 24;
          break;

        case 22:
          _context35.prev = 22;
          _context35.t0 = _context35["catch"](0);

        case 24:
        case "end":
          return _context35.stop();
      }
    }
  }, _marked35, this, [[0, 22]]);
}

function countryOfEmployBusinessSchoolSaga(action) {
  var occupation, occupationCurrently, newCountryOfEmployBusinessSchool, employStatusData, birthdayData, ageBelowSix, type;
  return regeneratorRuntime.wrap(function countryOfEmployBusinessSchoolSaga$(_context36) {
    while (1) {
      switch (_context36.prev = _context36.next) {
        case 0:
          _context36.prev = 0;
          _context36.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].occupation;
          });

        case 3:
          occupation = _context36.sent;
          occupationCurrently = action.occupationCurrently;
          _context36.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].countryOfEmployBusinessSchool;
          });

        case 7:
          newCountryOfEmployBusinessSchool = _context36.sent;
          _context36.next = 10;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].employStatus;
          });

        case 10:
          employStatusData = _context36.sent;
          _context36.next = 13;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].birthday;
          });

        case 13:
          birthdayData = _context36.sent;
          ageBelowSix = (0, _common.calcAge)(birthdayData) < 6;

          /* rule of occupation change */

          if (employStatusCheckListWithOutStudent.indexOf(employStatusData) === -1) {
            newCountryOfEmployBusinessSchool = newCountryOfEmployBusinessSchool === "na" ? "R2" : newCountryOfEmployBusinessSchool;
          } else if (!action["@@redux-saga/SAGA_ACTION"] && occupationCheckList.indexOf(occupation) > -1 && occupation.indexOf(occupationCurrently) === -1) {
            newCountryOfEmployBusinessSchool = newCountryOfEmployBusinessSchool === "na" ? "R2" : newCountryOfEmployBusinessSchool;
          }

          if (ageBelowSix) {
            newCountryOfEmployBusinessSchool = "na";
          }

          type = _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].COUNTRY_OF_EMPLOY_BUSINESS_SCHOOL_ON_CHANGE;
          _context36.next = 20;
          return (0, _effects.put)({
            type: type,
            newCountryOfEmployBusinessSchool: newCountryOfEmployBusinessSchool,
            employStatusData: employStatusData,
            birthdayData: birthdayData
          });

        case 20:
          _context36.next = 24;
          break;

        case 22:
          _context36.prev = 22;
          _context36.t0 = _context36["catch"](0);

        case 24:
        case "end":
          return _context36.stop();
      }
    }
  }, _marked36, this, [[0, 22]]);
}

function validateTypeOfPass() {
  var isAPP, typeOfPassData, nationalityData, singaporePRStatusData;
  return regeneratorRuntime.wrap(function validateTypeOfPass$(_context37) {
    while (1) {
      switch (_context37.prev = _context37.next) {
        case 0:
          _context37.prev = 0;
          _context37.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isAPP;
          });

        case 3:
          isAPP = _context37.sent;
          _context37.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].typeOfPass;
          });

        case 6:
          typeOfPassData = _context37.sent;
          _context37.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].nationality;
          });

        case 9:
          nationalityData = _context37.sent;
          _context37.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].singaporePRStatus;
          });

        case 12:
          singaporePRStatusData = _context37.sent;
          _context37.next = 15;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_TYPE_OF_PASS,
            typeOfPassData: typeOfPassData,
            nationalityData: nationalityData,
            singaporePRStatusData: singaporePRStatusData,
            isAPP: isAPP
          });

        case 15:
          if (!isAPP) {
            _context37.next = 18;
            break;
          }

          _context37.next = 18;
          return multiClientFormValidation();

        case 18:
          _context37.next = 22;
          break;

        case 20:
          _context37.prev = 20;
          _context37.t0 = _context37["catch"](0);

        case 22:
        case "end":
          return _context37.stop();
      }
    }
  }, _marked37, this, [[0, 20]]);
}

function validateTypeOfPassOther() {
  var isAPP, typeOfPassData, typeOfPassOtherData;
  return regeneratorRuntime.wrap(function validateTypeOfPassOther$(_context38) {
    while (1) {
      switch (_context38.prev = _context38.next) {
        case 0:
          _context38.prev = 0;
          _context38.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isAPP;
          });

        case 3:
          isAPP = _context38.sent;
          _context38.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].typeOfPass;
          });

        case 6:
          typeOfPassData = _context38.sent;
          _context38.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].typeOfPassOther;
          });

        case 9:
          typeOfPassOtherData = _context38.sent;
          _context38.next = 12;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_TYPE_OF_PASS_OTHER,
            typeOfPassData: typeOfPassData,
            typeOfPassOtherData: typeOfPassOtherData
          });

        case 12:
          if (!isAPP) {
            _context38.next = 15;
            break;
          }

          _context38.next = 15;
          return multiClientFormValidation();

        case 15:
          _context38.next = 19;
          break;

        case 17:
          _context38.prev = 17;
          _context38.t0 = _context38["catch"](0);

        case 19:
        case "end":
          return _context38.stop();
      }
    }
  }, _marked38, this, [[0, 17]]);
}

function TypeOfPassOtherSaga(action) {
  var typeOfPassData, typeOfPassOtherData;
  return regeneratorRuntime.wrap(function TypeOfPassOtherSaga$(_context39) {
    while (1) {
      switch (_context39.prev = _context39.next) {
        case 0:
          _context39.prev = 0;
          _context39.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].typeOfPass;
          });

        case 3:
          typeOfPassData = _context39.sent;
          _context39.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].typeOfPassOther;
          });

        case 6:
          typeOfPassOtherData = _context39.sent;

          if (!(!action["@@redux-saga/SAGA_ACTION"] && typeOfPassData.indexOf("o") === -1 && typeOfPassOtherData !== "")) {
            _context39.next = 10;
            break;
          }

          _context39.next = 10;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].TYPE_OF_PASS_OTHER_ON_CHANGE,
            newTypeOfPassOther: ""
          });

        case 10:
          _context39.next = 14;
          break;

        case 12:
          _context39.prev = 12;
          _context39.t0 = _context39["catch"](0);

        case 14:
        case "end":
          return _context39.stop();
      }
    }
  }, _marked39, this, [[0, 12]]);
}

function singaporePRStatusSaga() {
  var nationality, singaporePRStatus;
  return regeneratorRuntime.wrap(function singaporePRStatusSaga$(_context40) {
    while (1) {
      switch (_context40.prev = _context40.next) {
        case 0:
          _context40.prev = 0;
          _context40.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].nationality;
          });

        case 3:
          nationality = _context40.sent;
          _context40.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].singaporePRStatus;
          });

        case 6:
          singaporePRStatus = _context40.sent;

          if (!(!!nationality && (nationality === "N1" || nationality === "N2") && singaporePRStatus !== "")) {
            _context40.next = 10;
            break;
          }

          _context40.next = 10;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].SINGAPORE_PR_STATUS_ON_CHANGE,
            newSingaporePRStatus: ""
          });

        case 10:
          _context40.next = 14;
          break;

        case 12:
          _context40.prev = 12;
          _context40.t0 = _context40["catch"](0);

        case 14:
        case "end":
          return _context40.stop();
      }
    }
  }, _marked40, this, [[0, 12]]);
}

function validateNationality() {
  var isAPP, isProposerMissingData, isInsuredMissingData, nationalityData, isShieldQuotation;
  return regeneratorRuntime.wrap(function validateNationality$(_context41) {
    while (1) {
      switch (_context41.prev = _context41.next) {
        case 0:
          _context41.prev = 0;
          _context41.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isAPP;
          });

        case 3:
          isAPP = _context41.sent;
          _context41.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isProposerMissing;
          });

        case 6:
          isProposerMissingData = _context41.sent;
          _context41.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isInsuredMissing;
          });

        case 9:
          isInsuredMissingData = _context41.sent;
          _context41.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].nationality;
          });

        case 12:
          nationalityData = _context41.sent;
          _context41.next = 15;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isShieldQuotation;
          });

        case 15:
          isShieldQuotation = _context41.sent;
          _context41.next = 18;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_NATIONALITY,
            nationalityData: nationalityData,
            isProposerMissingData: isProposerMissingData || isInsuredMissingData
          });

        case 18:
          if (!isAPP) {
            _context41.next = 21;
            break;
          }

          _context41.next = 21;
          return multiClientFormValidation();

        case 21:
          if (!isShieldQuotation) {
            _context41.next = 24;
            break;
          }

          _context41.next = 24;
          return availableInsuredProfileValidation();

        case 24:
          _context41.next = 28;
          break;

        case 26:
          _context41.prev = 26;
          _context41.t0 = _context41["catch"](0);

        case 28:
        case "end":
          return _context41.stop();
      }
    }
  }, _marked41, this, [[0, 26]]);
}

function countrySaga() {
  var newCountry, countryOfResidenceData;
  return regeneratorRuntime.wrap(function countrySaga$(_context42) {
    while (1) {
      switch (_context42.prev = _context42.next) {
        case 0:
          _context42.prev = 0;
          _context42.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].country;
          });

        case 3:
          newCountry = _context42.sent;
          _context42.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].countryOfResidence;
          });

        case 6:
          countryOfResidenceData = _context42.sent;
          _context42.next = 9;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].COUNTRY_ON_CHANGE,
            newCountry: newCountry,
            countryOfResidenceData: countryOfResidenceData
          });

        case 9:
          _context42.next = 13;
          break;

        case 11:
          _context42.prev = 11;
          _context42.t0 = _context42["catch"](0);

        case 13:
        case "end":
          return _context42.stop();
      }
    }
  }, _marked42, this, [[0, 11]]);
}

function postalCodeSaga(action) {
  var countryOfResidence, postalCode, callServer, covPostalCode, resultFilename, mappingTable, keyNumber, resp;
  return regeneratorRuntime.wrap(function postalCodeSaga$(_context43) {
    while (1) {
      switch (_context43.prev = _context43.next) {
        case 0:
          _context43.prev = 0;
          _context43.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].countryOfResidence;
          });

        case 3:
          countryOfResidence = _context43.sent;

          if (!(countryOfResidence === "R2")) {
            _context43.next = 40;
            break;
          }

          _context43.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].postal;
          });

        case 7:
          postalCode = _context43.sent;
          _context43.next = 10;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 10:
          callServer = _context43.sent;
          covPostalCode = postalCode && postalCode.length === 6 ? Number(_.toString(postalCode).substr(0, 2)) : Number("0" + _.toString(postalCode).substr(0, 1));
          resultFilename = void 0;
          mappingTable = {
            0: "_00_19",
            19: "_20_29",
            29: "_30_39",
            39: "_40_49",
            49: "_50_59",
            59: "_60_69",
            69: "_70_79",
            79: "_80_89"
          };
          keyNumber = void 0;

          _.forEach(Object.keys(mappingTable).sort(function (a, b) {
            return b - a;
          }), function (obj) {
            keyNumber = Number(obj);
            if (covPostalCode > keyNumber && !resultFilename) {
              resultFilename = mappingTable[obj];
            }
          });

          if (!resultFilename) {
            _context43.next = 40;
            break;
          }

          _context43.next = 19;
          return (0, _effects.call)(callServer, {
            url: "client",
            data: {
              action: "getAddressByPostalCode",
              pC: postalCode,
              fileName: resultFilename
            }
          });

        case 19:
          resp = _context43.sent;

          if (!resp.success) {
            _context43.next = 31;
            break;
          }

          _context43.next = 23;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].BLOCK_HOUSE_ON_CHANGE,
            newBlockHouse: resp.BLDGNO
          });

        case 23:
          _context43.next = 25;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].STREET_ROAD_ON_CHANGE,
            newStreetRoad: resp.STREETNAME
          });

        case 25:
          _context43.next = 27;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].UNIT_ON_CHANGE,
            newUnit: ""
          });

        case 27:
          _context43.next = 29;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].BUILDING_ESTATE_ON_CHANGE,
            newBuildingEstate: resp.BLDGNAME
          });

        case 29:
          _context43.next = 40;
          break;

        case 31:
          /* To notify the user in alert function */
          action.alertFunction();
          _context43.next = 34;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].BLOCK_HOUSE_ON_CHANGE,
            newBlockHouse: ""
          });

        case 34:
          _context43.next = 36;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].STREET_ROAD_ON_CHANGE,
            newStreetRoad: ""
          });

        case 36:
          _context43.next = 38;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].UNIT_ON_CHANGE,
            newUnit: ""
          });

        case 38:
          _context43.next = 40;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].BUILDING_ESTATE_ON_CHANGE,
            newBuildingEstate: ""
          });

        case 40:
          _context43.next = 44;
          break;

        case 42:
          _context43.prev = 42;
          _context43.t0 = _context43["catch"](0);

        case 44:
        case "end":
          return _context43.stop();
      }
    }
  }, _marked43, this, [[0, 42]]);
}

function isCityOfResidenceErrorSaga() {
  var optionsMapData, cityOfResidenceData, countryOfResidenceData;
  return regeneratorRuntime.wrap(function isCityOfResidenceErrorSaga$(_context44) {
    while (1) {
      switch (_context44.prev = _context44.next) {
        case 0:
          _context44.prev = 0;
          _context44.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.OPTIONS_MAP];
          });

        case 3:
          optionsMapData = _context44.sent;
          _context44.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].cityOfResidence;
          });

        case 6:
          cityOfResidenceData = _context44.sent;
          _context44.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].countryOfResidence;
          });

        case 9:
          countryOfResidenceData = _context44.sent;
          _context44.next = 12;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_CITY_OF_RESIDENCE,
            cityOfResidenceData: cityOfResidenceData,
            countryOfResidenceData: countryOfResidenceData,
            optionsMapData: optionsMapData
          });

        case 12:
          _context44.next = 16;
          break;

        case 14:
          _context44.prev = 14;
          _context44.t0 = _context44["catch"](0);

        case 16:
        case "end":
          return _context44.stop();
      }
    }
  }, _marked44, this, [[0, 14]]);
}

function cityOfResidenceSaga() {
  var optionsMap, cityOfResidence, newCityOfResidence;
  return regeneratorRuntime.wrap(function cityOfResidenceSaga$(_context45) {
    while (1) {
      switch (_context45.prev = _context45.next) {
        case 0:
          _context45.prev = 0;
          _context45.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.OPTIONS_MAP];
          });

        case 3:
          optionsMap = _context45.sent;
          _context45.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].cityOfResidence;
          });

        case 6:
          cityOfResidence = _context45.sent;
          newCityOfResidence = null;


          if (!(0, _trigger.optionCondition)({
            value: cityOfResidence,
            options: "city",
            optionsMap: optionsMap,
            showIfNoValue: false
          }) && cityOfResidence !== "") {
            newCityOfResidence = "";
          }

          if (!(newCityOfResidence !== null)) {
            _context45.next = 12;
            break;
          }

          _context45.next = 12;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CITY_OF_RESIDENCE_ON_CHANGE,
            newCityOfResidence: newCityOfResidence
          });

        case 12:
          _context45.next = 16;
          break;

        case 14:
          _context45.prev = 14;
          _context45.t0 = _context45["catch"](0);

        case 16:
        case "end":
          return _context45.stop();
      }
    }
  }, _marked45, this, [[0, 14]]);
}

function validateOtherCityOfResidence() {
  var isAPP, optionsMap, optionsMapData, isApplicationData, cityOfResidenceData, otherCityOfResidenceData;
  return regeneratorRuntime.wrap(function validateOtherCityOfResidence$(_context46) {
    while (1) {
      switch (_context46.prev = _context46.next) {
        case 0:
          _context46.prev = 0;
          _context46.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isAPP;
          });

        case 3:
          isAPP = _context46.sent;
          _context46.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.OPTIONS_MAP];
          });

        case 6:
          optionsMap = _context46.sent;
          _context46.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.OPTIONS_MAP];
          });

        case 9:
          optionsMapData = _context46.sent;
          _context46.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isApplication;
          });

        case 12:
          isApplicationData = _context46.sent;
          _context46.next = 15;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].cityOfResidence;
          });

        case 15:
          cityOfResidenceData = _context46.sent;
          _context46.next = 18;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].otherCityOfResidence;
          });

        case 18:
          otherCityOfResidenceData = _context46.sent;

          if (!(!_.find(_.get(optionsMap, "city.options"), function (opt) {
            return opt.value === cityOfResidenceData && (opt.value === "T110" || opt.value === "T120" || opt.value === "T180" || opt.value === "T205" || opt.value === "T207" || opt.value === "T208" || opt.value === "T209" || opt.value === "T224");
          }) && otherCityOfResidenceData !== "")) {
            _context46.next = 24;
            break;
          }

          _context46.next = 22;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].OTHER_CITY_OF_RESIDENCE_ON_CHANGE,
            newOtherCityOfResidence: ""
          });

        case 22:
          _context46.next = 26;
          break;

        case 24:
          _context46.next = 26;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_OTHER_CITY_OF_RESIDENCE,
            optionsMapData: optionsMapData,
            isApplicationData: isApplicationData,
            cityOfResidenceData: cityOfResidenceData,
            otherCityOfResidenceData: otherCityOfResidenceData
          });

        case 26:
          if (!isAPP) {
            _context46.next = 29;
            break;
          }

          _context46.next = 29;
          return multiClientFormValidation();

        case 29:
          _context46.next = 33;
          break;

        case 31:
          _context46.prev = 31;
          _context46.t0 = _context46["catch"](0);

        case 33:
        case "end":
          return _context46.stop();
      }
    }
  }, _marked46, this, [[0, 31]]);
}

function validateID() {
  var isAPP, IDData, IDDocumentTypeData, configData;
  return regeneratorRuntime.wrap(function validateID$(_context47) {
    while (1) {
      switch (_context47.prev = _context47.next) {
        case 0:
          _context47.prev = 0;
          _context47.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isAPP;
          });

        case 3:
          isAPP = _context47.sent;
          _context47.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].ID;
          });

        case 6:
          IDData = _context47.sent;
          _context47.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].IDDocumentType;
          });

        case 9:
          IDDocumentTypeData = _context47.sent;
          _context47.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config;
          });

        case 12:
          configData = _context47.sent;
          _context47.next = 15;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_ID,
            IDData: IDData,
            IDDocumentTypeData: IDDocumentTypeData,
            configData: configData
          });

        case 15:
          if (!isAPP) {
            _context47.next = 18;
            break;
          }

          _context47.next = 18;
          return multiClientFormValidation();

        case 18:
          _context47.next = 22;
          break;

        case 20:
          _context47.prev = 20;
          _context47.t0 = _context47["catch"](0);

        case 22:
        case "end":
          return _context47.stop();
      }
    }
  }, _marked47, this, [[0, 20]]);
}

function validateBirthday() {
  var birthdayData, isProposerMissingData, isInsuredMissingData, isFamilyMemberData, isFNA, isPDA, isAPP, isShieldQuotation;
  return regeneratorRuntime.wrap(function validateBirthday$(_context48) {
    while (1) {
      switch (_context48.prev = _context48.next) {
        case 0:
          _context48.prev = 0;
          _context48.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].birthday;
          });

        case 3:
          birthdayData = _context48.sent;
          _context48.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isProposerMissing;
          });

        case 6:
          isProposerMissingData = _context48.sent;
          _context48.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isInsuredMissing;
          });

        case 9:
          isInsuredMissingData = _context48.sent;
          _context48.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isFamilyMember;
          });

        case 12:
          isFamilyMemberData = _context48.sent;
          _context48.next = 15;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isFNA;
          });

        case 15:
          isFNA = _context48.sent;
          _context48.next = 18;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isPDA;
          });

        case 18:
          isPDA = _context48.sent;
          _context48.next = 21;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isAPP;
          });

        case 21:
          isAPP = _context48.sent;
          _context48.next = 24;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isShieldQuotation;
          });

        case 24:
          isShieldQuotation = _context48.sent;
          _context48.next = 27;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_BIRTHDAY,
            birthdayData: birthdayData,
            isProposerMissingData: isProposerMissingData || isInsuredMissingData,
            isFamilyMemberData: isFamilyMemberData,
            isFNA: isFNA,
            isPDA: isPDA,
            isAPP: isAPP
          });

        case 27:
          if (!isAPP) {
            _context48.next = 30;
            break;
          }

          _context48.next = 30;
          return multiClientFormValidation();

        case 30:
          if (!isShieldQuotation) {
            _context48.next = 33;
            break;
          }

          _context48.next = 33;
          return availableInsuredProfileValidation();

        case 33:
          _context48.next = 37;
          break;

        case 35:
          _context48.prev = 35;
          _context48.t0 = _context48["catch"](0);

        case 37:
        case "end":
          return _context48.stop();
      }
    }
  }, _marked48, this, [[0, 35]]);
}

function cityStateSaga() {
  var optionsMap, cityOfResidence;
  return regeneratorRuntime.wrap(function cityStateSaga$(_context49) {
    while (1) {
      switch (_context49.prev = _context49.next) {
        case 0:
          _context49.prev = 0;
          _context49.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.OPTIONS_MAP];
          });

        case 3:
          optionsMap = _context49.sent;
          _context49.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].cityOfResidence;
          });

        case 6:
          cityOfResidence = _context49.sent;
          _context49.next = 9;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].CITY_STATE_ON_CHANGE,
            newCityState: _.find(_.get(optionsMap, "city.options"), function (opt) {
              return opt.value === cityOfResidence;
            }).value
          });

        case 9:
          _context49.next = 13;
          break;

        case 11:
          _context49.prev = 11;
          _context49.t0 = _context49["catch"](0);

        case 13:
        case "end":
          return _context49.stop();
      }
    }
  }, _marked49, this, [[0, 11]]);
}

function mobileNoASaga() {
  var newMobileNoA, prefixAData;
  return regeneratorRuntime.wrap(function mobileNoASaga$(_context50) {
    while (1) {
      switch (_context50.prev = _context50.next) {
        case 0:
          _context50.prev = 0;
          _context50.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].mobileNoA;
          });

        case 3:
          newMobileNoA = _context50.sent;
          _context50.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].prefixA;
          });

        case 6:
          prefixAData = _context50.sent;
          _context50.next = 9;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].MOBILE_NO_A_ON_CHANGE,
            newMobileNoA: newMobileNoA,
            prefixAData: prefixAData
          });

        case 9:
          _context50.next = 13;
          break;

        case 11:
          _context50.prev = 11;
          _context50.t0 = _context50["catch"](0);

        case 13:
        case "end":
          return _context50.stop();
      }
    }
  }, _marked50, this, [[0, 11]]);
}

function mobileNoBSaga() {
  var newMobileNoB, prefixBData;
  return regeneratorRuntime.wrap(function mobileNoBSaga$(_context51) {
    while (1) {
      switch (_context51.prev = _context51.next) {
        case 0:
          _context51.prev = 0;
          _context51.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].mobileNoB;
          });

        case 3:
          newMobileNoB = _context51.sent;
          _context51.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].prefixB;
          });

        case 6:
          prefixBData = _context51.sent;
          _context51.next = 9;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].MOBILE_NO_B_ON_CHANGE,
            newMobileNoB: newMobileNoB,
            prefixBData: prefixBData
          });

        case 9:
          _context51.next = 13;
          break;

        case 11:
          _context51.prev = 11;
          _context51.t0 = _context51["catch"](0);

        case 13:
        case "end":
          return _context51.stop();
      }
    }
  }, _marked51, this, [[0, 11]]);
}

function validateLanguageOther() {
  var isAPP, languageData, languageOtherData;
  return regeneratorRuntime.wrap(function validateLanguageOther$(_context52) {
    while (1) {
      switch (_context52.prev = _context52.next) {
        case 0:
          _context52.prev = 0;
          _context52.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isAPP;
          });

        case 3:
          isAPP = _context52.sent;
          _context52.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].language;
          });

        case 6:
          languageData = _context52.sent;
          _context52.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].languageOther;
          });

        case 9:
          languageOtherData = _context52.sent;
          _context52.next = 12;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_LANGUAGE_OTHER,
            languageData: languageData,
            languageOtherData: languageOtherData
          });

        case 12:
          if (!isAPP) {
            _context52.next = 15;
            break;
          }

          _context52.next = 15;
          return multiClientFormValidation();

        case 15:
          _context52.next = 19;
          break;

        case 17:
          _context52.prev = 17;
          _context52.t0 = _context52["catch"](0);

        case 19:
        case "end":
          return _context52.stop();
      }
    }
  }, _marked52, this, [[0, 17]]);
}

function documentTypeSaga() {
  var singaporePRStatusData, nationalityData, newIDDocType;
  return regeneratorRuntime.wrap(function documentTypeSaga$(_context53) {
    while (1) {
      switch (_context53.prev = _context53.next) {
        case 0:
          _context53.prev = 0;
          _context53.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].singaporePRStatus;
          });

        case 3:
          singaporePRStatusData = _context53.sent;
          _context53.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].nationality;
          });

        case 6:
          nationalityData = _context53.sent;
          newIDDocType = null;

          if (!(nationalityData === "N1" || nationalityData === "N2" || singaporePRStatusData === "Y")) {
            _context53.next = 12;
            break;
          }

          newIDDocType = "nric";
          _context53.next = 15;
          break;

        case 12:
          _context53.next = 14;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].IDDocumentType;
          });

        case 14:
          newIDDocType = _context53.sent;

        case 15:
          _context53.next = 17;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].ID_DOCUMENT_TYPE_ON_CHANGE,
            newIDDocType: newIDDocType
          });

        case 17:
          _context53.next = 21;
          break;

        case 19:
          _context53.prev = 19;
          _context53.t0 = _context53["catch"](0);

        case 21:
        case "end":
          return _context53.stop();
      }
    }
  }, _marked53, this, [[0, 19]]);
}

function validateIncome() {
  var isFNA, isAPP, incomeData;
  return regeneratorRuntime.wrap(function validateIncome$(_context54) {
    while (1) {
      switch (_context54.prev = _context54.next) {
        case 0:
          _context54.prev = 0;
          _context54.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isFNA;
          });

        case 3:
          isFNA = _context54.sent;
          _context54.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isAPP;
          });

        case 6:
          isAPP = _context54.sent;
          _context54.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].income;
          });

        case 9:
          incomeData = _context54.sent;
          _context54.next = 12;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_INCOME,
            incomeData: incomeData,
            isFNA: isFNA,
            isAPP: isAPP
          });

        case 12:
          if (!isAPP) {
            _context54.next = 15;
            break;
          }

          _context54.next = 15;
          return multiClientFormValidation();

        case 15:
          _context54.next = 19;
          break;

        case 17:
          _context54.prev = 17;
          _context54.t0 = _context54["catch"](0);

        case 19:
        case "end":
          return _context54.stop();
      }
    }
  }, _marked54, this, [[0, 17]]);
}

function validateEducation() {
  var isAPP, isFNA, educationData;
  return regeneratorRuntime.wrap(function validateEducation$(_context55) {
    while (1) {
      switch (_context55.prev = _context55.next) {
        case 0:
          _context55.prev = 0;
          _context55.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isAPP;
          });

        case 3:
          isAPP = _context55.sent;
          _context55.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isFNA;
          });

        case 6:
          isFNA = _context55.sent;
          _context55.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].education;
          });

        case 9:
          educationData = _context55.sent;
          _context55.next = 12;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_EDUCATION,
            educationData: educationData,
            isFNA: isFNA
          });

        case 12:
          if (!isAPP) {
            _context55.next = 15;
            break;
          }

          _context55.next = 15;
          return multiClientFormValidation();

        case 15:
          _context55.next = 19;
          break;

        case 17:
          _context55.prev = 17;
          _context55.t0 = _context55["catch"](0);

        case 19:
        case "end":
          return _context55.stop();
      }
    }
  }, _marked55, this, [[0, 17]]);
}

function validateMobileNoA() {
  var isAPP, configData, mobileNoAData;
  return regeneratorRuntime.wrap(function validateMobileNoA$(_context56) {
    while (1) {
      switch (_context56.prev = _context56.next) {
        case 0:
          _context56.prev = 0;
          _context56.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isAPP;
          });

        case 3:
          isAPP = _context56.sent;
          _context56.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config;
          });

        case 6:
          configData = _context56.sent;
          _context56.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].mobileNoA;
          });

        case 9:
          mobileNoAData = _context56.sent;
          _context56.next = 12;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_MOBILE_A_NO,
            mobileNoAData: mobileNoAData,
            configData: configData
          });

        case 12:
          if (!isAPP) {
            _context56.next = 15;
            break;
          }

          _context56.next = 15;
          return multiClientFormValidation();

        case 15:
          _context56.next = 19;
          break;

        case 17:
          _context56.prev = 17;
          _context56.t0 = _context56["catch"](0);

        case 19:
        case "end":
          return _context56.stop();
      }
    }
  }, _marked56, this, [[0, 17]]);
}

function validatePrefixA() {
  var isFNA, isAPP, prefixAData;
  return regeneratorRuntime.wrap(function validatePrefixA$(_context57) {
    while (1) {
      switch (_context57.prev = _context57.next) {
        case 0:
          _context57.prev = 0;
          _context57.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isFNA;
          });

        case 3:
          isFNA = _context57.sent;
          _context57.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isAPP;
          });

        case 6:
          isAPP = _context57.sent;
          _context57.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].prefixA;
          });

        case 9:
          prefixAData = _context57.sent;
          _context57.next = 12;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_PREFIX_A,
            prefixAData: prefixAData,
            isFNA: isFNA
          });

        case 12:
          if (!isAPP) {
            _context57.next = 15;
            break;
          }

          _context57.next = 15;
          return multiClientFormValidation();

        case 15:
          _context57.next = 19;
          break;

        case 17:
          _context57.prev = 17;
          _context57.t0 = _context57["catch"](0);

        case 19:
        case "end":
          return _context57.stop();
      }
    }
  }, _marked57, this, [[0, 17]]);
}

function validateEmail() {
  var isAPP, isProposerMissingData, emailData;
  return regeneratorRuntime.wrap(function validateEmail$(_context58) {
    while (1) {
      switch (_context58.prev = _context58.next) {
        case 0:
          _context58.prev = 0;
          _context58.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isAPP;
          });

        case 3:
          isAPP = _context58.sent;
          _context58.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isProposerMissing;
          });

        case 6:
          isProposerMissingData = _context58.sent;
          _context58.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].email;
          });

        case 9:
          emailData = _context58.sent;
          _context58.next = 12;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_EMAIL,
            emailData: emailData,
            isAPP: isAPP,
            isProposerMissingData: isProposerMissingData
          });

        case 12:
          if (!isAPP) {
            _context58.next = 15;
            break;
          }

          _context58.next = 15;
          return multiClientFormValidation();

        case 15:
          _context58.next = 19;
          break;

        case 17:
          _context58.prev = 17;
          _context58.t0 = _context58["catch"](0);

        case 19:
        case "end":
          return _context58.stop();
      }
    }
  }, _marked58, this, [[0, 17]]);
}

function validateLanguage() {
  var isAPP, languageData, configData;
  return regeneratorRuntime.wrap(function validateLanguage$(_context59) {
    while (1) {
      switch (_context59.prev = _context59.next) {
        case 0:
          _context59.next = 2;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config.isAPP;
          });

        case 2:
          isAPP = _context59.sent;
          _context59.next = 5;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].language;
          });

        case 5:
          languageData = _context59.sent;
          _context59.next = 8;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM].config;
          });

        case 8:
          configData = _context59.sent;
          _context59.next = 11;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].VALIDATE_LANGUAGE,
            languageData: languageData,
            configData: configData
          });

        case 11:
          if (!isAPP) {
            _context59.next = 14;
            break;
          }

          _context59.next = 14;
          return multiClientFormValidation();

        case 14:
        case "end":
          return _context59.stop();
      }
    }
  }, _marked59, this);
}

function updateClientProfileAndValidate(action) {
  var client, cid, profile, configData, optionsMapData, loadClientData, clientForm, validatedClientForm, hasErrorList, newHasErrorList, _isClientFormHasError3, hasError, errorFields, clientHasErrorIndex;

  return regeneratorRuntime.wrap(function updateClientProfileAndValidate$(_context60) {
    while (1) {
      switch (_context60.prev = _context60.next) {
        case 0:
          _context60.prev = 0;
          _context60.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT];
          });

        case 3:
          client = _context60.sent;
          cid = action.cid, profile = action.profile, configData = action.configData;
          _context60.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.OPTIONS_MAP];
          });

        case 7:
          optionsMapData = _context60.sent;
          loadClientData = Object.assign({}, (0, _dataMapping.profileMapForm)(_.cloneDeep(profile)), {
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT
          });
          _context60.next = 11;
          return (0, _effects.put)(loadClientData);

        case 11:
          _context60.next = 13;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT_FORM_CONFIG,
            configData: configData
          });

        case 13:
          _context60.next = 15;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM];
          });

        case 15:
          clientForm = _context60.sent;
          _context60.next = 18;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE,
            configData: clientForm.config,
            isPDA: clientForm.config.isPDA,
            isFNA: clientForm.config.isFNA,
            isAPP: clientForm.config.isAPP,
            isShieldQuotation: clientForm.config.isShieldQuotation,
            relationshipData: clientForm.relationship,
            genderData: clientForm.gender,
            profileData: client.profile,
            incomeData: clientForm.income,
            maritalData: clientForm.maritalStatus,
            educationData: clientForm.education,
            isFamilyMemberData: clientForm.config.isFamilyMember,
            isProposerMissingData: clientForm.config.isProposerMissing,
            isInsuredMissingData: clientForm.config.isInsuredMissing,
            relationshipOtherData: clientForm.relationshipOther,
            givenNameData: clientForm.givenName,
            nameData: clientForm.givenName,
            titleData: clientForm.title,
            isCreateData: clientForm.config.isCreate,
            singaporePRStatusData: clientForm.singaporePRStatus,
            nationalityData: clientForm.nationality,
            cityOfResidenceData: clientForm.cityOfResidence,
            countryOfResidenceData: clientForm.countryOfResidence,
            optionsMapData: optionsMapData,
            isApplicationData: clientForm.config.isApplication,
            otherCityOfResidenceData: clientForm.otherCityOfResidence,
            IDData: clientForm.ID,
            IDDocumentTypeData: clientForm.IDDocumentType,
            IDDocumentTypeOtherData: clientForm.IDDocumentTypeOther,
            employStatusData: clientForm.employStatus,
            employStatusOtherData: clientForm.employStatusOther,
            industryData: clientForm.industry,
            occupationData: clientForm.occupation,
            birthdayData: clientForm.birthday,
            smokingStatusData: clientForm.smokingStatus,
            occupationOtherData: clientForm.occupationOther,
            languageData: clientForm.language,
            languageOtherData: clientForm.languageOther,
            emailData: clientForm.email,
            clientFormCid: clientForm.profileBackUp.cid,
            prefixAData: clientForm.prefixA,
            mobileNoAData: clientForm.mobileNoA,
            typeOfPassData: clientForm.typeOfPass,
            typeOfPassOtherData: clientForm.typeOfPassOther
          });

        case 18:
          _context60.next = 20;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM];
          });

        case 20:
          validatedClientForm = _context60.sent;
          _context60.next = 23;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].availableInsuredProfile.hasErrorList;
          });

        case 23:
          hasErrorList = _context60.sent;
          newHasErrorList = _.cloneDeep(hasErrorList);
          _isClientFormHasError3 = (0, _validation.isClientFormHasError)(validatedClientForm), hasError = _isClientFormHasError3.hasError, errorFields = _isClientFormHasError3.errorFields;
          clientHasErrorIndex = newHasErrorList.findIndex(function (clientHasError) {
            return clientHasError.cid === cid;
          });

          if (clientHasErrorIndex >= 0) {
            newHasErrorList[clientHasErrorIndex].hasError = hasError;
          } else if (hasError) {
            newHasErrorList.push({ cid: cid, hasError: hasError, errorFields: errorFields });
          }

          _context60.next = 30;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT].UPDATE_AVAILABLE_INSURED_PROFILE_HAS_ERROR,
            newHasErrorList: newHasErrorList
          });

        case 30:
          _context60.next = 32;
          return (0, _effects.call)(action.callback);

        case 32:
          _context60.next = 36;
          break;

        case 34:
          _context60.prev = 34;
          _context60.t0 = _context60["catch"](0);

        case 36:
        case "end":
          return _context60.stop();
      }
    }
  }, _marked60, this, [[0, 34]]);
}

function loadClientProfileAndInitValidate(action) {
  var client, cid, profile, configData, optionsMapData, loadClientData, clientForm, validatedClientForm, hasErrorList, newHasErrorList, _isClientFormHasError4, hasError, errorFields, clientHasErrorIndex;

  return regeneratorRuntime.wrap(function loadClientProfileAndInitValidate$(_context61) {
    while (1) {
      switch (_context61.prev = _context61.next) {
        case 0:
          _context61.prev = 0;
          _context61.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT];
          });

        case 3:
          client = _context61.sent;
          cid = action.cid, profile = action.profile, configData = action.configData;
          _context61.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.OPTIONS_MAP];
          });

        case 7:
          optionsMapData = _context61.sent;
          loadClientData = Object.assign({}, (0, _dataMapping.profileMapForm)(_.cloneDeep(profile)), {
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT
          });
          _context61.next = 11;
          return (0, _effects.put)(loadClientData);

        case 11:
          _context61.next = 13;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].LOAD_CLIENT_FORM_CONFIG,
            configData: configData
          });

        case 13:
          _context61.next = 15;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM];
          });

        case 15:
          clientForm = _context61.sent;
          _context61.next = 18;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT_FORM].INITIAL_VALIDATE,
            configData: clientForm.config,
            isPDA: clientForm.config.isPDA,
            isFNA: clientForm.config.isFNA,
            isAPP: clientForm.config.isAPP,
            isShieldQuotation: clientForm.config.isShieldQuotation,
            relationshipData: clientForm.relationship,
            genderData: clientForm.gender,
            profileData: client.profile,
            incomeData: clientForm.income,
            maritalData: clientForm.maritalStatus,
            educationData: clientForm.education,
            isFamilyMemberData: clientForm.config.isFamilyMember,
            isProposerMissingData: clientForm.config.isProposerMissing,
            isInsuredMissingData: clientForm.config.isInsuredMissing,
            relationshipOtherData: clientForm.relationshipOther,
            givenNameData: clientForm.givenName,
            nameData: clientForm.givenName,
            titleData: clientForm.title,
            isCreateData: clientForm.config.isCreate,
            singaporePRStatusData: clientForm.singaporePRStatus,
            nationalityData: clientForm.nationality,
            cityOfResidenceData: clientForm.cityOfResidence,
            countryOfResidenceData: clientForm.countryOfResidence,
            optionsMapData: optionsMapData,
            isApplicationData: clientForm.config.isApplication,
            otherCityOfResidenceData: clientForm.otherCityOfResidence,
            IDData: clientForm.ID,
            IDDocumentTypeData: clientForm.IDDocumentType,
            IDDocumentTypeOtherData: clientForm.IDDocumentTypeOther,
            employStatusData: clientForm.employStatus,
            employStatusOtherData: clientForm.employStatusOther,
            industryData: clientForm.industry,
            occupationData: clientForm.occupation,
            birthdayData: clientForm.birthday,
            smokingStatusData: clientForm.smokingStatus,
            occupationOtherData: clientForm.occupationOther,
            languageData: clientForm.language,
            languageOtherData: clientForm.languageOther,
            emailData: clientForm.email,
            clientFormCid: clientForm.profileBackUp.cid,
            prefixAData: clientForm.prefixA,
            mobileNoAData: clientForm.mobileNoA,
            typeOfPassData: clientForm.typeOfPass,
            typeOfPassOtherData: clientForm.typeOfPassOther
          });

        case 18:
          _context61.next = 20;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT_FORM];
          });

        case 20:
          validatedClientForm = _context61.sent;
          _context61.next = 23;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].availableInsuredProfile.hasErrorList;
          });

        case 23:
          hasErrorList = _context61.sent;
          newHasErrorList = _.cloneDeep(hasErrorList);
          _isClientFormHasError4 = (0, _validation.isClientFormHasError)(validatedClientForm), hasError = _isClientFormHasError4.hasError, errorFields = _isClientFormHasError4.errorFields;
          clientHasErrorIndex = newHasErrorList.findIndex(function (clientHasError) {
            return clientHasError.cid === cid;
          });


          if (clientHasErrorIndex >= 0) {
            newHasErrorList[clientHasErrorIndex].hasError = hasError;
          } else if (hasError) {
            newHasErrorList.push({ cid: cid, hasError: hasError, errorFields: errorFields });
          }

          _context61.next = 30;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT].UPDATE_AVAILABLE_INSURED_PROFILE_HAS_ERROR,
            newHasErrorList: newHasErrorList
          });

        case 30:
          _context61.next = 32;
          return (0, _effects.call)(action.callback);

        case 32:
          _context61.next = 36;
          break;

        case 34:
          _context61.prev = 34;
          _context61.t0 = _context61["catch"](0);

        case 36:
        case "end":
          return _context61.stop();
      }
    }
  }, _marked61, this, [[0, 34]]);
}

function checkInsuredList(action) {
  var pCid, iCids, isInsuredMissing, dependantProfiles, i, iCid, dependantProfile;
  return regeneratorRuntime.wrap(function checkInsuredList$(_context62) {
    while (1) {
      switch (_context62.prev = _context62.next) {
        case 0:
          _context62.prev = 0;
          pCid = action.pCid, iCids = action.iCids, isInsuredMissing = action.isInsuredMissing;
          _context62.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].dependantProfiles;
          });

        case 4:
          dependantProfiles = _context62.sent;
          i = 0;

        case 6:
          if (!(i < iCids.length)) {
            _context62.next = 16;
            break;
          }

          iCid = iCids[i];

          if (!(pCid !== iCid)) {
            _context62.next = 13;
            break;
          }

          dependantProfile = dependantProfiles[iCid];

          if (!dependantProfile) {
            _context62.next = 13;
            break;
          }

          _context62.next = 13;
          return loadClientProfileAndInitValidate({
            cid: iCid,
            profile: dependantProfile,
            configData: {
              isCreate: false,
              isFamilyMember: false,
              isProposerMissing: isInsuredMissing,
              isFNA: false,
              isAPP: false,
              isShieldQuotation: true
            }
          });

        case 13:
          i += 1;
          _context62.next = 6;
          break;

        case 16:
          _context62.next = 18;
          return (0, _effects.call)(action.callback);

        case 18:
          _context62.next = 22;
          break;

        case 20:
          _context62.prev = 20;
          _context62.t0 = _context62["catch"](0);

        case 22:
        case "end":
          return _context62.stop();
      }
    }
  }, _marked62, this, [[0, 20]]);
}

function removeCompleteProfileFromList(action) {
  var hasErrorList, newHasErrorList;
  return regeneratorRuntime.wrap(function removeCompleteProfileFromList$(_context63) {
    while (1) {
      switch (_context63.prev = _context63.next) {
        case 0:
          _context63.prev = 0;
          _context63.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].availableInsuredProfile.hasErrorList;
          });

        case 3:
          hasErrorList = _context63.sent;
          newHasErrorList = _.cloneDeep(hasErrorList);


          _.forEach(newHasErrorList, function (newHasErrorListItem, newHasErrorListIndex) {
            if (!newHasErrorListItem.hasError) {
              newHasErrorList = _.remove(newHasErrorList[newHasErrorListIndex]);
            }
          });

          _context63.next = 8;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT].UPDATE_AVAILABLE_INSURED_PROFILE_HAS_ERROR,
            newHasErrorList: newHasErrorList
          });

        case 8:
          _context63.next = 10;
          return (0, _effects.call)(action.callback);

        case 10:
          _context63.next = 14;
          break;

        case 12:
          _context63.prev = 12;
          _context63.t0 = _context63["catch"](0);

        case 14:
        case "end":
          return _context63.stop();
      }
    }
  }, _marked63, this, [[0, 12]]);
}