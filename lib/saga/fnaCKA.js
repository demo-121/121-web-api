"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.updateCKAinit = updateCKAinit;
exports.initCKA = initCKA;
exports.courseSaga = courseSaga;
exports.institutionSaga = institutionSaga;
exports.studyPeriodEndYearSaga = studyPeriodEndYearSaga;
exports.collectiveInvestmentSaga = collectiveInvestmentSaga;
exports.transactionTypeSaga = transactionTypeSaga;
exports.insuranceInvestmentSaga = insuranceInvestmentSaga;
exports.insuranceTypeSaga = insuranceTypeSaga;
exports.professionSaga = professionSaga;
exports.yearsSaga = yearsSaga;

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _moment = require("moment");

var _moment2 = _interopRequireDefault(_moment);

var _effects = require("redux-saga/effects");

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

var _PRODUCT_TYPES = require("../constants/PRODUCT_TYPES");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var _marked = /*#__PURE__*/regeneratorRuntime.mark(updateCKAisValid),
    _marked2 = /*#__PURE__*/regeneratorRuntime.mark(updateCKAinit),
    _marked3 = /*#__PURE__*/regeneratorRuntime.mark(initCKA),
    _marked4 = /*#__PURE__*/regeneratorRuntime.mark(courseSaga),
    _marked5 = /*#__PURE__*/regeneratorRuntime.mark(institutionSaga),
    _marked6 = /*#__PURE__*/regeneratorRuntime.mark(studyPeriodEndYearSaga),
    _marked7 = /*#__PURE__*/regeneratorRuntime.mark(collectiveInvestmentSaga),
    _marked8 = /*#__PURE__*/regeneratorRuntime.mark(transactionTypeSaga),
    _marked9 = /*#__PURE__*/regeneratorRuntime.mark(insuranceInvestmentSaga),
    _marked10 = /*#__PURE__*/regeneratorRuntime.mark(insuranceTypeSaga),
    _marked11 = /*#__PURE__*/regeneratorRuntime.mark(professionSaga),
    _marked12 = /*#__PURE__*/regeneratorRuntime.mark(yearsSaga);

function isPassCKA(ckaData) {
  var course = ckaData.course || "";
  var collectiveInvestment = ckaData.collectiveInvestment || "";
  var insuranceInvestment = ckaData.insuranceInvestment || "";
  var profession = ckaData.profession || "";

  var result = (course !== "notApplicable" ? 1 : 0) + (collectiveInvestment === "Y" ? 1 : 0) + (insuranceInvestment === "Y" ? 1 : 0) + (profession !== "notApplicable" ? 1 : 0) >= 1 ? "Y" : "N";

  return result;
}

function updateCKAisValid() {
  var ckaData, prodType, raSection, prodTypeArr, isCompleted, ckaIsValid;
  return regeneratorRuntime.wrap(function updateCKAisValid$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.ckaSection.owner;
          });

        case 2:
          ckaData = _context.sent;
          _context.next = 5;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.productType.prodType;
          });

        case 5:
          prodType = _context.sent;
          _context.next = 8;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.raSection;
          });

        case 8:
          raSection = _context.sent;
          prodTypeArr = prodType.split(",");
          isCompleted = true;
          ckaIsValid = false;


          if (ckaData.isCourseError.hasError === false && ckaData.isInstitutionError.hasError === false && ckaData.isStudyPeriodEndYearError.hasError === false && ckaData.isCollectiveInvestmentError.hasError === false && ckaData.isTransactionTypeError.hasError === false && ckaData.isInsuranceInvestmentError.hasError === false && ckaData.isInsuranceTypeError.hasError === false && ckaData.isProfessionError.hasError === false && ckaData.isYearsError.hasError === false) {
            ckaIsValid = true;
          }

          if (prodTypeArr.indexOf(_PRODUCT_TYPES.INVESTLINKEDPLANS) > -1 && (!ckaIsValid || !raSection.isValid) || prodTypeArr.indexOf(_PRODUCT_TYPES.PARTICIPATINGPLANS) > -1 && !raSection.isValid) {
            isCompleted = false;
          }

          _context.next = 16;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].UPDATE_CKA_IS_VALID,
            isValid: ckaIsValid
          });

        case 16:
          _context.next = 18;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_IS_COMPLETED,
            isCompleted: isCompleted
          });

        case 18:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, this);
}

function updateCKAinit(action) {
  var ckaData;
  return regeneratorRuntime.wrap(function updateCKAinit$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          _context2.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.ckaSection.owner;
          });

        case 3:
          ckaData = _context2.sent;

          if (!ckaData.init) {
            _context2.next = 9;
            break;
          }

          _context2.next = 7;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].UPDATE_CKA_OWNER_INIT,
            newInit: false
          });

        case 7:
          _context2.next = 9;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_CHANGED,
            newNaIsChanged: true
          });

        case 9:
          if (action && _.isFunction(action.callback)) {
            action.callback();
          }
          _context2.next = 14;
          break;

        case 12:
          _context2.prev = 12;
          _context2.t0 = _context2["catch"](0);

        case 14:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2, this, [[0, 12]]);
}

function initCKA(action) {
  var ckaSection, ckaData, dob, birthYear, newCkaData, noError;
  return regeneratorRuntime.wrap(function initCKA$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          _context3.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.ckaSection;
          });

        case 3:
          ckaSection = _context3.sent;
          ckaData = ckaSection.owner;
          _context3.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile.dob;
          });

        case 7:
          dob = _context3.sent;
          birthYear = (0, _moment2.default)(dob).format("YYYY");
          _context3.next = 11;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].INITIAL_VALIDATE,
            ckaData: ckaData,
            birthYear: birthYear
          });

        case 11:
          _context3.next = 13;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.ckaSection.owner;
          });

        case 13:
          newCkaData = _context3.sent;
          noError = true;


          if (newCkaData.isCourseError.hasError === true || newCkaData.isInstitutionError.hasError === true || newCkaData.isStudyPeriodEndYearError.hasError === true || newCkaData.isCollectiveInvestmentError.hasError === true || newCkaData.isTransactionTypeError.hasError === true || newCkaData.isInsuranceInvestmentError.hasError === true || newCkaData.isInsuranceTypeError.hasError === true || newCkaData.isProfessionError.hasError === true || newCkaData.isYearsError.hasError === true) {
            noError = false;
          }

          // if it already completed but it shows incomplete,
          // it won't trigger the save action.
          // So, set NA is changed, in order to trigger the save action.

          if (!(noError && !ckaSection.isValid)) {
            _context3.next = 19;
            break;
          }

          _context3.next = 19;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_CHANGED,
            newNaIsChanged: true
          });

        case 19:
          _context3.next = 21;
          return updateCKAisValid();

        case 21:
          _context3.next = 23;
          return updateCKAinit();

        case 23:

          if (_.isFunction(action.callback)) {
            action.callback();
          }
          _context3.next = 28;
          break;

        case 26:
          _context3.prev = 26;
          _context3.t0 = _context3["catch"](0);

        case 28:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3, this, [[0, 26]]);
}

function courseSaga() {
  var ckaData, newCourse, dob, birthYear;
  return regeneratorRuntime.wrap(function courseSaga$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.prev = 0;
          _context4.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.ckaSection.owner;
          });

        case 3:
          ckaData = _context4.sent;
          _context4.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.ckaSection.owner.course;
          });

        case 6:
          newCourse = _context4.sent;
          _context4.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile.dob;
          });

        case 9:
          dob = _context4.sent;
          birthYear = (0, _moment2.default)(dob).format("YYYY");

          if (!(newCourse !== "notApplicable" && newCourse !== "")) {
            _context4.next = 16;
            break;
          }

          _context4.next = 14;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].VALIDATE_INSTITUTION,
            newInstitution: ckaData.institution
          });

        case 14:
          _context4.next = 16;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].VALIDATE_STUDYPERIODENDYEAR,
            newStudyPeriodEndYear: ckaData.studyPeriodEndYear,
            birthYear: birthYear
          });

        case 16:
          _context4.next = 18;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].VALIDATE_COURSE,
            newCourse: newCourse
          });

        case 18:
          _context4.next = 20;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_CHANGED,
            newNaIsChanged: true
          });

        case 20:
          _context4.next = 22;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].UPDATE_PASS_CKA,
            newPassCka: isPassCKA(ckaData)
          });

        case 22:
          _context4.next = 24;
          return updateCKAisValid();

        case 24:
          _context4.next = 28;
          break;

        case 26:
          _context4.prev = 26;
          _context4.t0 = _context4["catch"](0);

        case 28:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4, this, [[0, 26]]);
}
function institutionSaga() {
  var newInstitution;
  return regeneratorRuntime.wrap(function institutionSaga$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.prev = 0;
          _context5.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.ckaSection.owner.institution;
          });

        case 3:
          newInstitution = _context5.sent;
          _context5.next = 6;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].VALIDATE_INSTITUTION,
            newInstitution: newInstitution
          });

        case 6:
          _context5.next = 8;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_CHANGED,
            newNaIsChanged: true
          });

        case 8:
          _context5.next = 10;
          return updateCKAisValid();

        case 10:
          _context5.next = 14;
          break;

        case 12:
          _context5.prev = 12;
          _context5.t0 = _context5["catch"](0);

        case 14:
        case "end":
          return _context5.stop();
      }
    }
  }, _marked5, this, [[0, 12]]);
}
function studyPeriodEndYearSaga() {
  var newStudyPeriodEndYear, dob, birthYear;
  return regeneratorRuntime.wrap(function studyPeriodEndYearSaga$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.prev = 0;
          _context6.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.ckaSection.owner.studyPeriodEndYear;
          });

        case 3:
          newStudyPeriodEndYear = _context6.sent;
          _context6.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile.dob;
          });

        case 6:
          dob = _context6.sent;
          birthYear = (0, _moment2.default)(dob).format("YYYY");
          _context6.next = 10;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].VALIDATE_STUDYPERIODENDYEAR,
            newStudyPeriodEndYear: newStudyPeriodEndYear,
            birthYear: birthYear
          });

        case 10:
          _context6.next = 12;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_CHANGED,
            newNaIsChanged: true
          });

        case 12:
          _context6.next = 14;
          return updateCKAisValid();

        case 14:
          _context6.next = 18;
          break;

        case 16:
          _context6.prev = 16;
          _context6.t0 = _context6["catch"](0);

        case 18:
        case "end":
          return _context6.stop();
      }
    }
  }, _marked6, this, [[0, 16]]);
}
function collectiveInvestmentSaga() {
  var ckaData, newCollectiveInvestment;
  return regeneratorRuntime.wrap(function collectiveInvestmentSaga$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          _context7.prev = 0;
          _context7.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.ckaSection.owner;
          });

        case 3:
          ckaData = _context7.sent;
          _context7.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.ckaSection.owner.collectiveInvestment;
          });

        case 6:
          newCollectiveInvestment = _context7.sent;

          if (!(newCollectiveInvestment === "Y")) {
            _context7.next = 10;
            break;
          }

          _context7.next = 10;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].VALIDATE_TRANSACTIONTYPE,
            newTransactionType: ckaData.transactionType
          });

        case 10:
          _context7.next = 12;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].VALIDATE_COLLECTIVEINVESTMENT,
            newCollectiveInvestment: newCollectiveInvestment
          });

        case 12:
          _context7.next = 14;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_CHANGED,
            newNaIsChanged: true
          });

        case 14:
          _context7.next = 16;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].UPDATE_PASS_CKA,
            newPassCka: isPassCKA(ckaData)
          });

        case 16:
          _context7.next = 18;
          return updateCKAisValid();

        case 18:
          _context7.next = 22;
          break;

        case 20:
          _context7.prev = 20;
          _context7.t0 = _context7["catch"](0);

        case 22:
        case "end":
          return _context7.stop();
      }
    }
  }, _marked7, this, [[0, 20]]);
}
function transactionTypeSaga() {
  var newTransactionType;
  return regeneratorRuntime.wrap(function transactionTypeSaga$(_context8) {
    while (1) {
      switch (_context8.prev = _context8.next) {
        case 0:
          _context8.prev = 0;
          _context8.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.ckaSection.owner.transactionType;
          });

        case 3:
          newTransactionType = _context8.sent;
          _context8.next = 6;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_CHANGED,
            newNaIsChanged: true
          });

        case 6:
          _context8.next = 8;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].VALIDATE_TRANSACTIONTYPE,
            newTransactionType: newTransactionType
          });

        case 8:
          _context8.next = 10;
          return updateCKAisValid();

        case 10:
          _context8.next = 14;
          break;

        case 12:
          _context8.prev = 12;
          _context8.t0 = _context8["catch"](0);

        case 14:
        case "end":
          return _context8.stop();
      }
    }
  }, _marked8, this, [[0, 12]]);
}
function insuranceInvestmentSaga() {
  var ckaData, newInsuranceInvestment;
  return regeneratorRuntime.wrap(function insuranceInvestmentSaga$(_context9) {
    while (1) {
      switch (_context9.prev = _context9.next) {
        case 0:
          _context9.prev = 0;
          _context9.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.ckaSection.owner;
          });

        case 3:
          ckaData = _context9.sent;
          _context9.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.ckaSection.owner.insuranceInvestment;
          });

        case 6:
          newInsuranceInvestment = _context9.sent;

          if (!(newInsuranceInvestment === "Y")) {
            _context9.next = 10;
            break;
          }

          _context9.next = 10;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].VALIDATE_INSURANCETYPE,
            newInsuranceType: ckaData.insuranceType
          });

        case 10:
          _context9.next = 12;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].VALIDATE_INSURANCEINVESTMENT,
            newInsuranceInvestment: newInsuranceInvestment
          });

        case 12:
          _context9.next = 14;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_CHANGED,
            newNaIsChanged: true
          });

        case 14:
          _context9.next = 16;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].UPDATE_PASS_CKA,
            newPassCka: isPassCKA(ckaData)
          });

        case 16:
          _context9.next = 18;
          return updateCKAisValid();

        case 18:
          _context9.next = 22;
          break;

        case 20:
          _context9.prev = 20;
          _context9.t0 = _context9["catch"](0);

        case 22:
        case "end":
          return _context9.stop();
      }
    }
  }, _marked9, this, [[0, 20]]);
}
function insuranceTypeSaga() {
  var newInsuranceType;
  return regeneratorRuntime.wrap(function insuranceTypeSaga$(_context10) {
    while (1) {
      switch (_context10.prev = _context10.next) {
        case 0:
          _context10.prev = 0;
          _context10.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.ckaSection.owner.insuranceType;
          });

        case 3:
          newInsuranceType = _context10.sent;
          _context10.next = 6;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].VALIDATE_INSURANCETYPE,
            newInsuranceType: newInsuranceType
          });

        case 6:
          _context10.next = 8;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_CHANGED,
            newNaIsChanged: true
          });

        case 8:
          _context10.next = 10;
          return updateCKAisValid();

        case 10:
          _context10.next = 14;
          break;

        case 12:
          _context10.prev = 12;
          _context10.t0 = _context10["catch"](0);

        case 14:
        case "end":
          return _context10.stop();
      }
    }
  }, _marked10, this, [[0, 12]]);
}
function professionSaga() {
  var ckaData, newProfession;
  return regeneratorRuntime.wrap(function professionSaga$(_context11) {
    while (1) {
      switch (_context11.prev = _context11.next) {
        case 0:
          _context11.prev = 0;
          _context11.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.ckaSection.owner;
          });

        case 3:
          ckaData = _context11.sent;
          _context11.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.ckaSection.owner.profession;
          });

        case 6:
          newProfession = _context11.sent;

          if (!(newProfession !== "notApplicable" && newProfession !== "")) {
            _context11.next = 10;
            break;
          }

          _context11.next = 10;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].VALIDATE_YEARS,
            newYears: ckaData.years
          });

        case 10:
          _context11.next = 12;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].VALIDATE_PROFESSION,
            newProfession: newProfession
          });

        case 12:
          _context11.next = 14;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_CHANGED,
            newNaIsChanged: true
          });

        case 14:
          _context11.next = 16;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].UPDATE_PASS_CKA,
            newPassCka: isPassCKA(ckaData)
          });

        case 16:
          _context11.next = 18;
          return updateCKAisValid();

        case 18:
          _context11.next = 22;
          break;

        case 20:
          _context11.prev = 20;
          _context11.t0 = _context11["catch"](0);

        case 22:
        case "end":
          return _context11.stop();
      }
    }
  }, _marked11, this, [[0, 20]]);
}
function yearsSaga() {
  var newYears;
  return regeneratorRuntime.wrap(function yearsSaga$(_context12) {
    while (1) {
      switch (_context12.prev = _context12.next) {
        case 0:
          _context12.prev = 0;
          _context12.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.ckaSection.owner.years;
          });

        case 3:
          newYears = _context12.sent;
          _context12.next = 6;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].VALIDATE_YEARS,
            newYears: newYears
          });

        case 6:
          _context12.next = 8;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_CHANGED,
            newNaIsChanged: true
          });

        case 8:
          _context12.next = 10;
          return updateCKAisValid();

        case 10:
          _context12.next = 14;
          break;

        case 12:
          _context12.prev = 12;
          _context12.t0 = _context12["catch"](0);

        case 14:
        case "end":
          return _context12.stop();
      }
    }
  }, _marked12, this, [[0, 12]]);
}