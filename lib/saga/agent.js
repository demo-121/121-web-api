"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.updateAgentProfileAvatar = updateAgentProfileAvatar;
exports.assignEligProductsToAgent = assignEligProductsToAgent;

var _effects = require("redux-saga/effects");

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var _marked = /*#__PURE__*/regeneratorRuntime.mark(updateAgentProfileAvatar),
    _marked2 = /*#__PURE__*/regeneratorRuntime.mark(assignEligProductsToAgent);

function updateAgentProfileAvatar(action) {
  var callServer, imageType, base64, callback;
  return regeneratorRuntime.wrap(function updateAgentProfileAvatar$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context.sent;
          imageType = action.imageType, base64 = action.base64, callback = action.callback;
          _context.next = 7;
          return (0, _effects.call)(callServer, {
            url: "agent",
            data: {
              action: "updateProfilePic",
              type: imageType,
              data: base64
            }
          });

        case 7:

          if (_.isFunction(callback)) {
            callback();
          }
          _context.next = 12;
          break;

        case 10:
          _context.prev = 10;
          _context.t0 = _context["catch"](0);

        case 12:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, this, [[0, 10]]);
}

function assignEligProductsToAgent(action) {
  var callServer, agent, callback, response, newAgent;
  return regeneratorRuntime.wrap(function assignEligProductsToAgent$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          _context2.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context2.sent;
          _context2.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.AGENT];
          });

        case 6:
          agent = _context2.sent;
          callback = action.callback;
          _context2.next = 10;
          return (0, _effects.call)(callServer, {
            url: "agent",
            data: {
              action: "assignEligProductsToAgent"
            }
          });

        case 10:
          response = _context2.sent;

          if (!response.success) {
            _context2.next = 16;
            break;
          }

          newAgent = _.cloneDeep(agent);

          newAgent.eligProducts = response.agent.eligProducts;
          _context2.next = 16;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.AGENT].UPDATE_AGENT_DATA,
            data: newAgent
          });

        case 16:

          if (_.isFunction(callback)) {
            callback();
          }
          _context2.next = 21;
          break;

        case 19:
          _context2.prev = 19;
          _context2.t0 = _context2["catch"](0);

        case 21:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2, this, [[0, 19]]);
}