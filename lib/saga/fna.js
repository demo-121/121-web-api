"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.resetProductTypeCkaRaIsValid = resetProductTypeCkaRaIsValid;
exports.initNaAnalysis = initNaAnalysis;
exports.validateNeedsAnalysisData = validateNeedsAnalysisData;
exports.updateNaAnalysisDataAndValidate = updateNaAnalysisDataAndValidate;
exports.updateNaAnalysis = updateNaAnalysis;
exports.updateNaAspects = updateNaAspects;
exports.initCompletedStepSaga = initCompletedStepSaga;
exports.roiValidation = roiValidation;
exports.iarRateNotMatchReasonSaga = iarRateNotMatchReasonSaga;
exports.validateFE = validateFE;
exports.getFNA = getFNA;
exports.saveNA = saveNA;
exports.updateProductTypesCompletedStep = updateProductTypesCompletedStep;
exports.initPDA = initPDA;
exports.updateLastSelectedProduct = updateLastSelectedProduct;
exports.initProductTypesSaga = initProductTypesSaga;
exports.validateNA = validateNA;
exports.updateNAValidStatus = updateNAValidStatus;
exports.saveFE = saveFE;
exports.loadFNAReport = loadFNAReport;
exports.openEmailDialog = openEmailDialog;
exports.sendEmail = sendEmail;
exports.validateGivenName = validateGivenName;
exports.validateSurname = validateSurname;
exports.validateName = validateName;
exports.validateIDDocumentType = validateIDDocumentType;
exports.validateIDDocumentTypeOther = validateIDDocumentTypeOther;
exports.validateID = validateID;
exports.validateRelationship = validateRelationship;
exports.relationShipOtherSaga = relationShipOtherSaga;
exports.validatePrefix = validatePrefix;
exports.validateMobileNo = validateMobileNo;
exports.initialValidateTrustedIndividualSaga = initialValidateTrustedIndividualSaga;
exports.updatePDA = updatePDA;
exports.savePDA = savePDA;

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _effects = require("redux-saga/effects");

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _FIELD_TYPES = require("../constants/FIELD_TYPES");

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

var _dataMapping = require("../utilities/dataMapping");

var _naAnalysis = require("../utilities/naAnalysis");

var naAnalysisUtils = _interopRequireWildcard(_naAnalysis);

var _PROGRESS_TABS = require("../constants/PROGRESS_TABS");

var _PROGRESS_TABS2 = _interopRequireDefault(_PROGRESS_TABS);

var _NEEDS = require("../constants/NEEDS");

var _PRODUCT_TYPES = require("../constants/PRODUCT_TYPES");

var _locales = require("../locales");

var _common = require("../utilities/common");

var _validation = require("../utilities/validation");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _marked = /*#__PURE__*/regeneratorRuntime.mark(resetProductTypeCkaRaIsValid),
    _marked2 = /*#__PURE__*/regeneratorRuntime.mark(initNaAnalysis),
    _marked3 = /*#__PURE__*/regeneratorRuntime.mark(validateNeedsAnalysisData),
    _marked4 = /*#__PURE__*/regeneratorRuntime.mark(updateNaAnalysisDataAndValidate),
    _marked5 = /*#__PURE__*/regeneratorRuntime.mark(updateNaAnalysis),
    _marked6 = /*#__PURE__*/regeneratorRuntime.mark(updateNaAspects),
    _marked7 = /*#__PURE__*/regeneratorRuntime.mark(initCompletedStepSaga),
    _marked8 = /*#__PURE__*/regeneratorRuntime.mark(roiValidation),
    _marked9 = /*#__PURE__*/regeneratorRuntime.mark(iarRateNotMatchReasonSaga),
    _marked10 = /*#__PURE__*/regeneratorRuntime.mark(validateFE),
    _marked11 = /*#__PURE__*/regeneratorRuntime.mark(getFNA),
    _marked12 = /*#__PURE__*/regeneratorRuntime.mark(saveNA),
    _marked13 = /*#__PURE__*/regeneratorRuntime.mark(updateProductTypesCompletedStep),
    _marked14 = /*#__PURE__*/regeneratorRuntime.mark(initPDA),
    _marked15 = /*#__PURE__*/regeneratorRuntime.mark(updateLastSelectedProduct),
    _marked16 = /*#__PURE__*/regeneratorRuntime.mark(initProductTypesSaga),
    _marked17 = /*#__PURE__*/regeneratorRuntime.mark(validateNA),
    _marked18 = /*#__PURE__*/regeneratorRuntime.mark(updateNAValidStatus),
    _marked19 = /*#__PURE__*/regeneratorRuntime.mark(saveFE),
    _marked20 = /*#__PURE__*/regeneratorRuntime.mark(loadFNAReport),
    _marked21 = /*#__PURE__*/regeneratorRuntime.mark(openEmailDialog),
    _marked22 = /*#__PURE__*/regeneratorRuntime.mark(sendEmail),
    _marked23 = /*#__PURE__*/regeneratorRuntime.mark(validateGivenName),
    _marked24 = /*#__PURE__*/regeneratorRuntime.mark(validateSurname),
    _marked25 = /*#__PURE__*/regeneratorRuntime.mark(validateName),
    _marked26 = /*#__PURE__*/regeneratorRuntime.mark(validateIDDocumentType),
    _marked27 = /*#__PURE__*/regeneratorRuntime.mark(validateIDDocumentTypeOther),
    _marked28 = /*#__PURE__*/regeneratorRuntime.mark(validateID),
    _marked29 = /*#__PURE__*/regeneratorRuntime.mark(validateRelationship),
    _marked30 = /*#__PURE__*/regeneratorRuntime.mark(relationShipOtherSaga),
    _marked31 = /*#__PURE__*/regeneratorRuntime.mark(validatePrefix),
    _marked32 = /*#__PURE__*/regeneratorRuntime.mark(validateMobileNo),
    _marked33 = /*#__PURE__*/regeneratorRuntime.mark(initialValidateTrustedIndividualSaga),
    _marked34 = /*#__PURE__*/regeneratorRuntime.mark(updatePDA),
    _marked35 = /*#__PURE__*/regeneratorRuntime.mark(savePDA);

var initNaValueAndError = function initNaValueAndError(_ref) {
  var rule = _ref.rule,
      data = _ref.data,
      error = _ref.error;

  _.forEach(rule, function (r, componentId) {
    // init value
    if (!_.has(data, componentId)) {
      if (_.toLower(r.type) === "assets") {
        data[componentId] = [];
      } else if (_.toLower(r.type) === "int") {
        data[componentId] = 0;
      } else {
        data[componentId] = "";
      }
    }

    // init error
    var validateResult = componentId === "assets" ? (0, _validation.validateAssets)({
      rule: r,
      value: data[componentId]
    }) : (0, _validation.validateNeedsAnalysis)({
      rule: r,
      value: data[componentId]
    });
    _.set(error, componentId, validateResult);
    error.isValid = error.isValid && !validateResult.hasError;
  });
};

var addProdType = function addProdType(product, products) {
  var productsArr = products.split(",");
  var index = productsArr.indexOf(product);
  if (index === -1) {
    productsArr.push(product);
  }
  productsArr = productsArr.filter(function (x) {
    return x !== "";
  });
  return productsArr.join();
};

function resetProductTypeCkaRaIsValid() {
  var na, needsAspect, productType;
  return regeneratorRuntime.wrap(function resetProductTypeCkaRaIsValid$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na;
          });

        case 2:
          na = _context.sent;
          needsAspect = na.aspects.split(",");
          productType = "";


          if (needsAspect.indexOf(_NEEDS.CIPROTECTION) > -1 || needsAspect.indexOf(_NEEDS.PCHEADSTART) > -1) {
            productType = addProdType(_PRODUCT_TYPES.NONPARTICIPATINGPLAN, productType);
          }

          if (na.aspects === _NEEDS.PAPROTECTION || na.aspects === _NEEDS.HCPROTECTION || _.isEqual(needsAspect.sort(), [_NEEDS.PAPROTECTION, _NEEDS.HCPROTECTION].sort())) {
            productType = addProdType(_PRODUCT_TYPES.ACCIDENTHEALTHPLANS, productType);
          } else if (needsAspect.indexOf(_NEEDS.PAPROTECTION) > -1 || needsAspect.indexOf(_NEEDS.HCPROTECTION) > -1) {
            productType = addProdType(_PRODUCT_TYPES.ACCIDENTHEALTHPLANS, productType);
          }

          _context.next = 9;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].RESET_PRODUCT_TYPE_CKA_RA_IS_VALID,
            value: productType,
            ckaIsValid: false,
            raIsValid: false
          });

        case 9:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, this);
}

function initNaAnalysis(action) {
  var aspect, na, isNaError, analysisAspect, newNa, newIsNaError, needsAspect;
  return regeneratorRuntime.wrap(function initNaAnalysis$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          aspect = action.aspect;
          _context2.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na;
          });

        case 4:
          na = _context2.sent;
          _context2.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].isNaError;
          });

        case 7:
          isNaError = _context2.sent;
          analysisAspect = naAnalysisUtils.analysisAspect;
          newNa = _.cloneDeep(na);
          newIsNaError = _.cloneDeep(isNaError);
          needsAspect = _.find(analysisAspect.needs, function (needs) {
            return needs.value === aspect;
          });

          // init aspect level

          if (!_.has(newNa, aspect)) {
            newNa[aspect] = { isValid: true };
          }
          if (!_.has(newIsNaError, aspect)) {
            newIsNaError[aspect] = { isValid: true };
          }

          // init owner / spouse / dependants level
          _.forEach(newNa[aspect], function (analysisData, dataProfile) {
            if (["owner", "spouse", "dependants"].indexOf(dataProfile) >= 0) {
              if (!_.has(newIsNaError[aspect], dataProfile)) {
                newIsNaError[aspect][dataProfile] = dataProfile === "dependants" ? [] : {};
              }

              if (dataProfile === "dependants") {
                _.forEach(analysisData, function (dependant) {
                  if (_.findIndex(newIsNaError[aspect].dependants, function (d) {
                    return d.cid === dependant.cid;
                  }) < 0) {
                    newIsNaError[aspect].dependants.push({ cid: dependant.cid });
                  }
                });
              }

              if (needsAspect) {
                var rule = needsAspect.rule;

                if (dataProfile === "dependants") {
                  _.forEach(analysisData, function (dependant, index) {
                    var profileContent = newNa[aspect][dataProfile][index];
                    var profileError = { isValid: true };
                    initNaValueAndError({
                      rule: rule,
                      data: profileContent,
                      error: profileError
                    });
                    newNa[aspect].isValid = newNa[aspect].isValid && !profileError.hasError;
                    newIsNaError[aspect][dataProfile][index] = profileError;
                    newIsNaError[aspect].isValid = newIsNaError[aspect].isValid && !profileError.hasError;
                  });
                } else {
                  var profileContent = newNa[aspect][dataProfile];
                  var profileError = { isValid: true };
                  initNaValueAndError({
                    rule: rule,
                    data: profileContent,
                    error: profileError
                  });
                  newNa[aspect].isValid = newNa[aspect].isValid && !profileError.hasError;
                  newIsNaError[aspect][dataProfile] = profileError;
                  newIsNaError[aspect].isValid = newIsNaError[aspect].isValid && !profileError.hasError;
                }
              }
            }
          });

          _context2.next = 17;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_ANALYSIS,
            value: newNa,
            newIsNaError: newIsNaError
          });

        case 17:
          _context2.next = 21;
          break;

        case 19:
          _context2.prev = 19;
          _context2.t0 = _context2["catch"](0);

        case 21:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2, this, [[0, 19]]);
}

function validateNeedsAnalysisData(action) {
  var value, na, pda, isNaError, dependantProfilesData, newNa, newIsNaError, newTargetAspectError, key, validateResult, targetIndex, profileError, profileIsValid, _profileError, _profileIsValid, aspectisValid, completedStep, analysisCompleted;

  return regeneratorRuntime.wrap(function validateNeedsAnalysisData$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          value = action.value;
          _context3.next = 4;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na;
          });

        case 4:
          na = _context3.sent;
          _context3.next = 7;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].pda;
          });

        case 7:
          pda = _context3.sent;
          _context3.next = 10;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].isNaError;
          });

        case 10:
          isNaError = _context3.sent;
          _context3.next = 13;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].dependantProfiles;
          });

        case 13:
          dependantProfilesData = _context3.sent;
          newNa = _.cloneDeep(na);
          newIsNaError = _.cloneDeep(isNaError);
          newTargetAspectError = newIsNaError[value.targetProduct];
          key = Object.keys(value.targetData)[0];

          // validation

          validateResult = key === "assets" ? (0, _validation.validateAssets)({
            rule: value.rule,
            value: value
          }) : (0, _validation.validateNeedsAnalysis)({
            rule: value.rule,
            value: value.targetData[key]
          });


          if (value.targetProfile !== "owner" && value.targetProfile !== "spouse") {
            // dependants
            targetIndex = _.findIndex(na[value.targetProduct].dependants, function (dependant) {
              return dependant.cid === value.targetProfile;
            });


            if (targetIndex >= 0) {
              _.set(newTargetAspectError, "dependants[" + targetIndex + "][" + key + "]", validateResult);

              profileError = _.get(newTargetAspectError, "dependants[" + targetIndex + "]");
              profileIsValid = true;

              _.forEach(profileError, function (fieldError) {
                if (_.has(fieldError, "hasError")) {
                  profileIsValid = profileIsValid && !fieldError.hasError;
                }
              });
              _.set(newTargetAspectError, "dependants[" + targetIndex + "].isValid", profileIsValid);
            }
          } else {
            // owner / spouse
            _.set(newTargetAspectError, "[" + value.targetProfile + "]" + key, validateResult);

            _profileError = _.get(newTargetAspectError, "[" + value.targetProfile + "]");
            _profileIsValid = true;

            _.forEach(_profileError, function (fieldError) {
              if (_.has(fieldError, "hasError")) {
                _profileIsValid = _profileIsValid && !fieldError.hasError;
              }
            });
            _.set(newTargetAspectError, "[" + value.targetProfile + "].isValid", _profileIsValid);
          }

          // Check isValid of targetProduct
          aspectisValid = true;

          if (_.has(newTargetAspectError, "owner.isValid") && _.get(na, "[" + value.targetProduct + "].owner.isActive")) {
            aspectisValid = aspectisValid && newTargetAspectError.owner.isValid;
          }
          if (_.has(newTargetAspectError, "spouse")) {
            if (pda.applicant === "single" && _.get(na, "[" + value.targetProduct + "].spouse.isActive") && _.has(newTargetAspectError, "spouse.isValid")) {
              aspectisValid = aspectisValid && true;
            } else {
              aspectisValid = aspectisValid && newTargetAspectError.spouse.isValid;
            }
          }
          if (_.has(newTargetAspectError, "dependants")) {
            _.forEach(newTargetAspectError.dependants, function (dependant, index) {
              _.forEach(dependantProfilesData, function (profile) {
                var cid = profile.cid;

                if (pda.dependants.indexOf(cid) === -1 && _.get(na, "[" + value.targetProduct + "].dependants[" + index + "].isActive") && _.has(dependant, "isValid")) {
                  aspectisValid = aspectisValid && true;
                } else if (pda.dependants.indexOf(cid) > -1) {
                  _.forEach(newTargetAspectError.dependants, function () {
                    aspectisValid = aspectisValid && (dependant.isValid || !_.has(dependant, "isValid"));
                  });
                }
              });
            });
          }
          _.set(newNa, "[" + value.targetProduct + "].isValid", aspectisValid);

          _context3.next = 27;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].NA_ANALYSIS_USERDATA_VALIDATE,
            value: newNa,
            newIsNaError: newIsNaError
          });

        case 27:
          completedStep = 1;
          analysisCompleted = true;

          newNa.aspects.split(",").forEach(function (aspect) {
            if (!newNa[aspect].isValid) {
              analysisCompleted = false;
            }
          });
          if (analysisCompleted) {
            completedStep = 2;
          } else {
            completedStep = 1;
          }

          _context3.next = 33;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_COMPLETED_STEP,
            completedStep: completedStep
          });

        case 33:
          _context3.next = 37;
          break;

        case 35:
          _context3.prev = 35;
          _context3.t0 = _context3["catch"](0);

        case 37:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3, this, [[0, 35]]);
}

function updateNaAnalysisDataAndValidate(action) {
  return regeneratorRuntime.wrap(function updateNaAnalysisDataAndValidate$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.prev = 0;
          _context4.next = 3;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_ANALYSIS_USERDATA,
            value: action.value
          });

        case 3:
          if (!action.value.targetData.assets) {
            _context4.next = 6;
            break;
          }

          _context4.next = 6;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_ANALYSIS_USERDATA_ASSETS,
            value: action.value
          });

        case 6:
          if (!action.value.rule) {
            _context4.next = 9;
            break;
          }

          _context4.next = 9;
          return validateNeedsAnalysisData(action);

        case 9:
          _context4.next = 11;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_CHANGED,
            newNaIsChanged: true
          });

        case 11:
          _context4.next = 13;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_GLOBALLY_CHANGED,
            newNaIsGloballyChanged: true
          });

        case 13:
          _context4.next = 17;
          break;

        case 15:
          _context4.prev = 15;
          _context4.t0 = _context4["catch"](0);

        case 17:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4, this, [[0, 15]]);
}

// updateNaAnalysis: do logical calculation of raw data in saga,
// then call updateNaAnalysisDataAndValidate to update the store
function updateNaAnalysis(action) {
  var _action$value, targetFunction, data, func, targetData, nextAction;

  return regeneratorRuntime.wrap(function updateNaAnalysis$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _action$value = action.value, targetFunction = _action$value.targetFunction, data = _action$value.data;
          func = naAnalysisUtils[targetFunction];

          if (!func) {
            _context5.next = 9;
            break;
          }

          targetData = func(data);

          if (action.value.data) {
            delete action.value.data;
          }
          nextAction = Object.assign({}, action, {
            value: Object.assign({}, action.value, { targetData: targetData })
          });
          return _context5.delegateYield(updateNaAnalysisDataAndValidate(nextAction), "t0", 7);

        case 7:
          _context5.next = 10;
          break;

        case 9:
          return _context5.delegateYield(updateNaAnalysisDataAndValidate(action), "t1", 10);

        case 10:
        case "end":
          return _context5.stop();
      }
    }
  }, _marked5, this);
}

function updateNaAspects() {
  return regeneratorRuntime.wrap(function updateNaAspects$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.next = 2;
          return resetProductTypeCkaRaIsValid();

        case 2:
        case "end":
          return _context6.stop();
      }
    }
  }, _marked6, this);
}

function initCompletedStepSaga(action) {
  var na, completedStep, lastStepIndex, needsAspect, iarRate, iarRateNotMatchReason, mandatory, ruleA, ruleB, iarRateNotMatchReasonValid, analysisCompleted;
  return regeneratorRuntime.wrap(function initCompletedStepSaga$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          _context7.next = 2;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na;
          });

        case 2:
          na = _context7.sent;
          completedStep = na.completedStep, lastStepIndex = na.lastStepIndex;
          _context7.t0 = action.value;
          _context7.next = _context7.t0 === _PROGRESS_TABS2.default[_REDUCER_TYPES.FNA].ROI ? 7 : _context7.t0 === _PROGRESS_TABS2.default[_REDUCER_TYPES.FNA].ANALYSIS ? 18 : 23;
          break;

        case 7:
          needsAspect = na.aspects.split(",");
          iarRate = na.iarRate, iarRateNotMatchReason = na.iarRateNotMatchReason;
          mandatory = false;


          if (needsAspect.indexOf(_NEEDS.CIPROTECTION) > -1 || needsAspect.indexOf(_NEEDS.FIPROTECTION) > -1 || needsAspect.indexOf(_NEEDS.RPLANNING) > -1) {
            mandatory = true;
          }

          // The rules that allow to pass
          ruleA = mandatory && iarRate !== "";
          ruleB = !mandatory;
          iarRateNotMatchReasonValid = true;

          if ((parseFloat(na.iarRate) < 0 || parseFloat(na.iarRate) > 6) && iarRateNotMatchReason === "") {
            iarRateNotMatchReasonValid = false;
          }

          if (completedStep <= 1 && (ruleA || ruleB) && iarRateNotMatchReasonValid) {
            completedStep = 1;
          }
          lastStepIndex = 1;
          return _context7.abrupt("break", 24);

        case 18:
          analysisCompleted = true;

          na.aspects.split(",").forEach(function (aspect) {
            if (!na[aspect].isValid) {
              analysisCompleted = false;
            }
          });
          if (analysisCompleted && completedStep <= 2) {
            completedStep = 2;
          }
          lastStepIndex = 2;
          return _context7.abrupt("break", 24);

        case 23:
          return _context7.abrupt("break", 24);

        case 24:
          _context7.next = 26;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].INIT_COMPLETED_STEP,
            completedStep: completedStep,
            lastStepIndex: lastStepIndex
          });

        case 26:
        case "end":
          return _context7.stop();
      }
    }
  }, _marked7, this);
}

function roiValidation() {
  var _message;

  var newIarRate, newIarRateNotMatchReason, checkMandatory, completedStep;
  return regeneratorRuntime.wrap(function roiValidation$(_context8) {
    while (1) {
      switch (_context8.prev = _context8.next) {
        case 0:
          _context8.next = 2;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.iarRate;
          });

        case 2:
          newIarRate = _context8.sent;
          _context8.next = 5;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.iarRateNotMatchReason;
          });

        case 5:
          newIarRateNotMatchReason = _context8.sent;
          checkMandatory = {
            hasError: false,
            message: (_message = {}, _defineProperty(_message, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message)
          };
          completedStep = 1;


          if (parseFloat(newIarRate) < 0 || parseFloat(newIarRate) > 6) {
            checkMandatory = (0, _validation.validateMandatory)({
              field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: true, disabled: false },
              value: newIarRateNotMatchReason
            });
          }

          if (checkMandatory.hasError) {
            completedStep = 0;
          }

          _context8.next = 12;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].VALIDATE_IAR_RATE_NOT_MATCH_REASON,
            newIsIarRateNotMatchReasonError: checkMandatory,
            completedStep: completedStep
          });

        case 12:
          _context8.next = 14;
          return resetProductTypeCkaRaIsValid();

        case 14:
        case "end":
          return _context8.stop();
      }
    }
  }, _marked8, this);
}

function iarRateNotMatchReasonSaga() {
  return regeneratorRuntime.wrap(function iarRateNotMatchReasonSaga$(_context9) {
    while (1) {
      switch (_context9.prev = _context9.next) {
        case 0:
          _context9.prev = 0;
          _context9.next = 3;
          return roiValidation();

        case 3:
          _context9.next = 5;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_CHANGED,
            newNaIsChanged: true
          });

        case 5:
          _context9.next = 7;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_GLOBALLY_CHANGED,
            newNaIsGloballyChanged: true
          });

        case 7:
          _context9.next = 9;
          return resetProductTypeCkaRaIsValid();

        case 9:
          _context9.next = 13;
          break;

        case 11:
          _context9.prev = 11;
          _context9.t0 = _context9["catch"](0);

        case 13:
        case "end":
          return _context9.stop();
      }
    }
  }, _marked9, this, [[0, 11]]);
}

function validateFE() {
  var profileData, pdaData, dependantProfilesData, feData;
  return regeneratorRuntime.wrap(function validateFE$(_context10) {
    while (1) {
      switch (_context10.prev = _context10.next) {
        case 0:
          _context10.next = 2;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile;
          });

        case 2:
          profileData = _context10.sent;
          _context10.next = 5;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].pda;
          });

        case 5:
          pdaData = _context10.sent;
          _context10.next = 8;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].dependantProfiles;
          });

        case 8:
          dependantProfilesData = _context10.sent;
          _context10.next = 11;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].fe;
          });

        case 11:
          feData = _context10.sent;

          if (!pdaData.isCompleted) {
            _context10.next = 15;
            break;
          }

          _context10.next = 15;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].VALIDATE_FE,
            feData: feData,
            pdaData: pdaData,
            profileData: profileData,
            dependantProfilesData: dependantProfilesData
          });

        case 15:
        case "end":
          return _context10.stop();
      }
    }
  }, _marked10, this);
}

/*
Get the FNA Data from Database
*/
function getFNA(action) {
  var callServer, profileData, dependantProfilesData, clientProfile, needsResponseData, fnaData, pdaData, hasSpouse, pdaError, tiInfo, newNeedsResponseData;
  return regeneratorRuntime.wrap(function getFNA$(_context11) {
    while (1) {
      switch (_context11.prev = _context11.next) {
        case 0:
          _context11.prev = 0;
          _context11.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context11.sent;
          _context11.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile;
          });

        case 6:
          profileData = _context11.sent;
          _context11.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].dependantProfiles;
          });

        case 9:
          dependantProfilesData = _context11.sent;
          _context11.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile;
          });

        case 12:
          clientProfile = _context11.sent;
          _context11.next = 15;
          return (0, _effects.call)(callServer, {
            url: "needs",
            data: {
              action: "initNeeds",
              cid: clientProfile.cid
            }
          });

        case 15:
          needsResponseData = _context11.sent;

          if (!needsResponseData.success) {
            _context11.next = 38;
            break;
          }

          fnaData = (0, _dataMapping.naMapForm)(_.cloneDeep(needsResponseData.fna));
          pdaData = (0, _dataMapping.pdaMapForm)(_.cloneDeep(needsResponseData.pda));
          hasSpouse = false;


          Object.values(dependantProfilesData).forEach(function (arr) {
            arr.dependants.forEach(function (dependant) {
              if (dependant.relationship === "SPO") {
                hasSpouse = true;
              }
            });
          });

          // Prod issue in EASE Web: does not reset pda.applicant after spouse is removed
          pdaError = pdaData.applicant === "joint" && hasSpouse === false;

          if (pdaError) {
            pdaData.isCompleted = false;
            pdaData.lastStepIndex = 0;
          }

          if (!pdaError) {
            _context11.next = 34;
            break;
          }

          tiInfo = _.isEmpty(pdaData.tiInfo) && _.isEqual(pdaData.tiInfo, clientProfile.trustedIndividuals) ? _.cloneDeep(pdaData.tiInfo) : _.cloneDeep(clientProfile.trustedIndividuals);
          _context11.next = 27;
          return (0, _effects.call)(callServer, {
            url: "needs",
            data: {
              action: "savePDA",
              cid: clientProfile.cid,
              pda: pdaData,
              tiInfo: tiInfo,
              confirm: true
            }
          });

        case 27:
          _context11.next = 29;
          return (0, _effects.call)(callServer, {
            url: "needs",
            data: {
              action: "initNeeds",
              cid: clientProfile.cid
            }
          });

        case 29:
          newNeedsResponseData = _context11.sent;
          _context11.next = 32;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_FNA,
            // fna comes from web, should be named na (Needs analysis)
            fnaData: newNeedsResponseData.fna,
            feData: newNeedsResponseData.fe,
            pdaData: newNeedsResponseData.pda,
            profileData: profileData,
            dependantProfilesData: dependantProfilesData,
            newPda: newNeedsResponseData.pda,
            needsSummaryData: newNeedsResponseData.needsSummary
          });

        case 32:
          _context11.next = 36;
          break;

        case 34:
          _context11.next = 36;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_FNA,
            // fna comes from web, should be named na (Needs analysis)
            fnaData: fnaData,
            feData: needsResponseData.fe,
            pdaData: pdaData,
            profileData: profileData,
            dependantProfilesData: dependantProfilesData,
            newPda: pdaData,
            needsSummaryData: needsResponseData.needsSummary
          });

        case 36:
          _context11.next = 38;
          return validateFE();

        case 38:

          if (_.isFunction(action.callback)) {
            action.callback();
          }
          _context11.next = 43;
          break;

        case 41:
          _context11.prev = 41;
          _context11.t0 = _context11["catch"](0);

        case 43:
        case "end":
          return _context11.stop();
      }
    }
  }, _marked11, this, [[0, 41]]);
}

function saveNA(action) {
  var callServer, clientProfile, na, feData, pdaData, confirm, callback, ckaSection, reMappedNa, serverResponse, dependantProfilesData;
  return regeneratorRuntime.wrap(function saveNA$(_context12) {
    while (1) {
      switch (_context12.prev = _context12.next) {
        case 0:
          _context12.prev = 0;
          _context12.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context12.sent;
          _context12.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile;
          });

        case 6:
          clientProfile = _context12.sent;
          _context12.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na;
          });

        case 9:
          na = _context12.sent;
          _context12.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].fe;
          });

        case 12:
          feData = _context12.sent;
          _context12.next = 15;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].pda;
          });

        case 15:
          pdaData = _context12.sent;
          confirm = action.confirm, callback = action.callback;
          ckaSection = (0, _dataMapping.mapCKA)(_.cloneDeep(na.ckaSection ? na.ckaSection : {}));
          reMappedNa = Object.assign({}, na, {
            ckaSection: Object.assign({}, na.ckaSection, ckaSection)
          });
          _context12.next = 21;
          return (0, _effects.call)(callServer, {
            url: "needs",
            data: {
              action: "saveFNA",
              cid: clientProfile.cid,
              fna: reMappedNa,
              confirm: confirm
            }
          });

        case 21:
          serverResponse = _context12.sent;

          if (!(serverResponse && serverResponse.success)) {
            _context12.next = 36;
            break;
          }

          if (!serverResponse.code) {
            _context12.next = 27;
            break;
          }

          callback(serverResponse.code);
          _context12.next = 36;
          break;

        case 27:
          _context12.next = 29;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].dependantProfiles;
          });

        case 29:
          dependantProfilesData = _context12.sent;
          _context12.next = 32;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].SAVE_FNA_COMPLETE,
            feData: feData,
            profileData: serverResponse.profile || clientProfile,
            pdaData: pdaData,
            dependantProfilesData: dependantProfilesData,
            naData: serverResponse.fna,
            newPda: pdaData,
            newIsFnaInvalid: !!serverResponse.showFnaInvalidFlag
          });

        case 32:
          _context12.next = 34;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_CHANGED,
            newNaIsChanged: false
          });

        case 34:
          _context12.next = 36;
          return (0, _effects.call)(callback);

        case 36:
          _context12.next = 40;
          break;

        case 38:
          _context12.prev = 38;
          _context12.t0 = _context12["catch"](0);

        case 40:
        case "end":
          return _context12.stop();
      }
    }
  }, _marked12, this, [[0, 38]]);
}

function updateProductTypesCompletedStep(action) {
  var ckaSection, raSection, value, isCompleted;
  return regeneratorRuntime.wrap(function updateProductTypesCompletedStep$(_context13) {
    while (1) {
      switch (_context13.prev = _context13.next) {
        case 0:
          _context13.prev = 0;
          _context13.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.ckaSection;
          });

        case 3:
          ckaSection = _context13.sent;
          _context13.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na.raSection;
          });

        case 6:
          raSection = _context13.sent;
          value = action.value;
          isCompleted = false;
          _context13.next = 11;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_PRODUCT_TYPES,
            value: value
          });

        case 11:
          _context13.next = 13;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_COMPLETED_STEP,
            value: value
          });

        case 13:

          if (value !== "" && (value.indexOf("investLinkedPlans") === -1 || value.indexOf("investLinkedPlans") !== -1 && ckaSection.isValid) && (value.indexOf("investLinkedPlans") === -1 && value.indexOf("participatingPlans") === -1 || (value.indexOf("investLinkedPlans") !== -1 || value.indexOf("participatingPlans") !== -1) && raSection.isValid)

          // (prodTypeArr.indexOf(INVESTLINKEDPLANS) > -1 &&
          //   (!ckaSection.isValid || !raSection.isValid)) ||
          // (prodTypeArr.indexOf(PARTICIPATINGPLANS) > -1 && !raSection.isValid)
          ) {
              isCompleted = true;
            }

          _context13.next = 16;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_IS_COMPLETED,
            isCompleted: isCompleted
          });

        case 16:
          _context13.next = 20;
          break;

        case 18:
          _context13.prev = 18;
          _context13.t0 = _context13["catch"](0);

        case 20:
        case "end":
          return _context13.stop();
      }
    }
  }, _marked13, this, [[0, 18]]);
}

function initPDA(action) {
  var callServer, clientProfile, serverResponse;
  return regeneratorRuntime.wrap(function initPDA$(_context14) {
    while (1) {
      switch (_context14.prev = _context14.next) {
        case 0:
          _context14.prev = 0;
          _context14.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context14.sent;
          _context14.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile;
          });

        case 6:
          clientProfile = _context14.sent;
          _context14.next = 9;
          return (0, _effects.call)(callServer, {
            url: "needs",
            data: {
              action: "initNeeds",
              cid: clientProfile.cid
            }
          });

        case 9:
          serverResponse = _context14.sent;
          _context14.next = 12;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_PDA,
            // TOBE remove null
            newPda: serverResponse.pda
          });

        case 12:

          action.callback();
          _context14.next = 17;
          break;

        case 15:
          _context14.prev = 15;
          _context14.t0 = _context14["catch"](0);

        case 17:
        case "end":
          return _context14.stop();
      }
    }
  }, _marked14, this, [[0, 15]]);
}

function updateLastSelectedProduct(action) {
  return regeneratorRuntime.wrap(function updateLastSelectedProduct$(_context15) {
    while (1) {
      switch (_context15.prev = _context15.next) {
        case 0:
          _context15.prev = 0;
          _context15.next = 3;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_LAST_SELECTED_PRODUCT,
            value: action.lastSelectedProduct
          });

        case 3:
          _context15.next = 7;
          break;

        case 5:
          _context15.prev = 5;
          _context15.t0 = _context15["catch"](0);

        case 7:
        case "end":
          return _context15.stop();
      }
    }
  }, _marked15, this, [[0, 5]]);
}

function initProductTypesSaga() {
  var na, needsAspect, prodType;
  return regeneratorRuntime.wrap(function initProductTypesSaga$(_context16) {
    while (1) {
      switch (_context16.prev = _context16.next) {
        case 0:
          _context16.next = 2;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na;
          });

        case 2:
          na = _context16.sent;
          needsAspect = na.aspects.split(",");
          prodType = na.productType.prodType;

          if (!(
          // There is default selected Product Type
          needsAspect.indexOf(_NEEDS.CIPROTECTION) > -1 || needsAspect.indexOf(_NEEDS.PCHEADSTART) > -1 || na.aspects === _NEEDS.PAPROTECTION || na.aspects === _NEEDS.HCPROTECTION || _.isEqual(needsAspect.sort(), [_NEEDS.PAPROTECTION, _NEEDS.HCPROTECTION].sort()) || needsAspect.indexOf(_NEEDS.PAPROTECTION) > -1 || needsAspect.indexOf(_NEEDS.HCPROTECTION) > -1)) {
            _context16.next = 8;
            break;
          }

          _context16.next = 8;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_PRODUCT_TYPES,
            value: prodType
          });

        case 8:
          _context16.next = 10;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_COMPLETED_STEP,
            value: prodType
          });

        case 10:
        case "end":
          return _context16.stop();
      }
    }
  }, _marked16, this);
}

function validateNA(action) {
  var dependantProfilesData, profileData, naData, feData;
  return regeneratorRuntime.wrap(function validateNA$(_context17) {
    while (1) {
      switch (_context17.prev = _context17.next) {
        case 0:
          _context17.next = 2;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].dependantProfiles;
          });

        case 2:
          dependantProfilesData = _context17.sent;
          _context17.next = 5;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile;
          });

        case 5:
          profileData = _context17.sent;
          _context17.next = 8;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na;
          });

        case 8:
          naData = _context17.sent;
          _context17.next = 11;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].fe;
          });

        case 11:
          feData = _context17.sent;

          if (!feData.isCompleted) {
            _context17.next = 22;
            break;
          }

          _context17.next = 15;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].VALIDATE_NA,
            naData: naData,
            profileData: profileData,
            dependantProfilesData: dependantProfilesData,
            feData: feData
          });

        case 15:
          _context17.next = 17;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_CHANGED,
            newNaIsChanged: true
          });

        case 17:
          _context17.next = 19;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_GLOBALLY_CHANGED,
            newNaIsGloballyChanged: true
          });

        case 19:
          if (!(action.resetProdType !== false)) {
            _context17.next = 22;
            break;
          }

          _context17.next = 22;
          return resetProductTypeCkaRaIsValid();

        case 22:
        case "end":
          return _context17.stop();
      }
    }
  }, _marked17, this);
}

function updateNAValidStatus() {
  var isNaErrorData, naData;
  return regeneratorRuntime.wrap(function updateNAValidStatus$(_context18) {
    while (1) {
      switch (_context18.prev = _context18.next) {
        case 0:
          _context18.next = 2;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].isNaError;
          });

        case 2:
          isNaErrorData = _context18.sent;
          _context18.next = 5;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na;
          });

        case 5:
          naData = _context18.sent;
          _context18.next = 8;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_VALID_STATUS,
            isNaErrorData: isNaErrorData,
            naData: naData
          });

        case 8:
        case "end":
          return _context18.stop();
      }
    }
  }, _marked18, this);
}

function saveFE(action) {
  var callServer, clientProfile, dependantProfilesData, cid, pdaData, feData, na, feErrors, confirm, callback, hasError, response;
  return regeneratorRuntime.wrap(function saveFE$(_context19) {
    while (1) {
      switch (_context19.prev = _context19.next) {
        case 0:
          _context19.prev = 0;
          _context19.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context19.sent;
          _context19.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile;
          });

        case 6:
          clientProfile = _context19.sent;
          _context19.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].dependantProfiles;
          });

        case 9:
          dependantProfilesData = _context19.sent;
          _context19.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile.cid;
          });

        case 12:
          cid = _context19.sent;
          _context19.next = 15;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].pda;
          });

        case 15:
          pdaData = _context19.sent;
          _context19.next = 18;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].fe;
          });

        case 18:
          feData = _context19.sent;
          _context19.next = 21;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].na;
          });

        case 21:
          na = _context19.sent;
          _context19.next = 24;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].feErrors;
          });

        case 24:
          feErrors = _context19.sent;
          confirm = action.confirm, callback = action.callback;

          // get isCompleted

          hasError = false;

          Object.keys(feErrors).forEach(function (clientCid) {
            Object.keys(feErrors[clientCid]).forEach(function (key) {
              hasError = hasError || feErrors[clientCid][key].hasError;
            });
          });

          _context19.next = 30;
          return (0, _effects.call)(callServer, {
            url: "needs",
            data: {
              action: "saveFE",
              cid: cid,
              fe: Object.assign({}, feData, { isCompleted: !hasError }),
              confirm: confirm
            }
          });

        case 30:
          response = _context19.sent;

          if (!response.success) {
            _context19.next = 41;
            break;
          }

          if (!response.code) {
            _context19.next = 36;
            break;
          }

          callback(response.code);
          _context19.next = 41;
          break;

        case 36:
          _context19.next = 38;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].SAVE_FNA_COMPLETE,
            feData: response.fe,
            profileData: response.profile || clientProfile,
            pdaData: pdaData,
            dependantProfilesData: dependantProfilesData,
            naData: response.fna || na,
            newPda: pdaData,
            newIsFnaInvalid: !!response.showFnaInvalidFlag
          });

        case 38:
          _context19.next = 40;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_COMPLETED_STEP,
            completedStep: -1
          });

        case 40:

          callback();

        case 41:
          _context19.next = 45;
          break;

        case 43:
          _context19.prev = 43;
          _context19.t0 = _context19["catch"](0);

        case 45:
        case "end":
          return _context19.stop();
      }
    }
  }, _marked19, this, [[0, 43]]);
}

function loadFNAReport(action) {
  var callServer, profile, response;
  return regeneratorRuntime.wrap(function loadFNAReport$(_context20) {
    while (1) {
      switch (_context20.prev = _context20.next) {
        case 0:
          _context20.prev = 0;
          _context20.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context20.sent;
          _context20.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile;
          });

        case 6:
          profile = _context20.sent;
          _context20.next = 9;
          return (0, _effects.call)(callServer, {
            url: "needs",
            data: {
              action: "generateFNAReport",
              cid: profile.cid,
              profile: profile
            }
          });

        case 9:
          response = _context20.sent;

          if (!response.success) {
            _context20.next = 14;
            break;
          }

          _context20.next = 13;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].SAVE_FNA_REPORT,
            FNAReportData: response.fnaReport
          });

        case 13:

          if (_.isFunction(action.callback)) {
            action.callback();
          }

        case 14:
          _context20.next = 18;
          break;

        case 16:
          _context20.prev = 16;
          _context20.t0 = _context20["catch"](0);

        case 18:
        case "end":
          return _context20.stop();
      }
    }
  }, _marked20, this, [[0, 16]]);
}

function openEmailDialog(action) {
  var callServer, profile, agent, response, agentContent, clientContent, agentSubject, clientSubject, embedImgs, emails;
  return regeneratorRuntime.wrap(function openEmailDialog$(_context21) {
    while (1) {
      switch (_context21.prev = _context21.next) {
        case 0:
          _context21.prev = 0;
          _context21.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context21.sent;
          _context21.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile;
          });

        case 6:
          profile = _context21.sent;
          _context21.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.AGENT];
          });

        case 9:
          agent = _context21.sent;
          _context21.next = 12;
          return (0, _effects.call)(callServer, {
            url: "needs",
            data: {
              action: "getFNAEmailTemplate"
            }
          });

        case 12:
          response = _context21.sent;

          if (!(response && response.success)) {
            _context21.next = 28;
            break;
          }

          agentContent = response.agentContent, clientContent = response.clientContent;
          agentSubject = response.agentSubject, clientSubject = response.clientSubject, embedImgs = response.embedImgs;


          agentContent = (0, _common.replaceAll)(agentContent, "{{agentName}}", agent.name);
          agentContent = (0, _common.replaceAll)(agentContent, "{{agentContactNum}}", agent.mobile);
          agentContent = (0, _common.replaceAll)(agentContent, "{{recipientName}}", agent.name);
          agentContent = (0, _common.replaceAll)(agentContent, '<img src="cid:axaLogo">', "");

          clientContent = (0, _common.replaceAll)(clientContent, "{{agentName}}", agent.name);
          clientContent = (0, _common.replaceAll)(clientContent, "{{agentContactNum}}", agent.mobile);
          clientContent = (0, _common.replaceAll)(clientContent, "{{recipientName}}", profile.fullName);
          clientContent = (0, _common.replaceAll)(clientContent, '<img src="cid:axaLogo">', "");

          emails = [{
            id: "agent",
            agentCode: agent.agentCode,
            dob: null,
            name: "Agent",
            to: [agent.email],
            title: agentSubject,
            content: agentContent,
            embedImgs: embedImgs
          }, {
            id: "client",
            agentCode: null,
            dob: profile.dob,
            name: "Client",
            to: [profile.email],
            title: clientSubject,
            content: clientContent,
            embedImgs: embedImgs
          }];
          _context21.next = 27;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].OPEN_EMAIL,
            emails: emails
          });

        case 27:

          if (_.isFunction(action.callback)) {
            action.callback();
          }

        case 28:
          _context21.next = 32;
          break;

        case 30:
          _context21.prev = 30;
          _context21.t0 = _context21["catch"](0);

        case 32:
        case "end":
          return _context21.stop();
      }
    }
  }, _marked21, this, [[0, 30]]);
}

function sendEmail(action) {
  var callServer, profile, agent, emails, reportOptions, skipEmailInReport, authToken, webServiceUrl, newEmail, response;
  return regeneratorRuntime.wrap(function sendEmail$(_context22) {
    while (1) {
      switch (_context22.prev = _context22.next) {
        case 0:
          _context22.prev = 0;
          _context22.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context22.sent;
          _context22.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile;
          });

        case 6:
          profile = _context22.sent;
          _context22.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.AGENT];
          });

        case 9:
          agent = _context22.sent;
          _context22.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].FNAReportEmail.emails;
          });

        case 12:
          emails = _context22.sent;
          _context22.next = 15;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].FNAReportEmail.reportOptions;
          });

        case 15:
          reportOptions = _context22.sent;
          _context22.next = 18;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].FNAReportEmail.skipEmailInReport;
          });

        case 18:
          skipEmailInReport = _context22.sent;
          _context22.next = 21;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.LOGIN].authToken;
          });

        case 21:
          authToken = _context22.sent;
          _context22.next = 24;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.LOGIN].webServiceUrl;
          });

        case 24:
          webServiceUrl = _context22.sent;
          newEmail = [];


          Object.keys(reportOptions).forEach(function (key) {
            if (reportOptions[key].fnaReport === true) {
              newEmail.push(emails.find(function (x) {
                return x.id === key;
              }));
            }
          });
          _.forEach(newEmail, function (email) {
            email.content += "<img src='cid:axaLogo'>";
          });
          _context22.next = 30;
          return (0, _effects.call)(callServer, {
            url: "needs",
            data: {
              action: "emailFnaReport",
              emails: newEmail,
              profile: profile,
              agentProfile: agent,
              skipEmailInReport: skipEmailInReport,
              authToken: authToken,
              webServiceUrl: webServiceUrl
            }
          });

        case 30:
          response = _context22.sent;


          if (response && response.success && _.isFunction(action.callback)) {
            action.callback();

            // TODO: will close email in coming version
            // yield put({
            //   type: ACTION_TYPES[FNA].CLOSE_EMAIL
            // });
          }
          _context22.next = 36;
          break;

        case 34:
          _context22.prev = 34;
          _context22.t0 = _context22["catch"](0);

        case 36:
        case "end":
          return _context22.stop();
      }
    }
  }, _marked22, this, [[0, 34]]);
}

function validateGivenName() {
  var givenNameData;
  return regeneratorRuntime.wrap(function validateGivenName$(_context23) {
    while (1) {
      switch (_context23.prev = _context23.next) {
        case 0:
          _context23.prev = 0;
          _context23.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].givenName;
          });

        case 3:
          givenNameData = _context23.sent;
          _context23.next = 6;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].VALIDATE_TRUSTED_INDIVIDUAL_GIVEN_NAME,
            givenNameData: givenNameData
          });

        case 6:
          _context23.next = 10;
          break;

        case 8:
          _context23.prev = 8;
          _context23.t0 = _context23["catch"](0);

        case 10:
        case "end":
          return _context23.stop();
      }
    }
  }, _marked23, this, [[0, 8]]);
}

function validateSurname() {
  var surnameData;
  return regeneratorRuntime.wrap(function validateSurname$(_context24) {
    while (1) {
      switch (_context24.prev = _context24.next) {
        case 0:
          _context24.prev = 0;
          _context24.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].surname;
          });

        case 3:
          surnameData = _context24.sent;
          _context24.next = 6;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].VALIDATE_TRUSTED_INDIVIDUAL_SURNAME,
            surnameData: surnameData
          });

        case 6:
          _context24.next = 10;
          break;

        case 8:
          _context24.prev = 8;
          _context24.t0 = _context24["catch"](0);

        case 10:
        case "end":
          return _context24.stop();
      }
    }
  }, _marked24, this, [[0, 8]]);
}

function validateName() {
  var nameData;
  return regeneratorRuntime.wrap(function validateName$(_context25) {
    while (1) {
      switch (_context25.prev = _context25.next) {
        case 0:
          _context25.prev = 0;
          _context25.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].name;
          });

        case 3:
          nameData = _context25.sent;
          _context25.next = 6;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].VALIDATE_TRUSTED_INDIVIDUAL_NAME,
            nameData: nameData
          });

        case 6:
          _context25.next = 10;
          break;

        case 8:
          _context25.prev = 8;
          _context25.t0 = _context25["catch"](0);

        case 10:
        case "end":
          return _context25.stop();
      }
    }
  }, _marked25, this, [[0, 8]]);
}

function validateIDDocumentType() {
  var IDDocumentTypeData;
  return regeneratorRuntime.wrap(function validateIDDocumentType$(_context26) {
    while (1) {
      switch (_context26.prev = _context26.next) {
        case 0:
          _context26.prev = 0;
          _context26.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].IDDocumentType;
          });

        case 3:
          IDDocumentTypeData = _context26.sent;
          _context26.next = 6;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].VALIDATE_TRUSTED_INDIVIDUAL_ID_DOCUMENT_TYPE,
            IDDocumentTypeData: IDDocumentTypeData
          });

        case 6:
          _context26.next = 10;
          break;

        case 8:
          _context26.prev = 8;
          _context26.t0 = _context26["catch"](0);

        case 10:
        case "end":
          return _context26.stop();
      }
    }
  }, _marked26, this, [[0, 8]]);
}

function validateIDDocumentTypeOther() {
  var IDDocumentTypeOtherData, IDDocumentTypeData;
  return regeneratorRuntime.wrap(function validateIDDocumentTypeOther$(_context27) {
    while (1) {
      switch (_context27.prev = _context27.next) {
        case 0:
          _context27.prev = 0;
          _context27.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].IDDocumentTypeOther;
          });

        case 3:
          IDDocumentTypeOtherData = _context27.sent;
          _context27.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].IDDocumentType;
          });

        case 6:
          IDDocumentTypeData = _context27.sent;
          _context27.next = 9;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].VALIDATE_TRUSTED_ID_DOCUMENT_TYPE_OTHER,
            IDDocumentTypeOtherData: IDDocumentTypeOtherData,
            IDDocumentTypeData: IDDocumentTypeData
          });

        case 9:
          _context27.next = 13;
          break;

        case 11:
          _context27.prev = 11;
          _context27.t0 = _context27["catch"](0);

        case 13:
        case "end":
          return _context27.stop();
      }
    }
  }, _marked27, this, [[0, 11]]);
}

function validateID() {
  var IDData, IDDocumentTypeData;
  return regeneratorRuntime.wrap(function validateID$(_context28) {
    while (1) {
      switch (_context28.prev = _context28.next) {
        case 0:
          _context28.prev = 0;
          _context28.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].ID;
          });

        case 3:
          IDData = _context28.sent;
          _context28.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].IDDocumentType;
          });

        case 6:
          IDDocumentTypeData = _context28.sent;
          _context28.next = 9;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].VALIDATE_TRUSTED_INDIVIDUAL_ID,
            IDData: IDData,
            IDDocumentTypeData: IDDocumentTypeData
          });

        case 9:
          _context28.next = 13;
          break;

        case 11:
          _context28.prev = 11;
          _context28.t0 = _context28["catch"](0);

        case 13:
        case "end":
          return _context28.stop();
      }
    }
  }, _marked28, this, [[0, 11]]);
}

function validateRelationship() {
  var relationshipData;
  return regeneratorRuntime.wrap(function validateRelationship$(_context29) {
    while (1) {
      switch (_context29.prev = _context29.next) {
        case 0:
          _context29.prev = 0;
          _context29.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].relationship;
          });

        case 3:
          relationshipData = _context29.sent;
          _context29.next = 6;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].VALIDATE_TRUSTED_INDIVIDUAL_RELATIONSHIP,
            relationshipData: relationshipData
          });

        case 6:
          _context29.next = 10;
          break;

        case 8:
          _context29.prev = 8;
          _context29.t0 = _context29["catch"](0);

        case 10:
        case "end":
          return _context29.stop();
      }
    }
  }, _marked29, this, [[0, 8]]);
}

function relationShipOtherSaga() {
  var relationshipData, relationshipOtherData;
  return regeneratorRuntime.wrap(function relationShipOtherSaga$(_context30) {
    while (1) {
      switch (_context30.prev = _context30.next) {
        case 0:
          _context30.prev = 0;
          _context30.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].relationship;
          });

        case 3:
          relationshipData = _context30.sent;
          _context30.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].relationshipOther;
          });

        case 6:
          relationshipOtherData = _context30.sent;
          _context30.next = 9;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].VALIDATE_TRUSTED_INDIVIDUAL_RELATIONSHIP_OTHER,
            relationshipOtherData: relationshipOtherData,
            relationshipData: relationshipData
          });

        case 9:
          _context30.next = 13;
          break;

        case 11:
          _context30.prev = 11;
          _context30.t0 = _context30["catch"](0);

        case 13:
        case "end":
          return _context30.stop();
      }
    }
  }, _marked30, this, [[0, 11]]);
}

function validatePrefix() {
  var prefixData;
  return regeneratorRuntime.wrap(function validatePrefix$(_context31) {
    while (1) {
      switch (_context31.prev = _context31.next) {
        case 0:
          _context31.prev = 0;
          _context31.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].prefix;
          });

        case 3:
          prefixData = _context31.sent;
          _context31.next = 6;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].VALIDATE_TRUSTED_INDIVIDUAL_PREFIX,
            prefixData: prefixData
          });

        case 6:
          _context31.next = 10;
          break;

        case 8:
          _context31.prev = 8;
          _context31.t0 = _context31["catch"](0);

        case 10:
        case "end":
          return _context31.stop();
      }
    }
  }, _marked31, this, [[0, 8]]);
}

function validateMobileNo() {
  var mobileNoData;
  return regeneratorRuntime.wrap(function validateMobileNo$(_context32) {
    while (1) {
      switch (_context32.prev = _context32.next) {
        case 0:
          _context32.prev = 0;
          _context32.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].mobileNo;
          });

        case 3:
          mobileNoData = _context32.sent;
          _context32.next = 6;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].VALIDATE_TRUSTED_INDIVIDUAL_MOBILE_NO,
            mobileNoData: mobileNoData
          });

        case 6:
          _context32.next = 10;
          break;

        case 8:
          _context32.prev = 8;
          _context32.t0 = _context32["catch"](0);

        case 10:
        case "end":
          return _context32.stop();
      }
    }
  }, _marked32, this, [[0, 8]]);
}

function initialValidateTrustedIndividualSaga(action) {
  var fna;
  return regeneratorRuntime.wrap(function initialValidateTrustedIndividualSaga$(_context33) {
    while (1) {
      switch (_context33.prev = _context33.next) {
        case 0:
          _context33.prev = 0;
          _context33.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA];
          });

        case 3:
          fna = _context33.sent;
          _context33.next = 6;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].INITIAL_VALIDATE,
            surnameData: fna.surname,
            nameData: fna.name,
            relationshipData: fna.relationship,
            givenNameData: fna.givenName,
            IDData: fna.ID,
            IDDocumentTypeData: fna.IDDocumentType,
            prefixData: fna.prefix,
            mobileNoData: fna.mobileNo,
            relationshipOtherData: fna.relationshipOther
          });

        case 6:
          _context33.next = 8;
          return (0, _effects.call)(action.callback);

        case 8:
          _context33.next = 12;
          break;

        case 10:
          _context33.prev = 10;
          _context33.t0 = _context33["catch"](0);

        case 12:
        case "end":
          return _context33.stop();
      }
    }
  }, _marked33, this, [[0, 10]]);
}

function updatePDA(action) {
  var callServer, clientProfile, serverResponse;
  return regeneratorRuntime.wrap(function updatePDA$(_context34) {
    while (1) {
      switch (_context34.prev = _context34.next) {
        case 0:
          _context34.prev = 0;
          _context34.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context34.sent;
          _context34.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile;
          });

        case 6:
          clientProfile = _context34.sent;
          _context34.next = 9;
          return (0, _effects.call)(callServer, {
            url: "needs",
            data: {
              action: "initNeeds",
              cid: clientProfile.cid
            }
          });

        case 9:
          serverResponse = _context34.sent;
          _context34.next = 12;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_PDA,
            newPda: serverResponse.pda
          });

        case 12:

          action.callback();
          _context34.next = 17;
          break;

        case 15:
          _context34.prev = 15;
          _context34.t0 = _context34["catch"](0);

        case 17:
        case "end":
          return _context34.stop();
      }
    }
  }, _marked34, this, [[0, 15]]);
}

function savePDA(action) {
  var callServer, clientProfile, pda, fe, fna, tiInfo, confirm, callback, serverResponse, dependantProfilesData;
  return regeneratorRuntime.wrap(function savePDA$(_context35) {
    while (1) {
      switch (_context35.prev = _context35.next) {
        case 0:
          _context35.prev = 0;
          _context35.next = 3;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CONFIG].callServer;
          });

        case 3:
          callServer = _context35.sent;
          _context35.next = 6;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].profile;
          });

        case 6:
          clientProfile = _context35.sent;
          _context35.next = 9;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].pda;
          });

        case 9:
          pda = _context35.sent;
          _context35.next = 12;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].fe;
          });

        case 12:
          fe = _context35.sent;
          _context35.next = 15;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.FNA].fna;
          });

        case 15:
          fna = _context35.sent;
          tiInfo = _.isEmpty(pda.tiInfo) && _.isEqual(pda.tiInfo, clientProfile.trustedIndividuals) ? _.cloneDeep(pda.tiInfo) : _.cloneDeep(clientProfile.trustedIndividuals);
          confirm = action.confirm, callback = action.callback;
          _context35.next = 20;
          return (0, _effects.call)(callServer, {
            url: "needs",
            data: {
              action: "savePDA",
              cid: clientProfile.cid,
              pda: pda,
              tiInfo: tiInfo,
              confirm: confirm
            }
          });

        case 20:
          serverResponse = _context35.sent;

          if (!(serverResponse && serverResponse.success)) {
            _context35.next = 29;
            break;
          }

          _context35.next = 24;
          return (0, _effects.select)(function (state) {
            return state[_REDUCER_TYPES.CLIENT].dependantProfiles;
          });

        case 24:
          dependantProfilesData = _context35.sent;
          _context35.next = 27;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].SAVE_FNA_COMPLETE,
            feData: serverResponse.fe || fe,
            profileData: serverResponse.profile || clientProfile,
            pdaData: serverResponse.pda,
            dependantProfilesData: dependantProfilesData,
            naData: serverResponse.fna || fna,
            newPda: serverResponse.pda,
            newIsFnaInvalid: !!serverResponse.showFnaInvalidFlag
          });

        case 27:
          _context35.next = 29;
          return (0, _effects.put)({
            type: _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_COMPLETED_STEP,
            completedStep: -1
          });

        case 29:
          if (!(serverResponse && serverResponse.code)) {
            _context35.next = 34;
            break;
          }

          _context35.next = 32;
          return (0, _effects.call)(callback, serverResponse.code);

        case 32:
          _context35.next = 36;
          break;

        case 34:
          _context35.next = 36;
          return (0, _effects.call)(callback);

        case 36:
          _context35.next = 40;
          break;

        case 38:
          _context35.prev = 38;
          _context35.t0 = _context35["catch"](0);

        case 40:
        case "end":
          return _context35.stop();
      }
    }
  }, _marked35, this, [[0, 38]]);
}