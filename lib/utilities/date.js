"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAttainedAge = getAttainedAge;
exports.getNearestAge = getNearestAge;
exports.getAgeNextBirthday = getAgeNextBirthday;
function getAttainedAge(targetDate, dob) {
  var birthDate = new Date(dob.getFullYear(), dob.getMonth(), dob.getDate());
  var tDate = new Date(targetDate.getFullYear(), targetDate.getMonth(), targetDate.getDate());

  var age = tDate.getFullYear() - birthDate.getFullYear();
  var m = tDate.getMonth() - birthDate.getMonth();
  var d = tDate.getDate() - birthDate.getDate();
  var monthNum = age * 12 + tDate.getMonth() - birthDate.getMonth();
  var dayNum = Math.round((tDate.getTime() - birthDate.getTime()) / (24 * 3600000));
  if (m < 0 || m === 0 && d < 0) {
    age -= 1;
  }
  if (d < 0) {
    monthNum -= 1;
  }
  return {
    year: age,
    month: monthNum,
    day: dayNum
  };
}

/**
 * Get the nearest age on a specific date.
 * e.g. attained age: 19.2 -> nearest age 19
 *      attained age: 19.8 -> nearest age 20
 *
 * @param {Date} targetDate target date, will only use the date part
 * @param {Date} dob date of birth
 */
function getNearestAge(targetDate, dob) {
  var tYr = targetDate.getFullYear();
  var tDate = new Date(tYr, targetDate.getMonth(), targetDate.getDate());

  var dobYr = dob.getFullYear();
  var dobMonth = dob.getMonth();
  var dobDay = dob.getDate();

  var tyBirthday = new Date(tYr, dobMonth, dobDay);
  var prevDob = tDate - tyBirthday >= 0 ? tyBirthday : new Date(tYr - 1, dobMonth, dobDay);
  var nextDob = tDate - tyBirthday < 0 ? tyBirthday : new Date(tYr + 1, dobMonth, dobDay);

  var lastDiff = Math.abs(tDate - prevDob);
  var nextDiff = Math.abs(tDate - nextDob);
  var finalDob = lastDiff > nextDiff ? nextDob : prevDob;

  return finalDob.getFullYear() - dobYr;
}

/**
 * Get the age on next birthday for a specific date.
 * e.g. attained age: 19.2 -> nearest age 20
 *      attained age: 19.8 -> nearest age 20
 *
 * @param {Date} targetDate target date, will only use the date part
 * @param {Date} dob date of birth
 */
function getAgeNextBirthday(targetDate, dob) {
  var tYr = targetDate.getFullYear();
  var tDate = new Date(tYr, targetDate.getMonth(), targetDate.getDate());

  var dobYr = dob.getFullYear();
  var dobMonth = dob.getMonth();
  var dobDay = dob.getDate();

  var tyBirthday = new Date(tYr, dobMonth, dobDay);
  var nextDob = tDate - tyBirthday < 0 ? tyBirthday : new Date(tYr + 1, dobMonth, dobDay);

  return nextDob.getFullYear() - dobYr;
}