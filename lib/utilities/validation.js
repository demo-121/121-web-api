"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.validateROPTable = undefined;
exports.validateID = validateID;
exports.validateEmail = validateEmail;
exports.validateText = validateText;
exports.validateBankNumber = validateBankNumber;
exports.validateMandatory = validateMandatory;
exports.validateAssets = validateAssets;
exports.validateNeedsAnalysis = validateNeedsAnalysis;
exports.validateRecommendationROP = validateRecommendationROP;
exports.validateBudgetChoice = validateBudgetChoice;
exports.validateNetWorth = validateNetWorth;
exports.validateEIP = validateEIP;
exports.validateCashFlow = validateCashFlow;
exports.validateBudgetAndForceIncome = validateBudgetAndForceIncome;
exports.validateApplicationFormPolicyTable = validateApplicationFormPolicyTable;
exports.validateApplicationFormTemplate = validateApplicationFormTemplate;
exports.validateApplicationFormTableDialog = validateApplicationFormTableDialog;
exports.validateApplicationInitPaymentMethod = validateApplicationInitPaymentMethod;
exports.isClientFormHasError = isClientFormHasError;

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _locales = require("../locales");

var _locales2 = _interopRequireDefault(_locales);

var _FIELD_TYPES = require("../constants/FIELD_TYPES");

var _PAYMENT_BANK = require("../constants/PAYMENT_BANK");

var _PAYMENT_BANK2 = _interopRequireDefault(_PAYMENT_BANK);

var _trigger = require("./trigger");

var _ROP_TABLE = require("../constants/ROP_TABLE");

var ROP_TABLE = _interopRequireWildcard(_ROP_TABLE);

var _DYNAMIC_VIEWS = require("../constants/DYNAMIC_VIEWS");

var DYNAMIC_VIEWS = _interopRequireWildcard(_DYNAMIC_VIEWS);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function validateID(_ref) {
  var _message5;

  var ID = _ref.ID,
      IDDocumentType = _ref.IDDocumentType;

  function validateFunction(checkArray) {
    var IDCopy = ID;

    /* check length */
    if (IDCopy.length !== 9) {
      return false;
    }

    /* check format */
    var icArray = [];
    var supportArray = [2, 7, 6, 5, 4, 3, 2];

    for (var i = 0; i < 9; i += 1) {
      icArray[i] = IDCopy.charAt(i);
    }

    for (var _i = 1; _i < 8; _i += 1) {
      icArray[_i] = parseInt(icArray[_i], 10) * supportArray[_i - 1];
    }

    /* get weight */
    var weight = 0;
    for (var _i2 = 1; _i2 < 8; _i2 += 1) {
      weight += icArray[_i2];
    }

    var offset = icArray[0] === "T" || icArray[0] === "G" ? 4 : 0;
    var temp = (offset + weight) % 11;

    var theAlpha = checkArray[temp];
    if (icArray[8] !== theAlpha) {
      return false;
    }

    /* all check pass */
    return true;
  }

  if (ID !== "") {
    var supportArray = [];
    switch (IDDocumentType) {
      case "nric":
        {
          var _message, _message2;

          if (ID[0] === "S" || ID[0] === "T") {
            supportArray = ["J", "Z", "I", "H", "G", "F", "E", "D", "C", "B", "A"];
          }
          return validateFunction(supportArray) ? {
            hasError: false,
            message: (_message = {}, _defineProperty(_message, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message)
          } : {
            hasError: true,
            message: (_message2 = {}, _defineProperty(_message2, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.604"]), _defineProperty(_message2, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message2)
          };
        }
      case "fin":
        {
          var _message3, _message4;

          if (ID[0] === "F" || ID[0] === "G") {
            supportArray = ["X", "W", "U", "T", "R", "Q", "P", "N", "M", "L", "K"];
          }
          return validateFunction(supportArray) ? {
            hasError: false,
            message: (_message3 = {}, _defineProperty(_message3, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message3, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message3)
          } : {
            hasError: true,
            message: (_message4 = {}, _defineProperty(_message4, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.606"]), _defineProperty(_message4, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message4)
          };
        }
      default:
        break;
    }
  }
  return {
    hasError: false,
    message: (_message5 = {}, _defineProperty(_message5, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message5, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message5)
  };
}

function validateEmail(email) {
  var _message6, _message7;

  return (/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/.test(email) ? {
      hasError: false,
      message: (_message6 = {}, _defineProperty(_message6, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message6, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message6)
    } : {
      hasError: true,
      message: (_message7 = {}, _defineProperty(_message7, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.401"]), _defineProperty(_message7, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message7)
    }
  );
}

/**
 * validateText
 * @description validate TextField / TextBox
 * @param cousMessage a string with message code stroed in TEXT_STORE
 * */
function validateText(_ref2) {
  var _message12;

  var field = _ref2.field,
      value = _ref2.value;
  var min = field.min,
      max = field.max;

  if (_.isNumber(min)) {
    if (_.isString(value) && _.toLength(value) < min) {
      var _message8;

      return {
        hasError: true,
        message: (_message8 = {}, _defineProperty(_message8, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.407"]), _defineProperty(_message8, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message8)
      };
    } else if (_.isNumber(value) && value < min) {
      var _message9;

      return {
        hasError: true,
        message: (_message9 = {}, _defineProperty(_message9, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.405"]), _defineProperty(_message9, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message9)
      };
    }
  }

  if (_.isNumber(max)) {
    if (_.isString(value) && _.toLength(value) > max) {
      var _message10;

      return {
        hasError: true,
        message: (_message10 = {}, _defineProperty(_message10, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.406"]), _defineProperty(_message10, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message10)
      };
    } else if (_.isNumber(value) && value > max) {
      var _message11;

      return {
        hasError: true,
        message: (_message11 = {}, _defineProperty(_message11, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.404"]), _defineProperty(_message11, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message11)
      };
    }
  }

  return {
    hasError: false,
    message: (_message12 = {}, _defineProperty(_message12, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message12, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message12)
  };
}

/**
 * validateBankNumber
 * @description validate validateBankNumber
 * @param cousMessage a string with message code stroed in TEXT_STORE
 * */
function validateBankNumber(_ref3) {
  var _message15;

  var field = _ref3.field,
      value = _ref3.value;
  var bankName = field.bankName;

  var max = _PAYMENT_BANK2.default[bankName] ? _PAYMENT_BANK2.default[bankName].maxLength : 0;
  if (field.cpf) {
    max = _PAYMENT_BANK2.default[bankName] ? _PAYMENT_BANK2.default[bankName].cpfMaxLength : 0;
  }

  if (_.isNumber(max)) {
    var path = "error.302";
    switch (max) {
      case 14:
        path = "paymentAndSubmission.section.payment.placeholder.accountNumberLength14";
        break;
      case 13:
        path = "paymentAndSubmission.section.payment.placeholder.accountNumberLength13";
        break;
      case 12:
        path = "paymentAndSubmission.section.payment.placeholder.accountNumberLength12";
        break;
      case 9:
        path = "paymentAndSubmission.section.payment.placeholder.accountNumberLength9";
        break;
      default:
        path = "error.302";
    }
    if (value.toString().length === 0) {
      var _message13;

      return {
        hasError: true,
        message: (_message13 = {}, _defineProperty(_message13, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.302"]), _defineProperty(_message13, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message13)
      };
    } else if (value.toString().length !== max) {
      var _message14;

      return {
        hasError: true,
        message: (_message14 = {}, _defineProperty(_message14, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH][path]), _defineProperty(_message14, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message14)
      };
    }
  }

  return {
    hasError: false,
    message: (_message15 = {}, _defineProperty(_message15, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message15, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message15)
  };
}

/**
 * validateMandatory
 * @description validate mandatory for component
 * */
function validateMandatory(_ref4) {
  var _message17;

  var field = _ref4.field,
      value = _ref4.value;
  var type = field.type,
      mandatory = field.mandatory,
      disabled = field.disabled,
      numberAllowZero = field.numberAllowZero;

  var fieldType = _.toUpper(type);

  if (!disabled && mandatory && [_FIELD_TYPES.TEXT_FIELD, _FIELD_TYPES.TEXT_BOX, _FIELD_TYPES.TEXT_SELECTION].indexOf(fieldType) >= 0) {
    if (_.isString(value) && _.isEmpty(value) || _.isNull(value) || _.isNumber(value) && !numberAllowZero && value === 0 || _.isNaN(value) || value === undefined || value === "") {
      var _message16;

      return {
        hasError: true,
        message: (_message16 = {}, _defineProperty(_message16, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.302"]), _defineProperty(_message16, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.302"]), _message16)
      };
    }
  }

  return {
    hasError: false,
    message: (_message17 = {}, _defineProperty(_message17, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message17, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message17)
  };
}

/**
 * validateMandatoryForApplicationForm
 * TODO: This function should be removed, and use validateMandatory
 * @description validate mandatory for ApplicationForm component
 * */
function validateMandatoryForApplicationForm(_ref5) {
  var _message28;

  var field = _ref5.field,
      value = _ref5.value,
      rootValues = _ref5.rootValues,
      path = _ref5.path;
  var type = field.type,
      mandatory = field.mandatory,
      disabled = field.disabled,
      numberAllowZero = field.numberAllowZero,
      id = field.id,
      validation = field.validation;

  var fieldType = _.toUpper(type);

  if (!disabled && mandatory || _.includes(id, "LIFESTYLE02")) {
    if ([_FIELD_TYPES.TEXTSELECTION, _FIELD_TYPES.TEXTFIELD, _FIELD_TYPES.TEXT, _FIELD_TYPES.TEXTAREA, _FIELD_TYPES.TEXT_SELECTION, _FIELD_TYPES.TEXT_FIELD, _FIELD_TYPES.TEXT_BOX].indexOf(fieldType) >= 0) {
      if (_.isString(value) && _.isEmpty(value) || _.isNull(value) || _.isNumber(value) && !numberAllowZero && value === 0 || _.isNaN(value) || _.isUndefined(value)) {
        var _message18;

        return {
          hasError: true,
          message: (_message18 = {}, _defineProperty(_message18, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.302"]), _defineProperty(_message18, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.302"]), _message18)
        };
      }
      if (id === "HEALTH16a" && value < 4) {
        var _message19;

        return {
          hasError: true,
          message: (_message19 = {}, _defineProperty(_message19, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.310"]), _defineProperty(_message19, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.310"]), _message19)
        };
      }
    } else if ([_FIELD_TYPES.CHECKBOX].indexOf(fieldType) >= 0 && !_.includes(id, "LIFESTYLE02")) {
      if (value !== "Y") {
        var _message20;

        return {
          hasError: true,
          message: (_message20 = {}, _defineProperty(_message20, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.301"]), _defineProperty(_message20, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.301"]), _message20)
        };
      }
    } else if ([_FIELD_TYPES.CHECKBOX].indexOf(fieldType) >= 0 && _.includes(id, "LIFESTYLE02") && !_.includes(id, "LIFESTYLE02a")) {
      if (_.get(rootValues, path + ".LIFESTYLE02a_1") !== "Y" && _.get(rootValues, path + ".LIFESTYLE02b_1") !== "Y" && _.get(rootValues, path + ".LIFESTYLE02c_1") !== "Y") {
        var _message21;

        return {
          hasError: true,
          message: (_message21 = {}, _defineProperty(_message21, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message21, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message21)
        };
      }
    } else if ([_FIELD_TYPES.CHECKBOX].indexOf(fieldType) >= 0 && _.includes(id, "LIFESTYLE02a")) {
      if (_.get(rootValues, path + ".LIFESTYLE02a_1") !== "Y" && _.get(rootValues, path + ".LIFESTYLE02b_1") !== "Y" && _.get(rootValues, path + ".LIFESTYLE02c_1") !== "Y") {
        var _message22;

        return {
          hasError: true,
          message: (_message22 = {}, _defineProperty(_message22, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.301"]), _defineProperty(_message22, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.301"]), _message22)
        };
      }
    } else if ([_FIELD_TYPES.QRADIOGROUP].indexOf(fieldType) >= 0) {
      if (value === "") {
        var _message23;

        return {
          hasError: true,
          message: (_message23 = {}, _defineProperty(_message23, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.301"]), _defineProperty(_message23, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.301"]), _message23)
        };
      }
    } else if ([_FIELD_TYPES.DATEPICKER].indexOf(fieldType) >= 0) {
      if (validation) {
        if (validation.type === "month_greater") {
          if (!_.isEmpty(value)) {
            var fromDate = value[validation.id];
            var toDate = value[id];
            if (toDate && fromDate) {
              if (new Date(fromDate.slice(0, 7)).valueOf() > new Date(toDate.slice(0, 7)).valueOf()) {
                var _message24;

                return {
                  hasError: true,
                  message: (_message24 = {}, _defineProperty(_message24, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.409"]), _defineProperty(_message24, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.409"]), _message24)
                };
              }
            }
          }

          if (_.isEmpty(value[id])) {
            var _message25;

            return {
              hasError: true,
              message: (_message25 = {}, _defineProperty(_message25, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.302"]), _defineProperty(_message25, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.302"]), _message25)
            };
          }
        }
      } else if (_.isEmpty(value[id])) {
        var _message26;

        return {
          hasError: true,
          message: (_message26 = {}, _defineProperty(_message26, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.302"]), _defineProperty(_message26, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.302"]), _message26)
        };
      }
    } else if ([_FIELD_TYPES.PICKER].indexOf(fieldType) >= 0) {
      if (value === "") {
        var _message27;

        return {
          hasError: true,
          message: (_message27 = {}, _defineProperty(_message27, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.302"]), _defineProperty(_message27, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.302"]), _message27)
        };
      }
    }
  }

  return {
    hasError: false,
    message: (_message28 = {}, _defineProperty(_message28, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message28, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message28)
  };
}

var validateROPTableExisting = function validateROPTableExisting(_ref6) {
  var iErrorObj = _ref6.iErrorObj,
      iValues = _ref6.iValues,
      iTemplate = _ref6.iTemplate,
      baseProductCode = _ref6.baseProductCode;
  var SUMASSURED = ROP_TABLE.SUMASSURED,
      EXITSTING_TABLE_MAPPING = ROP_TABLE.EXITSTING_TABLE_MAPPING,
      COMPANY = ROP_TABLE.COMPANY,
      SHOW_EXIST_TABLE = ROP_TABLE.SHOW_EXIST_TABLE,
      SHOW_ACC_EXIST_TABLE = ROP_TABLE.SHOW_ACC_EXIST_TABLE;


  var isShow = false;

  if (baseProductCode) {
    if (baseProductCode === "BAA") {
      isShow = _.get(iValues, SHOW_ACC_EXIST_TABLE, "") === "Y";
    } else {
      isShow = _.get(iValues, SHOW_EXIST_TABLE, "") === "Y";
    }
  } else {
    isShow = _.get(iValues, SHOW_EXIST_TABLE, "") === "Y" && _.includes(iTemplate.presentation, SHOW_EXIST_TABLE, false) || _.get(iValues, SHOW_ACC_EXIST_TABLE, "") === "Y" && _.includes(iTemplate.presentation, SHOW_ACC_EXIST_TABLE, false);
  }

  if (!isShow) {
    _.each(COMPANY, function (company) {
      _.each(EXITSTING_TABLE_MAPPING[company], function (id) {
        if (_.get(iErrorObj, id + ".hasError")) {
          _.set(iErrorObj, "" + id, {
            hasError: false,
            message: {}
          });
        }
        if (_.get(iValues, "" + id)) {
          _.set(iValues, "" + id, 0);
        }
      });
    });
    return;
  }

  _.each(SUMASSURED, function (sumAssuredField) {
    var axaId = _.get(EXITSTING_TABLE_MAPPING, COMPANY.AXA + "." + sumAssuredField, "");
    var otherId = _.get(EXITSTING_TABLE_MAPPING, COMPANY.OTHER + "." + sumAssuredField, "");
    var totalId = _.get(EXITSTING_TABLE_MAPPING, COMPANY.TOTAL + "." + sumAssuredField, "");

    var axaValue = _.get(iValues, "" + axaId, 0);
    var otherValue = _.get(iValues, "" + otherId, 0);
    var totalValue = _.get(iValues, "" + totalId, 0);

    if (axaValue + otherValue === totalValue) {
      if (_.get(iErrorObj, axaId + ".hasError") || _.get(iErrorObj, otherId + ".hasError")) {
        _.set(iErrorObj, "" + axaId, {
          hasError: false,
          message: {}
        });
        _.set(iErrorObj, "" + otherId, {
          hasError: false,
          message: {}
        });
      }
    } else if (!_.get(iErrorObj, axaId + ".hasError", false) || !_.get(iErrorObj, otherId + ".hasError", false)) {
      _.set(iErrorObj, "" + axaId, {
        hasError: true,
        message: {}
      });
      _.set(iErrorObj, "" + otherId, {
        hasError: true,
        message: {}
      });
    }
  });
};

var validateROPTablePending = function validateROPTablePending(_ref7) {
  var iErrorObj = _ref7.iErrorObj,
      iValues = _ref7.iValues;
  var PENDING_LIFE_AXA = ROP_TABLE.PENDING_LIFE_AXA,
      PENDING_CI_AXA = ROP_TABLE.PENDING_CI_AXA,
      PENDING_TPD_AXA = ROP_TABLE.PENDING_TPD_AXA,
      PENDING_PAADB_AXA = ROP_TABLE.PENDING_PAADB_AXA,
      PENDING_TOTALPREM_AXA = ROP_TABLE.PENDING_TOTALPREM_AXA,
      PENDING_LIFE_OTHER = ROP_TABLE.PENDING_LIFE_OTHER,
      PENDING_CI_OTHER = ROP_TABLE.PENDING_CI_OTHER,
      PENDING_TPD_OTHER = ROP_TABLE.PENDING_TPD_OTHER,
      PENDING_PAADB_OTHER = ROP_TABLE.PENDING_PAADB_OTHER,
      PENDING_TOTALPREM_OTHER = ROP_TABLE.PENDING_TOTALPREM_OTHER,
      SHOW_PENDING_TABLE = ROP_TABLE.SHOW_PENDING_TABLE,
      COMPANY = ROP_TABLE.COMPANY,
      PENDING_TABLE_MAPPING = ROP_TABLE.PENDING_TABLE_MAPPING;


  if (_.get(iValues, SHOW_PENDING_TABLE, "") !== "Y") {
    _.each(COMPANY, function (company) {
      _.each(PENDING_TABLE_MAPPING[company], function (id) {
        if (_.get(iErrorObj, id + ".hasError")) {
          _.set(iErrorObj, "" + id, {
            hasError: false,
            message: {}
          });
        }
        if (_.get(iValues, "" + id)) {
          _.set(iValues, "" + id, 0);
        }
      });
    });
    return;
  }

  var notTotalIds = [PENDING_LIFE_AXA, PENDING_CI_AXA, PENDING_TPD_AXA, PENDING_PAADB_AXA, PENDING_LIFE_OTHER, PENDING_CI_OTHER, PENDING_TPD_OTHER, PENDING_PAADB_OTHER];

  var totalIds = [PENDING_TOTALPREM_AXA, PENDING_TOTALPREM_OTHER];
  var notTotalFieldsHasNonZero = false;
  var totalFieldsHasNonZero = false;

  _.each(notTotalIds, function (id) {
    var value = _.get(iValues, "" + id);
    if (_.isNumber(value) && value !== 0) {
      notTotalFieldsHasNonZero = true;
      return false;
    }
    return true;
  });

  _.each(totalIds, function (id) {
    var value = _.get(iValues, "" + id);
    if (_.isNumber(value) && value !== 0) {
      totalFieldsHasNonZero = true;
      return false;
    }
    return true;
  });

  if (notTotalFieldsHasNonZero) {
    _.each(notTotalIds, function (id) {
      _.set(iErrorObj, "" + id, {
        hasError: false,
        message: {}
      });
    });
  } else {
    _.each(notTotalIds, function (id) {
      _.set(iErrorObj, "" + id, {
        hasError: true,
        message: {}
      });
    });
  }

  if (totalFieldsHasNonZero) {
    _.each(totalIds, function (id) {
      _.set(iErrorObj, "" + id, {
        hasError: false,
        message: {}
      });
    });
  } else {
    _.each(totalIds, function (id) {
      _.set(iErrorObj, "" + id, {
        hasError: true,
        message: {}
      });
    });
  }
};

var validateROPTableReplace = function validateROPTableReplace(_ref8) {
  var iErrorObj = _ref8.iErrorObj,
      iValues = _ref8.iValues;
  var SUMASSURED = ROP_TABLE.SUMASSURED,
      REPLACE_TABLE_MAPPING = ROP_TABLE.REPLACE_TABLE_MAPPING,
      COMPANY = ROP_TABLE.COMPANY,
      REPLACE_POLICY_TYPE_AXA = ROP_TABLE.REPLACE_POLICY_TYPE_AXA,
      REPLACE_POLICY_TYPE_OTHER = ROP_TABLE.REPLACE_POLICY_TYPE_OTHER,
      SHOW_REPLACE_TABLE = ROP_TABLE.SHOW_REPLACE_TABLE,
      SHOW_ACC_TABLE = ROP_TABLE.SHOW_ACC_TABLE;


  if (_.get(iValues, SHOW_REPLACE_TABLE, "") !== "Y" && _.get(iValues, SHOW_ACC_TABLE, "") !== "Y") {
    _.each(COMPANY, function (company) {
      _.each(REPLACE_TABLE_MAPPING[company], function (id) {
        if (_.get(iErrorObj, id + ".hasError")) {
          _.set(iErrorObj, "" + id, {
            hasError: false,
            message: {}
          });
        }
        if (_.get(iValues, "" + id)) {
          _.set(iValues, "" + id, 0);
        }
      });
    });
    return;
  }

  var enableTypeOfPoliciesAXA = false;
  var enableTypeOfPoliciesOther = false;

  _.each(SUMASSURED, function (sumAssuredField) {
    var axaId = _.get(REPLACE_TABLE_MAPPING, COMPANY.AXA + "." + sumAssuredField, "");
    var otherId = _.get(REPLACE_TABLE_MAPPING, COMPANY.OTHER + "." + sumAssuredField, "");
    var totalId = _.get(REPLACE_TABLE_MAPPING, COMPANY.TOTAL + "." + sumAssuredField, "");

    var axaValue = _.get(iValues, "" + axaId, 0);
    var otherValue = _.get(iValues, "" + otherId, 0);
    var totalValue = _.get(iValues, "" + totalId, 0);

    if (axaValue + otherValue === totalValue) {
      if (_.get(iErrorObj, axaId + ".hasError") || _.get(iErrorObj, otherId + ".hasError")) {
        _.set(iErrorObj, "" + axaId, {
          hasError: false,
          message: {}
        });
        _.set(iErrorObj, "" + otherId, {
          hasError: false,
          message: {}
        });
      }
    } else if (!_.get(iErrorObj, axaId + ".hasError", false) || !_.get(iErrorObj, otherId + ".hasError", false)) {
      _.set(iErrorObj, "" + axaId, {
        hasError: true,
        message: {}
      });
      _.set(iErrorObj, "" + otherId, {
        hasError: true,
        message: {}
      });
    }

    // type of policies
    if (axaValue !== 0 && !enableTypeOfPoliciesAXA) {
      enableTypeOfPoliciesAXA = true;
    }
    if (otherValue !== 0 && !enableTypeOfPoliciesOther) {
      enableTypeOfPoliciesOther = true;
    }
  });

  if (enableTypeOfPoliciesAXA) {
    if (_.isEmpty(_.get(iValues, REPLACE_POLICY_TYPE_AXA, ""))) {
      var _message29;

      _.set(iErrorObj, "" + REPLACE_POLICY_TYPE_AXA, {
        hasError: true,
        message: (_message29 = {}, _defineProperty(_message29, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.309"]), _defineProperty(_message29, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.309"]), _message29)
      });
    } else if (_.get(iErrorObj, REPLACE_POLICY_TYPE_AXA + ".hasError")) {
      _.set(iErrorObj, "" + REPLACE_POLICY_TYPE_AXA, {
        hasError: false,
        message: {}
      });
    }
  } else {
    _.set(iValues, "" + REPLACE_POLICY_TYPE_AXA, "");
    if (_.get(iErrorObj, REPLACE_POLICY_TYPE_AXA + ".hasError")) {
      _.set(iErrorObj, "" + REPLACE_POLICY_TYPE_AXA, {
        hasError: false,
        message: {}
      });
    }
  }

  if (enableTypeOfPoliciesOther) {
    if (_.isEmpty(_.get(iValues, REPLACE_POLICY_TYPE_OTHER, ""))) {
      var _message30;

      _.set(iErrorObj, "" + REPLACE_POLICY_TYPE_OTHER, {
        hasError: true,
        message: (_message30 = {}, _defineProperty(_message30, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.309"]), _defineProperty(_message30, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.309"]), _message30)
      });
    } else if (_.get(iErrorObj, REPLACE_POLICY_TYPE_OTHER + ".hasError")) {
      _.set(iErrorObj, "" + REPLACE_POLICY_TYPE_OTHER, {
        hasError: false,
        message: {}
      });
    }
  } else {
    _.set(iValues, "" + REPLACE_POLICY_TYPE_OTHER, "");
    if (_.get(iErrorObj, REPLACE_POLICY_TYPE_OTHER + ".hasError")) {
      _.set(iErrorObj, "" + REPLACE_POLICY_TYPE_OTHER, {
        hasError: false,
        message: {}
      });
    }
  }
};

var validateROPTable = exports.validateROPTable = function validateROPTable(_ref9) {
  var iErrorObj = _ref9.iErrorObj,
      iValues = _ref9.iValues,
      iTemplate = _ref9.iTemplate,
      baseProductCode = _ref9.baseProductCode;

  validateROPTableExisting({ iErrorObj: iErrorObj, iValues: iValues, iTemplate: iTemplate, baseProductCode: baseProductCode });
  validateROPTablePending({ iErrorObj: iErrorObj, iValues: iValues });
  validateROPTableReplace({ iErrorObj: iErrorObj, iValues: iValues });
};

function validateMandatoryForDynamicTable(_ref10) {
  var _message32;

  var value = _ref10.value;

  if (_.isUndefined(value) || _.isEmpty(value)) {
    var _message31;

    return {
      hasError: true,
      message: (_message31 = {}, _defineProperty(_message31, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.302"]), _defineProperty(_message31, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.302"]), _message31)
    };
  }

  return {
    hasError: false,
    message: (_message32 = {}, _defineProperty(_message32, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message32, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message32)
  };
}

function validateAssets(_ref11) {
  var rule = _ref11.rule,
      value = _ref11.value;

  // TODO
  // keep this code
  // const assetsList = [
  //   {
  //     title: "Savings Account",
  //     value: "savAcc"
  //   },
  //   {
  //     title: "Fixed Deposits",
  //     value: "fixDeposit"
  //   },
  //   {
  //     title: "Investment(Mutual Fund/Direct investment",
  //     value: "invest"
  //   },
  //   {
  //     title: "CPF (OA)",
  //     value: "cpfOa"
  //   },
  //   {
  //     title: "CPF (SA)",
  //     value: "cpfSa"
  //   },
  //   {
  //     title: "CPF Medisave",
  //     value: "cpfMs"
  //   },
  //   {
  //     title: "SRS",
  //     value: "srs"
  //   },
  //   {
  //     title: "Please select assets",
  //     value: "all"
  //   }
  // ];
  // const targetTitle = assetsList.find(al => al.value === value[0].key).title;
  var validateArr = [];
  var assets = _.get(value, "targetData.assets", []);
  _.each(assets, function (asset, indexkey) {
    var validateValue = asset.calAsset;
    if (validateValue === "") {
      validateValue = null;
    }
    if (_.isString(validateValue)) {
      validateValue = parseInt(validateValue, 10);
    }
    var validateObj = validateMandatory({
      field: {
        type: _FIELD_TYPES.TEXT_FIELD,
        mandatory: rule.mandatory,
        disabled: false
      },
      value: validateValue
    });
    if (!validateObj.hasError && rule.min) {
      validateObj = validateText({
        field: {
          min: rule.min
        },
        value: validateValue
      });
    }
    if (!validateObj.hasError && rule.max) {
      validateObj = validateText({
        field: {
          max: rule.max
        },
        value: asset.totalValue || validateValue
        // TODO
        // keep this code
        // cousMessage: {
        //   code: "na.analysis.assetsTotalValueError",
        //   replacement: [
        //     {
        //       text: "{title}",
        //       message: targetTitle
        //     }
        //   ]
        // }
      });
    }
    validateArr.push(Object.assign({}, validateObj, {
      index: indexkey
    }));
  });
  return validateArr;
}

function validateNeedsAnalysis(_ref12) {
  var rule = _ref12.rule,
      value = _ref12.value;

  if (!rule) {
    return {};
  }

  if (rule.type && !_.isEmpty(value) && value !== 0) {
    switch (rule.type) {
      case "int":
        value = _.toNumber(value);
        break;
      case "string":
        value = _.toString(value);
        break;
      default:
        break;
    }
  }
  var validateObj = {};
  if (rule.mandatory) {
    validateObj = validateMandatory({
      field: {
        type: _FIELD_TYPES.TEXT_FIELD,
        mandatory: rule.mandatory,
        disabled: false,
        numberAllowZero: rule.numberAllowZero
      },
      value: value
    });
  }
  if (!validateObj.hasError && rule.min) {
    validateObj = validateText({
      field: {
        min: rule.min
      },
      value: value
    });
  }
  return validateObj;
}

/**
 * validateRecommendationROP
 * @description validate Replacement of Policy in Recommendation page
 * */
function validateRecommendationROP(_ref13) {
  var _message33, _message34;

  var field = _ref13.field,
      value = _ref13.value;
  var mandatory = field.mandatory,
      disabled = field.disabled;

  var replaceLife = _.toNumber(_.get(value, "rop.replaceLife", 0));
  var replaceTpd = _.toNumber(_.get(value, "rop.replaceTpd", 0));
  var replaceCi = _.toNumber(_.get(value, "rop.replaceCi", 0));
  var replacePaAdb = _.toNumber(_.get(value, "rop.replacePaAdb", 0));

  var valid = !(replaceLife === 0 && replaceTpd === 0 && replaceCi === 0 && replacePaAdb === 0);
  return !valid && !disabled && mandatory ? {
    hasError: true,
    message: (_message33 = {}, _defineProperty(_message33, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.801"]), _defineProperty(_message33, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message33)
  } : {
    hasError: false,
    message: (_message34 = {}, _defineProperty(_message34, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message34, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message34)
  };
}

/**
 * validateBudgetChoice
 * @description validate Budget choice in Budget page
 * */
function validateBudgetChoice(_ref14) {
  var _message35, _message36;

  var field = _ref14.field,
      value = _ref14.value;
  var mandatory = field.mandatory,
      disabled = field.disabled;

  var valid = value === "Y";
  return !valid && !disabled && mandatory ? {
    hasError: true,
    message: (_message35 = {}, _defineProperty(_message35, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.302"]), _defineProperty(_message35, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message35)
  } : {
    hasError: false,
    message: (_message36 = {}, _defineProperty(_message36, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message36, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message36)
  };
}
/**
 * @description validate fe section net worth
 * @param data - target client data (like fe.owner, fe.spouse and
 *   fe.dependants[0])
 * @param relationship - relationship between target client and owner
 * @return {object} - error object of net worth
 * */
function validateNetWorth(_ref15) {
  var data = _ref15.data,
      relationship = _ref15.relationship;
  var assets = data.assets,
      liabilities = data.liabilities,
      noALReason = data.noALReason,
      otherAssetTitle = data.otherAssetTitle,
      otherLiabilitesTitle = data.otherLiabilitesTitle,
      otherAsset = data.otherAsset,
      otherLiabilites = data.otherLiabilites;

  // check section mandatory

  var netWorthError = relationship === "owner" && assets === 0 && liabilities === 0 && noALReason === "" ? {
    hasError: true,
    message: "error.704"
  } : {
    hasError: false,
    message: ""
  };

  // check noAlReason mandatory
  var noALReasonError = relationship === "owner" && assets === 0 && liabilities === 0 && noALReason === "" ? {
    hasError: true,
    message: "error.302"
  } : {
    hasError: false,
    message: ""
  };

  // check otherAssetTitle mandatory
  var otherAssetTitleError = otherAssetTitle === "" && otherAsset !== 0 ? {
    hasError: true,
    message: "error.302"
  } : {
    hasError: false,
    message: ""
  };

  // check otherAsset mandatory
  var otherAssetError = otherAssetTitle !== "" && otherAsset === 0 ? {
    hasError: true,
    message: "error.302"
  } : {
    hasError: false,
    message: ""
  };

  // check otherLiabilitesTitle mandatory
  var otherLiabilitesTitleError = otherLiabilitesTitle === "" && otherLiabilites !== 0 ? {
    hasError: true,
    message: "error.302"
  } : {
    hasError: false,
    message: ""
  };

  // check otherLiabilites mandatory
  var otherLiabilitesError = otherLiabilitesTitle !== "" && otherLiabilites === 0 ? {
    hasError: true,
    message: "error.302"
  } : {
    hasError: false,
    message: ""
  };

  return {
    netWorth: netWorthError,
    noALReason: noALReasonError,
    otherAssetTitleError: otherAssetTitleError,
    otherAsset: otherAssetError,
    otherLiabilitesTitleError: otherLiabilitesTitleError,
    otherLiabilites: otherLiabilitesError
  };
}
/**
 * @description validate fe section existing insurance portfolio
 * @param data - target client data (like fe.owner, fe.spouse and
 *   fe.dependants[0])
 * @param relationship - relationship between target client and owner
 * @return {object} - error object of existing insurance portfolio
 * */
function validateEIP(_ref16) {
  var data = _ref16.data,
      relationship = _ref16.relationship;
  var eiPorf = data.eiPorf,
      aInsPrem = data.aInsPrem,
      sInsPrem = data.sInsPrem,
      noEIReason = data.noEIReason,
      hospNSurg = data.hospNSurg;

  // check noEIReason mandatory

  var noEIReasonError = relationship === "owner" && eiPorf === 0 && hospNSurg === "" && aInsPrem === 0 && sInsPrem === 0 && noEIReason === "" ? {
    hasError: true,
    message: "error.302"
  } : {
    hasError: false,
    message: ""
  };

  // check assured section mandatory
  var assuredError = eiPorf === 0 && hospNSurg === "" && (aInsPrem !== 0 || sInsPrem !== 0) ? {
    hasError: true,
    message: "error.714"
  } : {
    hasError: false,
    message: ""
  };

  // check premiums section mandatory
  var premiumsError = (eiPorf !== 0 || hospNSurg !== "") && aInsPrem === 0 && sInsPrem === 0 ? {
    hasError: true,
    message: "error.715"
  } : {
    hasError: false,
    message: ""
  };

  return {
    noEIReason: noEIReasonError,
    assured: assuredError,
    premiums: premiumsError
  };
}
/**
 * @description validate fe section cash flow
 * @param data - target client data (like fe.owner, fe.spouse and
 *   fe.dependants[0])
 * @param relationship - relationship between target client and owner
 * @return {object} - error object of cash flow
 * */
function validateCashFlow(_ref17) {
  var data = _ref17.data,
      relationship = _ref17.relationship;
  var totAIncome = data.totAIncome,
      totAExpense = data.totAExpense,
      otherExpenseTitle = data.otherExpenseTitle,
      otherExpense = data.otherExpense,
      noCFReason = data.noCFReason;

  // check CFReason mandatory

  var noCFReasonError = relationship === "owner" && (totAIncome === 0 || totAExpense === 0) && noCFReason === "" ? {
    hasError: true,
    message: "error.302"
  } : {
    hasError: false,
    message: ""
  };

  // check otherExpenseTitle mandatory
  var otherExpenseTitleError = otherExpenseTitle === "" && otherExpense !== 0 ? {
    hasError: true,
    message: "error.302"
  } : {
    hasError: false,
    message: ""
  };

  // check otherExpense mandatory
  var otherExpenseError = otherExpenseTitle !== "" && otherExpense === 0 ? {
    hasError: true,
    message: "error.302"
  } : {
    hasError: false,
    message: ""
  };

  return {
    noCFReason: noCFReasonError,
    otherExpenseTitle: otherExpenseTitleError,
    otherExpense: otherExpenseError
  };
}
/**
 * @description validate fe section budget (all budgets) and force income
 * @param data - target client data (like fe.owner, fe.spouse and
 *   fe.dependants[0])
 * @return {object} - error object of budget (all budgets) and force income
 * */
function validateBudgetAndForceIncome(data) {
  var cpfOa = data.cpfOa,
      cpfSa = data.cpfSa,
      cpfMs = data.cpfMs,
      srs = data.srs,
      forceIncome = data.forceIncome,
      forceIncomeReason = data.forceIncomeReason,
      aRegPremBudget = data.aRegPremBudget,
      aRegPremBudgetSrc = data.aRegPremBudgetSrc,
      confirmBudget = data.confirmBudget,
      confirmBudgetReason = data.confirmBudgetReason,
      singPrem = data.singPrem,
      singPremSrc = data.singPremSrc,
      confirmSingPremBudget = data.confirmSingPremBudget,
      confirmSingPremBudgetReason = data.confirmSingPremBudgetReason,
      cpfOaBudget = data.cpfOaBudget,
      cpfSaBudget = data.cpfSaBudget,
      cpfMsBudget = data.cpfMsBudget,
      srsBudget = data.srsBudget;

  // check budget section mandatory

  var budgetError = aRegPremBudget === 0 && singPrem === 0 && cpfOaBudget === 0 && cpfSaBudget === 0 && cpfMsBudget === 0 && srsBudget === 0 ? {
    hasError: true,
    message: "error.703"
  } : {
    hasError: false,
    message: ""
  };

  // check aRegPremBudgetSrc mandatory
  var aRegPremBudgetSrcError = aRegPremBudget !== 0 && aRegPremBudgetSrc === "" ? {
    hasError: true,
    message: "error.302"
  } : {
    hasError: false,
    message: ""
  };

  // check confirmBudget mandatory
  var confirmBudgetError = aRegPremBudget !== 0 && confirmBudget === "" ? {
    hasError: true,
    message: "error.302"
  } : {
    hasError: false,
    message: ""
  };

  // check confirmBudgetReason mandatory
  var confirmBudgetReasonError = aRegPremBudget !== 0 && confirmBudget === "Y" && confirmBudgetReason === "" ? {
    hasError: true,
    message: "error.302"
  } : {
    hasError: false,
    message: ""
  };

  // check singPremSrc mandatory
  var singPremSrcError = singPrem !== 0 && singPremSrc === "" ? {
    hasError: true,
    message: "error.302"
  } : {
    hasError: false,
    message: ""
  };

  // check confirmSingPremBudget mandatory
  var confirmSingPremBudgetError = singPrem !== 0 && confirmSingPremBudget === "" ? {
    hasError: true,
    message: "error.302"
  } : {
    hasError: false,
    message: ""
  };

  // check confirmBudgetReason mandatory
  var confirmSingPremBudgetReasonError = singPrem !== 0 && confirmSingPremBudget === "Y" && confirmSingPremBudgetReason === "" ? {
    hasError: true,
    message: "error.302"
  } : {
    hasError: false,
    message: ""
  };

  // Check if the cpf budget is over the asset
  var cpfOaBudgetError = {};
  var cpfSaBudgetError = {};
  var cpfMsBudgetError = {};
  var srsBudgetError = {};
  if (budgetError.hasError) {
    cpfOaBudgetError = budgetError;
    cpfSaBudgetError = budgetError;
    cpfMsBudgetError = budgetError;
    srsBudgetError = budgetError;
  } else {
    cpfOaBudgetError = cpfOaBudget > cpfOa ? {
      hasError: true,
      message: "error.706"
    } : {
      hasError: false,
      message: ""
    };
    cpfSaBudgetError = cpfSaBudget > cpfSa ? {
      hasError: true,
      message: "error.706"
    } : {
      hasError: false,
      message: ""
    };
    cpfMsBudgetError = cpfMsBudget > cpfMs ? {
      hasError: true,
      message: "error.706"
    } : {
      hasError: false,
      message: ""
    };
    srsBudgetError = srsBudget > srs ? {
      hasError: true,
      message: "error.706"
    } : {
      hasError: false,
      message: ""
    };
  }

  // check forceIncome mandatory
  var forceIncomeError = forceIncome === "" ? {
    hasError: true,
    message: "error.302"
  } : {
    hasError: false,
    message: ""
  };

  // check forceIncomeReason mandatory
  var forceIncomeReasonError = forceIncome === "Y" && forceIncomeReason === "" ? {
    hasError: true,
    message: "error.302"
  } : {
    hasError: false,
    message: ""
  };

  return {
    budget: budgetError,
    aRegPremBudgetSrc: aRegPremBudgetSrcError,
    confirmBudget: confirmBudgetError,
    confirmBudgetReason: confirmBudgetReasonError,
    singPremSrc: singPremSrcError,
    confirmSingPremBudget: confirmSingPremBudgetError,
    confirmSingPremBudgetReason: confirmSingPremBudgetReasonError,
    forceIncome: forceIncomeError,
    forceIncomeReason: forceIncomeReasonError,
    cpfOaBudget: cpfOaBudgetError,
    cpfSaBudget: cpfSaBudgetError,
    cpfMsBudget: cpfMsBudgetError,
    srsBudget: srsBudgetError
  };
}

function deleteErrorAndValues(_ref18) {
  var iTemplate = _ref18.iTemplate,
      iValues = _ref18.iValues,
      iErrorObj = _ref18.iErrorObj;

  var id = _.get(iTemplate, "id") || _.get(iTemplate, "key");

  if (!_.isUndefined(id)) {
    if (_.includes(DYNAMIC_VIEWS.DUPLICATE_IDS, id)) {
      return;
    }
    if (iValues && _.has(iValues, id)) {
      delete iValues[id];
    }
    if (iErrorObj && _.has(iErrorObj, id)) {
      delete iErrorObj[id];
    }
  }

  if (_.has(iTemplate, "items")) {
    _.each(iTemplate.items, function (item) {
      deleteErrorAndValues({ iTemplate: item, iValues: iValues, iErrorObj: iErrorObj });
    });
  }
}

function validateApplicationFormPolicyTable() {
  var _message37;

  // TODO: logic for validate saTable
  return {
    hasError: false,
    message: (_message37 = {}, _defineProperty(_message37, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message37, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message37)
  };
}

function validateApplicationFormTemplate(_ref19) {
  var iTemplate = _ref19.iTemplate,
      iValues = _ref19.iValues,
      iErrorObj = _ref19.iErrorObj,
      rootValues = _ref19.rootValues,
      path = _ref19.path;
  var type = iTemplate.type;


  var showTemplate = (0, _trigger.show)({
    template: iTemplate,
    rootValues: rootValues,
    valuePath: path
  });

  if (!showTemplate) {
    deleteErrorAndValues({ iTemplate: iTemplate, iValues: iValues, iErrorObj: iErrorObj });
    return;
  }

  (0, _trigger.setMandatory)({ template: iTemplate, rootValues: rootValues, valuePath: path });

  if (type && (_.toUpper(type) === "TABLE-BLOCK-T" || _.toUpper(type) === "TABLE-BLOCK" || _.toUpper(type) === "BLOCK")) {
    if (_.get(iTemplate, "mandatory") && _.has(iTemplate, "items")) {
      var blockId = iTemplate.id || iTemplate.key;
      var hasError = !_.some(iTemplate.items, function (item) {
        return _.get(iValues, item.id) === "Y";
      });
      if (hasError) {
        var _message38;

        iErrorObj[blockId] = {
          hasError: true,
          message: (_message38 = {}, _defineProperty(_message38, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.301"]), _defineProperty(_message38, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.301"]), _message38)
        };
      } else if (_.get(iErrorObj, blockId + ".hasError")) {
        _.set(iErrorObj, blockId + ".hasError", false);
      }
    }
  }

  if (_.isString(_.get(iTemplate, "type")) && _.toUpper(_.get(iTemplate, "type")) === "TABLE") {
    _.set(iErrorObj, iTemplate.id, validateMandatoryForDynamicTable({ value: iValues[iTemplate.id] }));
  } else if (_.toUpper(_.get(iTemplate, "type")) === "CHECKBOX") {
    if (!_.has(iValues, iTemplate.id)) {
      _.set(rootValues, path + "." + iTemplate.id, "N");
    }
    _.set(iErrorObj, iTemplate.id, validateMandatoryForApplicationForm({
      field: {
        type: "CHECKBOX",
        mandatory: iTemplate.mandatory,
        disabled: !!iTemplate.disabled,
        id: iTemplate.id
      },
      value: iValues[iTemplate.id] || "",
      rootValues: rootValues,
      path: path
    }));
  } else if (_.toUpper(_.get(iTemplate, "type")) === "PICKER" && _.has(iTemplate, "value") && !_.has(rootValues, path + "." + iTemplate.id)) {
    _.set(rootValues, path + "." + iTemplate.id, _.get(iTemplate, "value"));
  } else if (_.has(iTemplate, "items")) {
    _.each(iTemplate.items, function (item) {
      validateApplicationFormTemplate({
        iTemplate: item,
        iValues: iValues,
        iErrorObj: iErrorObj,
        rootValues: rootValues,
        path: path
      });
    });
  } else if (iTemplate.id) {
    if (iTemplate.type === "saTable") {
      // TODO validation for saTable
      validateROPTable({ iErrorObj: iErrorObj, iValues: iValues, iTemplate: iTemplate });
    } else {
      _.set(iErrorObj, iTemplate.id, validateMandatoryForApplicationForm({
        field: {
          type: _FIELD_TYPES.TEXT_SELECTION,
          mandatory: iTemplate.mandatory,
          disabled: !!iTemplate.disabled,
          id: iTemplate.id
        },
        value: iValues[iTemplate.id] || ""
      }));
    }
  }
}

function validateApplicationFormTableDialog(_ref20) {
  var iTemplate = _ref20.iTemplate,
      iValues = _ref20.iValues,
      iErrorObj = _ref20.iErrorObj,
      rootValues = _ref20.rootValues,
      path = _ref20.path;

  var showTemplate = (0, _trigger.show)({
    template: iTemplate,
    rootValues: rootValues,
    valuePath: path
  });

  if (!showTemplate) {
    deleteErrorAndValues({ iTemplate: iTemplate, iValues: iValues, iErrorObj: iErrorObj });
    return;
  }

  var type = iTemplate.type;


  (0, _trigger.setMandatory)({ template: iTemplate, rootValues: rootValues, valuePath: path });

  if (type && (_.toUpper(type) === "TABLE-BLOCK-T" || _.toUpper(type) === "TABLE-BLOCK" || _.toUpper(type) === "BLOCK")) {
    if (_.get(iTemplate, "mandatory") && _.has(iTemplate, "items")) {
      var blockId = iTemplate.id || iTemplate.key;
      var hasError = !_.some(iTemplate.items, function (item) {
        return _.get(iValues, item.id) === "Y";
      });
      if (hasError) {
        var _message39;

        iErrorObj[blockId] = {
          hasError: true,
          message: (_message39 = {}, _defineProperty(_message39, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.301"]), _defineProperty(_message39, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE]["error.301"]), _message39)
        };
      } else if (_.get(iErrorObj, blockId + ".hasError")) {
        _.set(iErrorObj, blockId + ".hasError", false);
      }
    }
  }

  if (_.has(iTemplate, "items")) {
    _.each(iTemplate.items, function (item) {
      validateApplicationFormTableDialog({
        iTemplate: item,
        iValues: iValues,
        iErrorObj: iErrorObj,
        rootValues: rootValues,
        path: path
      });
    });
  } else if (_.toUpper(iTemplate.type) === _FIELD_TYPES.TEXT || _.toUpper(iTemplate.type) === _FIELD_TYPES.TEXTSELECTION || _.toUpper(iTemplate.type) === _FIELD_TYPES.TEXTFIELD || _.toUpper(iTemplate.type) === _FIELD_TYPES.TEXTAREA || _.toUpper(iTemplate.type) === _FIELD_TYPES.QRADIOGROUP || _.toUpper(iTemplate.type) === _FIELD_TYPES.DATEPICKER || _.toUpper(iTemplate.type) === _FIELD_TYPES.CHECKBOX || _.toUpper(iTemplate.type) === _FIELD_TYPES.PICKER) {
    var capitalizedTypeString = _.toUpper(iTemplate.type);
    _.set(iErrorObj, iTemplate.id, validateMandatoryForApplicationForm({
      field: {
        type: capitalizedTypeString,
        mandatory: iTemplate.mandatory,
        disabled: !!iTemplate.disabled,
        id: iTemplate.id || "",
        validation: iTemplate.validation
      },
      value: capitalizedTypeString === _FIELD_TYPES.DATEPICKER ? iValues : iValues[iTemplate.id] || ""
    }));
  }
}

function validateApplicationInitPaymentMethod(value) {
  var _message42;

  var initPayMethod = value.initPayMethod,
      trxStatus = value.trxStatus,
      trxTime = value.trxTime,
      trxNo = value.trxNo;

  if (!_.isEmpty(initPayMethod)) {
    var _message41;

    if (["crCard", "eNets", "dbsCrCardIpp"].indexOf(initPayMethod) >= 0) {
      if (!trxNo || !trxTime || trxStatus !== "Y") {
        var _message40;

        return {
          hasError: true,
          message: (_message40 = {}, _defineProperty(_message40, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.1002"]), _defineProperty(_message40, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.1002"]), _message40)
        };
      }
    }
    return {
      hasError: false,
      message: (_message41 = {}, _defineProperty(_message41, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message41, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message41)
    };
  }

  return {
    hasError: true,
    message: (_message42 = {}, _defineProperty(_message42, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.302"]), _defineProperty(_message42, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.302"]), _message42)
  };
}
/**
 * @default indicate button should disable or not
 * @param {object} clientForm - reducer clientForm state
 * @return {boolean} disable status
 * */
function isClientFormHasError(clientForm) {
  var hasError = false;
  var errorFields = [];
  Object.keys(clientForm).forEach(function (key) {
    if (_.isObjectLike(clientForm[key]) && _.has(clientForm[key], "hasError")) {
      hasError = hasError || clientForm[key].hasError;
      if (clientForm[key].hasError) {
        errorFields.push(key);
      }
    }
  });

  return { hasError: hasError, errorFields: errorFields };
}