"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getCurrency = getCurrency;
exports.getCurrencySign = getCurrencySign;
exports.getOptionTitle = getOptionTitle;
exports.isEmpty = isEmpty;
exports.calcAge = calcAge;
exports.fillTextVariable = fillTextVariable;
exports.getAgeByUnit = getAgeByUnit;
exports.replaceAll = replaceAll;
exports.getEmailContent = getEmailContent;
exports.getCountryWithCityList = getCountryWithCityList;

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _date = require("./date");

var date = _interopRequireWildcard(_date);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function getCurrency(_ref) {
  var value = _ref.value,
      sign = _ref.sign,
      decimals = _ref.decimals;

  if (!_.isNumber(value)) {
    return "-";
  }
  if (!_.isNumber(decimals)) {
    decimals = 2;
  }
  var text = (sign && sign.trim().length > 0 ? sign : "") + parseFloat(value).toFixed(decimals);
  var parts = text.split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return parts.join(".");
}

function getCurrencySign(_ref2) {
  var compCode = _ref2.compCode,
      ccy = _ref2.ccy,
      optionsMap = _ref2.optionsMap;

  var currency = _.find(optionsMap.ccy.currencies, function (c) {
    return c.compCode === compCode;
  });
  if (currency) {
    var option = _.find(currency.options, function (opt) {
      return opt.value === ccy;
    });
    if (option) {
      return option.symbol;
    }
  }
  return "S$";
}

function getOptionTitle(_ref3) {
  var value = _ref3.value,
      optionsMap = _ref3.optionsMap,
      language = _ref3.language;
  var options = optionsMap.options;

  var option = void 0;
  if (options instanceof Array) {
    option = _.find(options, function (o) {
      return o.value === value;
    });
  } else {
    option = _.find(options, function (o, key) {
      return key === value;
    });
  }
  return option && option.title[language];
}

function isEmpty(value) {
  // if type == date, return false
  if (_.isDate(value)) {
    return false;
  } else if (_.isBoolean(value)) {
    return false;
  }
  // if type == number and != 0, return false
  else if (_.isNumber(value) && value !== 0) {
      return false;
    }

  if (_.isPlainObject(value) || _.isArray(value)) {
    var empty = true;
    _.forEach(value, function (v) {
      if (!isEmpty(v)) {
        empty = false;
      }
    });
    return empty;
  }

  return _.isEmpty(value);
}

function calcAge(birthday) {
  var today = new Date();
  var birthDate = new Date(birthday);
  var age = today.getFullYear() - birthDate.getFullYear();
  var m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || m === 0 && today.getDate() < birthDate.getDate()) {
    age -= 1;
  }
  return age;
}

function fillTextVariable(_ref4) {
  var text = _ref4.text,
      data = _ref4.data,
      EABi18n = _ref4.EABi18n,
      language = _ref4.language,
      textStore = _ref4.textStore;

  var returnText = text;

  if (data.length > 0) {
    _.forEach(data, function (item, index) {
      returnText = _.replace(returnText, "{" + (index + 1) + "}", item.isPath ? EABi18n({ path: item.value, language: language, textStore: textStore }) : item.value);
    });
  }

  return returnText;
}

function getAgeByUnit(unit, targetDate, dob) {
  switch (unit) {
    case "day":
      return date.getAttainedAge(targetDate, dob).day;
    case "month":
      return date.getAttainedAge(targetDate, dob).month;
    case "year":
      return date.getAttainedAge(targetDate, dob).year;
    case "nextAge":
      return date.getAgeNextBirthday(targetDate, dob);
    case "nearestAge":
    default:
      return date.getNearestAge(targetDate, dob);
  }
}

function replaceAll(string, search, replacement) {
  return string.split(search).join(replacement);
}

function getEmailContent(content, params, imageCode) {
  var ret = content;
  _.each(params, function (value, key) {
    ret = replaceAll(ret, "{{" + key + "}}", value);
  });
  ret = replaceAll(ret, imageCode, "");
  return ret;
}

function getCountryWithCityList(_ref5) {
  var optionsMap = _ref5.optionsMap,
      optionPath = _ref5.optionPath;

  var countryWithCityList = [];
  var optionList = _.get(optionsMap, optionPath).options;
  optionList.map(function (option) {
    if (option.condition && countryWithCityList.indexOf(option.condition) < 0) {
      countryWithCityList.push(option.condition);
    }
    return true;
  });
  return countryWithCityList;
}