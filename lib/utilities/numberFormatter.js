"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.currencyToNumber = exports.numberToCurrencyForQuotation = exports.numberToCurrency = undefined;

var _numeral = require("numeral");

var _numeral2 = _interopRequireDefault(_numeral);

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _NUMBER_FORMAT = require("../constants/NUMBER_FORMAT");

var NUMBER_FORMAT = _interopRequireWildcard(_NUMBER_FORMAT);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MAX_CURRENCY_VALUE = NUMBER_FORMAT.MAX_CURRENCY_VALUE;
/**
 * numberToCurrency
 * @description convert number into currency format
 * @param {number} value - inputted number
 * @param {number} [decimal=2] - number of decimal in returned string
 * @param {number} [isFocus=false] - number of decimal in returned string
 * @param {number} [maxValue=MAX_CURRENCY_VALUE]
 * @return {string} - string in currency format
 * */

var numberToCurrency = exports.numberToCurrency = function numberToCurrency(_ref) {
  var value = _ref.value,
      _ref$decimal = _ref.decimal,
      decimal = _ref$decimal === undefined ? 2 : _ref$decimal,
      _ref$isFocus = _ref.isFocus,
      isFocus = _ref$isFocus === undefined ? false : _ref$isFocus,
      _ref$maxValue = _ref.maxValue,
      maxValue = _ref$maxValue === undefined ? MAX_CURRENCY_VALUE : _ref$maxValue;

  if (_.isNumber(value)) {
    var trueValue = Number(value) > maxValue ? Number(String(value).substring(0, String(maxValue).length)) : Number(value);
    var format = "0,0";
    var dotFormat = isFocus ? "[.]" : ".";

    if (decimal > 0) {
      format += dotFormat;
      if (isFocus) {
        format += "[";
      }
      format += _.repeat("0", decimal);
      if (isFocus) {
        format += "]";
      }
    }

    return "" + (0, _numeral2.default)(trueValue).format(format);
  }
  return value || "";
};

/**
 * numberToCurrencyForQuotation
 * @description convert number into currency format for Quotation Page
 * @param {number} value - inputted number
 * @param {number} [decimal=2] - number of decimal in returned string
 * @param {number} [isFocus=false] - number of decimal in returned string
 * @param {number} [maxValue=MAX_CURRENCY_VALUE]
 * @return {string} - string in currency format
 * */
var numberToCurrencyForQuotation = exports.numberToCurrencyForQuotation = function numberToCurrencyForQuotation(_ref2) {
  var value = _ref2.value,
      _ref2$decimal = _ref2.decimal,
      decimal = _ref2$decimal === undefined ? 2 : _ref2$decimal,
      _ref2$isFocus = _ref2.isFocus,
      isFocus = _ref2$isFocus === undefined ? false : _ref2$isFocus,
      _ref2$maxValue = _ref2.maxValue,
      maxValue = _ref2$maxValue === undefined ? MAX_CURRENCY_VALUE : _ref2$maxValue;

  if (_.isNumber(value)) {
    var trueValue = Number(value) > maxValue ? Number(String(value).substring(0, String(maxValue).length)) : Number(value);
    var format = "0,0";
    var dotFormat = isFocus ? "[.]" : ".";

    if (decimal > 0) {
      format += dotFormat;
      if (isFocus) {
        format += "[";
      }
      format += _.repeat("0", decimal);
      if (isFocus) {
        format += "]";
      }
    }

    return "" + (0, _numeral2.default)(trueValue).format(format, Math.floor);
  }
  return value || "";
};

/**
 * currencyToNumber
 * @description convert currency format into number
 * @param {string} value - inputted string
 * @param {string} maxValue - maximum value allowed
 * @param {boolean} allowNegative - allow negative number
 * @return {number} - number without format
 * */
var currencyToNumber = exports.currencyToNumber = function currencyToNumber(value) {
  var maxValue = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : MAX_CURRENCY_VALUE;
  var allowNegative = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

  if (_.isString(value)) {
    if (!allowNegative) value = _.replace(value, /[-]/g, "");
    var negativeValue = _.replace(value, /[-]{2,}/, "-");
    var cleanValue = _.replace(negativeValue, /[^0-9,.-]/g, "");

    var valueSplitter = cleanValue.split(".");
    if (valueSplitter.length > 1) {
      cleanValue = valueSplitter[0] + "." + valueSplitter[1];
    }

    cleanValue = cleanValue.replace(/[,]/g, "");
    if (Number(cleanValue) > maxValue) {
      cleanValue = cleanValue.substring(0, maxValue.toString().length);
    }

    return (0, _numeral2.default)(cleanValue.slice(-1) === "." ? cleanValue.slice(0, cleanValue.length - 1) : cleanValue).value();
  }
  return value;
};