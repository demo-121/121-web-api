"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function (_ref) {
  var contactList = _ref.contactList,
      isViewAll = _ref.isViewAll,
      searchText = _ref.searchText;

  var returnArray = [];

  contactList.forEach(function (client) {
    var _client$idCardNo = client.idCardNo,
        idCardNo = _client$idCardNo === undefined ? "" : _client$idCardNo,
        _client$fullName = client.fullName,
        fullName = _client$fullName === undefined ? "" : _client$fullName,
        _client$mobileNo = client.mobileNo,
        mobileNo = _client$mobileNo === undefined ? "" : _client$mobileNo,
        _client$email = client.email,
        email = _client$email === undefined ? "" : _client$email;


    if (isViewAll || searchText.length > 1 && (_.toUpper(idCardNo).indexOf(_.toUpper(searchText)) > -1 || _.toUpper(fullName).indexOf(_.toUpper(searchText)) > -1 || _.toUpper(mobileNo).indexOf(_.toUpper(searchText)) > -1 || _.toUpper(email).indexOf(_.toUpper(searchText)) > -1)) {
      returnArray.push(client);
    }
  });

  return _.orderBy(returnArray, [function (item) {
    return _.toUpper(item.fullName);
  }], "asc");
};

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }