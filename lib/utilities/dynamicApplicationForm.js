"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initValidateApplicationFormError = exports.updateFieldValue = exports.getItemValueFunc = exports.getPathFunc = undefined;
exports.calculateRopTable = calculateRopTable;

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _validation = require("../utilities/validation");

var validation = _interopRequireWildcard(_validation);

var _ROP_TABLE = require("../constants/ROP_TABLE");

var ROP_TABLE = _interopRequireWildcard(_ROP_TABLE);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var REPLACE_POLICY_TYPE_AXA = ROP_TABLE.REPLACE_POLICY_TYPE_AXA,
    REPLACE_POLICY_TYPE_OTHER = ROP_TABLE.REPLACE_POLICY_TYPE_OTHER;
var getPathFunc = exports.getPathFunc = function getPathFunc(_ref) {
  var idArr = _ref.idArr,
      valuePath = _ref.valuePath;

  var originalIdArr = valuePath.split(".");
  var newIdCount = idArr.length;
  var resultIdArr = [].concat(_toConsumableArray(originalIdArr.slice(0, -newIdCount + 1)), _toConsumableArray(idArr));
  var result = _.join(resultIdArr, ".");

  return result;
};

var getItemValueFunc = exports.getItemValueFunc = function getItemValueFunc(_ref2) {
  var rootValues = _ref2.rootValues,
      valuePath = _ref2.valuePath,
      id = _ref2.id;

  if (id && id.indexOf("/") !== -1) {
    var idToSplit = id.indexOf("@") === 0 ? id.substring(1) : id;
    var idArr = idToSplit.split("/");
    var _path = getPathFunc({ idArr: idArr, valuePath: valuePath });
    return _.get(rootValues, _path);
  }
  var path = valuePath + "." + id;
  return _.get(rootValues, path);
};

var updateFieldValue = exports.updateFieldValue = function updateFieldValue(_ref3) {
  var iTemplate = _ref3.iTemplate,
      rootValues = _ref3.rootValues,
      path = _ref3.path,
      id = _ref3.id,
      pathValue = _ref3.pathValue;

  var itemPath = path + "." + id;
  _.set(rootValues, itemPath, pathValue);

  if (_.has(iTemplate, "valueTrigger")) {
    var valueTriggerObj = _.get(iTemplate, "valueTrigger", {});

    if (_.get(valueTriggerObj, "type", "") === "resetTargetValue") {
      var targetIdArr = _.split(_.get(valueTriggerObj, "id", ""), ",");
      var validateArr = _.split(_.get(valueTriggerObj, "value", ""), ",");
      if (pathValue === validateArr[0]) {
        _.each(targetIdArr, function (targetId) {
          _.set(rootValues, path + "." + targetId, validateArr[1]);
        });
      }
    }
  }
};

var initValidateApplicationFormError = exports.initValidateApplicationFormError = function initValidateApplicationFormError(_ref4) {
  var template = _ref4.template,
      rootValues = _ref4.rootValues,
      errorObj = _ref4.errorObj;

  var values = _.get(rootValues, "applicationForm.values", {});
  var applicationFormPageIds = ["residency", "insurability", "declaration"];
  var menuTemplate =
  // Shield case & non-Shield case
  _.toUpper(_.get(template, "items[0].type", "")) === "MENU" ? _.get(template, "items[0]") : _.get(template, "items[0].items[0]");
  var mapping = {
    residency: {
      template: _.get(menuTemplate, "items[0].items[1].items[0]", {})
    },
    insurability: {
      template: _.get(menuTemplate, "items[0].items[2].items[0]", {})
    },
    declaration: {
      template: _.get(menuTemplate, "items[2].items[0].items[0]", {})
    }
  };

  if (_.has(values, "proposer") && !_.isEmpty(values, "proposer")) {
    _.each(applicationFormPageIds, function (pageId) {
      var pageTemplate = _.get(mapping, pageId + ".template");
      var profileTemplate = _.find(pageTemplate.items, function (item) {
        return item.subType === "proposer";
      });
      if (!_.isUndefined(profileTemplate)) {
        _.each(_.get(profileTemplate, "items"), function (sectionTemplate) {
          var sectionId = sectionTemplate.id;
          var path = "applicationForm.values.proposer." + sectionId;
          if (_.isUndefined(_.get(errorObj, path))) {
            _.set(errorObj, path, {});
          }
          validation.validateApplicationFormTemplate({
            iTemplate: sectionTemplate,
            iValues: _.get(rootValues, path, {}),
            iErrorObj: _.get(errorObj, path),
            rootValues: rootValues,
            path: path
          });
        });
      }
    });
  }

  if (_.has(values, "insured") && !_.isEmpty(values, "insured")) {
    _.each(values.insured, function (insured, index) {
      _.each(applicationFormPageIds, function (pageId) {
        var pageTemplate = _.get(mapping, pageId + ".template");
        var arrProfileTemplate = _.filter(pageTemplate.items, function (item) {
          return item.subType === "insured";
        });
        if (!_.isUndefined(arrProfileTemplate)) {
          var profileTemplate = arrProfileTemplate[index];
          if (!_.isUndefined(profileTemplate)) {
            _.each(_.get(profileTemplate, "items"), function (sectionTemplate) {
              var sectionId = sectionTemplate.id;
              var path = "applicationForm.values.insured[" + index + "]." + sectionId;
              if (_.isUndefined(_.get(errorObj, path))) {
                _.set(errorObj, path, {});
              }
              validation.validateApplicationFormTemplate({
                iTemplate: sectionTemplate,
                iValues: _.get(rootValues, path, {}),
                iErrorObj: _.get(errorObj, path),
                rootValues: rootValues,
                path: path
              });
            });
          }
        }
      });
    });
  }
};

function calculateRopTable(_ref5) {
  var rootValues = _ref5.rootValues,
      path = _ref5.path,
      id = _ref5.id,
      pathValue = _ref5.pathValue;

  if (!_.isString(id)) {
    return;
  }

  if ([REPLACE_POLICY_TYPE_AXA, REPLACE_POLICY_TYPE_OTHER].indexOf(id) > -1) {
    _.set(rootValues, path + "." + id, pathValue);
  } else if (_.isNumber(pathValue)) {
    if (pathValue < 0) {
      return;
    }

    var values = _.get(rootValues, path, {});
    var relatedId = void 0;
    var relatedTotalId = void 0;

    if (id.indexOf("AXA") > -1) {
      relatedId = _.replace(id, "AXA", "Other");
      relatedTotalId = _.replace(id, "AXA", "");
    } else if (id.indexOf("Other") > -1) {
      relatedId = _.replace(id, "Other", "AXA");
      relatedTotalId = _.replace(id, "Other", "");
    }

    if (id.indexOf("exist") > -1 || id.indexOf("replace") > -1) {
      var relatedTotalValue = _.get(values, relatedTotalId, 0);
      if (pathValue > relatedTotalValue) {
        return;
      }
      _.set(values, id, pathValue);
      if (_.isNumber(relatedTotalValue)) {
        _.set(values, relatedId, relatedTotalValue - pathValue);
      }
    } else if (id.indexOf("pending") > -1) {
      var relatedValue = _.get(values, relatedId, 0);
      _.set(values, id, pathValue);
      if (_.isNumber(relatedValue)) {
        _.set(values, relatedTotalId, relatedValue + pathValue);
      }
    }

    _.set(rootValues, path, values);
  }
}