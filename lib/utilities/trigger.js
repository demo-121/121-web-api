"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setDisable = exports.setMandatory = exports.show = undefined;

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

exports.optionCondition = optionCondition;

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _common = require("./common");

var _date = require("./date");

var _dynamicApplicationForm = require("./dynamicApplicationForm");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

/**
 * Special handling for Dynamic Condition
 */
var checkTriggerForDynamicCondition = function checkTriggerForDynamicCondition(_ref) {
  var trigger = _ref.trigger,
      rootValues = _ref.rootValues,
      valuePath = _ref.valuePath;
  var id = trigger.id,
      triggerValue = trigger.value;

  var idAddressArr = id.split(",");
  var itemValue = idAddressArr.map(function (idAddress) {
    if (idAddress.indexOf("@") === 0 && idAddress.indexOf("@spouse") === -1) {
      var idArr = idAddress.slice(1).split(".");
      var path = (0, _dynamicApplicationForm.getPathFunc)({ idArr: idArr, valuePath: valuePath });
      return _.get(rootValues, path, "");
    } else if (idAddress.indexOf("#") === 0) {
      var _idArr = idAddress.slice(1).split(".");
      var valuePathTemp = valuePath.split(".").splice(0, 2);
      var resultIdArr = valuePathTemp.concat(_idArr);
      var _path = resultIdArr.join(".");
      return _.get(rootValues, _path, "");
    }
    return "";
  });
  var newTriggerValue = triggerValue;
  _.each(itemValue, function (value, index) {
    var reg = new RegExp("#" + (index + 1) + "\\b", "g");
    newTriggerValue = newTriggerValue.replace(reg, "itemValue[" + index + "]");
  });
  return eval(newTriggerValue);
  // return false;
};

var checkTriggerForComparePremium = function checkTriggerForComparePremium(_ref2) {
  var trigger = _ref2.trigger,
      rootValues = _ref2.rootValues,
      valuePath = _ref2.valuePath,
      itemValue = _ref2.itemValue;
  var ccys = trigger.ccys,
      id = trigger.id;

  var validRefValues = (0, _dynamicApplicationForm.getItemValueFunc)({
    rootValues: rootValues,
    valuePath: valuePath,
    id: id
  });
  var values = _.get(rootValues, valuePath);
  var ccy = values.ccy || (validRefValues.extra ? validRefValues.extra.ccy : null) || "SGD";
  var result = false;
  for (var i = 0; i < ccys.length; i += 1) {
    if (ccy === ccys[i].ccy) {
      if (itemValue >= ccys[i].amount) result = true;
      break;
    }
  }
  return result;
};

var checkTrigger = function checkTrigger(_ref3) {
  var trigger = _ref3.trigger,
      rootValues = _ref3.rootValues,
      valuePath = _ref3.valuePath;
  var type = trigger.type,
      id = trigger.id,
      triggerValue = trigger.value;

  if (type === "dynamicCondition") {
    return checkTriggerForDynamicCondition({ trigger: trigger, rootValues: rootValues, valuePath: valuePath });
  }
  // This type trigger does not mean show/hide function
  else if (type === "excludeSG") {
      return true;
    } else if (type === "showIfNotExist") {
      return true;
    }

  var itemValue = (0, _dynamicApplicationForm.getItemValueFunc)({ rootValues: rootValues, valuePath: valuePath, id: id });
  if (type === "empty") {
    return !itemValue;
  }
  if (_.isUndefined(itemValue)) {
    return false;
  }
  if (type === "showIfEqual") {
    return _.toUpper(itemValue) === _.toUpper(triggerValue);
  } else if (type === "showIfNotEqual") {
    return _.toUpper(itemValue) !== _.toUpper(triggerValue);
  } else if (type === "showIfExist") {
    var triggerValueArr = _.toUpper(triggerValue).split(",");
    return _.includes(triggerValueArr, _.toUpper(itemValue));
  } else if (type === "notEmpty") {
    return !_.isEmpty(itemValue);
  } else if (type === "showIfSmaller") {
    return _.isNumber(itemValue) && itemValue < triggerValue;
  } else if (type === "showIfGreater") {
    return _.isNumber(itemValue) && itemValue > triggerValue;
  } else if (type === "comparePremium") {
    return checkTriggerForComparePremium({
      trigger: trigger,
      rootValues: rootValues,
      valuePath: valuePath,
      itemValue: itemValue
    });
  } else if (type === "showIfWithinSixMonthOld") {
    var days = (0, _date.getAttainedAge)(new Date(), new Date(itemValue)).day;
    return days < 184 && days >= 0;
  }
  return true;
};

/**
 * This function is to find whether the component should be displayed or not
 * It depends on the component's template Json's "trigger" field
 */
var show = exports.show = function show(_ref4) {
  var template = _ref4.template,
      rootValues = _ref4.rootValues,
      valuePath = _ref4.valuePath;

  if (!_.has(template, "trigger")) {
    return true;
  }
  var trigger = template.trigger;

  // trigger can be array of objects or object

  if (_.isArray(trigger)) {
    if (template.triggerType === "and") {
      return !_.some(trigger, function (itemTrigger) {
        return !checkTrigger({
          trigger: itemTrigger,
          rootValues: rootValues,
          valuePath: valuePath
        });
      });
    }
    return _.some(trigger, function (itemTrigger) {
      return checkTrigger({ trigger: itemTrigger, rootValues: rootValues, valuePath: valuePath });
    });
  }
  return checkTrigger({ trigger: trigger, rootValues: rootValues, valuePath: valuePath });
};

var setMandatory = exports.setMandatory = function setMandatory(_ref5) {
  var template = _ref5.template,
      rootValues = _ref5.rootValues,
      valuePath = _ref5.valuePath;

  if (_.has(template, "mandatoryTrigger")) {
    var trigger = _.get(template, "mandatoryTrigger");
    var triggerId = _.get(trigger, "id");
    var triggerValue = _.get(trigger, "value");
    var triggerType = _.toUpper(_.get(trigger, "type"));
    var value = _.get(rootValues, valuePath + "." + triggerId);

    _.set(template, "mandatory", false);
    if (triggerType === _.toUpper("trueIfContains")) {
      if (_.includes(triggerValue, value)) {
        _.set(template, "mandatory", true);
      }
    } else if (triggerType === _.toUpper("trueIfNotContains")) {
      if (!_.includes(triggerValue, value)) {
        _.set(template, "mandatory", true);
      }
    } else if (triggerType === _.toUpper("trueIfEmpty")) {
      if (_.isUndefined(value) || value === "" || value !== "A" && value !== "B") {
        _.set(template, "mandatory", true);
      } else {
        delete template.mandatory;
      }
    } else if (triggerType === _.toUpper("falseIfContains")) {
      if (_.includes(triggerValue, value)) {
        _.set(template, "mandatory", false);
      } else {
        _.set(template, "mandatory", true);
      }
    }
  }
};

var setDisable = exports.setDisable = function setDisable(_ref6) {
  var template = _ref6.template,
      rootValues = _ref6.rootValues,
      valuePath = _ref6.valuePath,
      optionsMap = _ref6.optionsMap;
  var disableTrigger = template.disableTrigger;

  if (disableTrigger) {
    var triggerId = disableTrigger.id,
        triggerValue = disableTrigger.value,
        type = disableTrigger.type;

    var triggerType = type.toUpperCase();

    if (triggerType === "AnyFieldMatch".toUpperCase()) {
      var ids = triggerId.split(",");
      template.disabled = _.some(ids, function (id) {
        return (0, _dynamicApplicationForm.getItemValueFunc)({
          rootValues: rootValues,
          valuePath: valuePath,
          id: id
        }) === triggerValue;
      });
    } else {
      var value = (0, _dynamicApplicationForm.getItemValueFunc)({
        rootValues: rootValues,
        valuePath: valuePath,
        id: triggerId
      });

      if (triggerType === "trueIfEqual".toUpperCase()) {
        if (triggerValue === value) {
          template.disabled = true;
        } else {
          template.disabled = false;
        }
      } else if (triggerType === "trueIfContains".toUpperCase()) {
        var checkValue = triggerValue;
        if (_.isString(triggerValue) && triggerValue.toLowerCase() === "city") {
          checkValue = (0, _common.getCountryWithCityList)({
            optionsMap: optionsMap,
            optionPath: triggerValue.toLowerCase()
          });
          template.disableTrigger.value = checkValue;
        }

        if (checkValue.indexOf(value) > -1) {
          template.disabled = true;
        } else {
          template.disabled = false;
        }
      } else if (triggerType === "trueIfNotContains".toUpperCase()) {
        var _checkValue = triggerValue;
        if (_.isString(triggerValue) && triggerValue.toLowerCase() === "city") {
          _checkValue = (0, _common.getCountryWithCityList)({
            optionsMap: optionsMap,
            optionPath: triggerValue.toLowerCase()
          });
          template.disableTrigger.value = _checkValue;
        }

        if (_checkValue.indexOf(value) < 0) {
          template.disabled = true;
        } else {
          template.disabled = false;
        }
      } else if (triggerType === "trueIfNotEqual".toUpperCase()) {
        if (triggerValue !== value) {
          template.disabled = true;
        } else {
          template.disabled = false;
        }
      }
    }
  }
};

function optionCondition(_ref7) {
  var value = _ref7.value,
      options = _ref7.options,
      optionsMap = _ref7.optionsMap,
      showIfNoValue = _ref7.showIfNoValue;

  if (showIfNoValue) {
    return true;
  }
  var option = void 0;
  if (_.isString(options)) {
    var _$at = _.at(optionsMap, options + ".options");

    var _$at2 = _slicedToArray(_$at, 1);

    option = _$at2[0];
  }
  return !!(option && _.filter(option, function (opt) {
    return opt.condition === value;
  }).length);
}