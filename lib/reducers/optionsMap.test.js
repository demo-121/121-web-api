"use strict";

var _optionsMap = require("../assets/cache/optionsMap");

var _optionsMap2 = _interopRequireDefault(_optionsMap);

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

var _optionsMap3 = require("./optionsMap");

var _optionsMap4 = _interopRequireDefault(_optionsMap3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

describe("optionsMap reducer", function () {
  it("should return the initial state", function () {
    expect((0, _optionsMap4.default)(undefined, {})).toEqual(_optionsMap2.default);
  });
});

it("should handle UPDATE_OPTIONS_MAP", function () {
  expect((0, _optionsMap4.default)({}, {
    type: _ACTION_TYPES2.default[_REDUCER_TYPES.OPTIONS_MAP].UPDATE_OPTIONS_MAP,
    optionsMapData: {
      ccy: {
        type: "layout",
        currencies: [{
          compCode: "01",
          default: "ALL",
          options: [{
            value: "ALL",
            title: {
              en: "All Currencies",
              "zh-hant": "æ‰€æœ‰è²¨å¹£"
            },
            seq: 1
          }, {
            value: "SGD",
            title: {
              en: "SGD",
              "zh-hant": "SGD"
            },
            exRate: 1,
            symbol: "S$",
            seq: 2
          }, {
            value: "USD",
            title: {
              en: "USD",
              "zh-hant": "USD"
            },
            exRate: 0.718548,
            symbol: "US$",
            seq: 3
          }, {
            value: "GBP",
            title: {
              en: "GBP",
              "zh-hant": "GBP"
            },
            exRate: 0.563038,
            symbol: "Â£",
            seq: 4
          }, {
            value: "EUR",
            title: {
              en: "EUR",
              "zh-hant": "EUR"
            },
            exRate: 0.645927,
            symbol: "â‚¬",
            seq: 5
          }, {
            value: "AUD",
            title: {
              en: "AUD",
              "zh-hant": "AUD"
            },
            exRate: 0.941408,
            symbol: "A$",
            seq: 6
          }]
        }]
      }
    }
  })).toEqual({
    ccy: {
      type: "layout",
      currencies: [{
        compCode: "01",
        default: "ALL",
        options: [{
          value: "ALL",
          title: {
            en: "All Currencies",
            "zh-hant": "æ‰€æœ‰è²¨å¹£"
          },
          seq: 1
        }, {
          value: "SGD",
          title: {
            en: "SGD",
            "zh-hant": "SGD"
          },
          exRate: 1,
          symbol: "S$",
          seq: 2
        }, {
          value: "USD",
          title: {
            en: "USD",
            "zh-hant": "USD"
          },
          exRate: 0.718548,
          symbol: "US$",
          seq: 3
        }, {
          value: "GBP",
          title: {
            en: "GBP",
            "zh-hant": "GBP"
          },
          exRate: 0.563038,
          symbol: "Â£",
          seq: 4
        }, {
          value: "EUR",
          title: {
            en: "EUR",
            "zh-hant": "EUR"
          },
          exRate: 0.645927,
          symbol: "â‚¬",
          seq: 5
        }, {
          value: "AUD",
          title: {
            en: "AUD",
            "zh-hant": "AUD"
          },
          exRate: 0.941408,
          symbol: "A$",
          seq: 6
        }]
      }]
    }
  });
});