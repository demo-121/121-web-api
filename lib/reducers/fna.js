"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _traverse = require("traverse");

var _traverse2 = _interopRequireDefault(_traverse);

var _redux = require("redux");

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

var _FIELD_TYPES = require("../constants/FIELD_TYPES");

var _NEEDS = require("../constants/NEEDS");

var _DEPENDANT = require("../constants/DEPENDANT");

var _naUtilities = require("../utilities/naUtilities");

var _needsUtil = require("../utilities/needsUtil");

var _validation = require("../utilities/validation");

var _locales = require("../locales");

var _locales2 = _interopRequireDefault(_locales);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// =============================================================================
// support functions
// =============================================================================
var addAspect = function addAspect(need, aspects) {
  var aspectsArr = aspects.split(",");
  var index = aspectsArr.indexOf(need);
  if (index === -1) {
    aspectsArr.push(need);
  }
  aspectsArr = aspectsArr.filter(function (x) {
    return x !== "";
  });
  return aspectsArr.join();
};

var removeAspect = function removeAspect(need, aspects) {
  var aspectsArr = aspects.split(",");
  var index = aspectsArr.indexOf(need);
  if (index > -1) {
    aspectsArr.splice(index, 1);
  }
  return aspectsArr.join();
};

var genNeedsAnalysisNewData = function genNeedsAnalysisNewData(state, value) {
  var newState = _.cloneDeep(state);

  if (value.targetProfile !== "owner" && value.targetProfile !== "spouse" && state[value.targetProduct].dependants) {
    // set dependants new data
    var targetObj = {};
    var targetIndex = null;
    state[value.targetProduct].dependants.some(function (data, index) {
      if (data.cid === value.targetProfile) {
        targetObj = data;
        targetIndex = index;
        return true;
      }
      return false;
    });
    if (targetObj === {} || targetIndex === null) {
      return state;
    }
    var cloneDependants = _.cloneDeep(state[value.targetProduct].dependants);
    _.set(cloneDependants, "[" + targetIndex + "]", Object.assign({}, targetObj, value.targetData));

    var newTargetAspect = _.cloneDeep(state[value.targetProduct]);
    _.set(newTargetAspect, "dependants", cloneDependants);
    _.set(newState, "" + value.targetProduct, newTargetAspect);
    _.set(newState, "isCompleted", false);
  } else {
    // set owner & spouse new data
    var aspectData = _.get(state, value.targetProduct + "." + value.targetProfile, {});
    var newAspectData = Object.assign({}, aspectData, value.targetData);
    _.set(newState, value.targetProduct + "." + value.targetProfile, newAspectData);
    _.set(newState, "isCompleted", false);
  }
  return newState;
};

var feHandler = function feHandler(_ref) {
  var feData = _ref.feData,
      profileData = _ref.profileData,
      pdaData = _ref.pdaData,
      dependantProfilesData = _ref.dependantProfilesData;

  // if pdaData is not ready, skip the initialize
  if (pdaData.isCompleted) {
    var newFEData = _.cloneDeep(feData);

    // owner
    newFEData.owner = (0, _needsUtil.feDataGetter)({
      profileData: profileData,
      dependantProfilesData: dependantProfilesData,
      fe: newFEData,
      relationshipObject: {
        cid: profileData.cid,
        relationship: "owner"
      }
    });

    // spouse
    var spouseData = _.find(profileData.dependants, function (dependant) {
      return dependant.relationship === "SPO";
    });
    if (pdaData.applicant === "joint" && !_.isEmpty(spouseData)) {
      newFEData.spouse = (0, _needsUtil.feDataGetter)({
        profileData: profileData,
        dependantProfilesData: dependantProfilesData,
        fe: newFEData,
        relationshipObject: {
          cid: spouseData.cid,
          relationship: "SPO"
        }
      });
    }

    // dependants
    if (pdaData.dependants !== "") {
      pdaData.dependants.split(",").forEach(function (cid) {
        var dependantData = _.find(profileData.dependants, function (dependant) {
          return dependant.cid === cid;
        });

        // filter rule
        if (dependantData && (dependantData.relationship === "SON" || dependantData.relationship === "DAU")) {
          var dependants = _.cloneDeep(newFEData.dependants) || [];

          var index = _.findIndex(dependants, function (dependant) {
            return dependant.cid === dependantData.cid;
          });
          var dependantFEData = (0, _needsUtil.feDataGetter)({
            profileData: profileData,
            dependantProfilesData: dependantProfilesData,
            fe: newFEData,
            relationshipObject: dependantData
          });
          if (index !== -1) {
            dependants[index] = dependantFEData;
          } else {
            dependants.push(dependantFEData);
          }

          newFEData.dependants = dependants;
        }
      });
    }

    return newFEData;
  }
  return feData;
};

var nadHandler = function nadHandler(_ref2) {
  var naData = _ref2.naData,
      pdaData = _ref2.pdaData,
      profileData = _ref2.profileData,
      dependantProfilesData = _ref2.dependantProfilesData,
      feData = _ref2.feData,
      state = _ref2.state;

  // if feData is not ready, skip the initialize
  if (feData.isCompleted) {
    return (0, _naUtilities.naDataGetter)({
      naData: naData,
      pdaData: pdaData,
      profileData: profileData,
      dependantProfilesData: dependantProfilesData,
      feData: feData,
      shouldInitProtection: naData.iarRate !== state.iarRate
    });
  }
  return naData;
};
// =============================================================================
// initial states
// =============================================================================
var naState = {};
var isNaErrorState = {};
// =============================================================================
// reducers
// =============================================================================
var pda = function pda() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var _ref3 = arguments[1];
  var type = _ref3.type,
      newPda = _ref3.newPda;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_FNA:
      return Object.assign({}, state, Object.assign({}, newPda));
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_PDA:
      return Object.assign({}, state, Object.assign({}, newPda));
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].GET_PDA:
      state = newPda;
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_PDA_APPLICATE:
      return Object.assign({}, state, {
        applicant: newPda ? "single" : "joint"
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_PDA_DEPENDANTS:
      return Object.assign({}, state, {
        dependants: newPda
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_PDA_ISCOMPLETED:
      return Object.assign({}, state, {
        isCompleted: newPda
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_PDA_LAST_STEP_INDEX:
      return Object.assign({}, state, {
        lastStepIndex: newPda
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_APPLICANT_HAS_CHANGED:
      return Object.assign({}, state, {
        applicantHasChanged: newPda
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_CONSENT_NOTICES_HAS_CHANGED:
      return Object.assign({}, state, {
        consentNoticesHasChanged: newPda
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_PDA_OWNERCONSENTMETHOD:
      return Object.assign({}, state, {
        ownerConsentMethod: newPda
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_PDA_SPOUSECONSENTMETHOD:
      return Object.assign({}, state, {
        spouseConsentMethod: newPda
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_PDA_TRUSTINDIVIDUAL:
      return Object.assign({}, state, {
        trustedIndividual: newPda
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].SAVE_FNA_COMPLETE:
      return newPda !== undefined ? newPda : state;
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return {};
    default:
      return state;
  }
};
/**
 * trustedIndividual
 * @description trustedIndividual
 * @default ""
 * */
var trustedIndividual = function trustedIndividual() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref4 = arguments[1];
  var type = _ref4.type,
      newTrustedIndividual = _ref4.newTrustedIndividual;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].LOAD_TRUSTED_INDIVIDUAL:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].TRUSTED_INDIVIDUAL_ON_CHANGE:
      return newTrustedIndividual;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].CLEAN_TRUSTED_INDIVIDUAL_FORM:
      return "";
    default:
      return state;
  }
};

var na = function na() {
  var _message;

  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : naState;
  var _ref5 = arguments[1];
  var type = _ref5.type,
      value = _ref5.value,
      profileData = _ref5.profileData,
      dependantProfilesData = _ref5.dependantProfilesData,
      naData = _ref5.naData,
      isNaErrorData = _ref5.isNaErrorData,
      pdaData = _ref5.pdaData,
      feData = _ref5.feData,
      fnaData = _ref5.fnaData,
      ckaData = _ref5.ckaData,
      lastStepIndex = _ref5.lastStepIndex,
      completedStep = _ref5.completedStep,
      newIarRateNotMatchReason = _ref5.newIarRateNotMatchReason,
      newIsIarRateNotMatchReasonError = _ref5.newIsIarRateNotMatchReasonError,
      newCourse = _ref5.newCourse,
      newInstitution = _ref5.newInstitution,
      newStudyPeriodEndYear = _ref5.newStudyPeriodEndYear,
      birthYear = _ref5.birthYear,
      newCollectiveInvestment = _ref5.newCollectiveInvestment,
      newTransactionType = _ref5.newTransactionType,
      newInsuranceInvestment = _ref5.newInsuranceInvestment,
      newInsuranceType = _ref5.newInsuranceType,
      newProfession = _ref5.newProfession,
      newYears = _ref5.newYears,
      newPassCka = _ref5.newPassCka,
      newRiskPotentialReturn = _ref5.newRiskPotentialReturn,
      newAvgAGReturn = _ref5.newAvgAGReturn,
      newSmDroped = _ref5.newSmDroped,
      newAlofLosses = _ref5.newAlofLosses,
      newExpInvTime = _ref5.newExpInvTime,
      newInvPref = _ref5.newInvPref,
      newSelfSelectedRiskLevel = _ref5.newSelfSelectedRiskLevel,
      newSelfRLReasonRP = _ref5.newSelfRLReasonRP,
      newAssessedRL = _ref5.newAssessedRL,
      isValid = _ref5.isValid,
      ckaIsValid = _ref5.ckaIsValid,
      raIsValid = _ref5.raIsValid,
      isCompleted = _ref5.isCompleted,
      newInit = _ref5.newInit;

  var errorState = {
    hasError: false,
    message: (_message = {}, _defineProperty(_message, _locales.LANGUAGE_TYPES.ENGLISH, ""), _defineProperty(_message, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, ""), _message)
  };

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].INIT_NA_ASPECTS:
      {
        if (state.completedStep) {
          completedStep = _.cloneDeep(state.completedStep);
        } else {
          completedStep = -1;
        }

        if (value === "") {
          completedStep = -1;
        } else if (completedStep <= 0) {
          completedStep = 0;
        }

        return Object.assign({}, state, {
          aspects: value,
          completedStep: completedStep,
          lastStepIndex: 0
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_FNA:
      {
        // if feData is not ready, skip the initialize
        if (feData.isCompleted) {
          return (0, _naUtilities.naDataGetter)({
            naData: fnaData,
            pdaData: pdaData,
            profileData: profileData,
            dependantProfilesData: dependantProfilesData,
            feData: feData
          });
        }
        return fnaData;
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA:
      return nadHandler({
        naData: naData,
        pdaData: pdaData,
        profileData: profileData,
        dependantProfilesData: dependantProfilesData,
        feData: feData,
        state: state
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_VALID_STATUS:
      {
        var newNaData = _.cloneDeep(naData);
        var protectionList = ["fiProtection", "diProtection", "ciProtection", "rPlanning", "pcHeadstart", "paProtection", "hcProtection", "ePlanning", "other", "psGoals"];
        var analysisIsCompleted = true;
        var updateData = {};

        protectionList.forEach(function (protection) {
          var protectionError = isNaErrorData[protection];
          var protectionHasError = false;

          Object.keys(protectionError).forEach(function (dependantKey) {
            var dependantError = protectionError[dependantKey];

            Object.keys(dependantError).forEach(function (key) {
              var errorData = protectionError[dependantKey][key];

              // some error is array, so need special handling
              if (Array.isArray(errorData)) {
                if (protectionError[dependantKey].goals) {
                  // loop goals
                  protectionError[dependantKey].goals.forEach(function (goal, goalIndex) {
                    var goalError = errorData[goalIndex];

                    Object.keys(goalError).forEach(function (goalErrorKey) {
                      protectionHasError = goalError[goalErrorKey].hasError || protectionHasError;

                      // loop goals assets
                      if (goalErrorKey === "assets") {
                        goalError.assets.forEach(function (goalAsset) {
                          Object.keys(goalAsset).forEach(function (goalAssetKey) {
                            protectionHasError = goalAsset[goalAssetKey].hasError || protectionHasError;
                          });
                        });
                      }
                    });
                  });
                }
                if (protectionError[dependantKey].assets) {
                  // loop assets
                  protectionError[dependantKey].assets.forEach(function (asset, assetIndex) {
                    var assetError = errorData[assetIndex];

                    Object.keys(assetError).forEach(function (assetErrorKey) {
                      protectionHasError = assetError[assetErrorKey].hasError || protectionHasError;
                    });
                  });
                }
              } else {
                protectionHasError = errorData.hasError || protectionHasError;
              }
            });
          });

          updateData[protection] = Object.assign({}, newNaData[protection], {
            isValid: !protectionHasError
          });
          analysisIsCompleted = analysisIsCompleted && !protectionHasError;
        });
        if (newNaData.completedStep === 1 && analysisIsCompleted) {
          newNaData.completedStep = 2;
        }
        if (newNaData.completedStep >= 2 && !analysisIsCompleted) {
          newNaData.completedStep = 1;
        }

        /* update productType isValid */
        var productTypeIsValid = true;
        if (newNaData.productType.lastProdType.replace(/,/g, "") !== "") {
          productTypeIsValid = productTypeIsValid && true;
        }
        if (newNaData.productType.prodType.replace(/,/g, "") !== "") {
          productTypeIsValid = productTypeIsValid && true;
        }
        updateData.productType = Object.assign({}, newNaData.productType, {
          isValid: productTypeIsValid
        });

        return Object.assign({}, newNaData, updateData);
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].SAVE_FNA_COMPLETE:
      return naData !== undefined ? nadHandler({
        naData: naData,
        pdaData: pdaData,
        profileData: profileData,
        dependantProfilesData: dependantProfilesData,
        feData: feData,
        state: state
      }) : state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_NEEDS:
      {
        if (value.client === _DEPENDANT.OWNER) {
          return Object.assign({}, state, _defineProperty({}, value.need, Object.assign({}, state[value.need], {
            owner: Object.assign({}, state[value.need].owner, {
              isActive: value.isActive,
              cid: value.cid,
              init: false
            })
          })));
        } else if (value.client === _DEPENDANT.SPOUSE) {
          return Object.assign({}, state, _defineProperty({}, value.need, Object.assign({}, state[value.need], {
            spouse: Object.assign({}, state[value.need].spouse, {
              isActive: value.isActive,
              cid: value.cid,
              init: false
            })
          })));
        } else if (value.client === _DEPENDANT.DEPENDANTS) {
          var _$cloneDeep = _.cloneDeep(state[value.need]),
              dependants = _$cloneDeep.dependants;

          if (dependants) {
            var dependantIndex = dependants.findIndex(function (x) {
              return x.cid === value.cid;
            });

            if (dependantIndex > -1) {
              dependants[dependantIndex].isActive = value.isActive;
            } else {
              dependants.push({
                cid: value.cid,
                isActive: value.isActive,
                init: false
              });
            }
            return Object.assign({}, state, _defineProperty({}, value.need, Object.assign({}, state[value.need], {
              dependants: dependants
            })));
          }

          return Object.assign({}, state, _defineProperty({}, value.need, Object.assign({}, state[value.need], {
            dependants: [{
              cid: value.cid,
              isActive: value.isActive,
              init: false
            }]
          })));
        }
        return state;
      }

    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_ASPECTS:
      {
        var aspects = _.cloneDeep(state.aspects);
        var newAspects = void 0;

        var isActive = false;

        if (state[value.need]) {
          (0, _traverse2.default)(state[value.need]).forEach(function (x) {
            if (x && x.isActive) {
              isActive = true;
            }
          });
        }

        if (isActive === true) {
          newAspects = addAspect(value.need, aspects);
        } else {
          newAspects = removeAspect(value.need, aspects);
        }

        if (newAspects === "") {
          completedStep = -1;
        } else {
          completedStep = 0;
        }

        return Object.assign({}, state, {
          aspects: newAspects,
          preNeedsProductsList: newAspects,
          completedStep: completedStep,
          isCompleted: false
        });
      }

    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].IAR_RATE_NOT_MATCH_REASON_ON_CHANGE:
      return Object.assign({}, state, {
        iarRateNotMatchReason: newIarRateNotMatchReason
      });

    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].VALIDATE_IAR_RATE_NOT_MATCH_REASON:
      return Object.assign({}, state, {
        isIarRateNotMatchReasonError: newIsIarRateNotMatchReasonError,
        completedStep: completedStep,
        isCompleted: false
      });

    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_ANALYSIS:
      {
        return Object.assign({}, state, value);
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].INIT_NA_PRIORITY:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_PRIORITY:
      {
        if (state.completedStep) {
          completedStep = _.cloneDeep(state.completedStep);
        } else {
          completedStep = 2;
        }

        if (_.isEmpty(value.sfAspects)) {
          completedStep = 2;
        }

        if (completedStep <= 3 && !_.isEmpty(value.sfAspects) || type === _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_PRIORITY) {
          completedStep = 3;
        }

        return Object.assign({}, state, _.has(value, "sfAspects") ? {
          sfAspects: value.sfAspects
        } : {}, _.has(value, "spAspects") ? {
          spAspects: value.spAspects
        } : {}, {
          completedStep: completedStep,
          isCompleted: type === _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].INIT_NA_PRIORITY && state.isCompleted,
          lastStepIndex: 3
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_COMPLETED_STEP:
      {
        if (value === "") {
          completedStep = 3;
        } else {
          completedStep = 4;
        }

        return Object.assign({}, state, {
          completedStep: completedStep,
          lastStepIndex: 4
        });
      }

    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_PRODUCT_TYPES:
      {
        if (value === "") {
          isValid = false;
        } else {
          isValid = true;
        }

        return Object.assign({}, state, {
          productType: {
            isValid: isValid,
            lastProdType: _.get(state, "productType.lastProdType", value),
            prodType: value
          }
        });
      }

    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_LAST_STEP_INDEX:
      return Object.assign({}, state, {
        lastStepIndex: value
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].INIT_COMPLETED_STEP:
      return Object.assign({}, state, {
        completedStep: completedStep,
        lastStepIndex: lastStepIndex
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_COMPLETED_STEP:
      return Object.assign({}, state, {
        completedStep: completedStep
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_IS_COMPLETED:
      return Object.assign({}, state, {
        isCompleted: isCompleted
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_ANALYSIS_USERDATA:
      return genNeedsAnalysisNewData(state, value);
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].NA_ANALYSIS_USERDATA_VALIDATE:
      return Object.assign({}, state, value);
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_ANALYSIS_USERDATA_ASSETS:
      {
        var _ret = function () {
          var productList = [_NEEDS.FIPROTECTION, _NEEDS.CIPROTECTION, _NEEDS.DIPROTECTION, _NEEDS.PAPROTECTION, _NEEDS.PCHEADSTART, _NEEDS.HCPROTECTION, _NEEDS.RPLANNING, _NEEDS.EPLANNING, _NEEDS.PSGOALS, _NEEDS.OTHER];
          var assetKey = value.targetData.assets[0].key;
          var totalValue = 0;
          var productSet = [];
          for (var i = 0; i < productList.length; i += 1) {
            if (state[productList[i]]) {
              if (state[productList[i]][value.targetProfile]) {
                if (state[productList[i]][value.targetProfile].assets) {
                  var targetIndex = state[productList[i]][value.targetProfile].assets.findIndex(function (ass) {
                    return ass.key === assetKey;
                  });

                  totalValue += parseInt(state[productList[i]][value.targetProfile].assets[targetIndex].calAsset, 10);
                  productSet.push(productList[i]);
                }
              }
            }
          }

          productSet.forEach(function (product) {
            var targetIndex = state[product][value.targetProfile].assets.findIndex(function (ass) {
              return ass.key === assetKey;
            });
            state[product][value.targetProfile].assets[targetIndex].totalValue = totalValue;
          });
          return {
            v: state
          };
        }();

        if ((typeof _ret === "undefined" ? "undefined" : _typeof(_ret)) === "object") return _ret.v;
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_LAST_SELECTED_PRODUCT:
      {
        return Object.assign({}, state, {
          lastSelectedProduct: value
        });
      }

    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].RESET_PRODUCT_TYPE_CKA_RA_IS_VALID:
      {
        var _productTypeIsValid = false;
        if (value !== "") {
          _productTypeIsValid = true;
        }

        return Object.assign({}, state, {
          productType: {
            isValid: _productTypeIsValid,
            lastProdType: value,
            prodType: value
          },
          ckaSection: Object.assign({}, state.ckaSection, {
            isValid: ckaIsValid
          }),
          raSection: Object.assign({}, state.raSection, {
            isValid: raIsValid
          })
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].UPDATE_CKA_OWNER_INIT:
      {
        return Object.assign({}, state, {
          ckaSection: Object.assign({}, state.ckaSection, {
            owner: Object.assign({}, state.ckaSection.owner, {
              init: newInit
            })
          })
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].UPDATE_CKA_IS_VALID:
      {
        return Object.assign({}, state, {
          ckaSection: Object.assign({}, state.ckaSection, {
            isValid: isValid
          })
        });
      }

    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].INITIAL_VALIDATE:
      {
        var isCourseError = (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: true, disabled: false },
          value: ckaData.course
        });
        var isInstitutionError = (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: true, disabled: false },
          value: ckaData.institution
        });
        var isStudyPeriodEndYearError = (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: true, disabled: false },
          value: ckaData.studyPeriodEndYear
        });
        if (birthYear > ckaData.studyPeriodEndYear) {
          var _message2;

          isStudyPeriodEndYearError = {
            hasError: true,
            message: (_message2 = {}, _defineProperty(_message2, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.306"]), _defineProperty(_message2, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.306"]), _message2)
          };
        }
        if (ckaData.course === "notApplicable" || ckaData.course === "") {
          isInstitutionError = errorState;
          isStudyPeriodEndYearError = errorState;
        }
        var isCollectiveInvestmentError = (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: true, disabled: false },
          value: ckaData.collectiveInvestment
        });
        var isTransactionTypeError = (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: true, disabled: false },
          value: ckaData.transactionType
        });
        if (ckaData.collectiveInvestment === "N" || ckaData.collectiveInvestment === "") {
          isTransactionTypeError = errorState;
        }
        var isInsuranceInvestmentError = (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: true, disabled: false },
          value: ckaData.insuranceInvestment
        });
        var isInsuranceTypeError = (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: true, disabled: false },
          value: ckaData.insuranceType
        });
        if (ckaData.insuranceInvestment === "N" || ckaData.insuranceInvestment === "") {
          isInsuranceTypeError = errorState;
        }
        var isProfessionError = (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: true, disabled: false },
          value: ckaData.profession
        });
        var isYearsError = (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: true, disabled: false },
          value: ckaData.years
        });
        if (ckaData.years < 3) {
          var _message3;

          isYearsError = {
            hasError: true,
            message: (_message3 = {}, _defineProperty(_message3, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.405"].replace("{1}", "3")), _defineProperty(_message3, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.405"].replace("{1}", "3")), _message3)
          };
        }
        if (ckaData.profession === "notApplicable" || ckaData.profession === "") {
          isYearsError = errorState;
        }
        return Object.assign({}, state, {
          ckaSection: Object.assign({}, state.ckaSection, {
            owner: Object.assign({}, state.ckaSection.owner, {
              isCourseError: isCourseError,
              isInstitutionError: isInstitutionError,
              isStudyPeriodEndYearError: isStudyPeriodEndYearError,
              isCollectiveInvestmentError: isCollectiveInvestmentError,
              isTransactionTypeError: isTransactionTypeError,
              isInsuranceInvestmentError: isInsuranceInvestmentError,
              isInsuranceTypeError: isInsuranceTypeError,
              isProfessionError: isProfessionError,
              isYearsError: isYearsError
            })
          })
        });
      }

    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].COURSE_ON_CHANGE:
      {
        if (newCourse === "notApplicable" || newCourse === "") {
          return Object.assign({}, state, {
            ckaSection: Object.assign({}, state.ckaSection, {
              owner: Object.assign({}, state.ckaSection.owner, {
                course: newCourse,
                isInstitutionError: errorState,
                isStudyPeriodEndYearError: errorState
              })
            })
          });
        }
        return Object.assign({}, state, {
          ckaSection: Object.assign({}, state.ckaSection, {
            owner: Object.assign({}, state.ckaSection.owner, {
              course: newCourse
            })
          })
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].VALIDATE_COURSE:
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            isCourseError: (0, _validation.validateMandatory)({
              field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: true, disabled: false },
              value: newCourse
            })
          })
        })
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].INSTITUTION_ON_CHANGE:
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            institution: newInstitution
          })
        })
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].VALIDATE_INSTITUTION:
      {
        var errorResult = errorState;

        if (newInstitution.length > 100) {
          var _message4;

          errorResult = {
            hasError: true,
            message: (_message4 = {}, _defineProperty(_message4, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.406"].replace("{1}", "100")), _defineProperty(_message4, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.406"].replace("{1}", "100")), _message4)
          };
        }

        var checkMandatory = (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: true, disabled: false },
          value: newInstitution
        });

        if (checkMandatory.hasError) {
          errorResult = checkMandatory;
        }

        return Object.assign({}, state, {
          ckaSection: Object.assign({}, state.ckaSection, {
            owner: Object.assign({}, state.ckaSection.owner, {
              isInstitutionError: errorResult
            })
          })
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].STUDYPERIODENDYEAR_ON_CHANGE:
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            studyPeriodEndYear: newStudyPeriodEndYear
          })
        })
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].VALIDATE_STUDYPERIODENDYEAR:
      {
        var _errorResult = errorState;

        if (birthYear > newStudyPeriodEndYear) {
          var _message5;

          _errorResult = {
            hasError: true,
            message: (_message5 = {}, _defineProperty(_message5, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.306"]), _defineProperty(_message5, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.306"]), _message5)
          };
        }

        var _checkMandatory = (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: true, disabled: false },
          value: newStudyPeriodEndYear
        });

        if (_checkMandatory.hasError) {
          _errorResult = _checkMandatory;
        }
        return Object.assign({}, state, {
          ckaSection: Object.assign({}, state.ckaSection, {
            owner: Object.assign({}, state.ckaSection.owner, {
              isStudyPeriodEndYearError: _errorResult
            })
          })
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].COLLECTIVEINVESTMENT_ON_CHANGE:
      {
        if (newCollectiveInvestment === "N" || newCollectiveInvestment === "") {
          return Object.assign({}, state, {
            ckaSection: Object.assign({}, state.ckaSection, {
              owner: Object.assign({}, state.ckaSection.owner, {
                collectiveInvestment: newCollectiveInvestment,
                isTransactionTypeError: errorState
              })
            })
          });
        }
        return Object.assign({}, state, {
          ckaSection: Object.assign({}, state.ckaSection, {
            owner: Object.assign({}, state.ckaSection.owner, {
              collectiveInvestment: newCollectiveInvestment
            })
          })
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].VALIDATE_COLLECTIVEINVESTMENT:
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            isCollectiveInvestmentError: (0, _validation.validateMandatory)({
              field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: true, disabled: false },
              value: newCollectiveInvestment
            })
          })
        })
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].TRANSACTIONTYPE_ON_CHANGE:
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            transactionType: newTransactionType
          })
        })
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].VALIDATE_TRANSACTIONTYPE:
      {
        var _errorResult2 = errorState;

        if (newTransactionType.length > 100) {
          var _message6;

          _errorResult2 = {
            hasError: true,
            message: (_message6 = {}, _defineProperty(_message6, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.406"].replace("{1}", "100")), _defineProperty(_message6, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.406"].replace("{1}", "100")), _message6)
          };
        }

        var _checkMandatory2 = (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: true, disabled: false },
          value: newTransactionType
        });

        if (_checkMandatory2.hasError) {
          _errorResult2 = _checkMandatory2;
        }

        return Object.assign({}, state, {
          ckaSection: Object.assign({}, state.ckaSection, {
            owner: Object.assign({}, state.ckaSection.owner, {
              isTransactionTypeError: _errorResult2
            })
          })
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].INSURANCEINVESTMENT_ON_CHANGE:
      {
        if (newInsuranceInvestment === "N" || newInsuranceInvestment === "") {
          return Object.assign({}, state, {
            ckaSection: Object.assign({}, state.ckaSection, {
              owner: Object.assign({}, state.ckaSection.owner, {
                insuranceInvestment: newInsuranceInvestment,
                isInsuranceTypeError: errorState
              })
            })
          });
        }
        return Object.assign({}, state, {
          ckaSection: Object.assign({}, state.ckaSection, {
            owner: Object.assign({}, state.ckaSection.owner, {
              insuranceInvestment: newInsuranceInvestment
            })
          })
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].VALIDATE_INSURANCEINVESTMENT:
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            isInsuranceInvestmentError: (0, _validation.validateMandatory)({
              field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: true, disabled: false },
              value: newInsuranceInvestment
            })
          })
        })
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].INSURANCETYPE_ON_CHANGE:
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            insuranceType: newInsuranceType
          })
        })
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].VALIDATE_INSURANCETYPE:
      {
        var _errorResult3 = errorState;

        if (newInsuranceType.length > 100) {
          var _message7;

          _errorResult3 = {
            hasError: true,
            message: (_message7 = {}, _defineProperty(_message7, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.406"].replace("{1}", "100")), _defineProperty(_message7, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.406"].replace("{1}", "100")), _message7)
          };
        }

        var _checkMandatory3 = (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: true, disabled: false },
          value: newInsuranceType
        });

        if (_checkMandatory3.hasError) {
          _errorResult3 = _checkMandatory3;
        }
        return Object.assign({}, state, {
          ckaSection: Object.assign({}, state.ckaSection, {
            owner: Object.assign({}, state.ckaSection.owner, {
              isInsuranceTypeError: _errorResult3
            })
          })
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].PROFESSION_ON_CHANGE:
      {
        if (newProfession === "notApplicable" || newProfession === "") {
          return Object.assign({}, state, {
            ckaSection: Object.assign({}, state.ckaSection, {
              owner: Object.assign({}, state.ckaSection.owner, {
                profession: newProfession,
                isYearsError: errorState
              })
            })
          });
        }
        return Object.assign({}, state, {
          ckaSection: Object.assign({}, state.ckaSection, {
            owner: Object.assign({}, state.ckaSection.owner, {
              profession: newProfession
            })
          })
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].VALIDATE_PROFESSION:
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            isProfessionError: (0, _validation.validateMandatory)({
              field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: true, disabled: false },
              value: newProfession
            })
          })
        })
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].YEARS_ON_CHANGE:
      {
        var result = void 0;
        var numberValue = parseInt(newYears, 10);
        if (!Number.isNaN(numberValue) && numberValue >= 0) {
          result = numberValue;
        } else {
          result = "";
        }
        return Object.assign({}, state, {
          ckaSection: Object.assign({}, state.ckaSection, {
            owner: Object.assign({}, state.ckaSection.owner, {
              years: result
            })
          })
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].YEARS_ON_BLUR:
      {
        if (parseInt(newYears, 10) < 3) {
          return Object.assign({}, state, {
            ckaSection: Object.assign({}, state.ckaSection, {
              owner: Object.assign({}, state.ckaSection.owner, {
                years: 3,
                isYearsError: {
                  hasError: false,
                  message: ""
                }
              })
            })
          });
        }
        return Object.assign({}, state, {
          ckaSection: Object.assign({}, state.ckaSection, {
            owner: Object.assign({}, state.ckaSection.owner, {
              years: parseInt(newYears, 10)
            })
          })
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].VALIDATE_YEARS:
      {
        var _errorResult4 = errorState;

        if (newYears < 3) {
          var _message8;

          _errorResult4 = {
            hasError: true,
            message: (_message8 = {}, _defineProperty(_message8, _locales.LANGUAGE_TYPES.ENGLISH, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.405"].replace("{1}", "3")), _defineProperty(_message8, _locales.LANGUAGE_TYPES.TRADITIONAL_CHINESE, _locales2.default[_locales.LANGUAGE_TYPES.ENGLISH]["error.405"].replace("{1}", "3")), _message8)
          };
        }

        var _checkMandatory4 = (0, _validation.validateMandatory)({
          field: { type: _FIELD_TYPES.TEXT_FIELD, mandatory: true, disabled: false },
          value: newYears
        });

        if (_checkMandatory4.hasError) {
          _errorResult4 = _checkMandatory4;
        }
        return Object.assign({}, state, {
          ckaSection: Object.assign({}, state.ckaSection, {
            owner: Object.assign({}, state.ckaSection.owner, {
              isYearsError: _errorResult4
            })
          })
        });
      }

    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_CKA].UPDATE_PASS_CKA:
      {
        return Object.assign({}, state, {
          ckaSection: Object.assign({}, state.ckaSection, {
            owner: Object.assign({}, state.ckaSection.owner, {
              passCka: newPassCka
            })
          })
        });
      }

    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_RA].RISKPOTENTIALRETURN_ON_CHANGE:
      return Object.assign({}, state, {
        raSection: Object.assign({}, state.raSection, {
          owner: Object.assign({}, state.raSection.owner, {
            riskPotentialReturn: newRiskPotentialReturn
          })
        })
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_RA].AVGAGRETURN_ON_CHANGE:
      return Object.assign({}, state, {
        raSection: Object.assign({}, state.raSection, {
          owner: Object.assign({}, state.raSection.owner, {
            avgAGReturn: newAvgAGReturn
          })
        })
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_RA].SMDROPED_ON_CHANGE:
      return Object.assign({}, state, {
        raSection: Object.assign({}, state.raSection, {
          owner: Object.assign({}, state.raSection.owner, {
            smDroped: newSmDroped
          })
        })
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_RA].ALOFLOSSES_ON_CHANGE:
      return Object.assign({}, state, {
        raSection: Object.assign({}, state.raSection, {
          owner: Object.assign({}, state.raSection.owner, {
            alofLosses: newAlofLosses
          })
        })
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_RA].EXPINVTIME_ON_CHANGE:
      return Object.assign({}, state, {
        raSection: Object.assign({}, state.raSection, {
          owner: Object.assign({}, state.raSection.owner, {
            expInvTime: newExpInvTime
          })
        })
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_RA].INVPREF_ON_CHANGE:
      return Object.assign({}, state, {
        raSection: Object.assign({}, state.raSection, {
          owner: Object.assign({}, state.raSection.owner, {
            invPref: newInvPref
          })
        })
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_RA].SELF_SELECTED_RISK_LEVEL_ON_CHANGE:
      return Object.assign({}, state, {
        raSection: Object.assign({}, state.raSection, {
          owner: Object.assign({}, state.raSection.owner, {
            selfSelectedriskLevel: newSelfSelectedRiskLevel
          })
        })
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_RA].SELF_RL_REASON_RP_ON_CHANGE:
      return Object.assign({}, state, {
        raSection: Object.assign({}, state.raSection, {
          owner: Object.assign({}, state.raSection.owner, {
            selfRLReasonRP: newSelfRLReasonRP
          })
        })
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_RA].UPDATE_RA_IS_VALID:
      {
        return Object.assign({}, state, {
          raSection: Object.assign({}, state.raSection, {
            isValid: isValid
          })
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_RA].UPDATE_RA_OWNER_INIT:
      {
        return Object.assign({}, state, {
          raSection: Object.assign({}, state.raSection, {
            owner: Object.assign({}, state.raSection.owner, {
              init: newInit
            })
          })
        });
      }
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_RA].UPDATE_ASSESSED_RL:
      return Object.assign({}, state, {
        raSection: Object.assign({}, state.raSection, {
          owner: Object.assign({}, state.raSection.owner, {
            assessedRL: newAssessedRL
          })
        })
      });

    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return naState;
    default:
      return state;
  }
};

var isNaError = function isNaError() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : isNaErrorState;
  var _ref6 = arguments[1];
  var type = _ref6.type,
      naData = _ref6.naData,
      profileData = _ref6.profileData,
      dependantProfilesData = _ref6.dependantProfilesData,
      feData = _ref6.feData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].VALIDATE_NA:
      {
        var spouseKey = Object.keys(dependantProfilesData).find(function (key) {
          return dependantProfilesData[key].relationship === "SPO";
        });
        var spouseProfile = dependantProfilesData[spouseKey];

        // validate fiProtection
        var fiProtection = {
          owner: (0, _naUtilities.validateFiProtection)({
            fiProtectionData: naData.fiProtection.owner,
            dependantProfileData: profileData,
            na: naData,
            fe: feData,
            selectedProfile: "owner"
          })
        };
        if (naData.fiProtection.spouse) {
          fiProtection.spouse = (0, _naUtilities.validateFiProtection)({
            fiProtectionData: naData.fiProtection.spouse,
            dependantProfileData: spouseProfile,
            na: naData,
            fe: feData,
            selectedProfile: "spouse"
          });
        }

        // validate diProtection
        var diProtection = {
          owner: (0, _naUtilities.validateDiProtection)(naData.diProtection.owner)
        };
        if (naData.diProtection.spouse) {
          diProtection.spouse = (0, _naUtilities.validateDiProtection)(naData.diProtection.spouse);
        }
        if (naData.diProtection.dependants) {
          naData.diProtection.dependants.forEach(function (dependant) {
            diProtection[dependant.cid] = (0, _naUtilities.validateDiProtection)(dependant);
          });
        }

        // validate ciProtection
        var ciProtection = {
          owner: (0, _naUtilities.validateCiProtection)({
            ciProtectionData: naData.ciProtection.owner,
            profileData: profileData,
            na: naData,
            fe: feData,
            selectedProfile: "owner"
          })
        };
        if (naData.ciProtection.spouse) {
          ciProtection.spouse = (0, _naUtilities.validateCiProtection)({
            ciProtectionData: naData.ciProtection.spouse,
            profileData: profileData,
            na: naData,
            fe: feData,
            selectedProfile: "spouse"
          });
        }

        // validate rPlanning
        var rPlanning = {
          owner: (0, _naUtilities.validateRPlanning)({
            rPlanningData: naData.rPlanning.owner,
            dependantProfileData: profileData,
            na: naData,
            fe: feData,
            selectedProfile: "owner"
          })
        };
        if (naData.rPlanning.spouse) {
          rPlanning.spouse = (0, _naUtilities.validateRPlanning)({
            rPlanningData: naData.rPlanning.spouse,
            dependantProfileData: spouseProfile,
            na: naData,
            fe: feData,
            selectedProfile: "spouse"
          });
        }

        // validate pcHeadstart
        var pcHeadstart = {};
        if (naData.pcHeadstart.dependants) {
          naData.pcHeadstart.dependants.forEach(function (dependant) {
            pcHeadstart[dependant.cid] = (0, _naUtilities.validatePcHeadstart)(dependant);
          });
        }

        // validate paProtection
        var paProtection = {
          owner: naData.paProtection.owner ? (0, _naUtilities.validatePaProtection)(naData.paProtection.owner) : {},
          spouse: naData.fiProtection.spouse ? (0, _naUtilities.validatePaProtection)(naData.paProtection.spouse) : {}
        };
        if (naData.paProtection.spouse) {
          paProtection.spouse = (0, _naUtilities.validatePaProtection)(naData.paProtection.spouse);
        }
        if (naData.paProtection.dependants) {
          naData.paProtection.dependants.forEach(function (dependant) {
            paProtection[dependant.cid] = (0, _naUtilities.validatePaProtection)(dependant);
          });
        }

        // validate hcProtection
        var hcProtection = {
          owner: (0, _naUtilities.validateHcProtection)(naData.hcProtection.owner)
        };
        if (naData.hcProtection.spouse) {
          hcProtection.spouse = (0, _naUtilities.validateHcProtection)(naData.hcProtection.spouse);
        }
        if (naData.hcProtection.dependants) {
          naData.hcProtection.dependants.forEach(function (dependant) {
            hcProtection[dependant.cid] = (0, _naUtilities.validateHcProtection)(dependant);
          });
        }

        // validate ePlanning
        var ePlanning = {};
        if (naData.ePlanning.dependants) {
          naData.ePlanning.dependants.forEach(function (dependant) {
            ePlanning[dependant.cid] = (0, _naUtilities.validateEPlanning)({
              ePlanningData: dependant,
              na: naData,
              fe: feData
            });
          });
        }

        // validate other
        var other = {
          owner: (0, _naUtilities.validateOther)(naData.other.owner)
        };
        if (naData.other.spouse) {
          other.spouse = (0, _naUtilities.validateOther)(naData.other.spouse);
        }

        // validate psGoals
        var psGoals = {
          owner: (0, _naUtilities.validatePsGoals)({
            psGoalsData: naData.psGoals.owner,
            na: naData,
            fe: feData,
            selectedProfile: "owner"
          })
        };
        if (naData.psGoals.spouse) {
          psGoals.spouse = (0, _naUtilities.validatePsGoals)({
            psGoalsData: naData.psGoals.spouse,
            na: naData,
            fe: feData,
            selectedProfile: "spouse"
          });
        }

        return {
          aspects: {},
          roi: {},
          diProtection: diProtection,
          ciProtection: ciProtection,
          ePlanning: ePlanning,
          fiProtection: fiProtection,
          hcProtection: hcProtection,
          other: other,
          paProtection: paProtection,
          pcHeadstart: pcHeadstart,
          psGoals: psGoals,
          rPlanning: rPlanning,
          sfAspects: {},
          spAspects: {},
          productType: {},
          ckaSection: {},
          raSection: {}
        };
      }
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return isNaErrorState;
    default:
      return state;
  }
};
/**
 * @description financial evaluation section state
 * */
var fe = function fe() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var _ref7 = arguments[1];
  var type = _ref7.type,
      feData = _ref7.feData,
      profileData = _ref7.profileData,
      pdaData = _ref7.pdaData,
      dependantProfilesData = _ref7.dependantProfilesData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_FNA:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_FE:
      return feHandler({ feData: feData, profileData: profileData, pdaData: pdaData, dependantProfilesData: dependantProfilesData });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].SAVE_FNA_COMPLETE:
      return feData !== undefined ? feHandler({ feData: feData, profileData: profileData, pdaData: pdaData, dependantProfilesData: dependantProfilesData }) : state;
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return {};
    default:
      return state;
  }
};
/**
 * @description fe validation
 * */
var feErrors = function feErrors() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var _ref8 = arguments[1];
  var type = _ref8.type,
      pdaData = _ref8.pdaData,
      feData = _ref8.feData,
      profileData = _ref8.profileData,
      dependantProfilesData = _ref8.dependantProfilesData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].VALIDATE_FE:
      {
        // get all validation target
        var validationTargets = [];
        if (feData.owner && feData.owner.cid === profileData.cid) {
          validationTargets.push({
            data: feData.owner,
            relationship: "owner"
          });
        }
        if (pdaData.applicant === "joint" && feData.spouse) {
          validationTargets.push({
            data: feData.spouse,
            relationship: "spouse"
          });
        }
        if (pdaData.dependants !== "" && feData.dependants && feData.dependants.length > 0) {
          pdaData.dependants.split(",").forEach(function (dependantCid) {
            if (dependantProfilesData[dependantCid] && (dependantProfilesData[dependantCid].relationship === "SON" || dependantProfilesData[dependantCid].relationship === "DAU")) {
              var dependantIndex = _.findIndex(feData.dependants, function (dependant) {
                return dependant.cid === dependantCid;
              });

              validationTargets.push({
                data: feData.dependants[dependantIndex],
                relationship: "dependant"
              });
            }
          });
        }

        // validate all targets
        var feErrorsObject = {};
        validationTargets.forEach(function (_ref9) {
          var data = _ref9.data,
              relationship = _ref9.relationship;

          var errorObject = {
            isFinish: data.init !== false ? {
              hasError: true,
              message: ""
            } : {
              hasError: false,
              message: ""
            }
          };

          switch (relationship) {
            case "owner":
              {
                errorObject = Object.assign(errorObject, (0, _validation.validateNetWorth)({ data: data, relationship: relationship }), (0, _validation.validateEIP)({ data: data, relationship: relationship }), (0, _validation.validateCashFlow)({ data: data, relationship: relationship }), (0, _validation.validateBudgetAndForceIncome)(data));
                break;
              }
            case "spouse":
              {
                errorObject = Object.assign(errorObject, (0, _validation.validateNetWorth)({ data: data, relationship: relationship }), (0, _validation.validateEIP)({ data: data, relationship: relationship }), (0, _validation.validateCashFlow)({ data: data, relationship: relationship }));
                break;
              }
            case "dependant":
              {
                errorObject = Object.assign(errorObject, (0, _validation.validateEIP)({ data: data, relationship: relationship }));
                break;
              }
            default:
              throw new Error("reducer \"fna.feErrors\" error: unexpected relationship \"" + relationship + "\"");
          }

          feErrorsObject[data.cid] = errorObject;
        });
        return feErrorsObject;
      }
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return {};
    default:
      return state;
  }
};

/**
 * @description this reducer is use to save fna report PDF data. It should be
 * noted that we have already add a file header to the base64 data.
 * */
var FNAReport = function FNAReport() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref10 = arguments[1];
  var type = _ref10.type,
      FNAReportData = _ref10.FNAReportData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].SAVE_FNA_REPORT:
      return "data:application/pdf;base64," + FNAReportData;
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return "";
    default:
      return state;
  }
};

/**
 * @description this reducer is use to store the FNA Report Email
 * */

var FNAReportEmail = function FNAReportEmail() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var _ref11 = arguments[1];
  var type = _ref11.type,
      emails = _ref11.emails,
      tab = _ref11.tab,
      report = _ref11.report,
      isSelected = _ref11.isSelected,
      option = _ref11.option,
      value = _ref11.value;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].OPEN_EMAIL:
      return Object.assign({}, state, {
        emails: emails,
        reportOptions: {
          agent: { fnaReport: true },
          client: { fnaReport: true }
        },
        tab: "agent"
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].REPORT_ON_CHANGE:
      return Object.assign({}, state, {
        reportOptions: Object.assign({}, state.reportOptions, _defineProperty({}, tab, Object.assign({}, state.reportOptions[tab], _defineProperty({}, report, !isSelected))))
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].TAB_ON_CHANGE:
      return Object.assign({}, state, {
        tab: option
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_SKIP_EMAIL_IN_REPORT:
      return Object.assign({}, state, {
        skipEmailInReport: value
      });
    default:
      return state;
  }
};

/**
 * @description Needs Summary state
 */
var needsSummary = function needsSummary() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref12 = arguments[1];
  var type = _ref12.type,
      needsSummaryData = _ref12.needsSummaryData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_FNA:
      return needsSummaryData;
    default:
      return state;
  }
};

/**
 * currentPage
 * @description clientForm currentPage.
 * @default ""
 * */
var currentPage = function currentPage() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref13 = arguments[1];
  var type = _ref13.type,
      newPage = _ref13.newPage;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_CURRENT_PAGE:
      return newPage;
    default:
      return state;
  }
};

var naIsChanged = function naIsChanged() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
  var _ref14 = arguments[1];
  var type = _ref14.type,
      newNaIsChanged = _ref14.newNaIsChanged;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_CHANGED:
      return newNaIsChanged;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_NEEDS:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_ASPECTS:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_PRIORITY:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_PRODUCT_TYPES_COMPLETED_STEP:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_RA].UPDATE_ASSESSED_RL:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_RA].SELF_SELECTED_RISK_LEVEL_ON_CHANGE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_RA].SELF_RL_REASON_RP_ON_CHANGE:
      return true;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_FNA:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return false;
    default:
      return state;
  }
};

var naIsGloballyChanged = function naIsGloballyChanged() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
  var _ref15 = arguments[1];
  var type = _ref15.type,
      newNaIsGloballyChanged = _ref15.newNaIsGloballyChanged;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_IS_GLOBALLY_CHANGED:
      return newNaIsGloballyChanged;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_NEEDS:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_ASPECTS:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_PRIORITY:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_NA_PRODUCT_TYPES_COMPLETED_STEP:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_RA].UPDATE_ASSESSED_RL:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_RA].SELF_SELECTED_RISK_LEVEL_ON_CHANGE:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA_RA].SELF_RL_REASON_RP_ON_CHANGE:
      return true;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_FNA:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return false;
    default:
      return state;
  }
};

var isFnaInvalid = function isFnaInvalid() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
  var _ref16 = arguments[1];
  var type = _ref16.type,
      newIsFnaInvalid = _ref16.newIsFnaInvalid;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].UPDATE_IS_FNA_INVALID:
      return newIsFnaInvalid || false;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].SAVE_FNA_COMPLETE:
      return newIsFnaInvalid !== undefined ? newIsFnaInvalid : state;
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return false;
    default:
      return state;
  }
};

var fnaPage = function fnaPage() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var _ref17 = arguments[1];
  var type = _ref17.type,
      newPage = _ref17.newPage;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].REDIRECT_TO:
      return newPage;
    default:
      return state;
  }
};

exports.default = (0, _redux.combineReducers)({
  pda: pda,
  na: na,
  fe: fe,
  feErrors: feErrors,
  isNaError: isNaError,
  FNAReport: FNAReport,
  FNAReportEmail: FNAReportEmail,
  needsSummary: needsSummary,
  trustedIndividual: trustedIndividual,
  currentPage: currentPage,
  naIsChanged: naIsChanged,
  naIsGloballyChanged: naIsGloballyChanged,
  isFnaInvalid: isFnaInvalid,
  fnaPage: fnaPage
});