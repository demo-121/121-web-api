"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _redux = require("redux");

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var initStateRecommendation = {
  chosenList: [],
  notChosenList: []
};
/**
 * recommendation
 * @description store the recommendation data retrieved from core-api and web/app UI
 * @default { chosenList: [], notChosenList: [] }
 * */
var recommendation = function recommendation() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initStateRecommendation;
  var action = arguments[1];

  switch (action.type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].GET_RECOMMENDATION:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_RECOMMENDATION:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_RECOMMENDATION_DATA:
      return Object.assign({}, state, action.recommendation);
    case _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].CLOSE_RECOMMENDATION:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return initStateRecommendation;
    default:
      return state;
  }
};

/**
 * budget
 * @description store the budget data retrieved from core-api and web/app UI
 * @default {}
 * */
var budget = function budget() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var action = arguments[1];

  switch (action.type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].GET_RECOMMENDATION:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_RECOMMENDATION:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_BUDGET_DATA:
      return Object.assign({}, state, action.budget);
    case _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].CLOSE_RECOMMENDATION:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return {};
    default:
      return state;
  }
};

/**
 * acceptance
 * @description store the acceptance retrieved from core-api and web/app UI
 * @default []
 * */
var acceptance = function acceptance() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var action = arguments[1];

  switch (action.type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].GET_RECOMMENDATION:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_RECOMMENDATION:
      return action.acceptance;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].CLOSE_RECOMMENDATION:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return [];
    default:
      return state;
  }
};

var initStateError = {
  recommendation: {},
  budget: {},
  acceptance: {}
};
/**
 * error
 * @description store the error retrieved from web/app UI
 * @default { recommendation: {}, budget: {}, acceptance: {} }
 * */
var error = function error() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initStateError;
  var action = arguments[1];

  switch (action.type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].GET_RECOMMENDATION:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_RECOMMENDATION_ERROR:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_BUDGET_ERROR:
      return Object.assign({}, state, action.error);
    case _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].CLOSE_RECOMMENDATION:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return initStateError;
    default:
      return state;
  }
};

var initStateComponent = {
  completedStep: -1,
  currentStep: 0,
  completedSection: { recommendation: {}, budget: false },
  selectedQuotId: "",
  showDetails: false,
  disabled: false
};
/**
 * component
 * @description store the component status in UI
 * @default { completedStep: -1, currentStep: 0, completedSection: { recommendation: {}, budget: false }, showDetails: false, disabled: false }
 * */
var component = function component() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initStateComponent;
  var action = arguments[1];

  switch (action.type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].GET_RECOMMENDATION:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_RECOMMENDATION_COMPONENT:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_RECOMMENDATION:
      return Object.assign({}, state, action.component);
    case _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_RECOMMENDATION_SELECTED_QUOT:
      return Object.assign({}, state, {
        selectedQuotId: action.newSelectedQuotId
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].UPDATE_RECOMMENDATION_DETAILS:
      return Object.assign({}, state, { showDetails: action.newShowDetails });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.RECOMMENDATION].CLOSE_RECOMMENDATION:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return initStateComponent;
    default:
      return state;
  }
};

exports.default = (0, _redux.combineReducers)({
  recommendation: recommendation,
  budget: budget,
  acceptance: acceptance,
  error: error,
  component: component
});