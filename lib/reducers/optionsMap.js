"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _optionsMap = require("../assets/cache/optionsMap");

var _optionsMap2 = _interopRequireDefault(_optionsMap);

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function () {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : _optionsMap2.default;
  var _ref = arguments[1];
  var type = _ref.type,
      optionsMapData = _ref.optionsMapData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.OPTIONS_MAP].UPDATE_OPTIONS_MAP:
      return optionsMapData;
    default:
      return state;
  }
};