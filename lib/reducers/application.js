"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _redux = require("redux");

var _lodash = require("lodash");

var _ = _interopRequireWildcard(_lodash);

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

/**
 * template
 * @description store the template retrieved from core-api
 * @default {}
 * */
var template = function template() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var _ref = arguments[1];
  var type = _ref.type,
      newTemplate = _ref.newTemplate;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].GET_APPLICATION:
      if (newTemplate) {
        return Object.assign({}, state, newTemplate);
      }
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA:
      return {};
    default:
      return state;
  }
};

/**
 * application
 * @description store the application value retrieved from core-api or updated from app
 * @default {}
 * */
var application = function application() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var _ref2 = arguments[1];
  var type = _ref2.type,
      newApplication = _ref2.newApplication,
      newIsMandDocsAllUploaded = _ref2.newIsMandDocsAllUploaded,
      newPolicuNumnber = _ref2.newPolicuNumnber;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].GET_APPLICATION:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_PERSONAL_DETAILS_ADDRESS:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_APPLICATION_FORM_VALUES:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_APPLICATION_FORM_PERSONAL_DETAILS:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].GET_APPLICATION_FOR_SUPPORTING_DOCUMENT:
      if (newApplication) {
        return Object.assign({}, state, newApplication);
      }
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_POLICY_NUMBER:
      if (newPolicuNumnber) {
        return Object.assign({}, state, Object.assign({}, state.application, {
          policyNumber: newPolicuNumnber
        }));
      }
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA:
      return {};
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_SUPPORTING_DOCUMENT_ERROR_ICON:
      if (newIsMandDocsAllUploaded !== undefined && newIsMandDocsAllUploaded !== null) {
        return Object.assign({}, state, {
          isMandDocsAllUploaded: newIsMandDocsAllUploaded
        });
      }
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].INIT_PAYMENT_PAGE:
    default:
      return state;
  }
};

var initStateComponent = {
  completedStep: -1,
  currentStep: 0,
  selectedSectionKey: "",
  selectedCid: "",
  showPlanDetailsView: false,
  disabled: false,
  valueEdited: false,
  apiPage: "",
  shieldPolicyNumber: [],
  isAlertDelayed: false,
  isCrossAgeAlertShow: false,
  isSignatureExpiryAlertShow: false,
  isDisableEappButton: false
};
/**
 * component
 * @description store the component status in UI
 * @default {}
 * */
var component = function component() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initStateComponent;
  var _ref3 = arguments[1];
  var type = _ref3.type,
      newShieldPolicyNumber = _ref3.newShieldPolicyNumber,
      isPlanDetailsView = _ref3.isPlanDetailsView,
      newSelectedSectionKey = _ref3.newSelectedSectionKey,
      newValueEdited = _ref3.newValueEdited,
      applicationStatus = _ref3.applicationStatus,
      newCurrentStep = _ref3.newCurrentStep,
      newSelectedCid = _ref3.newSelectedCid,
      newCompletedStep = _ref3.newCompletedStep,
      newApiPage = _ref3.newApiPage,
      newIsAlertDelayed = _ref3.newIsAlertDelayed,
      newIsCrossAgeAlertShow = _ref3.newIsCrossAgeAlertShow,
      newIsSignatureExpiryAlertShow = _ref3.newIsSignatureExpiryAlertShow,
      newIsDisableEappButton = _ref3.newIsDisableEappButton;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].IS_DISABLE_EAPP_BUTTON:
      return Object.assign({}, state, {
        isDisableEappButton: newIsDisableEappButton
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_SHIELD_POLICY_NUMBER:
      return Object.assign({}, state, {
        shieldPolicyNumber: newShieldPolicyNumber
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_API_PAGE:
      return Object.assign({}, state, {
        apiPage: newApiPage
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_UI_VIEW_PLAN_DETAILS:
      return Object.assign({}, state, {
        showPlanDetailsView: isPlanDetailsView
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].VALUE_EDITED:
      return Object.assign({}, state, {
        valueEdited: newValueEdited
      });

    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_UI_SELECTED_SECTION_KEY:
      return Object.assign({}, state, {
        selectedSectionKey: newSelectedSectionKey
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_EAPP_STEP:
      return Object.assign({}, state, {
        currentStep: newCurrentStep
      }, _.isNumber(newCompletedStep) ? { completedStep: newCompletedStep } : {});
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_APPLICATION_STATUS:
      return Object.assign({}, state, applicationStatus);

    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_UI_SELECTED_CID:
      return Object.assign({}, state, {
        selectedCid: newSelectedCid
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_UI_SHOW_CROSS_AGE_ALERT:
      return Object.assign({}, state, {
        isAlertDelayed: newIsAlertDelayed,
        isCrossAgeAlertShow: newIsCrossAgeAlertShow
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_UI_SIGNATURE_EXPIRY_ALERT:
      return Object.assign({}, state, {
        isAlertDelayed: newIsAlertDelayed,
        isSignatureExpiryAlertShow: newIsSignatureExpiryAlertShow
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_UI_ALERT_DELAYED:
      return Object.assign({}, state, {
        isAlertDelayed: newIsAlertDelayed
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_COMPLETED_STEP:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].VALIDATE_APPLICATION_FORM:
      return Object.assign({}, state, {
        completedStep: newCompletedStep
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA:
      return initStateComponent;
    default:
      return state;
  }
};

var initStateError = { application: {}, payment: {} };

/**
 * error
 * @description store the error retrieved from web/app UI
 * @default { application: {}, payment: {} }
 * */
var error = function error() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initStateError;
  var _ref4 = arguments[1];
  var type = _ref4.type,
      errorObj = _ref4.errorObj;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].VALIDATE_APPLICATION_FORM:
      return Object.assign({}, state, errorObj);
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].INIT_VALIDATE_APPLICATION_FORM_PERSONAL_DETAILS:
      return Object.assign({}, state, errorObj);
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA:
      return initStateError;
    default:
      return state;
  }
};

var initStateCrossAge = {
  crossedAge: false,
  allowBackdate: false,
  // For normal product
  proposerStatus: 0,
  insuredStatus: 0,
  // For shield product
  crossedAgeCid: [],
  status: 0
};

/**
 * crossAge
 * @description store the crossAge object retrieved from web/app UI
 * @default {
 *   crossedAge: false,
 *   allowBackdate: false,
 *   proposerStatus: 0,
 *   insuredStatus: 0,
 *   crossedAgeCid: [],
 *   status: 0
 * }
 * */
var crossAge = function crossAge() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initStateCrossAge;
  var _ref5 = arguments[1];
  var type = _ref5.type,
      newCrossAge = _ref5.newCrossAge;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_CROSS_AGE:
      return Object.assign({}, state, Object.assign({}, initStateCrossAge, newCrossAge));
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA:
      return initStateCrossAge;
    default:
      return state;
  }
};

exports.default = (0, _redux.combineReducers)({
  template: template,
  application: application,
  error: error,
  component: component,
  crossAge: crossAge
});