"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _redux = require("redux");

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * paymentAndSubmissionData
 * @description affect product list filter
 * @default ALL
 * */
var payment = function payment() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var _ref = arguments[1];
  var type = _ref.type,
      newPayment = _ref.newPayment;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_AND_SUBMISSION:
      if (newPayment) {
        return Object.assign({}, newPayment);
      }
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA:
      return {};
    default:
      return state;
  }
};

var submission = function submission() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var _ref2 = arguments[1];
  var type = _ref2.type,
      newSubmission = _ref2.newSubmission;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_AND_SUBMISSION:
      if (newSubmission) {
        return Object.assign({}, newSubmission);
      }
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA:
      return {};
    default:
      return state;
  }
};

var error = function error() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var _ref3 = arguments[1];
  var type = _ref3.type,
      errorObj = _ref3.errorObj;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].VALIDATE_PAYMENT_AND_SUBMISSION:
      return Object.assign({}, errorObj);
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA:
      return {};
    default:
      return state;
  }
};

// this store is for shield use only
var template = function template() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var _ref4 = arguments[1];
  var type = _ref4.type,
      newTemplate = _ref4.newTemplate;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_AND_SUBMISSION:
      if (newTemplate) {
        return Object.assign({}, newTemplate);
      }
      return state;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA:
      return {};
    default:
      return state;
  }
};
/**
 * @description this reducer is for iOS only
 * */
var onlinePaymentInitialState = {
  trxStatusRemark: "",
  trxTime: NaN,
  trxNo: "",
  trxStatus: "",
  trxStartTime: NaN,
  trxMethod: "",
  initPayMethod: ""
};
var onlinePaymentStatus = function onlinePaymentStatus() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : onlinePaymentInitialState;
  var _ref5 = arguments[1];
  var type = _ref5.type,
      newStatus = _ref5.newStatus;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_STATUS:
      return {
        trxStatusRemark: newStatus.trxStatusRemark,
        trxTime: newStatus.trxTime,
        trxNo: newStatus.trxNo,
        trxStatus: newStatus.trxStatus,
        trxStartTime: newStatus.trxStartTime,
        trxMethod: newStatus.trxMethod,
        initPayMethod: newStatus.initPayMethod
      };
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA:
      return onlinePaymentInitialState;
    default:
      return state;
  }
};

exports.default = (0, _redux.combineReducers)({
  payment: payment,
  submission: submission,
  error: error,
  template: template,
  onlinePaymentStatus: onlinePaymentStatus
});