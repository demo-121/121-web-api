"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _redux = require("redux");

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var initStateSignature = {
  isFaChannel: false,
  selectedPdfIdx: 0,
  signingTabIdx: -1,
  isSigningProcess: [],
  numOfTabs: 0,
  attachments: [],
  attUrls: [],
  agentSignFields: [],
  clientSignFields: []
};

// TODO update commment
/**
 * signature
 * @description signature data
 * @default {}
 * */
var signature = function signature() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initStateSignature;
  var _ref = arguments[1];
  var type = _ref.type,
      newSignature = _ref.newSignature,
      newSelectedPdfIdx = _ref.newSelectedPdfIdx,
      newSigningTabIdx = _ref.newSigningTabIdx,
      newAttachments = _ref.newAttachments;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.SIGNATURE].UPDATE_SIGNATURE:
      return Object.assign({}, state, newSignature);
    case _ACTION_TYPES2.default[_REDUCER_TYPES.SIGNATURE].SIGNATURE_SWITCH_PDF:
      return Object.assign({}, state, { selectedPdfIdx: newSelectedPdfIdx });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.SIGNATURE].UPDATE_SIGNED_PDF:
      return Object.assign({}, state, {
        signingTabIdx: newSigningTabIdx,
        attachments: newAttachments
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].UPDATE_APPLICATION_BACKDATE:
      return Object.assign({}, state, {
        attachments: newAttachments
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA:
      return initStateSignature;
    default:
      return state;
  }
};

exports.default = (0, _redux.combineReducers)({
  signature: signature
});