"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _redux = require("redux");

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

var _FILTER_TYPES = require("../constants/FILTER_TYPES");

var _FILTER_TYPES2 = _interopRequireDefault(_FILTER_TYPES);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * applicationsList
 * @description store the applications list retrieved from core-api
 * @default []
 * */
var applicationsList = function applicationsList() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var action = arguments[1];

  switch (action.type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].UPDATE_APPLICATION_LIST:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].DELETE_APPLICATION_LIST:
      return action.applicationsList;
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return [];
    default:
      return state;
  }
};

/**
 * productList
 * @description store the product list, generated from applications list which is retrieved from core-api
 * @default []
 * */
var productList = function productList() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var action = arguments[1];

  switch (action.type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].UPDATE_APPLICATION_LIST:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].DELETE_APPLICATION_LIST:
      return action.productList;
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return [];
    default:
      return state;
  }
};

/**
 * pdaMembers
 * @description store the client pda member list retrieved from core-api
 * @default []
 * */
var pdaMembers = function pdaMembers() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var action = arguments[1];

  switch (action.type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].UPDATE_APPLICATION_LIST:
      return action.pdaMembers;
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return [];
    default:
      return state;
  }
};

/**
 * agentChannel
 * @description store the agent channel retrieved from core-api
 * @default ""
 * */
var agentChannel = function agentChannel() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
  var action = arguments[1];

  switch (action.type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].UPDATE_APPLICATION_LIST:
      return action.agentChannel;
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return "";
    default:
      return state;
  }
};

var initStateRecommendation = {
  completed: false,
  choiceList: []
};

/**
 * recommendation
 * @description store the information of recommendation retrieved from core-api
 * @default { completed: false, choiceList: [] }
 * */
var recommendation = function recommendation() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initStateRecommendation;
  var action = arguments[1];

  switch (action.type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].UPDATE_APPLICATION_LIST:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].DELETE_APPLICATION_LIST:
      return Object.assign({}, state, action.recommendation);
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].UPDATE_RECOMMENDATION_CHOICE_LIST:
      return Object.assign({}, state, {
        completed: action.completed,
        choiceList: action.choiceList
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].UPDATE_RECOMMENDATION_COMPLETED:
      return Object.assign({}, state, {
        completed: action.completed
      });
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return initStateRecommendation;
    default:
      return state;
  }
};

var initStateComponent = {
  selectedBundleId: "",
  selectModeOn: false,
  selectedIdList: [],
  selectedAppListFilter: _FILTER_TYPES2.default.APPLICATION_LIST.ALL
};

/**
 * component
 * @description store the information of UI component retrieved from core-api
 * @default {
 *  selectedBundleId: "",
 *  selectModeOn: false,
 *  selectedIdList: [],
 *  selectedAppListFilter: FILTER_TYPES.APPLICATION_LIST.ALL,
 *  isPlanDetailsPage: true
 * }
 * */
var component = function component() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initStateComponent;
  var _ref = arguments[1];
  var type = _ref.type,
      selectModeOn = _ref.selectModeOn,
      selectedIdList = _ref.selectedIdList,
      selectedAppListFilter = _ref.selectedAppListFilter,
      selectedBundleId = _ref.selectedBundleId;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].UPDATE_UI_SELECT_MODE:
      return Object.assign({}, state, {
        selectModeOn: selectModeOn
      }, !selectModeOn ? { selectedIdList: [] } : {});
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].UPDATE_UI_SELECTED_LIST:
      return Object.assign({}, state, {
        selectedIdList: selectedIdList
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].UPDATE_UI_APPLICATION_LIST_FILTER:
      return Object.assign({}, state, {
        selectedAppListFilter: selectedAppListFilter
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].UPDATE_UI_SELECTED_BUNDLE_ID:
      return Object.assign({}, state, {
        selectedBundleId: selectedBundleId
      });
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return initStateComponent;
    default:
      return state;
  }
};

var initStatePreApplicationEmail = {
  appSumEmailOpen: false,
  emails: [],
  attKeysMap: [],
  tab: "",
  options: []
};

var preApplicationEmail = function preApplicationEmail() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initStatePreApplicationEmail;
  var _ref2 = arguments[1];
  var type = _ref2.type,
      emails = _ref2.emails,
      attKeysMap = _ref2.attKeysMap,
      tab = _ref2.tab,
      options = _ref2.options;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].OPEN_EMAIL:
      return Object.assign({}, state, {
        appSumEmailOpen: true,
        attKeysMap: attKeysMap,
        emails: emails,
        tab: "agent",
        options: options
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].TAB_ON_CHANGE:
      return Object.assign({}, state, {
        tab: tab
      });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].REPORT_ON_CHANGE:
      return Object.assign({}, state, {
        options: options
      });
    default:
      return state;
  }
};

var initStateMultiClientProfile = {
  hasErrorList: []
};

var multiClientProfile = function multiClientProfile() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initStateMultiClientProfile;
  var _ref3 = arguments[1];
  var type = _ref3.type,
      newHasErrorList = _ref3.newHasErrorList;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].UPDATE_MULTI_CLIENT_PROFILE_HAS_ERROR:
      return Object.assign({}, state, { hasErrorList: newHasErrorList });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return initStateMultiClientProfile;
    default:
      return state;
  }
};

var productInvalidList = function productInvalidList() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var _ref4 = arguments[1];
  var type = _ref4.type,
      newProductInvalidList = _ref4.newProductInvalidList;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.PRE_APPLICATION].UPDATE_APPLICATION_LIST:
      return newProductInvalidList || [];
    default:
      return state;
  }
};

exports.default = (0, _redux.combineReducers)({
  applicationsList: applicationsList,
  productList: productList,
  pdaMembers: pdaMembers,
  agentChannel: agentChannel,
  recommendation: recommendation,
  component: component,
  preApplicationEmail: preApplicationEmail,
  multiClientProfile: multiClientProfile,
  productInvalidList: productInvalidList
});