"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _redux = require("redux");

var _ACTION_TYPES = require("../constants/ACTION_TYPES");

var _ACTION_TYPES2 = _interopRequireDefault(_ACTION_TYPES);

var _REDUCER_TYPES = require("../constants/REDUCER_TYPES");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * profile
 * @default {}
 * */
var profile = function profile() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var _ref = arguments[1];
  var type = _ref.type,
      profileData = _ref.profileData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT].UPDATE_CLIENT_AND_DEPENDANTS:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT].UPDATE_PROFILE:
      return profileData;
    case _ACTION_TYPES2.default[_REDUCER_TYPES.FNA].SAVE_FNA_COMPLETE:
      return profileData !== undefined ? profileData : state;
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return {};
    default:
      return state;
  }
};

/**
 * dependantProfiles
 * @default {}
 * */
var dependantProfiles = function dependantProfiles() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var _ref2 = arguments[1];
  var type = _ref2.type,
      dependantProfilesData = _ref2.dependantProfilesData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT].UPDATE_CLIENT_AND_DEPENDANTS:
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT].UPDATE_DEPENDANT_PROFILES:
      return dependantProfilesData;
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return {};
    default:
      return state;
  }
};

/**
 * contactList
 * @default []
 * */
var contactList = function contactList() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var _ref3 = arguments[1];
  var type = _ref3.type,
      contactListData = _ref3.contactListData;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT].UPDATE_CONTACT_LIST:
      return contactListData;
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return [];
    default:
      return state;
  }
};

var initAvailableInsuredProfile = {
  hasErrorList: []
};

var availableInsuredProfile = function availableInsuredProfile() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initAvailableInsuredProfile;
  var _ref4 = arguments[1];
  var type = _ref4.type,
      newHasErrorList = _ref4.newHasErrorList;

  switch (type) {
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT].UPDATE_AVAILABLE_INSURED_PROFILE_HAS_ERROR:
      return Object.assign({}, state, { hasErrorList: newHasErrorList });
    case _ACTION_TYPES2.default[_REDUCER_TYPES.CLIENT].CLEAN_AVAILABLE_INSURED_PROFILE:
    case _ACTION_TYPES2.default.root.CLEAN_CLIENT_DATA:
      return initAvailableInsuredProfile;
    default:
      return state;
  }
};

exports.default = (0, _redux.combineReducers)({
  profile: profile,
  dependantProfiles: dependantProfiles,
  contactList: contactList,
  availableInsuredProfile: availableInsuredProfile
});