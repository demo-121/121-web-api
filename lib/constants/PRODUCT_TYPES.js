"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var INVESTLINKEDPLANS = exports.INVESTLINKEDPLANS = "investLinkedPlans";
var PARTICIPATINGPLANS = exports.PARTICIPATINGPLANS = "participatingPlans";
var NONPARTICIPATINGPLAN = exports.NONPARTICIPATINGPLAN = "nonParticipatingPlan";
var ACCIDENTHEALTHPLANS = exports.ACCIDENTHEALTHPLANS = "accidentHealthPlans";