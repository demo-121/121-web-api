"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var BI = exports.BI = "Policy Illustration";
var PROD_SUMMARY = exports.PROD_SUMMARY = "Product Summary";
var FPX_PROD_SUMMARY = exports.FPX_PROD_SUMMARY = "Product Summary";
var SHIELD_PRODUCT_SUMMARY = exports.SHIELD_PRODUCT_SUMMARY = "Product Summary of the accepted plan(s)";
var PHS = exports.PHS = "Product Highlight Sheet";
var FIB = exports.FIB = "Fund Info Booklet";
var FNA = exports.FNA = "Financial Needs Analysis";