"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  dbs: {
    label: "DBS",
    value: "dbs",
    maxLength: 14,
    cpfMaxLength: 13
  },
  ocbc: {
    label: "OCBC",
    value: "ocbc",
    maxLength: 12,
    cpfMaxLength: 9
  },
  uob: {
    label: "UOB",
    value: "uob",
    maxLength: 9,
    cpfMaxLength: 9
  }
};