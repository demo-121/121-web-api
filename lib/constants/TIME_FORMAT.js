"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var DATE_TIME_FORMAT = exports.DATE_TIME_FORMAT = "dd mmm yyyy HH:MM";
var DATE_TIME_FORMAT_2 = exports.DATE_TIME_FORMAT_2 = "dd/mm/yyyy HH:MM";
var DATE_TIME_FORMAT_3 = exports.DATE_TIME_FORMAT_3 = "DD/MM/YYYY";
var MomentDateTimeFormatForApproval = exports.MomentDateTimeFormatForApproval = "DD/MM/YYYY HH:mm";
var DATE_TIME_WITH_TIME = exports.DATE_TIME_WITH_TIME = "DD/MM/YYYY HH:mm:ss";
var DATE_FORMAT = exports.DATE_FORMAT = "dd/mm/yyyy";
var DATE_FORMAT_INPUT = exports.DATE_FORMAT_INPUT = "YYYY-MM-DD";
var DATE_FORMAT_WITH_TIME = exports.DATE_FORMAT_WITH_TIME = "YYYY-MM-DD, h:mm:ss a";