"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isShieldQuotation = exports.isMobileNoAError = exports.isPrefixAError = exports.photo = exports.buildingEstate = exports.unit = exports.streetRoad = exports.blockHouse = exports.postal = exports.cityState = exports.country = exports.isEmailError = exports.email = exports.mobileNoB = exports.prefixB = exports.mobileNoA = exports.prefixA = exports.isIncomeError = exports.income = exports.passExpiryDate = exports.isTypeOfPassOtherError = exports.isTypeOfPassError = exports.typeOfPassOther = exports.typeOfPass = exports.countryOfEmployBusinessSchool = exports.nameOfEmployBusinessSchool = exports.isOccupationOtherError = exports.occupationOther = exports.isOccupationError = exports.occupation = exports.isIndustryError = exports.industry = exports.isEmployStatusOtherError = exports.employStatusOther = exports.isEmployStatusError = exports.employStatus = exports.isEducationError = exports.education = exports.isLanguageOtherError = exports.isLanguageError = exports.languageOther = exports.language = exports.isMaritalStatusError = exports.maritalStatus = exports.isSmokingError = exports.smokingStatus = exports.isIDError = exports.ID = exports.isIDDocumentTypeOtherError = exports.isIDDocumentTypeError = exports.IDDocumentTypeOther = exports.IDDocumentType = exports.isOtherCityOfResidenceError = exports.otherCityOfResidence = exports.isCityOfResidenceError = exports.cityOfResidence = exports.countryOfResidence = exports.isSingaporePRStatusError = exports.singaporePRStatus = exports.isNationalityError = exports.nationality = exports.isBirthdayError = exports.birthday = exports.isGenderError = exports.gender = exports.isTitleError = exports.title = exports.nameOrder = exports.isNameError = exports.name = exports.haveSignDoc = exports.hanYuPinYinName = exports.otherName = exports.surname = exports.isGivenNameError = exports.givenName = exports.isRelationshipOtherError = exports.relationshipOther = exports.isRelationshipError = exports.relationship = exports.isPDA = exports.isApplication = exports.isFromProduct = exports.isFromProfile = exports.isProposerMissing = exports.isFamilyMember = exports.isCreate = undefined;

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _common = require("./common");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isCreate = exports.isCreate = {
  isRequired: _propTypes2.default.bool
};

var isFamilyMember = exports.isFamilyMember = {
  isRequired: _propTypes2.default.bool
};

var isProposerMissing = exports.isProposerMissing = {
  isRequired: _propTypes2.default.bool
};

var isFromProfile = exports.isFromProfile = {
  isRequired: _propTypes2.default.bool
};

var isFromProduct = exports.isFromProduct = {
  isRequired: _propTypes2.default.bool
};

var isApplication = exports.isApplication = {
  isRequired: _propTypes2.default.bool
};

var isPDA = exports.isPDA = {
  isRequired: _propTypes2.default.bool
};

var relationship = exports.relationship = {
  isRequired: _propTypes2.default.string.isRequired
};

var isRelationshipError = exports.isRelationshipError = {
  isRequired: _common.error
};

var relationshipOther = exports.relationshipOther = {
  isRequired: _propTypes2.default.string.isRequired
};

var isRelationshipOtherError = exports.isRelationshipOtherError = {
  isRequired: _common.error
};

var givenName = exports.givenName = {
  isRequired: _propTypes2.default.string.isRequired
};

var isGivenNameError = exports.isGivenNameError = {
  isRequired: _common.error
};

var surname = exports.surname = {
  isRequired: _propTypes2.default.string.isRequired
};

var otherName = exports.otherName = {
  isRequired: _propTypes2.default.string.isRequired
};

var hanYuPinYinName = exports.hanYuPinYinName = {
  isRequired: _propTypes2.default.string.isRequired
};

var haveSignDoc = exports.haveSignDoc = {
  isRequired: _propTypes2.default.bool
};

var name = exports.name = {
  isRequired: _propTypes2.default.string.isRequired
};

var isNameError = exports.isNameError = {
  isRequired: _common.error
};

var nameOrder = exports.nameOrder = {
  isRequired: _propTypes2.default.string.isRequired
};

var title = exports.title = {
  isRequired: _propTypes2.default.string.isRequired
};

var isTitleError = exports.isTitleError = {
  isRequired: _common.error
};

var gender = exports.gender = {
  isRequired: _propTypes2.default.string.isRequired
};

var isGenderError = exports.isGenderError = {
  isRequired: _common.error
};

var birthday = exports.birthday = {
  isRequired: _propTypes2.default.string.isRequired
};

var isBirthdayError = exports.isBirthdayError = {
  isRequired: _common.error
};

var nationality = exports.nationality = {
  isRequired: _propTypes2.default.string.isRequired
};

var isNationalityError = exports.isNationalityError = {
  isRequired: _common.error
};

var singaporePRStatus = exports.singaporePRStatus = {
  isRequired: _propTypes2.default.string.isRequired
};

var isSingaporePRStatusError = exports.isSingaporePRStatusError = {
  isRequired: _common.error
};

var countryOfResidence = exports.countryOfResidence = {
  isRequired: _propTypes2.default.string.isRequired
};

var cityOfResidence = exports.cityOfResidence = {
  isRequired: _propTypes2.default.string.isRequired
};

var isCityOfResidenceError = exports.isCityOfResidenceError = {
  isRequired: _common.error
};

var otherCityOfResidence = exports.otherCityOfResidence = {
  isRequired: _propTypes2.default.string.isRequired
};

var isOtherCityOfResidenceError = exports.isOtherCityOfResidenceError = {
  isRequired: _common.error
};

var IDDocumentType = exports.IDDocumentType = {
  isRequired: _propTypes2.default.string.isRequired
};

var IDDocumentTypeOther = exports.IDDocumentTypeOther = {
  isRequired: _propTypes2.default.string.isRequired
};

var isIDDocumentTypeError = exports.isIDDocumentTypeError = {
  isRequired: _common.error
};

var isIDDocumentTypeOtherError = exports.isIDDocumentTypeOtherError = {
  isRequired: _common.error
};

var ID = exports.ID = {
  isRequired: _propTypes2.default.string.isRequired
};

var isIDError = exports.isIDError = {
  isRequired: _common.error
};

var smokingStatus = exports.smokingStatus = {
  isRequired: _propTypes2.default.string.isRequired
};

var isSmokingError = exports.isSmokingError = {
  isRequired: _common.error
};

var maritalStatus = exports.maritalStatus = {
  isRequired: _propTypes2.default.string.isRequired
};

var isMaritalStatusError = exports.isMaritalStatusError = {
  isRequired: _common.error
};

var language = exports.language = {
  isRequired: _propTypes2.default.string.isRequired
};

var languageOther = exports.languageOther = {
  isRequired: _propTypes2.default.string.isRequired
};

var isLanguageError = exports.isLanguageError = {
  isRequired: _common.error
};

var isLanguageOtherError = exports.isLanguageOtherError = {
  isRequired: _common.error
};

var education = exports.education = {
  isRequired: _propTypes2.default.string.isRequired
};

var isEducationError = exports.isEducationError = {
  isRequired: _common.error
};

var employStatus = exports.employStatus = {
  isRequired: _propTypes2.default.string.isRequired
};

var isEmployStatusError = exports.isEmployStatusError = {
  isRequired: _common.error
};

var employStatusOther = exports.employStatusOther = {
  isRequired: _propTypes2.default.string.isRequired
};

var isEmployStatusOtherError = exports.isEmployStatusOtherError = {
  isRequired: _common.error
};

var industry = exports.industry = {
  isRequired: _propTypes2.default.string.isRequired
};

var isIndustryError = exports.isIndustryError = {
  isRequired: _common.error
};

var occupation = exports.occupation = {
  isRequired: _propTypes2.default.string.isRequired
};

var isOccupationError = exports.isOccupationError = {
  isRequired: _common.error
};

var occupationOther = exports.occupationOther = {
  isRequired: _propTypes2.default.string.isRequired
};

var isOccupationOtherError = exports.isOccupationOtherError = {
  isRequired: _common.error
};

var nameOfEmployBusinessSchool = exports.nameOfEmployBusinessSchool = {
  isRequired: _propTypes2.default.string.isRequired
};

var countryOfEmployBusinessSchool = exports.countryOfEmployBusinessSchool = {
  isRequired: _propTypes2.default.string.isRequired
};

var typeOfPass = exports.typeOfPass = {
  isRequired: _propTypes2.default.string.isRequired
};

var typeOfPassOther = exports.typeOfPassOther = {
  isRequired: _propTypes2.default.string.isRequired
};

var isTypeOfPassError = exports.isTypeOfPassError = {
  isRequired: _common.error
};

var isTypeOfPassOtherError = exports.isTypeOfPassOtherError = {
  isRequired: _common.error
};

var passExpiryDate = exports.passExpiryDate = {
  isRequired: _propTypes2.default.number.isRequired
};

var income = exports.income = {
  isRequired: _propTypes2.default.oneOfType([_propTypes2.default.number, _propTypes2.default.oneOf([NaN, ""])]).isRequired
};

var isIncomeError = exports.isIncomeError = {
  isRequired: _common.error
};

var prefixA = exports.prefixA = {
  isRequired: _propTypes2.default.string.isRequired
};

var mobileNoA = exports.mobileNoA = {
  isRequired: _propTypes2.default.string.isRequired
};

var prefixB = exports.prefixB = {
  isRequired: _propTypes2.default.string.isRequired
};

var mobileNoB = exports.mobileNoB = {
  isRequired: _propTypes2.default.string.isRequired
};

var email = exports.email = {
  isRequired: _propTypes2.default.string.isRequired
};

var isEmailError = exports.isEmailError = {
  isRequired: _common.error
};

var country = exports.country = {
  isRequired: _propTypes2.default.string.isRequired
};

var cityState = exports.cityState = {
  isRequired: _propTypes2.default.string.isRequired
};

var postal = exports.postal = {
  isRequired: _propTypes2.default.string.isRequired
};

var blockHouse = exports.blockHouse = {
  isRequired: _propTypes2.default.string.isRequired
};

var streetRoad = exports.streetRoad = {
  isRequired: _propTypes2.default.string.isRequired
};

var unit = exports.unit = {
  isRequired: _propTypes2.default.string.isRequired
};

var buildingEstate = exports.buildingEstate = {
  isRequired: _propTypes2.default.string.isRequired
};

var photo = exports.photo = {
  isRequired: _propTypes2.default.object.isRequired
};

var isPrefixAError = exports.isPrefixAError = {
  isRequired: _common.error
};

var isMobileNoAError = exports.isMobileNoAError = {
  isRequired: _common.error
};

var isShieldQuotation = exports.isShieldQuotation = {
  isRequired: _propTypes2.default.bool
};