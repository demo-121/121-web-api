"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.template = exports.paymentShield = exports.application = exports.error = exports.payment = exports.submission = undefined;

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// TODO
var submission = exports.submission = {
  isRequired: _propTypes2.default.oneOfType([_propTypes2.default.object])
};
var payment = exports.payment = {
  isRequired: _propTypes2.default.oneOfType([_propTypes2.default.object])
};
var error = exports.error = {
  isRequired: _propTypes2.default.oneOfType([_propTypes2.default.object])
};
var application = exports.application = {
  isRequired: _propTypes2.default.oneOfType([_propTypes2.default.object])
};

var paymentShield = exports.paymentShield = {
  isRequired: _propTypes2.default.shape({
    cpfMedisaveAccDetails: _propTypes2.default.arrayOf(_propTypes2.default.shape({
      cpfAccHolderName: _propTypes2.default.string,
      cpfAccNo: _propTypes2.default.string
    })),
    initPayMethod: _propTypes2.default.string,
    isCompleted: _propTypes2.default.bool,
    premiumDetails: _propTypes2.default.arrayOf(_propTypes2.default.shape({
      applicationId: _propTypes2.default.string,
      cashPortion: _propTypes2.default.number,
      cid: _propTypes2.default.string,
      covName: _propTypes2.default.objectOf(_propTypes2.default.shape({
        en: _propTypes2.default.string,
        "zh-Hant": _propTypes2.default.string
      })),
      cpfPortion: _propTypes2.default.number,
      laName: _propTypes2.default.string,
      medisave: _propTypes2.default.number,
      payFrequency: _propTypes2.default.string,
      policyNumber: _propTypes2.default.string,
      subseqPayMethod: _propTypes2.default.string
    })),
    totCPFPortion: 973,
    totCashPortion: 75.4,
    totMedisave: 2523
  })
};

var template = exports.template = {
  isRequired: _propTypes2.default.objectOf(_propTypes2.default.shape({
    items: _propTypes2.default.arrayOf(_propTypes2.default.objectOf(_propTypes2.default.shape({
      items: _propTypes2.default.oneOfType([_propTypes2.default.array]),
      title: _propTypes2.default.string,
      type: _propTypes2.default.string
    }))),
    type: _propTypes2.default.string
  }))
};