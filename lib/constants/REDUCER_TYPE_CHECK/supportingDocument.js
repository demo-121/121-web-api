"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.application = exports.screenProps = exports.error = exports.SupportingDocumentData = undefined;

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// TODO
var SupportingDocumentData = exports.SupportingDocumentData = {
  isRequired: _propTypes2.default.oneOfType([_propTypes2.default.object])
};

var error = exports.error = {
  isRequired: _propTypes2.default.oneOfType([_propTypes2.default.object])
};

var screenProps = exports.screenProps = {
  isRequired: _propTypes2.default.shape({
    isShield: _propTypes2.default.bool
  })
};

var application = exports.application = {
  isRequired: _propTypes2.default.shape({
    applicationId: _propTypes2.default.string,
    bundleId: _propTypes2.default.string
  })
};