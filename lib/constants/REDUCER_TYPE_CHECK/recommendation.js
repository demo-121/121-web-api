"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.acceptance = exports.budget = exports.recommendation = undefined;

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _common = require("./common");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var recommendation = exports.recommendation = {
  value: _propTypes2.default.shape({
    chosenList: _propTypes2.default.arrayOf(_propTypes2.default.string.isRequired).isRequired,
    notChosenList: _propTypes2.default.arrayOf(_propTypes2.default.string).isRequired
  }),
  valueByQuotId: _propTypes2.default.shape({
    benefit: _propTypes2.default.string.isRequired,
    limitation: _propTypes2.default.string.isRequired,
    reason: _propTypes2.default.string.isRequired,
    rop: _propTypes2.default.shape({
      choiceQ1: _propTypes2.default.string.isRequired,
      choiceQ1Sub1: _propTypes2.default.string.isRequired,
      choiceQ1Sub2: _propTypes2.default.string.isRequired,
      choiceQ1Sub3: _propTypes2.default.string.isRequired,
      existCi: _propTypes2.default.number.isRequired,
      existLife: _propTypes2.default.number.isRequired,
      existPaAdb: _propTypes2.default.number.isRequired,
      existTotalPrem: _propTypes2.default.number.isRequired,
      existTpd: _propTypes2.default.number.isRequired,
      replaceCi: _propTypes2.default.number,
      replaceLife: _propTypes2.default.number,
      replacePaAdb: _propTypes2.default.number,
      replaceTotalPrem: _propTypes2.default.number,
      replaceTpd: _propTypes2.default.number
    }).isRequired,
    rop_shield: _propTypes2.default.shape({
      ropBlock: _propTypes2.default.shape({
        iCidRopAnswerMap: _propTypes2.default.objectOf(_propTypes2.default.string).isRequired,
        ropQ1sub3: _propTypes2.default.string.isRequired,
        ropQ2: _propTypes2.default.string.isRequired,
        ropQ3: _propTypes2.default.string.isRequired,
        shieldRopAnswer_0: _propTypes2.default.string.isRequired,
        shieldRopAnswer_1: _propTypes2.default.string.isRequired,
        shieldRopAnswer_2: _propTypes2.default.string.isRequired,
        shieldRopAnswer_3: _propTypes2.default.string.isRequired,
        shieldRopAnswer_4: _propTypes2.default.string.isRequired,
        shieldRopAnswer_5: _propTypes2.default.string.isRequired
      }).isRequired
    }).isRequired
  }),
  error: _propTypes2.default.shape({
    benefit: _common.error.isRequired,
    limitation: _common.error.isRequired,
    reason: _common.error.isRequired,
    rop: _propTypes2.default.shape({
      choiceQ1: _common.error.isRequired,
      choiceQ1Sub1: _common.error.isRequired,
      choiceQ1Sub2: _common.error.isRequired,
      choiceQ1Sub3: _common.error.isRequired,
      existCi: _common.error.isRequired,
      existLife: _common.error.isRequired,
      existPaAdb: _common.error.isRequired,
      existTotalPrem: _common.error.isRequired,
      existTpd: _common.error.isRequired,
      replaceCi: _common.error.isRequired,
      replaceLife: _common.error.isRequired,
      replacePaAdb: _common.error.isRequired,
      replaceTotalPrem: _common.error.isRequired,
      replaceTpd: _common.error.isRequired
    }).isRequired,
    rop_shield: _propTypes2.default.shape({
      ropBlock: _propTypes2.default.shape({
        iCidRopAnswerMap: _propTypes2.default.objectOf(_common.error).isRequired,
        ropQ1sub3: _common.error.isRequired,
        ropQ2: _common.error.isRequired,
        ropQ3: _common.error.isRequired,
        shieldRopAnswer_0: _common.error.isRequired,
        shieldRopAnswer_1: _common.error.isRequired,
        shieldRopAnswer_2: _common.error.isRequired,
        shieldRopAnswer_3: _common.error.isRequired,
        shieldRopAnswer_4: _common.error.isRequired,
        shieldRopAnswer_5: _common.error.isRequired
      }).isRequired
    }).isRequired
  })
};

var budget = exports.budget = {
  value: _propTypes2.default.shape({
    spBudget: _propTypes2.default.number.isRequired,
    spTotalPremium: _propTypes2.default.number.isRequired,
    spCompare: _propTypes2.default.number.isRequired,
    spCompareResult: _propTypes2.default.string.isRequired,
    rpBudget: _propTypes2.default.number.isRequired,
    rpTotalPremium: _propTypes2.default.number.isRequired,
    rpCompare: _propTypes2.default.number.isRequired,
    rpCompareResult: _propTypes2.default.string.isRequired,
    cpfOaBudget: _propTypes2.default.number.isRequired,
    cpfOaTotalPremium: _propTypes2.default.number.isRequired,
    cpfOaCompare: _propTypes2.default.number.isRequired,
    cpfOaCompareResult: _propTypes2.default.string.isRequired,
    cpfSaBudget: _propTypes2.default.number.isRequired,
    cpfSaTotalPremium: _propTypes2.default.number.isRequired,
    cpfSaCompare: _propTypes2.default.number.isRequired,
    cpfSaCompareResult: _propTypes2.default.string.isRequired,
    srsBudget: _propTypes2.default.number.isRequired,
    srsTotalPremium: _propTypes2.default.number.isRequired,
    srsCompare: _propTypes2.default.number.isRequired,
    srsCompareResult: _propTypes2.default.string.isRequired,
    cpfMsBudget: _propTypes2.default.number.isRequired,
    cpfMsTotalPremium: _propTypes2.default.number.isRequired,
    cpfMsCompare: _propTypes2.default.number.isRequired,
    cpfMsCompareResult: _propTypes2.default.string.isRequired,
    budgetMoreChoice: _propTypes2.default.string.isRequired,
    budgetMoreReason: _propTypes2.default.string.isRequired,
    budgetLessChoice: _propTypes2.default.string.isRequired,
    budgetLessReason: _propTypes2.default.string.isRequired
  }),
  error: _propTypes2.default.shape({
    spBudget: _common.error.isRequired,
    spTotalPremium: _common.error.isRequired,
    spCompare: _common.error.isRequired,
    spCompareResult: _common.error.isRequired,
    rpBudget: _common.error.isRequired,
    rpTotalPremium: _common.error.isRequired,
    rpCompare: _common.error.isRequired,
    rpCompareResult: _common.error.isRequired,
    cpfOaBudget: _common.error.isRequired,
    cpfOaTotalPremium: _common.error.isRequired,
    cpfOaCompare: _common.error.isRequired,
    cpfOaCompareResult: _common.error.isRequired,
    cpfSaBudget: _common.error.isRequired,
    cpfSaTotalPremium: _common.error.isRequired,
    cpfSaCompare: _common.error.isRequired,
    cpfSaCompareResult: _common.error.isRequired,
    srsBudget: _common.error.isRequired,
    srsTotalPremium: _common.error.isRequired,
    srsCompare: _common.error.isRequired,
    srsCompareResult: _common.error.isRequired,
    cpfMsBudget: _common.error.isRequired,
    cpfMsTotalPremium: _common.error.isRequired,
    cpfMsCompare: _common.error.isRequired,
    cpfMsCompareResult: _common.error.isRequired,
    budgetMoreChoice: _common.error.isRequired,
    budgetMoreReason: _common.error.isRequired,
    budgetLessChoice: _common.error.isRequired,
    budgetLessReason: _common.error.isRequired
  })
};

var acceptance = exports.acceptance = {
  value: _propTypes2.default.arrayOf(_propTypes2.default.shape({
    quotationDocId: _propTypes2.default.string.isRequired,
    proposerAndLifeAssuredName: _propTypes2.default.string.isRequired,
    basicPlanName: _propTypes2.default.string.isRequired,
    ridersName: _propTypes2.default.arrayOf(_propTypes2.default.shape({
      en: _propTypes2.default.string.isRequired,
      "zh-Hant": _propTypes2.default.string.isRequired
    })).isRequired
  }))
};