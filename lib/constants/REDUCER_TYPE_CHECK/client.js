"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.dependantProfiles = exports.contactList = exports.profile = undefined;

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var profile = exports.profile = {
  isRequired: _propTypes2.default.shape({
    addrBlock: _propTypes2.default.string,
    addrCity: _propTypes2.default.string,
    addrCountry: _propTypes2.default.string,
    addrEstate: _propTypes2.default.string,
    addrStreet: _propTypes2.default.string,
    age: _propTypes2.default.number,
    agentId: _propTypes2.default.string,
    allowance: _propTypes2.default.number,
    applicationCount: _propTypes2.default.number,
    branchInfo: _propTypes2.default.shape({
      bankRefId: _propTypes2.default.string,
      branch: _propTypes2.default.string
    }),
    bundle: _propTypes2.default.arrayOf(_propTypes2.default.shape({
      id: _propTypes2.default.string.isRequired,
      isValid: _propTypes2.default.bool.isRequired
    })),
    cid: _propTypes2.default.string,
    compCode: _propTypes2.default.string,
    dealerGroup: _propTypes2.default.string,
    dependants: _propTypes2.default.arrayOf(_propTypes2.default.shape({
      cid: _propTypes2.default.string.isRequired,
      relationship: _propTypes2.default.string.isRequired,
      relationshipOther: _propTypes2.default.string
    })),
    dob: _propTypes2.default.string,
    education: _propTypes2.default.string,
    email: _propTypes2.default.string,
    employStatus: _propTypes2.default.string,
    firstName: _propTypes2.default.string,
    fnaRecordIdArray: _propTypes2.default.string,
    fullName: _propTypes2.default.string,
    gender: _propTypes2.default.string,
    hanyuPinyinName: _propTypes2.default.string,
    haveSignDoc: _propTypes2.default.bool,
    idCardNo: _propTypes2.default.string,
    idDocType: _propTypes2.default.string,
    idDocTypeOther: _propTypes2.default.string,
    industry: _propTypes2.default.string,
    initial: _propTypes2.default.string,
    isSmoker: _propTypes2.default.string,
    isValid: _propTypes2.default.bool,
    language: _propTypes2.default.string,
    lastName: _propTypes2.default.string,
    lastUpdateDate: _propTypes2.default.string,
    marital: _propTypes2.default.string,
    mobileCountryCode: _propTypes2.default.string,
    mobileNo: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
    nameOrder: _propTypes2.default.string,
    nationality: _propTypes2.default.string,
    nearAge: _propTypes2.default.number,
    occupation: _propTypes2.default.string,
    occupationOther: _propTypes2.default.string,
    organization: _propTypes2.default.string,
    organizationCountry: _propTypes2.default.string,
    othName: _propTypes2.default.string,
    otherMobileCountryCode: _propTypes2.default.string,
    otherNo: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
    otherResidenceCity: _propTypes2.default.string,
    pass: _propTypes2.default.string,
    passExpDate: _propTypes2.default.number,
    photo: _propTypes2.default.object,
    postalCode: _propTypes2.default.string,
    prStatus: _propTypes2.default.string,
    referrals: _propTypes2.default.string,
    residenceCity: _propTypes2.default.string,
    residenceCountry: _propTypes2.default.string,
    title: _propTypes2.default.string,
    trustedIndividuals: _propTypes2.default.shape({
      firstName: _propTypes2.default.string.isRequired,
      fullName: _propTypes2.default.string.isRequired,
      idCardNo: _propTypes2.default.string.isRequired,
      idDocType: _propTypes2.default.string.isRequired,
      lastName: _propTypes2.default.string.isRequired,
      mobileCountryCode: _propTypes2.default.string.isRequired,
      mobileNo: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number, _propTypes2.default.object]),
      nameOrder: _propTypes2.default.string.isRequired,
      relationship: _propTypes2.default.string.isRequired,
      tiPhoto: _propTypes2.default.string.isRequired
    }),
    type: _propTypes2.default.string,
    unitNum: _propTypes2.default.string
  })
};

var contactList = exports.contactList = {
  isRequired: _propTypes2.default.arrayOf(_propTypes2.default.shape({
    applicationCount: _propTypes2.default.number.isRequired,
    email: _propTypes2.default.string.isRequired,
    firstName: _propTypes2.default.string.isRequired,
    fullName: _propTypes2.default.string.isRequired,
    id: _propTypes2.default.string.isRequired,
    idCardNo: _propTypes2.default.string.isRequired,
    idDocType: _propTypes2.default.string.isRequired,
    lastName: _propTypes2.default.string.isRequired,
    mobileNo: _propTypes2.default.oneOfType([_propTypes2.default.string, _propTypes2.default.number]),
    nameOrder: _propTypes2.default.string.isRequired,
    photo: _propTypes2.default.oneOfType([_propTypes2.default.number, _propTypes2.default.object, _propTypes2.default.string]).isRequired
  }))
};

var dependantProfiles = exports.dependantProfiles = {
  isRequired: _propTypes2.default.objectOf(profile.isRequired).isRequired
};