"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  isRequired: _propTypes2.default.objectOf(_propTypes2.default.shape({
    type: _propTypes2.default.string,
    options: _propTypes2.default.oneOfType([_propTypes2.default.arrayOf(_propTypes2.default.shape({
      title: _propTypes2.default.objectOf(_propTypes2.default.string),
      value: _propTypes2.default.oneOfType([_propTypes2.default.number, _propTypes2.default.string])
    })), _propTypes2.default.objectOf(_propTypes2.default.shape({
      title: _propTypes2.default.objectOf(_propTypes2.default.string)
    }))])
  }))
};