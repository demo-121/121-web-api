"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _REDUCER_TYPES = require("./REDUCER_TYPES");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

exports.default = _defineProperty({}, _REDUCER_TYPES.FNA, {
  NEEDS: "needs",
  ROI: "ROI",
  ANALYSIS: "analysis",
  PRIORITY: "priority",
  PRODUCT_TYPES: "productTypes"
});