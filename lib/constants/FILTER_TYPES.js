"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = {
  APPLICATION_LIST: {
    ALL: "ALL",
    PROPOSED: "PROPOSED",
    APPLYING: "APPLYING",
    SUBMITTED: "SUBMITTED",
    INVALIDATED: "INVALIDATED",
    INVALIDATED_SIGNED: "INVALIDATED_SIGNED"
  }
};