"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var FIPROTECTION = exports.FIPROTECTION = "fiProtection";
var CIPROTECTION = exports.CIPROTECTION = "ciProtection";
var DIPROTECTION = exports.DIPROTECTION = "diProtection";
var PAPROTECTION = exports.PAPROTECTION = "paProtection";
var PCHEADSTART = exports.PCHEADSTART = "pcHeadstart";
var HCPROTECTION = exports.HCPROTECTION = "hcProtection";
var PSGOALS = exports.PSGOALS = "psGoals";
var EPLANNING = exports.EPLANNING = "ePlanning";
var RPLANNING = exports.RPLANNING = "rPlanning";
var OTHER = exports.OTHER = "other";
var AVGINFLATRATE = exports.AVGINFLATRATE = 2.65;
var RETIREAGE = exports.RETIREAGE = 65;