import ACTION_TYPES from "../constants/ACTION_TYPES";
import { QUOTATION } from "../constants/REDUCER_TYPES";

/**
 * cleanQuotation
 * @description get quotation data
 * @reducer quotation
 * @param {function} dispatch - redux dispatch function
 * */
export const cleanQuotation = ({ dispatch }) => {
  dispatch({
    type: ACTION_TYPES[QUOTATION].CLEAN_QUOTATION
  });
};

/**
 * quickQuoteOnChange
 * @description change reducer quotation.isQuickQuote state
 * @reducer quotation.isQuickQuote
 * @param {function} dispatch - redux dispatch function
 * @param {string} isQuickQuoteData - new isQuickQuote data
 * */
export const quickQuoteOnChange = ({ dispatch, isQuickQuoteData }) => {
  dispatch({
    type: ACTION_TYPES[QUOTATION].UPDATE_IS_QUICK_QUOTE,
    isQuickQuoteData
  });
};

/**
 * getQuotation
 * @description get quotation data
 * @param {function} dispatch - redux dispatch function
 * @param {string} productID - ID of quotation source product
 * @param {function} openQuotationFunction - callback when quotation require
 *   data is ready
 * */
export const getQuotation = ({
  dispatch,
  productID,
  openQuotationFunction
}) => {
  dispatch({
    type: ACTION_TYPES[QUOTATION].GET_QUOTATION,
    productID,
    openQuotationFunction
  });
};

/**
 * getFunds
 * @description get quotation data
 * @param {function} dispatch - redux dispatch function
 * @param {string} productID - ID of quotation source product
 * @param {function} invOpt - investment option
 * @param {string} paymentMethod - payment method
 * @param {function} openSelectFundsDialogFunction - call back when select
 *   funds dialog require data is ready
 * */
export const getFunds = ({
  dispatch,
  productID,
  invOpt,
  paymentMethod,
  openSelectFundsDialogFunction
}) => {
  dispatch({
    type: ACTION_TYPES[QUOTATION].GET_FUNDS,
    productID,
    invOpt,
    paymentMethod,
    openSelectFundsDialogFunction
  });
};

/**
 * updateQuotation
 * @description update quotation
 * @reducer quotation.quotation
 * @param {function} dispatch - redux dispatch function
 * @param {object} newQuotation - new quotation object
 * */
export const updateQuotation = ({ dispatch, newQuotation }) => {
  dispatch({
    type: ACTION_TYPES[QUOTATION].UPDATE_QUOTATION,
    newQuotation
  });
};

/**
 * updatePlanDetails
 * @description update quotation plan details
 * @reducer quotation.planDetails
 * @param {function} dispatch - redux dispatch function
 * @param {object} newPlanDetails - new quotation plan details object
 * */
export const updatePlanDetails = ({ dispatch, newPlanDetails }) => {
  dispatch({
    type: ACTION_TYPES[QUOTATION].UPDATE_PLAN_DETAILS,
    newPlanDetails
  });
};

/**
 * updateInputConfigs
 * @description update quotation input configs
 * @reducer quotation.inputConfigs
 * @param {function} dispatch - redux dispatch function
 * @param {object} newQuotation - new quotation input configs object
 * */
export const updateInputConfigs = ({ dispatch, newInputConfigs }) => {
  dispatch({
    type: ACTION_TYPES[QUOTATION].UPDATE_INPUT_CONFIGS,
    newInputConfigs
  });
};

/**
 * updateQuotationErrors
 * @description update quotation errors configs
 * @reducer quotation.quotationErrors
 * @param {function} dispatch - redux dispatch function
 * @param {object} newQuotationErrors - new quotation errors object
 * */
export const updateQuotationErrors = ({ dispatch, newQuotationErrors }) => {
  dispatch({
    type: ACTION_TYPES[QUOTATION].UPDATE_QUOTATION_ERRORS,
    newQuotationErrors
  });
};

/**
 * updateQuotationWarnings
 * @description update quotation warning configs
 * @reducer quotation.quotationWarning
 * @param {function} dispatch - redux dispatch function
 * @param {object} newQuotationWarnings - new quotation warning object
 * */
export const updateQuotationWarnings = ({ dispatch, newQuotationWarnings }) => {
  dispatch({
    type: ACTION_TYPES[QUOTATION].UPDATE_QUOTATION_WARNINGS,
    newQuotationWarnings
  });
};

/**
 * updateAvailableInsureds
 * @description update quotation available insureds configs
 * @reducer quotation.availableInsureds
 * @param {function} dispatch - redux dispatch function
 * @param {object} newAvailableInsureds - new quotation available insureds
 *   object
 * */
export const updateAvailableInsureds = ({ dispatch, newAvailableInsureds }) => {
  dispatch({
    type: ACTION_TYPES[QUOTATION].UPDATE_AVAILABLE_INSUREDS,
    newAvailableInsureds
  });
};

/**
 * updateAvailableFunds
 * @description update quotation available funds configs
 * @reducer quotation.availableFunds
 * @param {function} dispatch - redux dispatch function
 * @param {object} newAvailableFunds - new quotation available funds object
 * */
export const updateAvailableFunds = ({ dispatch, newAvailableFunds }) => {
  dispatch({
    type: ACTION_TYPES[QUOTATION].UPDATE_AVAILABLE_FUNDS,
    newAvailableFunds
  });
};
