import ACTION_TYPES from "../constants/ACTION_TYPES";
import { FNA } from "../constants/REDUCER_TYPES";
import PROGRESS_TABS from "../constants/PROGRESS_TABS";

export const updatePDA = ({ dispatch, value }) => {
  dispatch({
    type: ACTION_TYPES[FNA].UPDATE_PDA,
    value
  });
};

export const updatePDAValue = ({ dispatch, target, value }) => {
  switch (target) {
    case "applicant":
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_PDA_APPLICATE,
        newPda: value
      });
      break;
    case "dependants":
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_PDA_DEPENDANTS,
        newPda: value
      });
      break;
    case "isCompleted":
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_PDA_ISCOMPLETED,
        newPda: value
      });
      break;
    case "lastStepIndex":
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_PDA_LAST_STEP_INDEX,
        newPda: value
      });
      break;
    case "applicantHasChanged":
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_APPLICANT_HAS_CHANGED,
        newPda: value
      });
      break;
    case "consentNoticesHasChanged":
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_CONSENT_NOTICES_HAS_CHANGED,
        newPda: value
      });
      break;
    case "ownerConsentMethod":
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_PDA_OWNERCONSENTMETHOD,
        newPda: value
      });
      break;
    case "spouseConsentMethod":
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_PDA_SPOUSECONSENTMETHOD,
        newPda: value
      });
      break;
    case "trustedIndividual":
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_PDA_TRUSTINDIVIDUAL,
        newPda: value
      });
      break;
    default:
      break;
  }
};

/**
 * updateNAValue
 * @description Update the Needs Analysis Value
 * @param {function} dispatch - redux dispatch function
 * @param {string} target - which type of Needs Analysis out of 5
 * @param {object} value - the content which use to change
 * */
export const updateNAValue = ({ dispatch, target, value }) => {
  switch (target) {
    case PROGRESS_TABS[FNA].NEEDS:
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_NA_NEEDS,
        value
      });
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_NA_ASPECTS,
        value
      });
      break;
    case PROGRESS_TABS[FNA].PRIORITY:
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_NA_PRIORITY,
        value
      });
      break;
    case PROGRESS_TABS[FNA].PRODUCT_TYPES:
      dispatch({
        type: ACTION_TYPES[FNA].UPDATE_NA_PRODUCT_TYPES_COMPLETED_STEP,
        value
      });
      break;

    default:
      break;
  }
};

/**
 * updateCKAValue
 * @description Update the Customer Knowledge Assessment Value
 * @param {function} dispatch - redux dispatch function
 * @param {string} key - the CKA key which need to change
 * @param {object} value - the value which need to change
 * */
export const updateCKAValue = ({ dispatch, data }) => {
  dispatch({
    type: ACTION_TYPES[FNA].UPDATE_CKA,
    data
  });
};
