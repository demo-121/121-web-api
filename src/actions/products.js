import ACTION_TYPES from "../constants/ACTION_TYPES";
import { PRODUCTS } from "../constants/REDUCER_TYPES";

/**
 * getDependants
 * @description get products required dependants data
 * @param {function} dispatch - redux dispatch function
 * */
export const getDependants = ({ dispatch }) => {
  dispatch({
    type: ACTION_TYPES[PRODUCTS].GET_DEPENDANTS
  });
};
/**
 * getProductList
 * @description get product list
 * @param {function} dispatch - redux dispatch function
 * @param {function} callback - callback of this dispatch
 * */
export const getProductList = ({ dispatch, callback = () => {} }) => {
  dispatch({
    type: ACTION_TYPES[PRODUCTS].GET_PRODUCT_LIST,
    callback
  });
};
/**
 * currencyOnChange
 * @description get product list
 * @param {function} dispatch - redux dispatch function
 * @param {string} currencyData - new currency data
 * */
export const currencyOnChange = ({ dispatch, currencyData }) => {
  dispatch({
    type: ACTION_TYPES[PRODUCTS].UPDATE_CURRENCY,
    currencyData
  });
};
/**
 * insuredOnChange
 * @description get product list
 * @param {function} dispatch - redux dispatch function
 * @param {string} insuredCidData - new insured data
 * */
export const insuredOnChange = ({ dispatch, insuredCidData }) => {
  dispatch({
    type: ACTION_TYPES[PRODUCTS].UPDATE_INSURED_CID,
    insuredCidData
  });
};
