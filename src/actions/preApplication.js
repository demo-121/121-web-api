import ACTION_TYPES from "../constants/ACTION_TYPES";
import { PRE_APPLICATION } from "../constants/REDUCER_TYPES";

/**
 * saga actions
 */

/**
 * deleteApplications
 * @description delete applications selected in app/web
 * @reducer preApplication
 * @param {function} dispatch - redux dispatch function
 * */
export const deleteApplications = ({ dispatch, callback }) => {
  dispatch({
    type: ACTION_TYPES[PRE_APPLICATION].SAGA_DELETE_APPLICATIONS,
    callback
  });
};

/**
 * updateRecommendChoiceList
 * @description update recommendation choice list selected in app/web
 * @reducer preApplication
 * @param {function} dispatch - redux dispatch function
 * @param {object} choiceData - updated data from app/web
 * @param {string} choiceData.id - quotation ID
 * @param {bool} choiceData.checked - choice is selected or not for the quotation
 * */
export const updateRecommendChoiceList = ({ dispatch, choiceData }) => {
  dispatch({
    type: ACTION_TYPES[PRE_APPLICATION].SAGA_UPDATE_RECOMMENDATION_CHOICE_LIST,
    choiceData
  });
};

/**
 * updateUiSelectedList
 * @description keep the select list under select mode in app/web UI
 * @reducer preApplication
 * @param {function} dispatch - redux dispatch function
 * @param {object} changeData - updated data from app/web
 * @param {string} changeData.id - quotation ID / application ID
 * @param {bool} changeData.selected - updated data from app/web
 * */
export const updateUiSelectedList = ({ dispatch, changeData }) => {
  dispatch({
    type: ACTION_TYPES[PRE_APPLICATION].SAGA_UPDATE_UI_SELECTED_LIST,
    changeData
  });
};

/**
 * normal actions
 */

/**
 * updateUiSelectMode
 * @description indicate if select mode is on in app/web UI
 * @reducer preApplication
 * @param {function} dispatch - redux dispatch function
 * @param {bool} selectModeOn - flag to indicate select mode is on or not
 * */
export const updateUiSelectMode = ({ dispatch, selectModeOn }) => {
  dispatch({
    type: ACTION_TYPES[PRE_APPLICATION].UPDATE_UI_SELECT_MODE,
    selectModeOn
  });
};

/**
 * updateUiAppListFilter
 * @description indicate which application list filter is selected in app/web UI
 * @reducer preApplication
 * @param {function} dispatch - redux dispatch function
 * @param {number} selectedAppListFilter - selected filter index
 * */
export const updateUiAppListFilter = ({ dispatch, selectedAppListFilter }) => {
  dispatch({
    type: ACTION_TYPES[PRE_APPLICATION].UPDATE_UI_APPLICATION_LIST_FILTER,
    selectedAppListFilter
  });
};
