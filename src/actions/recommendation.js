import ACTION_TYPES from "../constants/ACTION_TYPES";
import { RECOMMENDATION } from "../constants/REDUCER_TYPES";

/**
 * saga actions
 */

/**
 * getRecommendation
 * @description get data when open Recommendation page
 * @reducer recommendation
 * @param {function} dispatch - redux dispatch function
 * */
export const getRecommendation = ({ dispatch }) => {
  dispatch({
    type: ACTION_TYPES[RECOMMENDATION].SAGA_GET_RECOMMENDATION
  });
};

/**
 * closeRecommendation
 * @description save data when leave Recommendation page
 * @reducer recommendation
 * @param {function} dispatch - redux dispatch function
 * */
export const closeRecommendation = ({ dispatch }) => {
  dispatch({
    type: ACTION_TYPES[RECOMMENDATION].SAGA_CLOSE_RECOMMENDATION
  });
};

/**
 * updateRecommendationStep
 * @description update step info when move to next step
 * @reducer recommendation.recommendation
 * @reducer recommendation.budget
 * @reducer recommendation.acceptance
 * @reducer recommendation.component
 * @param {function} dispatch - redux dispatch function
 * @param {number} newCurrentStep - next step index
 * */
export const updateRecommendationStep = ({ dispatch, newCurrentStep }) => {
  dispatch({
    type: ACTION_TYPES[RECOMMENDATION].SAGA_UPDATE_RECOMMENDATION_STEP,
    newCurrentStep
  });
};

/**
 * validateRecommendation
 * @description perform whole page validation in recommendation step
 * @reducer recommendation.error
 * @reducer recommendation.component
 * @param {function} dispatch - redux dispatch function
 * @param {boolean} init - indicate the action is called when init component
 * */
export const validateRecommendation = ({ dispatch, init }) => {
  dispatch({
    type: ACTION_TYPES[RECOMMENDATION].SAGA_VALIDATE_RECOMMENDATION,
    init
  });
};

/**
 * updateRecommendationData
 * @description update data, error and component when user input value in recommendation step
 * @reducer recommendation.recommendation
 * @reducer recommendation.error
 * @reducer recommendation.component
 * @param {function} dispatch - redux dispatch function
 * @param {string} quotationId - quotation Id of new data for recommendation
 * @param {object} newValue - new data for recommendation
 * */
export const updateRecommendationData = ({
  dispatch,
  quotationId,
  newValue
}) => {
  dispatch({
    type: ACTION_TYPES[RECOMMENDATION].SAGA_UPDATE_RECOMMENDATION_DATA,
    quotationId,
    newValue
  });
};

/**
 * validateBudget
 * @description perform whole page validation in budget step
 * @reducer recommendation.error
 * @reducer recommendation.component
 * @param {function} dispatch - redux dispatch function
 * */
export const validateBudget = ({ dispatch }) => {
  dispatch({
    type: ACTION_TYPES[RECOMMENDATION].SAGA_VALIDATE_BUDGET
  });
};

/**
 * updateBudgetData
 * @description update data, error and component when user input value in budget step
 * @reducer recommendation.budget
 * @reducer recommendation.error
 * @reducer recommendation.component
 * @param {function} dispatch - redux dispatch function
 * @param {object} newValue - new data for budget
 * @param {object} newDataError - new error object for budget
 * */
export const updateBudgetData = ({ dispatch, newValue }) => {
  dispatch({
    type: ACTION_TYPES[RECOMMENDATION].SAGA_UPDATE_BUDGET_DATA,
    newValue
  });
};

/**
 * normal actions
 */

/**
 * updateRecommendationSelectedQuotation
 * @description update selected quotation id when click section list in Recommendation step
 * @reducer recommendation.component
 * @param {function} dispatch - redux dispatch function
 * @param {string} newSelectedQuotId - quotation id for selected quotation
 * */
export const updateRecommendationSelectedQuotation = ({
  dispatch,
  newSelectedQuotId
}) => {
  dispatch({
    type: ACTION_TYPES[RECOMMENDATION].UPDATE_RECOMMENDATION_SELECTED_QUOT,
    newSelectedQuotId
  });
};

/**
 * updateRecommendationDetails
 * @description update step when user click to next step
 * @reducer recommendation.component
 * @param {function} dispatch - redux dispatch function
 * @param {boolean} newShowDetails - show details flag
 * */
export const updateRecommendationDetails = ({ dispatch, newShowDetails }) => {
  dispatch({
    type: ACTION_TYPES[RECOMMENDATION].UPDATE_RECOMMENDATION_DETAILS,
    newShowDetails
  });
};
