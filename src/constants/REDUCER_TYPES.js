/**
 * although we save config constant in web-api, but the reducer source should
 * place in the front-end project. Because this reducer should be platform
 * specific
 *  */
export const CONFIG = "config";
export const CLIENT = "client";
export const FNA = "fna";
export const FNA_CKA = "fnaCKA";
export const FNA_RA = "fnaRA";
export const CLIENT_FORM = "clientForm";
export const PRE_APPLICATION = "preApplication";
export const RECOMMENDATION = "recommendation";
export const QUOTATION = "quotation";
export const PROPOSAL = "proposal";
export const PRODUCTS = "products";
export const OPTIONS_MAP = "optionsMap";
export const AGENT = "agent";
export const AGENTUX = "agentUX";
export const COMMON = "common";
export const APPLICATION = "application";
export const SUPPORTING_DOCUMENT = "supportingDocument";
export const PAYMENT_AND_SUBMISSION = "paymentAndSubmission";
export const PAYMENT = "payment";
export const SIGNATURE = "signature";
export const LOGIN = "login";
