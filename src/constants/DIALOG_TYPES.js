export const CONFIRMATION = "CONFIRMATION";

// User can continue after clicking
export const WARNING = "WARNING";

// User cannot continue after clicking
export const ERROR = "ERROR";

export const INVALID_DEFAULT = "requoteInvalidDefaultMsg";

export const OTHER = "OTHER";
