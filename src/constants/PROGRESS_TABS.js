import { FNA } from "./REDUCER_TYPES";

export default {
  [FNA]: {
    NEEDS: "needs",
    ROI: "ROI",
    ANALYSIS: "analysis",
    PRIORITY: "priority",
    PRODUCT_TYPES: "productTypes"
  }
};
