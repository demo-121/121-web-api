export const BI = "Policy Illustration";
export const PROD_SUMMARY = "Product Summary";
export const FPX_PROD_SUMMARY = "Product Summary";
export const SHIELD_PRODUCT_SUMMARY = "Product Summary of the accepted plan(s)";
export const PHS = "Product Highlight Sheet";
export const FIB = "Fund Info Booklet";
export const FNA = "Financial Needs Analysis";
