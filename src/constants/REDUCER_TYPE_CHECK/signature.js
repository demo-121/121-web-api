import PropTypes from "prop-types";

export const signature = {
  isRequired: PropTypes.shape({
    success: PropTypes.bool.isRequired,
    application: PropTypes.shape({
      showCoverTab: PropTypes.bool,
      isFaChannel: PropTypes.bool,
      tabCount: PropTypes.string,
      agentSignFields: PropTypes.array,
      clientSignFields: PropTypes.array,
      textFields: PropTypes.array,
      attachments: PropTypes.array.isRequired,
      isMandDocsAllUploaded: PropTypes.bool.isRequired,
      appStep: PropTypes.string
    }),
    signDoc: PropTypes.bool,
    crossAge: PropTypes.bool
  })
};

export const component = {
  isRequired: PropTypes.shape({
    selectedPdf: PropTypes.string
  })
};
