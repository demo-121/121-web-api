import PropTypes from "prop-types";
import { profile } from "./client";

export const isQuickQuote = {
  isRequired: PropTypes.bool.isRequired
};

export const quickQuotes = {
  isRequired: PropTypes.arrayOf(
    PropTypes.shape({
      covName: PropTypes.objectOf(PropTypes.string.isRequired).isRequired,
      createDate: PropTypes.string.isRequired,
      iFullName: PropTypes.string.isRequired,
      id: PropTypes.string.isRequired,
      lastUpdateDate: PropTypes.string.isRequired,
      pCid: PropTypes.string.isRequired,
      pFullName: PropTypes.string.isRequired,
      type: PropTypes.string.isRequired
    })
  ).isRequired
};

export const quotation = {
  isRequired: PropTypes.shape({
    type: PropTypes.oneOf(["quotation"]).isRequired,
    baseProductCode: PropTypes.string.isRequired,
    baseProductId: PropTypes.string.isRequired,
    productLine: PropTypes.string.isRequired,
    compCode: PropTypes.string.isRequired,
    agentCode: PropTypes.string.isRequired,
    dealerGroup: PropTypes.string.isRequired,
    agent: PropTypes.shape({
      agentCode: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      dealerGroup: PropTypes.string.isRequired,
      company: PropTypes.string.isRequired,
      tel: PropTypes.string.isRequired,
      mobile: PropTypes.string.isRequired,
      email: PropTypes.string.isRequired
    }).isRequired,
    lastUpdateDate: PropTypes.string,
    pCid: PropTypes.string.isRequired,
    pFullName: PropTypes.string.isRequired,
    pFirstName: PropTypes.string.isRequired,
    pLastName: PropTypes.string.isRequired,
    pGender: PropTypes.string.isRequired,
    pDob: PropTypes.string.isRequired,
    pAge: PropTypes.number.isRequired,
    pResidence: PropTypes.string.isRequired,
    pSmoke: PropTypes.oneOf(["Y", "N"]).isRequired,
    pEmail: PropTypes.string.isRequired,
    pOccupation: PropTypes.string.isRequired,
    paymentMode: PropTypes.PropTypes.oneOf(["A", "S", "Q", "M", "L"]),
    isBackDate: PropTypes.oneOf(["Y", "N"]).isRequired,
    riskCommenDate: PropTypes.string.isRequired,
    sameAs: PropTypes.oneOf(["Y", "N"]),
    iCid: PropTypes.string,
    iFullName: PropTypes.string,
    iFirstName: PropTypes.string,
    iLastName: PropTypes.string,
    iGender: PropTypes.string,
    iDob: PropTypes.string,
    iAge: PropTypes.number,
    iResidence: PropTypes.string,
    iSmoke: PropTypes.oneOf(["Y", "N"]),
    iEmail: PropTypes.string,
    iOccupation: PropTypes.string,
    ccy: PropTypes.string,
    plans: PropTypes.arrayOf(
      PropTypes.shape({
        covCode: PropTypes.string.isRequired,
        covName: PropTypes.objectOf(PropTypes.string).isRequired,
        covClass: PropTypes.string,
        policyTerm: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        policyTermYr: PropTypes.number,
        polTermDesc: PropTypes.string,
        premTerm: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        premTermYr: PropTypes.number,
        premTermDesc: PropTypes.string,
        calcBy: PropTypes.oneOf(["sumAssured", "premium"]),
        sumInsured: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        premium: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        yearPrem: PropTypes.number,
        halfYearPrem: PropTypes.number,
        quarterPrem: PropTypes.number,
        monthPrem: PropTypes.number,
        tax: PropTypes.shape({
          yearYax: PropTypes.number,
          halfYearTax: PropTypes.number,
          quarterTax: PropTypes.number,
          monthTax: PropTypes.number
        }),
        planCode: PropTypes.string
      })
    ),
    quotType: PropTypes.oneOf(["SHIELD"]),
    quickQuote: PropTypes.bool,
    fund: PropTypes.shape({
      funds: PropTypes.arrayOf(
        PropTypes.shape({
          alloc: PropTypes.number,
          fundCode: PropTypes.string.isRequired,
          fundName: PropTypes.objectOf(PropTypes.string).isRequired
        })
      ).isRequired,
      invOpt: PropTypes.string.isRequired,
      portfolio: PropTypes.string
    }),
    policyOptions: PropTypes.shape({}),
    premium: PropTypes.number,
    sumInsured: PropTypes.number,
    totHalfyearPrem: PropTypes.number,
    totMonthPrem: PropTypes.number,
    totQuarterPrem: PropTypes.number,
    totYearPrem: PropTypes.number,
    attachments: PropTypes.oneOfType([PropTypes.object])
  }).isRequired
};

/* TODO need more detail prop type check */
export const planDetails = {
  isRequired: PropTypes.objectOf(PropTypes.object).isRequired
};

/* TODO need more detail prop type check */
export const inputConfigs = {
  isRequired: PropTypes.objectOf(PropTypes.object).isRequired
};

/* TODO need more detail prop type check */
export const planErrors = {
  isRequired: PropTypes.objectOf(PropTypes.object).isRequired
};

/* TODO need more detail prop type check */
export const quotationErrors = {
  isRequired: PropTypes.objectOf(
    PropTypes.oneOfType([PropTypes.object, PropTypes.array])
  ).isRequired
};

/* TODO need more detail prop type check */
export const quotWarnings = {
  isRequired: PropTypes.arrayOf(PropTypes.string).isRequired
};

/* TODO need more detail prop type check */
export const availableInsureds = {
  isRequired: PropTypes.arrayOf(
    PropTypes.shape({
      availableClasses: PropTypes.arrayOf(
        PropTypes.shape({
          covClass: PropTypes.string.isRequired,
          className: PropTypes.objectOf(PropTypes.string).isRequired
        })
      ),
      eligible: PropTypes.bool.isRequired,
      profile: profile.isRequired,
      valid: PropTypes.bool.isRequired
    })
  ).isRequired
};

/* TODO need more detail prop type check */
export const availableFunds = {
  isRequired: PropTypes.arrayOf(PropTypes.object).isRequired
};
