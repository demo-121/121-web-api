import PropTypes from "prop-types";

export default {
  isRequired: PropTypes.objectOf(
    PropTypes.shape({
      type: PropTypes.string,
      options: PropTypes.oneOfType([
        PropTypes.arrayOf(
          PropTypes.shape({
            title: PropTypes.objectOf(PropTypes.string),
            value: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
          })
        ),
        PropTypes.objectOf(
          PropTypes.shape({
            title: PropTypes.objectOf(PropTypes.string)
          })
        )
      ])
    })
  )
};
