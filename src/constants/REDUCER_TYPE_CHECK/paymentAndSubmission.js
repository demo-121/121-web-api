import PropTypes from "prop-types";

// TODO
export const submission = {
  isRequired: PropTypes.oneOfType([PropTypes.object])
};
export const payment = {
  isRequired: PropTypes.oneOfType([PropTypes.object])
};
export const error = {
  isRequired: PropTypes.oneOfType([PropTypes.object])
};
export const application = {
  isRequired: PropTypes.oneOfType([PropTypes.object])
};

export const paymentShield = {
  isRequired: PropTypes.shape({
    cpfMedisaveAccDetails: PropTypes.arrayOf(
      PropTypes.shape({
        cpfAccHolderName: PropTypes.string,
        cpfAccNo: PropTypes.string
      })
    ),
    initPayMethod: PropTypes.string,
    isCompleted: PropTypes.bool,
    premiumDetails: PropTypes.arrayOf(
      PropTypes.shape({
        applicationId: PropTypes.string,
        cashPortion: PropTypes.number,
        cid: PropTypes.string,
        covName: PropTypes.objectOf(
          PropTypes.shape({
            en: PropTypes.string,
            "zh-Hant": PropTypes.string
          })
        ),
        cpfPortion: PropTypes.number,
        laName: PropTypes.string,
        medisave: PropTypes.number,
        payFrequency: PropTypes.string,
        policyNumber: PropTypes.string,
        subseqPayMethod: PropTypes.string
      })
    ),
    totCPFPortion: 973,
    totCashPortion: 75.4,
    totMedisave: 2523
  })
};

export const template = {
  isRequired: PropTypes.objectOf(
    PropTypes.shape({
      items: PropTypes.arrayOf(
        PropTypes.objectOf(
          PropTypes.shape({
            items: PropTypes.oneOfType([PropTypes.array]),
            title: PropTypes.string,
            type: PropTypes.string
          })
        )
      ),
      type: PropTypes.string
    })
  )
};
