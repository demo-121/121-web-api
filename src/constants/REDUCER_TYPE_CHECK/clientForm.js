import PropTypes from "prop-types";
import { error } from "./common";

export const isCreate = {
  isRequired: PropTypes.bool
};

export const isFamilyMember = {
  isRequired: PropTypes.bool
};

export const isProposerMissing = {
  isRequired: PropTypes.bool
};

export const isFromProfile = {
  isRequired: PropTypes.bool
};

export const isFromProduct = {
  isRequired: PropTypes.bool
};

export const isApplication = {
  isRequired: PropTypes.bool
};

export const isPDA = {
  isRequired: PropTypes.bool
};

export const relationship = {
  isRequired: PropTypes.string.isRequired
};

export const isRelationshipError = {
  isRequired: error
};

export const relationshipOther = {
  isRequired: PropTypes.string.isRequired
};

export const isRelationshipOtherError = {
  isRequired: error
};

export const givenName = {
  isRequired: PropTypes.string.isRequired
};

export const isGivenNameError = {
  isRequired: error
};

export const surname = {
  isRequired: PropTypes.string.isRequired
};

export const otherName = {
  isRequired: PropTypes.string.isRequired
};

export const hanYuPinYinName = {
  isRequired: PropTypes.string.isRequired
};

export const haveSignDoc = {
  isRequired: PropTypes.bool
};

export const name = {
  isRequired: PropTypes.string.isRequired
};

export const isNameError = {
  isRequired: error
};

export const nameOrder = {
  isRequired: PropTypes.string.isRequired
};

export const title = {
  isRequired: PropTypes.string.isRequired
};

export const isTitleError = {
  isRequired: error
};

export const gender = {
  isRequired: PropTypes.string.isRequired
};

export const isGenderError = {
  isRequired: error
};

export const birthday = {
  isRequired: PropTypes.string.isRequired
};

export const isBirthdayError = {
  isRequired: error
};

export const nationality = {
  isRequired: PropTypes.string.isRequired
};

export const isNationalityError = {
  isRequired: error
};

export const singaporePRStatus = {
  isRequired: PropTypes.string.isRequired
};

export const isSingaporePRStatusError = {
  isRequired: error
};

export const countryOfResidence = {
  isRequired: PropTypes.string.isRequired
};

export const cityOfResidence = {
  isRequired: PropTypes.string.isRequired
};

export const isCityOfResidenceError = {
  isRequired: error
};

export const otherCityOfResidence = {
  isRequired: PropTypes.string.isRequired
};

export const isOtherCityOfResidenceError = {
  isRequired: error
};

export const IDDocumentType = {
  isRequired: PropTypes.string.isRequired
};

export const IDDocumentTypeOther = {
  isRequired: PropTypes.string.isRequired
};

export const isIDDocumentTypeError = {
  isRequired: error
};

export const isIDDocumentTypeOtherError = {
  isRequired: error
};

export const ID = {
  isRequired: PropTypes.string.isRequired
};

export const isIDError = {
  isRequired: error
};

export const smokingStatus = {
  isRequired: PropTypes.string.isRequired
};

export const isSmokingError = {
  isRequired: error
};

export const maritalStatus = {
  isRequired: PropTypes.string.isRequired
};

export const isMaritalStatusError = {
  isRequired: error
};

export const language = {
  isRequired: PropTypes.string.isRequired
};

export const languageOther = {
  isRequired: PropTypes.string.isRequired
};

export const isLanguageError = {
  isRequired: error
};

export const isLanguageOtherError = {
  isRequired: error
};

export const education = {
  isRequired: PropTypes.string.isRequired
};

export const isEducationError = {
  isRequired: error
};

export const employStatus = {
  isRequired: PropTypes.string.isRequired
};

export const isEmployStatusError = {
  isRequired: error
};

export const employStatusOther = {
  isRequired: PropTypes.string.isRequired
};

export const isEmployStatusOtherError = {
  isRequired: error
};

export const industry = {
  isRequired: PropTypes.string.isRequired
};

export const isIndustryError = {
  isRequired: error
};

export const occupation = {
  isRequired: PropTypes.string.isRequired
};

export const isOccupationError = {
  isRequired: error
};

export const occupationOther = {
  isRequired: PropTypes.string.isRequired
};

export const isOccupationOtherError = {
  isRequired: error
};

export const nameOfEmployBusinessSchool = {
  isRequired: PropTypes.string.isRequired
};

export const countryOfEmployBusinessSchool = {
  isRequired: PropTypes.string.isRequired
};

export const typeOfPass = {
  isRequired: PropTypes.string.isRequired
};

export const typeOfPassOther = {
  isRequired: PropTypes.string.isRequired
};

export const isTypeOfPassError = {
  isRequired: error
};

export const isTypeOfPassOtherError = {
  isRequired: error
};

export const passExpiryDate = {
  isRequired: PropTypes.number.isRequired
};

export const income = {
  isRequired: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.oneOf([NaN, ""])
  ]).isRequired
};

export const isIncomeError = {
  isRequired: error
};

export const prefixA = {
  isRequired: PropTypes.string.isRequired
};

export const mobileNoA = {
  isRequired: PropTypes.string.isRequired
};

export const prefixB = {
  isRequired: PropTypes.string.isRequired
};

export const mobileNoB = {
  isRequired: PropTypes.string.isRequired
};

export const email = {
  isRequired: PropTypes.string.isRequired
};

export const isEmailError = {
  isRequired: error
};

export const country = {
  isRequired: PropTypes.string.isRequired
};

export const cityState = {
  isRequired: PropTypes.string.isRequired
};

export const postal = {
  isRequired: PropTypes.string.isRequired
};

export const blockHouse = {
  isRequired: PropTypes.string.isRequired
};

export const streetRoad = {
  isRequired: PropTypes.string.isRequired
};

export const unit = {
  isRequired: PropTypes.string.isRequired
};

export const buildingEstate = {
  isRequired: PropTypes.string.isRequired
};

export const photo = {
  isRequired: PropTypes.object.isRequired
};

export const isPrefixAError = {
  isRequired: error
};

export const isMobileNoAError = {
  isRequired: error
};

export const isShieldQuotation = {
  isRequired: PropTypes.bool
};
