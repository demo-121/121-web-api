import PropTypes from "prop-types";

export const currency = {
  isRequired: PropTypes.string.isRequired
};

export const insuredCid = {
  isRequired: PropTypes.oneOfType([PropTypes.string, PropTypes.oneOf([null])])
};

export const dependants = {
  isRequired: PropTypes.arrayOf(
    PropTypes.shape({
      cid: PropTypes.string.isRequired,
      fullName: PropTypes.string.isRequired
    })
  ).isRequired
};

export const productList = {
  isRequired: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.objectOf(PropTypes.string).isRequired,
      seq: PropTypes.number.isRequired,
      products: PropTypes.arrayOf(
        PropTypes.shape({
          compCode: PropTypes.string.isRequired,
          covCode: PropTypes.string.isRequired,
          covName: PropTypes.objectOf(PropTypes.string).isRequired,
          version: PropTypes.number.isRequired
        })
      ).isRequired
    })
  ).isRequired
};
