export const DATE_TIME_FORMAT = "dd mmm yyyy HH:MM";
export const DATE_TIME_FORMAT_2 = "dd/mm/yyyy HH:MM";
export const DATE_TIME_FORMAT_3 = "DD/MM/YYYY";
export const MomentDateTimeFormatForApproval = "DD/MM/YYYY HH:mm";
export const DATE_TIME_WITH_TIME = "DD/MM/YYYY HH:mm:ss";
export const DATE_FORMAT = "dd/mm/yyyy";
export const DATE_FORMAT_INPUT = "YYYY-MM-DD";
export const DATE_FORMAT_WITH_TIME = "YYYY-MM-DD, h:mm:ss a";
