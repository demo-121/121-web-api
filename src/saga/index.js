import { takeLatest } from "redux-saga/effects";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import * as REDUCER_TYPES from "../constants/REDUCER_TYPES";
import * as clientForm from "./clientForm";
import * as preApplication from "./preApplication";
import * as products from "./products";
import * as quotation from "./quotation";
import * as recommendation from "./recommendation";
import * as client from "./client";
import * as fna from "./fna";
import * as fnaCKA from "./fnaCKA";
import * as fnaRA from "./fnaRA";
import * as application from "./application";
import * as supportingDocument from "./supportingDocument";
import * as paymentAndSubmission from "./paymentAndSubmission";
import * as signature from "./signature";
import * as agent from "./agent";

export default function* saga() {
  /**
   * @reducer agent
   * */
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.AGENT].UPDATE_AGENT_PROFILE_AVATAR,
    agent.updateAgentProfileAvatar
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.AGENT].ASSIGN_ELIG_PRODUCTS_TO_AGENT,
    agent.assignEligProductsToAgent
  );
  /**
   * @reducer clientForm
   * */
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].GET_PROFILE_DATA,
    clientForm.getProfileData
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].DELETE_CLIENT,
    clientForm.deleteClient
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].UNLINK_CLIENT,
    clientForm.unlinkRelationship
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].SAVE_CLIENT,
    clientForm.saveClient
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].SAVE_TRUSTED_INDIVIDUAL,
    clientForm.saveTrustedIndividual
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].INITIAL_CLIENT_FORM,
    clientForm.initialValidateSaga
  );
  yield takeLatest(
    [
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].RELATIONSHIP_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].RELATIONSHIP_OTHER_ON_CHANGE
    ],
    clientForm.relationShipOtherSaga
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].GIVEN_NAME_ON_CHANGE,
    clientForm.validateGivenName
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].SMOKING_STATUS_ON_CHANGE,
    clientForm.validateSmoking
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].MARITAL_STATUS_ON_CHANGE,
    clientForm.validateMaritalStatus
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].GENDER_ON_CHANGE,
    clientForm.validateGender
  );
  yield takeLatest(
    [
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].GENDER_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].MARITAL_STATUS_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].RELATIONSHIP_ON_CHANGE
    ],
    clientForm.validateRelationship
  );
  yield takeLatest(
    [
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].GENDER_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].EMPLOY_STATUS_ON_CHANGE
    ],
    clientForm.validateEmployStatus
  );
  yield takeLatest(
    [
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].EMPLOY_STATUS_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].EMPLOY_STATUS_OTHER_ON_CHANGE
    ],
    clientForm.validateEmployStatusOther
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].SINGAPORE_PR_STATUS_ON_CHANGE,
    clientForm.passSaga
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].RELATIONSHIP_ON_CHANGE,
    clientForm.genderSaga
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].NATIONALITY_ON_CHANGE,
    clientForm.validateNationality
  );
  yield takeLatest(
    [
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].NATIONALITY_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].SINGAPORE_PR_STATUS_ON_CHANGE
    ],
    clientForm.singaporePRStatusValidation
  );
  yield takeLatest(
    [
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].ID_DOCUMENT_TYPE_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].ID_DOCUMENT_TYPE_OTHER_ON_CHANGE
    ],
    clientForm.validateIDDocumentTypeOther
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].RELATIONSHIP_ON_CHANGE,
    clientForm.maritalStatusSaga
  );
  yield takeLatest(
    [
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].GIVEN_NAME_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].SURNAME_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].NAME_ORDER_ON_CHANGE
    ],
    clientForm.autoFillName
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].NAME_ON_CHANGE,
    clientForm.nameSaga
  );
  yield takeLatest(
    [
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].GENDER_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].TITLE_ON_CHANGE
    ],
    clientForm.validateTitle
  );
  yield takeLatest(
    [
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].OCCUPATION_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].BIRTHDAY_ON_CHANGE
    ],
    clientForm.employStatusSaga
  );
  yield takeLatest(
    [
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].EMPLOY_STATUS_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].EMPLOY_STATUS_OTHER_ON_CHANGE
    ],
    clientForm.employStatusOtherSaga
  );
  yield takeLatest(
    [
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].BIRTHDAY_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].EMPLOY_STATUS_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].INDUSTRY_ON_CHANGE
    ],
    clientForm.occupationSaga
  );
  yield takeLatest(
    [
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].BIRTHDAY_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].EMPLOY_STATUS_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].OCCUPATION_ON_CHANGE
    ],
    clientForm.industrySaga
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].LANGUAGE_ON_CHANGE,
    clientForm.validateLanguage
  );
  yield takeLatest(
    [
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].LANGUAGE_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].LANGUAGE_OTHER_ON_CHANGE
    ],
    clientForm.validateLanguageOther
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].OCCUPATION_ON_CHANGE,
    clientForm.validateOccupation
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].INDUSTRY_ON_CHANGE,
    clientForm.validateIndustry
  );
  yield takeLatest(
    [
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].OCCUPATION_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].BIRTHDAY_ON_CHANGE
    ],
    clientForm.validateOccupation
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].OCCUPATION_ON_CHANGE,
    clientForm.occupationOtherSaga
  );
  yield takeLatest(
    [
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].OCCUPATION_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].BIRTHDAY_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].EMPLOY_STATUS_ON_CHANGE
    ],
    clientForm.nameOfEmployBusinessSchoolSaga
  );
  yield takeLatest(
    [
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].OCCUPATION_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].BIRTHDAY_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].EMPLOY_STATUS_ON_CHANGE
    ],
    clientForm.countryOfEmployBusinessSchoolSaga
  );
  yield takeLatest(
    [
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].TYPE_OF_PASS_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].TYPE_OF_PASS_OTHER_ON_CHANGE
    ],
    clientForm.validateTypeOfPass
  );
  yield takeLatest(
    [
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].TYPE_OF_PASS_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].TYPE_OF_PASS_OTHER_ON_CHANGE
    ],
    clientForm.TypeOfPassOtherSaga
  );
  yield takeLatest(
    [
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].TYPE_OF_PASS_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].TYPE_OF_PASS_OTHER_ON_CHANGE
    ],
    clientForm.validateTypeOfPassOther
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].NATIONALITY_ON_CHANGE,
    clientForm.singaporePRStatusSaga
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].COUNTRY_OF_RESIDENCE_ON_CHANGE,
    clientForm.countrySaga
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].POSTALCODE_ON_BLUR,
    clientForm.postalCodeSaga
  );
  yield takeLatest(
    [
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].SINGAPORE_PR_STATUS_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].NATIONALITY_ON_CHANGE
    ],
    clientForm.documentTypeSaga
  );
  yield takeLatest(
    [
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].COUNTRY_OF_RESIDENCE_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].CITY_OF_RESIDENCE_ON_CHANGE
    ],
    clientForm.isCityOfResidenceErrorSaga
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].COUNTRY_OF_RESIDENCE_ON_CHANGE,
    clientForm.cityOfResidenceSaga
  );
  yield takeLatest(
    [
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].CITY_OF_RESIDENCE_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].OTHER_CITY_OF_RESIDENCE_ON_CHANGE
    ],
    clientForm.validateOtherCityOfResidence
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].BIRTHDAY_ON_CHANGE,
    clientForm.validateBirthday
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].ID_DOCUMENT_TYPE_ON_CHANGE,
    clientForm.validateID
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].CITY_OF_RESIDENCE_ON_CHANGE,
    clientForm.cityStateSaga
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].PREFIX_A_ON_CHANGE,
    clientForm.mobileNoASaga
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].PREFIX_B_ON_CHANGE,
    clientForm.mobileNoBSaga
  );
  yield takeLatest(
    [
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].LANGUAGE_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].LANGUAGE_OTHER_ON_CHANGE
    ],
    clientForm.languageOtherSaga
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].INCOME_ON_CHANGE,
    clientForm.validateIncome
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].EDUCATION_ON_CHANGE,
    clientForm.validateEducation
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].ID_DOCUMENT_TYPE_ON_CHANGE,
    clientForm.validateIDDocumentType
  );

  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].PREFIX_A_ON_CHANGE,
    clientForm.validatePrefixA
  );

  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].MOBILE_NO_A_ON_CHANGE,
    clientForm.validateMobileNoA
  );

  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].ID_ON_CHANGE,
    clientForm.validateID
  );

  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].EMAIL_ON_CHANGE,
    clientForm.validateEmail
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].SAVE_CLIENT_EMAIL,
    clientForm.saveClientEmail
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM]
      .LOAD_CLIENT_PROFILE_AND_INIT_VALIDATE,
    clientForm.loadClientProfileAndInitValidate
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].UPDATE_CLIENT_PROFILE_AND_VALIDATE,
    clientForm.updateClientProfileAndValidate
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].CHECK_INSURED_LIST,
    clientForm.checkInsuredList
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT_FORM].REMOVE_COMPLETE_PROFILE_FROM_LIST,
    clientForm.removeCompleteProfileFromList
  );
  /**
   * @reducer quotation
   * */
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.QUOTATION].GET_QUOTATION,
    quotation.getQuotation
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.QUOTATION].GET_FUNDS,
    quotation.getFunds
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.QUOTATION].ALLOC_FUNDS,
    quotation.allocFunds
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.QUOTATION].QUOTE,
    quotation.quote
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.QUOTATION].RESET_QUOTE,
    quotation.resetQuot
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.QUOTATION].UPDATE_IS_QUICK_QUOTE,
    quotation.updateIsQuickQuote
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.QUOTATION].GET_QUICK_QUOTES,
    quotation.queryQuickQuotes
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.QUOTATION].DELETE_QUICK_QUOTES,
    quotation.deleteQuickQuotes
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.QUOTATION].CREATE_PROPOSAL,
    quotation.createProposal
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.QUOTATION].UPDATE_SHIELD_RIDERS,
    quotation.updateShieldRiders
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.QUOTATION].UPDATE_CLIENTS,
    quotation.updateClient
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.QUOTATION].UPDATE_RIDERS,
    quotation.updateRiders
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.QUOTATION].SAGA_VIEW_PROPOSAL,
    quotation.viewProposal
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.QUOTATION].SELECT_INSURED_QUOTATION,
    quotation.selectInsuredQuotation
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.QUOTATION].OPEN_EMAIL_DIALOG,
    quotation.openEmailDialog
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.QUOTATION].SEND_EMAIL,
    quotation.sendEmail
  );
  /**
   * @reducer proposal
   * */
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.PROPOSAL].SAGA_REQUOTE,
    quotation.requote
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.PROPOSAL].SAGA_REQUOTE_INVALID,
    quotation.requoteInvalid
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.PROPOSAL].SAGA_CLONE,
    quotation.clone
  );
  /**
   * @reducer preApplication
   * */

  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.PRE_APPLICATION].SAGA_GET_APPLICATIONS,
    preApplication.getApplications
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.PRE_APPLICATION].SAGA_DELETE_APPLICATIONS,
    preApplication.deleteApplications
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.PRE_APPLICATION].SAGA_UPDATE_UI_SELECTED_LIST,
    preApplication.updateUiSelectedList
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.PRE_APPLICATION]
      .SAGA_UPDATE_RECOMMENDATION_CHOICE_LIST,
    preApplication.updateRecommendChoiceList
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.PRE_APPLICATION].OPEN_EMAIL_DIALOG,
    preApplication.openEmailDialog
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.PRE_APPLICATION].SEND_EMAIL,
    preApplication.sendEmail
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.PRE_APPLICATION].CHECK_BEFORE_APPLY,
    preApplication.checkBeforeApply
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.PRE_APPLICATION].LOAD_CLIENT_AND_INIT_VALIDATE,
    preApplication.loadClientProfileAndInitValidate
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.PRE_APPLICATION].CONFIRM_APPLICATION_PAID,
    preApplication.confirmApplicationPaid
  );
  /**
   * @reducer recommendation
   * */
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.RECOMMENDATION].SAGA_GET_RECOMMENDATION,
    recommendation.getRecommendation
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.RECOMMENDATION].SAGA_CLOSE_RECOMMENDATION,
    recommendation.closeRecommendation
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.RECOMMENDATION].SAGA_VALIDATE_RECOMMENDATION,
    recommendation.validateRecommendation
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.RECOMMENDATION].SAGA_UPDATE_RECOMMENDATION_DATA,
    recommendation.updateRecommendationData
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.RECOMMENDATION].SAGA_UPDATE_RECOMMENDATION_STEP,
    recommendation.updateRecommendationStep
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.RECOMMENDATION].SAGA_VALIDATE_BUDGET,
    recommendation.validateBudget
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.RECOMMENDATION].SAGA_UPDATE_BUDGET_DATA,
    recommendation.updateBudgetData
  );
  /**
   * @reducer products
   * */
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.PRODUCTS].PRODUCT_TAB_BAR_SAGA,
    products.productTabBarSaga
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.PRODUCTS].GET_DEPENDANTS,
    products.getDependants
  );
  yield takeLatest(
    [
      ACTION_TYPES[REDUCER_TYPES.PRODUCTS].GET_PRODUCT_LIST,
      ACTION_TYPES[REDUCER_TYPES.PRODUCTS].UPDATE_CURRENCY,
      ACTION_TYPES[REDUCER_TYPES.PRODUCTS].UPDATE_INSURED_CID
    ],
    products.getProductList
  );
  /**
   * @reducer client
   */
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT].GET_PROFILE,
    client.getProfile
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.CLIENT].GET_CONTACT_LIST,
    client.getContactList
  );
  /**
   * @reducer fna - financial needs analysis
   */
  yield takeLatest(ACTION_TYPES[REDUCER_TYPES.FNA].INIT_PDA, fna.initPDA);
  yield takeLatest(ACTION_TYPES[REDUCER_TYPES.FNA].SAVE_PDA, fna.savePDA);

  yield takeLatest(ACTION_TYPES[REDUCER_TYPES.FNA].UPDATE_NA, fna.validateNA);
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA].VALIDATE_NA,
    fna.updateNAValidStatus
  );

  yield takeLatest(ACTION_TYPES[REDUCER_TYPES.FNA].UPDATE_FE, fna.validateFE);
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA].INIT_NA_ANALYSIS,
    fna.initNaAnalysis
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA].INIT_COMPLETED_STEP_SAGA,
    fna.initCompletedStepSaga
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA].IAR_RATE_ON_CHANGE,
    fna.roiValidation
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA].IAR_RATE_NOT_MATCH_REASON_ON_CHANGE,
    fna.iarRateNotMatchReasonSaga
  );
  // TODO: use UPDATE_NA_ANALYSIS_DATASET to replace this function
  // UPDATE_NA_ANALYSIS_DATA: Container sends updated value to saga,
  // saga just need to update the store with the updated value
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA].UPDATE_NA_ANALYSIS_DATA,
    fna.updateNaAnalysisDataAndValidate
  );
  // UPDATE_NA_ANALYSIS_DATASET: Container sends raw value to saga,
  // saga does some logical calculation to get the updated value, then update
  // the store with updated value
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA].UPDATE_NA_ANALYSIS_DATASET,
    fna.updateNaAnalysis
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA].UPDATE_NA_ASPECTS,
    fna.updateNaAspects
  );
  yield takeLatest(ACTION_TYPES[REDUCER_TYPES.FNA].SAVE_FE, fna.saveFE);
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA].SAVE_LAST_SELECTED_PRODUCT,
    fna.updateLastSelectedProduct
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA].UPDATE_NA_PRIORITY,
    fna.resetProductTypeCkaRaIsValid
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA].INIT_PRODUCT_TYPES,
    fna.initProductTypesSaga
  );
  yield takeLatest(ACTION_TYPES[REDUCER_TYPES.FNA].GET_FNA, fna.getFNA);
  yield takeLatest(ACTION_TYPES[REDUCER_TYPES.FNA].SAVE_NA, fna.saveNA);
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA].UPDATE_NA_PRODUCT_TYPES_COMPLETED_STEP,
    fna.updateProductTypesCompletedStep
  );

  yield takeLatest(ACTION_TYPES[REDUCER_TYPES.FNA].SEND_EMAIL, fna.sendEmail);

  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA_CKA].INIT_CKA,
    fnaCKA.initCKA
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA_CKA].UPDATE_CKA_INIT,
    fnaCKA.updateCKAinit
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA_CKA].COURSE_ON_CHANGE,
    fnaCKA.courseSaga
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA_CKA].INSTITUTION_ON_CHANGE,
    fnaCKA.institutionSaga
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA_CKA].STUDYPERIODENDYEAR_ON_CHANGE,
    fnaCKA.studyPeriodEndYearSaga
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA_CKA].COLLECTIVEINVESTMENT_ON_CHANGE,
    fnaCKA.collectiveInvestmentSaga
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA_CKA].TRANSACTIONTYPE_ON_CHANGE,
    fnaCKA.transactionTypeSaga
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA_CKA].INSURANCEINVESTMENT_ON_CHANGE,
    fnaCKA.insuranceInvestmentSaga
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA_CKA].INSURANCETYPE_ON_CHANGE,
    fnaCKA.insuranceTypeSaga
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA_CKA].PROFESSION_ON_CHANGE,
    fnaCKA.professionSaga
  );
  yield takeLatest(
    [
      ACTION_TYPES[REDUCER_TYPES.FNA_CKA].YEARS_ON_CHANGE,
      ACTION_TYPES[REDUCER_TYPES.FNA_CKA].YEARS_ON_BLUR
    ],
    fnaCKA.yearsSaga
  );

  yield takeLatest(ACTION_TYPES[REDUCER_TYPES.FNA_RA].INIT_RA, fnaRA.initRA);
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA_RA].UPDATE_RA_INIT,
    fnaRA.updateRAinit
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA_RA].RISKPOTENTIALRETURN_ON_CHANGE,
    fnaRA.riskPotentialReturnSaga
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA_RA].AVGAGRETURN_ON_CHANGE,
    fnaRA.avgAGReturnSaga
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA_RA].SMDROPED_ON_CHANGE,
    fnaRA.smDropedSaga
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA_RA].ALOFLOSSES_ON_CHANGE,
    fnaRA.alofLossesSaga
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA_RA].EXPINVTIME_ON_CHANGE,
    fnaRA.expInvTimeSaga
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA_RA].INVPREF_ON_CHANGE,
    fnaRA.invPrefSaga
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA_RA].SELF_SELECTED_RISK_LEVEL_ON_CHANGE,
    fnaRA.selfSelectedRiskLevelSaga
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA_RA].SELF_RL_REASON_RP_ON_CHANGE,
    fnaRA.selfRLReasonRpSaga
  );

  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA].GET_FNA_REPORT,
    fna.loadFNAReport
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA].OPEN_EMAIL_DIALOG,
    fna.openEmailDialog
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA].INITIAL_TRUSTED_INDIVIDUAL_FORM,
    fna.initialValidateTrustedIndividualSaga
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.FNA].TRUSTED_INDIVIDUAL_GIVEN_NAME_ON_CHANGE,
    fna.validateGivenName
  );

  /**
   * @reducer Application
   * */
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION].SAGA_GEN_FNA,
    application.genFNA
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION]
      .SAGA_UPDATE_PERSONAL_DETAILS_ADDRESS,
    application.updatePersonalDetailsAddress
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION].SAGA_UPDATE_POLICY_NUMBER,
    application.updatePolicyNumber
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION].SAGA_APPLY_APPLICATION_FORM,
    application.applyApplicationForm
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION].SAGA_CONTINUE_APPLICATION_FORM,
    application.continueApplicationForm
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION]
      .SAGA_CONTINUE_APPLICATION_FORM_SHIELD,
    application.continueApplicationFormShield
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION].SAGA_CHANGE_TAB_SHIELD,
    application.changeTabShield
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION]
      .SAGA_UPDATE_APPLICATION_FORM_PERSONAL_DETAILS,
    application.updateApplicationFormPersonalDetails
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION].SAVE_APPLICATION_FORM_BEFORE,
    application.saveApplicationFormBefore
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION].SAVE_APPLICATION_FORM_SHIELD_BEFORE,
    application.saveApplicationFormShieldBefore
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION].SAGA_UPDATE_APPLICATION_FORM_VALUES,
    application.updateApplicationFormDynamicQuestions
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION]
      .SAGA_UPDATE_DYNAMIC_APPLICATION_FORM_ADDRESS,
    application.updateDynamicApplicationFormAddress
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION].SAGA_UPDATE_ROP_TABLE,
    application.updateROPTable
  );
  // save table record
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION]
      .SAGA_SAVE_APPLICATION_FORM_TABLE_RECORD,
    application.saveApplicationFormTableRecord
  );
  // delete table record
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION]
      .SAGA_DELETE_APPLICATION_FORM_TABLE_RECORD,
    application.deleteApplicationFormTableRecord
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION].SAGA_APPLICATION_RERENDER_PAGE,
    application.reRenderPage
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION].SAGA_UPDATE_EAPP_STEP,
    application.updateEappStep
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION].SAGA_UPDATE_APPLICATION_BACKDATE,
    application.updateApplicationBackDate
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION].SAGA_INVALIDATE_APPLICATION,
    application.invalidateApplication
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION]
      .SAGA_UPDATE_SHOWN_SIGNATURE_EXPIRY_ALERT,
    application.updateShownSignatureExpiryAlert
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION]
      .SAGA_CLOSE_DIALOG_AND_GET_APPLICATION_LIST,
    application.closeDialogAndGetApplicationList
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION].SAGA_REDIRECT_TO_FNA,
    application.redirectToFNA
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION].SAGA_UPDATE_SELECTED_SECTION_KEY,
    application.updateSelectedSectionKey
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION]
      .SAGA_GET_APPLICATIONS_AFTER_SUBMISSION,
    application.getApplicationsAfterSubmission
  );
  /**
   * @reducer Supporting Document
   * */
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.SUPPORTING_DOCUMENT]
      .SAGA_SAVE_SUBMITTED_DOCUMENT,
    supportingDocument.saveSubmittedDocument
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.SUPPORTING_DOCUMENT]
      .SAGA_GET_SUPPORTING_DOCUMENT_DATA,
    supportingDocument.getSupportingDocument
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.SUPPORTING_DOCUMENT]
      .SAGA_VALIDATE_SUPPORTING_DOCUMENT,
    supportingDocument.validateSupportingDocument
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.SUPPORTING_DOCUMENT]
      .SAGA_ADD_OTHER_SUPPORTING_DOCUMENT,
    supportingDocument.addOtherSupportingDocument
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.SUPPORTING_DOCUMENT]
      .SAGA_EDIT_OTHER_SUPPORTING_DOCUMENT,
    supportingDocument.editOtherDocumentSection
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.SUPPORTING_DOCUMENT]
      .SAGA_DELETE_OTHER_DOCUMENT_SECTION,
    supportingDocument.deleteOtherDocumentSection
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.SUPPORTING_DOCUMENT]
      .SAGA_SAVE_SUPPORTING_DOCUMENT_DATA,
    supportingDocument.saveSupportingDocument
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.SUPPORTING_DOCUMENT]
      .SAGA_DELETE_FILE_SUPPORTING_DOCUMENT,
    supportingDocument.deleteFile
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.SUPPORTING_DOCUMENT]
      .SAGA_ON_FILE_UPLOAD_SUPPORTING_DOCUMENT,
    supportingDocument.fileUpload
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.SUPPORTING_DOCUMENT]
      .SAGA_GET_SUPPORTING_DOCUMENT_DATA_SHIELD,
    supportingDocument.getSupportingDocumentShield
  );
  /**
   * @reducer payment
   //  */
  // yield takeLatest(
  //   ACTION_TYPES[REDUCER_TYPES.APPLICATION].INIT_PAYMENT_STORE,
  //   payment.initPayStore
  // );
  // yield takeLatest(
  //   ACTION_TYPES[REDUCER_TYPES.APPLICATION].START_PAYMENT,
  //   payment.startPayment
  // );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION].GET_APPLICATION_FOR_PAYMENT,
    application.getApplication
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION].GET_APPLICATION_FOR_SUBMISSION,
    application.getApplication
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION].VALIDATE_PAYMENT,
    application.validatePayment
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.APPLICATION].VALIDATE_SUBMISSION,
    application.validateSubmission
  );
  /*
   * @reducer payment and submission
   * */
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].SAGA_GET_PAYMENT,
    paymentAndSubmission.getPayment
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].SAGA_GET_SUBMISSION,
    paymentAndSubmission.getSubmission
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION]
      .SAGA_UPDATE_PAYMENT_AND_SUBMISSION,
    paymentAndSubmission.updatePayment
  );
  yield takeLatest(
    [
      ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION]
        .BEFORE_UPDATE_PAYMENT_STATUS,
      ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION]
        .UPDATE_PAYMENT_AND_SUBMISSION
    ],
    paymentAndSubmission.beforeUpdatePaymentStatus
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION]
      .SAGA_UPDATE_PAYMENT_METHODS,
    paymentAndSubmission.updatePaymentMethods
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].SAGA_SUBMIT_APPLICATION,
    paymentAndSubmission.callApiSubmitApplication
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].SAGA_EAPP_DATA_SYNC,
    paymentAndSubmission.eappDataSync
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].REFRESH_PAYMENT_STATUS,
    paymentAndSubmission.refreshPaymentStatus
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.PAYMENT_AND_SUBMISSION].GET_PAYMENT_URL,
    paymentAndSubmission.getPaymentURL
  );
  /**
   * @reducer signature
   * */
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.SIGNATURE].SAGA_GET_SIGNATURE,
    signature.getSignature
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.SIGNATURE].SAGA_GET_SIGNATURE_SIGNATURE,
    signature.getSignatureShield
  );
  yield takeLatest(
    ACTION_TYPES[REDUCER_TYPES.SIGNATURE].SAGA_SAVE_SIGNED_PDF,
    signature.saveSignedPdf
  );
}
