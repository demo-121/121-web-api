import { call, put, select } from "redux-saga/effects";
import * as _ from "lodash";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import EAPP from "../constants/EAPP";
import * as validation from "../utilities/validation";
import { TEXT_FIELD } from "../constants/FIELD_TYPES";
import {
  CONFIG,
  SUPPORTING_DOCUMENT,
  PAYMENT_AND_SUBMISSION,
  APPLICATION,
  AGENT,
  LOGIN
} from "../constants/REDUCER_TYPES";

export function* validateSupportingDocument() {
  try {
    const supportingDocument = yield select(
      state => state[SUPPORTING_DOCUMENT].supportingDocument
    );
    const newError = {};
    Object.entries(supportingDocument.template).forEach(
      ([profileName, template]) => {
        let haveMandDocs = false;
        if (template.tabContent) {
          if (template.tabContent.mandDocs) {
            template.tabContent.mandDocs.items.forEach(item => {
              _.set(newError, `${profileName}.${item.id}`, {});
            });
          }
        } else {
          template.some(value => {
            if (value.id === "mandDocs") {
              haveMandDocs = true;
              value.items.forEach(item => {
                _.set(newError, `${profileName}.${item.id}`, {});
              });
            }
            return haveMandDocs;
          });
        }
      }
    );
    let isMandDocsAllUploaded = true;
    Object.entries(newError).forEach(([profileName, content]) => {
      Object.keys(content).forEach(fieldName => {
        const result = validation.validateMandatory({
          field: {
            type: TEXT_FIELD,
            mandatory: true,
            disabled: false
          },
          value: supportingDocument.values[profileName].mandDocs[fieldName]
            .length
            ? 1
            : null
        });
        if (result.hasError) {
          isMandDocsAllUploaded = false;
        }
        _.set(newError, `${profileName}.${fieldName}`, result);
      });
    });

    yield put({
      type: ACTION_TYPES[SUPPORTING_DOCUMENT].VALIDATE_SUPPORTING_DOCUMENT,
      newError
    });
    if (!isMandDocsAllUploaded) {
      yield put({
        type: ACTION_TYPES[APPLICATION].UPDATE_SUPPORTING_DOCUMENT_ERROR_ICON,
        newIsMandDocsAllUploaded: false
      });
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* getSupportingDocument(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const { appId, appStatus, callback } = action;

    const res = yield call(callServer, {
      url: `application`,
      data: {
        action: "getSupportDocuments",
        applicationId: appId,
        appStatus
      }
    });
    if (res.success) {
      yield put({
        type: ACTION_TYPES[SUPPORTING_DOCUMENT].UPDATE_SUPPORTING_DOCUMENT_DATA,
        newSupportingDocument: res
      });

      if (appStatus === EAPP.appStatus.SUBMITTED) {
        yield put({
          type:
            ACTION_TYPES[APPLICATION].GET_APPLICATION_FOR_SUPPORTING_DOCUMENT,
          newApplication: res.application
        });
      }
      yield call(validateSupportingDocument);
    }
    if (_.isFunction(callback)) {
      callback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* saveSupportingDocument(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const supportingDocument = yield select(
      state => state[SUPPORTING_DOCUMENT].supportingDocument
    );
    const { appId, callback } = action;
    const response = yield call(callServer, {
      url: `application`,
      data: {
        action: "closeSuppDocs",
        template: supportingDocument.template,
        values: supportingDocument.values,
        appId
      }
    });
    if (response) {
      yield put({
        type:
          ACTION_TYPES[PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_AND_SUBMISSION,
        newSubmission: response
      });
      yield put({
        type: ACTION_TYPES[APPLICATION].UPDATE_SUPPORTING_DOCUMENT_ERROR_ICON,
        newIsMandDocsAllUploaded: response.isMandDocsAllUploaded
      });
    }
    if (_.isFunction(callback)) {
      callback();
    }
    // TODO the response stored some ui template that maybe need to handle
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* updateSupportingDocumentData(action) {
  try {
    const supportingDocument = yield select(
      state => state[SUPPORTING_DOCUMENT].supportingDocument
    );
    const { path, newValue } = action;
    _.set(supportingDocument, path, newValue);
    yield put({
      type: ACTION_TYPES[SUPPORTING_DOCUMENT].UPDATE_SUPPORTING_DOCUMENT_DATA,
      newSupportingDocument: supportingDocument
    });
    if (_.isFunction(action.callback)) {
      action.callback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* addOtherSupportingDocument(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const supportingDocument = yield select(
      state => state[SUPPORTING_DOCUMENT].supportingDocument
    );
    const { appId, docName, docNameOption, tabId } = action;
    const res = yield call(callServer, {
      url: `application`,
      data: {
        action: "addOtherDocument",
        values: supportingDocument.values[tabId],
        docName,
        docNameOption,
        appId,
        tabId,
        rootValues: supportingDocument.values
      }
    });
    if (res.success) {
      supportingDocument.values = res.values;
      yield put({
        type: ACTION_TYPES[SUPPORTING_DOCUMENT].UPDATE_SUPPORTING_DOCUMENT_DATA,
        newSupportingDocument: Object.assign({}, supportingDocument)
      });
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* fileUpload(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const supportingDocument = yield select(
      state => state[SUPPORTING_DOCUMENT].supportingDocument
    );
    const viewedList = yield select(
      state => state[SUPPORTING_DOCUMENT].viewedList
    );
    const {
      attachment,
      applicationId,
      attachmentValues,
      valueLocation,
      tabId,
      isSupervisorChannel,
      callback
    } = action;
    const res = yield call(callServer, {
      url: `application`,
      data: {
        action: "upsertSuppDocsFile",
        attachment,
        applicationId,
        attachmentValues,
        valueLocation,
        tabId,
        suppDocsValues: supportingDocument.values[tabId],
        viewedList,
        isSupervisorChannel,
        rootValues: supportingDocument.values
      }
    });
    if (res.success) {
      if (res.showTotalFilesSizeLargeDialog) {
        supportingDocument.exceededMaximumFileSize = true;
      } else {
        supportingDocument.values = res.values;
        supportingDocument.exceededMaximumFileSize = false;
      }
      yield put({
        type:
          ACTION_TYPES[SUPPORTING_DOCUMENT]
            .UPDATE_SUPPORTING_DOCUMENT_PENDING_LIST,
        newPendingSubmitList: res.pendingSubmitList || []
      });
      yield put({
        type: ACTION_TYPES[SUPPORTING_DOCUMENT].UPDATE_SUPPORTING_DOCUMENT_DATA,
        newSupportingDocument: Object.assign({}, supportingDocument)
      });
      yield put({
        type:
          ACTION_TYPES[SUPPORTING_DOCUMENT]
            .UPDATE_SUPPORTING_DOCUMENT_VIEWED_LIST_DATA,
        newViewedList: res.viewedList
      });
      yield call(validateSupportingDocument);
      if (_.isFunction(callback)) {
        callback();
      }
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* deleteFile(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const supportingDocument = yield select(
      state => state[SUPPORTING_DOCUMENT].supportingDocument
    );
    const { applicationId, attachmentId, valueLocation, tabId } = action;
    const res = yield call(callServer, {
      url: `application`,
      data: {
        action: "deleteSupportDocumentFile",
        applicationId,
        attachmentId,
        valueLocation,
        tabId
      }
    });
    if (res.success) {
      supportingDocument.values = res.supportDocuments.values;
      yield put({
        type: ACTION_TYPES[SUPPORTING_DOCUMENT].UPDATE_SUPPORTING_DOCUMENT_DATA,
        newSupportingDocument: Object.assign({}, supportingDocument)
      });
      yield call(validateSupportingDocument);
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* deleteOtherDocumentSection(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const supportingDocument = yield select(
      state => state[SUPPORTING_DOCUMENT].supportingDocument
    );
    const { applicationId, documentId, tabId } = action;
    const res = yield call(callServer, {
      url: `application`,
      data: {
        action: "deleteSupportDocument",
        applicationId,
        documentId,
        tabId
      }
    });
    supportingDocument.values = res.values;
    yield put({
      type: ACTION_TYPES[SUPPORTING_DOCUMENT].UPDATE_SUPPORTING_DOCUMENT_DATA,
      newSupportingDocument: Object.assign({}, supportingDocument)
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* editOtherDocumentSection(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const supportingDocument = yield select(
      state => state[SUPPORTING_DOCUMENT].supportingDocument
    );
    const { applicationId, documentId, tabId, docName } = action;
    const res = yield call(callServer, {
      url: `application`,
      data: {
        action: "editSupportDocument",
        applicationId,
        docName,
        documentId,
        tabId
      }
    });
    supportingDocument.values = res.values;
    yield put({
      type: ACTION_TYPES[SUPPORTING_DOCUMENT].UPDATE_SUPPORTING_DOCUMENT_DATA,
      newSupportingDocument: Object.assign({}, supportingDocument)
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* getSupportingDocumentShield(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const { appId, appStatus, callback } = action;

    const res = yield call(callServer, {
      url: `shieldApplication`,
      data: {
        action: "showSupportDocments",
        applicationId: appId,
        appStatus
      }
    });
    if (res.success) {
      yield put({
        type: ACTION_TYPES[SUPPORTING_DOCUMENT].UPDATE_SUPPORTING_DOCUMENT_DATA,
        newSupportingDocument: res
      });

      if (appStatus === EAPP.appStatus.SUBMITTED) {
        yield put({
          type:
            ACTION_TYPES[APPLICATION].GET_APPLICATION_FOR_SUPPORTING_DOCUMENT,
          newApplication: res.application
        });
      }
      yield call(validateSupportingDocument);
    }
    if (_.isFunction(callback)) {
      callback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* AdditionalDocumentNotification(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const agentId = yield select(state => state[AGENT].agentCode);
    const authToken = yield select(state => state[LOGIN].authToken);
    const webServiceUrl = yield select(state => state[LOGIN].webServiceUrl);
    const { isHealthDeclaration } = action;
    const appId = yield select(
      state => state[APPLICATION].application.policyNumber
    );
    yield call(callServer, {
      url: `approvalNotification`,
      data: {
        action: "AdditionalDocumentNotification",
        id: appId,
        agentId,
        isHealthDeclaration,
        authToken,
        webServiceUrl
      }
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* saveSubmittedDocument(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const supportingDocument = yield select(
      state => state[SUPPORTING_DOCUMENT].supportingDocument
    );
    const viewedList = yield select(
      state => state[SUPPORTING_DOCUMENT].viewedList
    );
    const { appId, isSupervisorChannel, callback } = action;

    const res = yield call(callServer, {
      url: `application`,
      data: {
        action: "submitSuppDocsFiles",
        appId,
        values: supportingDocument.values,
        viewedList,
        isSupervisorChannel
      }
    });
    if (res.success) {
      yield put({
        type:
          ACTION_TYPES[SUPPORTING_DOCUMENT]
            .UPDATE_SUPPORTING_DOCUMENT_PENDING_LIST,
        newPendingSubmitList: []
      });
    }
    yield call(AdditionalDocumentNotification, {
      appId,
      isHealthDeclaration: res.sendHealthDeclarationNoti
    });
    if (_.isFunction(callback)) {
      callback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}
