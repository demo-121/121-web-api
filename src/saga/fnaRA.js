import * as _ from "lodash";
import { put, select } from "redux-saga/effects";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import { FNA, FNA_RA } from "../constants/REDUCER_TYPES";
import {
  INVESTLINKEDPLANS,
  PARTICIPATINGPLANS
} from "../constants/PRODUCT_TYPES";

function calAssessedRL(raData) {
  const riskPotentialReturn = Number(raData.riskPotentialReturn) || 0;
  const avgAGReturn = Number(raData.avgAGReturn) || 0;
  const smDroped = Number(raData.smDroped) || 0;
  const alofLosses = Number(raData.alofLosses) || 0;
  const expInvTime = Number(raData.expInvTime) || 0;
  const invPref = Number(raData.invPref) || 0;

  const source =
    riskPotentialReturn +
    avgAGReturn +
    smDroped +
    alofLosses +
    expInvTime +
    invPref;

  if (source > 26) {
    return 5;
  } else if (source > 21) {
    return 4;
  } else if (source > 14) {
    return 3;
  } else if (source > 9) {
    return 2;
  }
  return 1;
}

export function* updateRAisValid() {
  const raData = yield select(state => state[FNA].na.raSection.owner);
  const prodType = yield select(state => state[FNA].na.productType.prodType);
  const ckaSection = yield select(state => state[FNA].na.ckaSection);
  const prodTypeArr = prodType.split(",");
  let isCompleted = true;
  let raIsValid = true;

  if (
    raData.riskPotentialReturn === null ||
    raData.avgAGReturn === null ||
    raData.smDroped === null ||
    raData.alofLosses === null ||
    raData.expInvTime === null ||
    raData.invPref === null
  ) {
    raIsValid = false;
  }

  if (
    raData.selfSelectedriskLevel !== "" &&
    raData.selfSelectedriskLevel !== raData.assessedRL &&
    raData.selfRLReasonRP === ""
  ) {
    raIsValid = false;
  }

  if (
    (prodTypeArr.indexOf(INVESTLINKEDPLANS) > -1 &&
      (!ckaSection.isValid || !raIsValid)) ||
    (prodTypeArr.indexOf(PARTICIPATINGPLANS) > -1 && !raIsValid)
  ) {
    isCompleted = false;
  }

  yield put({
    type: ACTION_TYPES[FNA_RA].UPDATE_RA_IS_VALID,
    isValid: raIsValid
  });

  yield put({
    type: ACTION_TYPES[FNA].UPDATE_IS_COMPLETED,
    isCompleted
  });
}

export function* updateRAinit(action) {
  try {
    const raData = yield select(state => state[FNA].na.raSection.owner);

    if (raData.init) {
      yield put({
        type: ACTION_TYPES[FNA_RA].UPDATE_RA_OWNER_INIT,
        newInit: false
      });

      yield put({
        type: ACTION_TYPES[FNA].UPDATE_NA_IS_CHANGED,
        newNaIsChanged: true
      });
    }
    if (action && _.isFunction(action.callback)) {
      action.callback();
    }
  } catch (e) {
    // TODO:
  }
}

export function* initRA(action) {
  try {
    const raSection = yield select(state => state[FNA].na.raSection);
    const raData = raSection.owner;

    let noError = true;

    if (
      raData.riskPotentialReturn === null ||
      raData.avgAGReturn === null ||
      raData.smDroped === null ||
      raData.alofLosses === null ||
      raData.expInvTime === null ||
      raData.invPref === null
    ) {
      noError = false;
    }

    if (
      raData.selfSelectedriskLevel !== "" &&
      raData.selfSelectedriskLevel !== raData.assessedRL &&
      raData.selfRLReasonRP === ""
    ) {
      noError = false;
    }

    // if it already completed but it shows incomplete,
    // it won't trigger the save action.
    // So, set NA is changed, in order to trigger the save action.
    if (noError && !raSection.isValid) {
      yield put({
        type: ACTION_TYPES[FNA].UPDATE_NA_IS_CHANGED,
        newNaIsChanged: true
      });
    }

    yield updateRAisValid();
    yield updateRAinit();

    if (_.isFunction(action.callback)) {
      action.callback();
    }
  } catch (e) {
    // TODO need error handling
  }
}

export function* riskPotentialReturnSaga() {
  try {
    const raData = yield select(state => state[FNA].na.raSection.owner);

    yield put({
      type: ACTION_TYPES[FNA_RA].UPDATE_ASSESSED_RL,
      newAssessedRL: calAssessedRL(raData)
    });

    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_IS_CHANGED,
      newNaIsChanged: true
    });

    yield updateRAisValid();
  } catch (e) {
    // TODO:
  }
}
export function* avgAGReturnSaga() {
  try {
    const raData = yield select(state => state[FNA].na.raSection.owner);

    yield put({
      type: ACTION_TYPES[FNA_RA].UPDATE_ASSESSED_RL,
      newAssessedRL: calAssessedRL(raData)
    });

    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_IS_CHANGED,
      newNaIsChanged: true
    });

    yield updateRAisValid();
  } catch (e) {
    // TODO:
  }
}
export function* smDropedSaga() {
  try {
    const raData = yield select(state => state[FNA].na.raSection.owner);

    yield put({
      type: ACTION_TYPES[FNA_RA].UPDATE_ASSESSED_RL,
      newAssessedRL: calAssessedRL(raData)
    });

    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_IS_CHANGED,
      newNaIsChanged: true
    });

    yield updateRAisValid();
  } catch (e) {
    // TODO:
  }
}
export function* alofLossesSaga() {
  try {
    const raData = yield select(state => state[FNA].na.raSection.owner);

    yield put({
      type: ACTION_TYPES[FNA_RA].UPDATE_ASSESSED_RL,
      newAssessedRL: calAssessedRL(raData)
    });

    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_IS_CHANGED,
      newNaIsChanged: true
    });

    yield updateRAisValid();
  } catch (e) {
    // TODO:
  }
}
export function* expInvTimeSaga() {
  try {
    const raData = yield select(state => state[FNA].na.raSection.owner);

    yield put({
      type: ACTION_TYPES[FNA_RA].UPDATE_ASSESSED_RL,
      newAssessedRL: calAssessedRL(raData)
    });

    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_IS_CHANGED,
      newNaIsChanged: true
    });

    yield updateRAisValid();
  } catch (e) {
    // TODO:
  }
}
export function* invPrefSaga() {
  try {
    const raData = yield select(state => state[FNA].na.raSection.owner);

    yield put({
      type: ACTION_TYPES[FNA_RA].UPDATE_ASSESSED_RL,
      newAssessedRL: calAssessedRL(raData)
    });

    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_IS_CHANGED,
      newNaIsChanged: true
    });

    yield updateRAisValid();
  } catch (e) {
    // TODO:
  }
}
export function* selfSelectedRiskLevelSaga() {
  try {
    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_IS_CHANGED,
      newNaIsChanged: true
    });

    yield updateRAisValid();
  } catch (e) {
    // TODO
  }
}
export function* selfRLReasonRpSaga() {
  try {
    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_IS_CHANGED,
      newNaIsChanged: true
    });

    yield updateRAisValid();
  } catch (e) {
    // TODO
  }
}
