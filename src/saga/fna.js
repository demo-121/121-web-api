import * as _ from "lodash";
import { call, put, select } from "redux-saga/effects";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import { TEXT_FIELD } from "../constants/FIELD_TYPES";
import { FNA, CONFIG, CLIENT, AGENT, LOGIN } from "../constants/REDUCER_TYPES";
import { mapCKA, naMapForm, pdaMapForm } from "../utilities/dataMapping";
import * as naAnalysisUtils from "../utilities/naAnalysis";
import PROGRESS_TABS from "../constants/PROGRESS_TABS";
import {
  CIPROTECTION,
  PAPROTECTION,
  PCHEADSTART,
  HCPROTECTION,
  FIPROTECTION,
  RPLANNING
} from "../constants/NEEDS";
import {
  NONPARTICIPATINGPLAN,
  ACCIDENTHEALTHPLANS
} from "../constants/PRODUCT_TYPES";
import { LANGUAGE_TYPES } from "../locales";
import { replaceAll } from "../utilities/common";
import {
  validateMandatory,
  validateAssets,
  validateNeedsAnalysis
} from "../utilities/validation";

const initNaValueAndError = ({ rule, data, error }) => {
  _.forEach(rule, (r, componentId) => {
    // init value
    if (!_.has(data, componentId)) {
      if (_.toLower(r.type) === "assets") {
        data[componentId] = [];
      } else if (_.toLower(r.type) === "int") {
        data[componentId] = 0;
      } else {
        data[componentId] = "";
      }
    }

    // init error
    const validateResult =
      componentId === "assets"
        ? validateAssets({
            rule: r,
            value: data[componentId]
          })
        : validateNeedsAnalysis({
            rule: r,
            value: data[componentId]
          });
    _.set(error, componentId, validateResult);
    error.isValid = error.isValid && !validateResult.hasError;
  });
};

const addProdType = (product, products) => {
  let productsArr = products.split(",");
  const index = productsArr.indexOf(product);
  if (index === -1) {
    productsArr.push(product);
  }
  productsArr = productsArr.filter(x => x !== "");
  return productsArr.join();
};

export function* resetProductTypeCkaRaIsValid() {
  const na = yield select(state => state[FNA].na);

  const needsAspect = na.aspects.split(",");
  let productType = "";

  if (
    needsAspect.indexOf(CIPROTECTION) > -1 ||
    needsAspect.indexOf(PCHEADSTART) > -1
  ) {
    productType = addProdType(NONPARTICIPATINGPLAN, productType);
  }

  if (
    na.aspects === PAPROTECTION ||
    na.aspects === HCPROTECTION ||
    _.isEqual(needsAspect.sort(), [PAPROTECTION, HCPROTECTION].sort())
  ) {
    productType = addProdType(ACCIDENTHEALTHPLANS, productType);
  } else if (
    needsAspect.indexOf(PAPROTECTION) > -1 ||
    needsAspect.indexOf(HCPROTECTION) > -1
  ) {
    productType = addProdType(ACCIDENTHEALTHPLANS, productType);
  }

  yield put({
    type: ACTION_TYPES[FNA].RESET_PRODUCT_TYPE_CKA_RA_IS_VALID,
    value: productType,
    ckaIsValid: false,
    raIsValid: false
  });
}

export function* initNaAnalysis(action) {
  try {
    const { aspect } = action;

    const na = yield select(state => state[FNA].na);
    const isNaError = yield select(state => state[FNA].isNaError);
    const { analysisAspect } = naAnalysisUtils;
    const newNa = _.cloneDeep(na);
    const newIsNaError = _.cloneDeep(isNaError);
    const needsAspect = _.find(
      analysisAspect.needs,
      needs => needs.value === aspect
    );

    // init aspect level
    if (!_.has(newNa, aspect)) {
      newNa[aspect] = { isValid: true };
    }
    if (!_.has(newIsNaError, aspect)) {
      newIsNaError[aspect] = { isValid: true };
    }

    // init owner / spouse / dependants level
    _.forEach(newNa[aspect], (analysisData, dataProfile) => {
      if (["owner", "spouse", "dependants"].indexOf(dataProfile) >= 0) {
        if (!_.has(newIsNaError[aspect], dataProfile)) {
          newIsNaError[aspect][dataProfile] =
            dataProfile === "dependants" ? [] : {};
        }

        if (dataProfile === "dependants") {
          _.forEach(analysisData, dependant => {
            if (
              _.findIndex(
                newIsNaError[aspect].dependants,
                d => d.cid === dependant.cid
              ) < 0
            ) {
              newIsNaError[aspect].dependants.push({ cid: dependant.cid });
            }
          });
        }

        if (needsAspect) {
          const { rule } = needsAspect;
          if (dataProfile === "dependants") {
            _.forEach(analysisData, (dependant, index) => {
              const profileContent = newNa[aspect][dataProfile][index];
              const profileError = { isValid: true };
              initNaValueAndError({
                rule,
                data: profileContent,
                error: profileError
              });
              newNa[aspect].isValid =
                newNa[aspect].isValid && !profileError.hasError;
              newIsNaError[aspect][dataProfile][index] = profileError;
              newIsNaError[aspect].isValid =
                newIsNaError[aspect].isValid && !profileError.hasError;
            });
          } else {
            const profileContent = newNa[aspect][dataProfile];
            const profileError = { isValid: true };
            initNaValueAndError({
              rule,
              data: profileContent,
              error: profileError
            });
            newNa[aspect].isValid =
              newNa[aspect].isValid && !profileError.hasError;
            newIsNaError[aspect][dataProfile] = profileError;
            newIsNaError[aspect].isValid =
              newIsNaError[aspect].isValid && !profileError.hasError;
          }
        }
      }
    });

    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_ANALYSIS,
      value: newNa,
      newIsNaError
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateNeedsAnalysisData(action) {
  try {
    const { value } = action;
    const na = yield select(state => state[FNA].na);
    const pda = yield select(state => state[FNA].pda);
    const isNaError = yield select(state => state[FNA].isNaError);
    const dependantProfilesData = yield select(
      state => state[CLIENT].dependantProfiles
    );

    const newNa = _.cloneDeep(na);
    const newIsNaError = _.cloneDeep(isNaError);

    const newTargetAspectError = newIsNaError[value.targetProduct];
    const key = Object.keys(value.targetData)[0];

    // validation
    const validateResult =
      key === "assets"
        ? validateAssets({
            rule: value.rule,
            value
          })
        : validateNeedsAnalysis({
            rule: value.rule,
            value: value.targetData[key]
          });

    if (value.targetProfile !== "owner" && value.targetProfile !== "spouse") {
      // dependants
      const targetIndex = _.findIndex(
        na[value.targetProduct].dependants,
        dependant => dependant.cid === value.targetProfile
      );

      if (targetIndex >= 0) {
        _.set(
          newTargetAspectError,
          `dependants[${targetIndex}][${key}]`,
          validateResult
        );

        const profileError = _.get(
          newTargetAspectError,
          `dependants[${targetIndex}]`
        );
        let profileIsValid = true;
        _.forEach(profileError, fieldError => {
          if (_.has(fieldError, "hasError")) {
            profileIsValid = profileIsValid && !fieldError.hasError;
          }
        });
        _.set(
          newTargetAspectError,
          `dependants[${targetIndex}].isValid`,
          profileIsValid
        );
      }
    } else {
      // owner / spouse
      _.set(
        newTargetAspectError,
        `[${value.targetProfile}]${key}`,
        validateResult
      );

      const profileError = _.get(
        newTargetAspectError,
        `[${value.targetProfile}]`
      );
      let profileIsValid = true;
      _.forEach(profileError, fieldError => {
        if (_.has(fieldError, "hasError")) {
          profileIsValid = profileIsValid && !fieldError.hasError;
        }
      });
      _.set(
        newTargetAspectError,
        `[${value.targetProfile}].isValid`,
        profileIsValid
      );
    }

    // Check isValid of targetProduct
    let aspectisValid = true;
    if (
      _.has(newTargetAspectError, "owner.isValid") &&
      _.get(na, `[${value.targetProduct}].owner.isActive`)
    ) {
      aspectisValid = aspectisValid && newTargetAspectError.owner.isValid;
    }
    if (_.has(newTargetAspectError, "spouse")) {
      if (
        pda.applicant === "single" &&
        _.get(na, `[${value.targetProduct}].spouse.isActive`) &&
        _.has(newTargetAspectError, "spouse.isValid")
      ) {
        aspectisValid = aspectisValid && true;
      } else {
        aspectisValid = aspectisValid && newTargetAspectError.spouse.isValid;
      }
    }
    if (_.has(newTargetAspectError, "dependants")) {
      _.forEach(newTargetAspectError.dependants, (dependant, index) => {
        _.forEach(dependantProfilesData, profile => {
          const { cid } = profile;
          if (
            pda.dependants.indexOf(cid) === -1 &&
            _.get(
              na,
              `[${value.targetProduct}].dependants[${index}].isActive`
            ) &&
            _.has(dependant, "isValid")
          ) {
            aspectisValid = aspectisValid && true;
          } else if (pda.dependants.indexOf(cid) > -1) {
            _.forEach(newTargetAspectError.dependants, () => {
              aspectisValid =
                aspectisValid &&
                (dependant.isValid || !_.has(dependant, "isValid"));
            });
          }
        });
      });
    }
    _.set(newNa, `[${value.targetProduct}].isValid`, aspectisValid);

    yield put({
      type: ACTION_TYPES[FNA].NA_ANALYSIS_USERDATA_VALIDATE,
      value: newNa,
      newIsNaError
    });

    let completedStep = 1;
    let analysisCompleted = true;
    newNa.aspects.split(",").forEach(aspect => {
      if (!newNa[aspect].isValid) {
        analysisCompleted = false;
      }
    });
    if (analysisCompleted) {
      completedStep = 2;
    } else {
      completedStep = 1;
    }

    yield put({
      type: ACTION_TYPES[FNA].UPDATE_COMPLETED_STEP,
      completedStep
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* updateNaAnalysisDataAndValidate(action) {
  try {
    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_ANALYSIS_USERDATA,
      value: action.value
    });
    if (action.value.targetData.assets) {
      yield put({
        type: ACTION_TYPES[FNA].UPDATE_NA_ANALYSIS_USERDATA_ASSETS,
        value: action.value
      });
    }
    if (action.value.rule) {
      yield validateNeedsAnalysisData(action);
    }
    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_IS_CHANGED,
      newNaIsChanged: true
    });
    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_IS_GLOBALLY_CHANGED,
      newNaIsGloballyChanged: true
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

// updateNaAnalysis: do logical calculation of raw data in saga,
// then call updateNaAnalysisDataAndValidate to update the store
export function* updateNaAnalysis(action) {
  const { targetFunction, data } = action.value;
  const func = naAnalysisUtils[targetFunction];
  if (func) {
    const targetData = func(data);
    if (action.value.data) {
      delete action.value.data;
    }
    const nextAction = Object.assign({}, action, {
      value: Object.assign({}, action.value, { targetData })
    });
    yield* updateNaAnalysisDataAndValidate(nextAction);
  } else {
    yield* updateNaAnalysisDataAndValidate(action);
  }
}

export function* updateNaAspects() {
  yield resetProductTypeCkaRaIsValid();
}

export function* initCompletedStepSaga(action) {
  const na = yield select(state => state[FNA].na);
  let { completedStep, lastStepIndex } = na;

  switch (action.value) {
    // NEEDS completed step runs at INIT_NA_ASPECTS
    case PROGRESS_TABS[FNA].ROI: {
      const needsAspect = na.aspects.split(",");
      const { iarRate, iarRateNotMatchReason } = na;

      let mandatory = false;

      if (
        needsAspect.indexOf(CIPROTECTION) > -1 ||
        needsAspect.indexOf(FIPROTECTION) > -1 ||
        needsAspect.indexOf(RPLANNING) > -1
      ) {
        mandatory = true;
      }

      // The rules that allow to pass
      const ruleA = mandatory && iarRate !== "";
      const ruleB = !mandatory;

      let iarRateNotMatchReasonValid = true;
      if (
        (parseFloat(na.iarRate) < 0 || parseFloat(na.iarRate) > 6) &&
        iarRateNotMatchReason === ""
      ) {
        iarRateNotMatchReasonValid = false;
      }

      if (
        completedStep <= 1 &&
        (ruleA || ruleB) &&
        iarRateNotMatchReasonValid
      ) {
        completedStep = 1;
      }
      lastStepIndex = 1;
      break;
    }

    case PROGRESS_TABS[FNA].ANALYSIS: {
      let analysisCompleted = true;
      na.aspects.split(",").forEach(aspect => {
        if (!na[aspect].isValid) {
          analysisCompleted = false;
        }
      });
      if (analysisCompleted && completedStep <= 2) {
        completedStep = 2;
      }
      lastStepIndex = 2;
      break;
    }

    // PRIORITY completed step runs at UPDATE_NA_PRIORITY

    default:
      break;
  }

  yield put({
    type: ACTION_TYPES[FNA].INIT_COMPLETED_STEP,
    completedStep,
    lastStepIndex
  });
}

export function* roiValidation() {
  const newIarRate = yield select(state => state[FNA].na.iarRate);
  const newIarRateNotMatchReason = yield select(
    state => state[FNA].na.iarRateNotMatchReason
  );

  let checkMandatory = {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  };
  let completedStep = 1;

  if (parseFloat(newIarRate) < 0 || parseFloat(newIarRate) > 6) {
    checkMandatory = validateMandatory({
      field: { type: TEXT_FIELD, mandatory: true, disabled: false },
      value: newIarRateNotMatchReason
    });
  }

  if (checkMandatory.hasError) {
    completedStep = 0;
  }

  yield put({
    type: ACTION_TYPES[FNA].VALIDATE_IAR_RATE_NOT_MATCH_REASON,
    newIsIarRateNotMatchReasonError: checkMandatory,
    completedStep
  });

  yield resetProductTypeCkaRaIsValid();
}

export function* iarRateNotMatchReasonSaga() {
  try {
    yield roiValidation();

    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_IS_CHANGED,
      newNaIsChanged: true
    });
    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_IS_GLOBALLY_CHANGED,
      newNaIsGloballyChanged: true
    });
    yield resetProductTypeCkaRaIsValid();
  } catch (e) {
    // TODO: need erro handling
  }
}

export function* validateFE() {
  const profileData = yield select(state => state[CLIENT].profile);
  const pdaData = yield select(state => state[FNA].pda);
  const dependantProfilesData = yield select(
    state => state[CLIENT].dependantProfiles
  );
  const feData = yield select(state => state[FNA].fe);

  if (pdaData.isCompleted) {
    yield put({
      type: ACTION_TYPES[FNA].VALIDATE_FE,
      feData,
      pdaData,
      profileData,
      dependantProfilesData
    });
  }
}

/*
Get the FNA Data from Database
*/
export function* getFNA(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const profileData = yield select(state => state[CLIENT].profile);
    const dependantProfilesData = yield select(
      state => state[CLIENT].dependantProfiles
    );
    const clientProfile = yield select(state => state[CLIENT].profile);
    const needsResponseData = yield call(callServer, {
      url: `needs`,
      data: {
        action: "initNeeds",
        cid: clientProfile.cid
      }
    });
    if (needsResponseData.success) {
      const fnaData = naMapForm(_.cloneDeep(needsResponseData.fna));
      const pdaData = pdaMapForm(_.cloneDeep(needsResponseData.pda));
      let hasSpouse = false;

      Object.values(dependantProfilesData).forEach(arr => {
        arr.dependants.forEach(dependant => {
          if (dependant.relationship === "SPO") {
            hasSpouse = true;
          }
        });
      });

      // Prod issue in EASE Web: does not reset pda.applicant after spouse is removed
      const pdaError = pdaData.applicant === "joint" && hasSpouse === false;
      if (pdaError) {
        pdaData.isCompleted = false;
        pdaData.lastStepIndex = 0;
      }

      if (pdaError) {
        const tiInfo =
          _.isEmpty(pdaData.tiInfo) &&
          _.isEqual(pdaData.tiInfo, clientProfile.trustedIndividuals)
            ? _.cloneDeep(pdaData.tiInfo)
            : _.cloneDeep(clientProfile.trustedIndividuals);
        yield call(callServer, {
          url: `needs`,
          data: {
            action: "savePDA",
            cid: clientProfile.cid,
            pda: pdaData,
            tiInfo,
            confirm: true
          }
        });

        const newNeedsResponseData = yield call(callServer, {
          url: `needs`,
          data: {
            action: "initNeeds",
            cid: clientProfile.cid
          }
        });
        yield put({
          type: ACTION_TYPES[FNA].UPDATE_FNA,
          // fna comes from web, should be named na (Needs analysis)
          fnaData: newNeedsResponseData.fna,
          feData: newNeedsResponseData.fe,
          pdaData: newNeedsResponseData.pda,
          profileData,
          dependantProfilesData,
          newPda: newNeedsResponseData.pda,
          needsSummaryData: newNeedsResponseData.needsSummary
        });
      } else {
        yield put({
          type: ACTION_TYPES[FNA].UPDATE_FNA,
          // fna comes from web, should be named na (Needs analysis)
          fnaData,
          feData: needsResponseData.fe,
          pdaData,
          profileData,
          dependantProfilesData,
          newPda: pdaData,
          needsSummaryData: needsResponseData.needsSummary
        });
      }

      yield validateFE();
    }

    if (_.isFunction(action.callback)) {
      action.callback();
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* saveNA(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const clientProfile = yield select(state => state[CLIENT].profile);
    const na = yield select(state => state[FNA].na);
    const feData = yield select(state => state[FNA].fe);
    const pdaData = yield select(state => state[FNA].pda);
    const { confirm, callback } = action;

    const ckaSection = mapCKA(_.cloneDeep(na.ckaSection ? na.ckaSection : {}));
    const reMappedNa = Object.assign({}, na, {
      ckaSection: Object.assign({}, na.ckaSection, ckaSection)
    });

    const serverResponse = yield call(callServer, {
      url: "needs",
      data: {
        action: "saveFNA",
        cid: clientProfile.cid,
        fna: reMappedNa,
        confirm
      }
    });

    if (serverResponse && serverResponse.success) {
      if (serverResponse.code) {
        callback(serverResponse.code);
      } else {
        const dependantProfilesData = yield select(
          state => state[CLIENT].dependantProfiles
        );
        yield put({
          type: ACTION_TYPES[FNA].SAVE_FNA_COMPLETE,
          feData,
          profileData: serverResponse.profile || clientProfile,
          pdaData,
          dependantProfilesData,
          naData: serverResponse.fna,
          newPda: pdaData,
          newIsFnaInvalid: !!serverResponse.showFnaInvalidFlag
        });
        yield put({
          type: ACTION_TYPES[FNA].UPDATE_NA_IS_CHANGED,
          newNaIsChanged: false
        });
        yield call(callback);
      }
    }
  } catch (e) {
    // TODO
  }
}

export function* updateProductTypesCompletedStep(action) {
  try {
    const ckaSection = yield select(state => state[FNA].na.ckaSection);
    const raSection = yield select(state => state[FNA].na.raSection);
    const { value } = action;
    let isCompleted = false;

    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_PRODUCT_TYPES,
      value
    });

    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_COMPLETED_STEP,
      value
    });

    if (
      value !== "" &&
      (value.indexOf("investLinkedPlans") === -1 ||
        (value.indexOf("investLinkedPlans") !== -1 && ckaSection.isValid)) &&
      ((value.indexOf("investLinkedPlans") === -1 &&
        value.indexOf("participatingPlans") === -1) ||
        ((value.indexOf("investLinkedPlans") !== -1 ||
          value.indexOf("participatingPlans") !== -1) &&
          raSection.isValid))

      // (prodTypeArr.indexOf(INVESTLINKEDPLANS) > -1 &&
      //   (!ckaSection.isValid || !raSection.isValid)) ||
      // (prodTypeArr.indexOf(PARTICIPATINGPLANS) > -1 && !raSection.isValid)
    ) {
      isCompleted = true;
    }

    yield put({
      type: ACTION_TYPES[FNA].UPDATE_IS_COMPLETED,
      isCompleted
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* initPDA(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const clientProfile = yield select(state => state[CLIENT].profile);

    const serverResponse = yield call(callServer, {
      url: `needs`,
      data: {
        action: "initNeeds",
        cid: clientProfile.cid
      }
    });

    yield put({
      type: ACTION_TYPES[FNA].UPDATE_PDA,
      // TOBE remove null
      newPda: serverResponse.pda
    });

    action.callback();
  } catch (e) {
    // TODO
  }
}

export function* updateLastSelectedProduct(action) {
  try {
    yield put({
      type: ACTION_TYPES[FNA].UPDATE_LAST_SELECTED_PRODUCT,
      value: action.lastSelectedProduct
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* initProductTypesSaga() {
  const na = yield select(state => state[FNA].na);
  const needsAspect = na.aspects.split(",");
  const { prodType } = na.productType;

  if (
    // There is default selected Product Type
    needsAspect.indexOf(CIPROTECTION) > -1 ||
    needsAspect.indexOf(PCHEADSTART) > -1 ||
    (na.aspects === PAPROTECTION ||
      na.aspects === HCPROTECTION ||
      _.isEqual(needsAspect.sort(), [PAPROTECTION, HCPROTECTION].sort()) ||
      needsAspect.indexOf(PAPROTECTION) > -1 ||
      needsAspect.indexOf(HCPROTECTION) > -1)
  ) {
    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_PRODUCT_TYPES,
      value: prodType
    });
  }
  yield put({
    type: ACTION_TYPES[FNA].UPDATE_NA_COMPLETED_STEP,
    value: prodType
  });
}

export function* validateNA(action) {
  const dependantProfilesData = yield select(
    state => state[CLIENT].dependantProfiles
  );
  const profileData = yield select(state => state[CLIENT].profile);
  const naData = yield select(state => state[FNA].na);
  const feData = yield select(state => state[FNA].fe);

  if (feData.isCompleted) {
    yield put({
      type: ACTION_TYPES[FNA].VALIDATE_NA,
      naData,
      profileData,
      dependantProfilesData,
      feData
    });

    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_IS_CHANGED,
      newNaIsChanged: true
    });
    yield put({
      type: ACTION_TYPES[FNA].UPDATE_NA_IS_GLOBALLY_CHANGED,
      newNaIsGloballyChanged: true
    });
    if (action.resetProdType !== false) {
      yield resetProductTypeCkaRaIsValid();
    }
  }
}

export function* updateNAValidStatus() {
  const isNaErrorData = yield select(state => state[FNA].isNaError);
  const naData = yield select(state => state[FNA].na);

  yield put({
    type: ACTION_TYPES[FNA].UPDATE_NA_VALID_STATUS,
    isNaErrorData,
    naData
  });
}

export function* saveFE(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const clientProfile = yield select(state => state[CLIENT].profile);
    const dependantProfilesData = yield select(
      state => state[CLIENT].dependantProfiles
    );
    const cid = yield select(state => state[CLIENT].profile.cid);
    const pdaData = yield select(state => state[FNA].pda);
    const feData = yield select(state => state[FNA].fe);
    const na = yield select(state => state[FNA].na);
    const feErrors = yield select(state => state[FNA].feErrors);
    const { confirm, callback } = action;

    // get isCompleted
    let hasError = false;
    Object.keys(feErrors).forEach(clientCid => {
      Object.keys(feErrors[clientCid]).forEach(key => {
        hasError = hasError || feErrors[clientCid][key].hasError;
      });
    });

    const response = yield call(callServer, {
      url: `needs`,
      data: {
        action: "saveFE",
        cid,
        fe: Object.assign({}, feData, { isCompleted: !hasError }),
        confirm
      }
    });

    if (response.success) {
      if (response.code) {
        callback(response.code);
      } else {
        yield put({
          type: ACTION_TYPES[FNA].SAVE_FNA_COMPLETE,
          feData: response.fe,
          profileData: response.profile || clientProfile,
          pdaData,
          dependantProfilesData,
          naData: response.fna || na,
          newPda: pdaData,
          newIsFnaInvalid: !!response.showFnaInvalidFlag
        });

        yield put({
          type: ACTION_TYPES[FNA].UPDATE_COMPLETED_STEP,
          completedStep: -1
        });

        callback();
      }
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* loadFNAReport(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const profile = yield select(state => state[CLIENT].profile);

    const response = yield call(callServer, {
      url: `needs`,
      data: {
        action: "generateFNAReport",
        cid: profile.cid,
        profile
      }
    });

    if (response.success) {
      yield put({
        type: ACTION_TYPES[FNA].SAVE_FNA_REPORT,
        FNAReportData: response.fnaReport
      });

      if (_.isFunction(action.callback)) {
        action.callback();
      }
    }
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* openEmailDialog(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const profile = yield select(state => state[CLIENT].profile);
    const agent = yield select(state => state[AGENT]);
    const response = yield call(callServer, {
      url: `needs`,
      data: {
        action: "getFNAEmailTemplate"
      }
    });

    if (response && response.success) {
      let { agentContent, clientContent } = response;
      const { agentSubject, clientSubject, embedImgs } = response;

      agentContent = replaceAll(agentContent, "{{agentName}}", agent.name);
      agentContent = replaceAll(
        agentContent,
        "{{agentContactNum}}",
        agent.mobile
      );
      agentContent = replaceAll(agentContent, "{{recipientName}}", agent.name);
      agentContent = replaceAll(agentContent, '<img src="cid:axaLogo">', "");

      clientContent = replaceAll(clientContent, "{{agentName}}", agent.name);
      clientContent = replaceAll(
        clientContent,
        "{{agentContactNum}}",
        agent.mobile
      );
      clientContent = replaceAll(
        clientContent,
        "{{recipientName}}",
        profile.fullName
      );
      clientContent = replaceAll(clientContent, '<img src="cid:axaLogo">', "");

      const emails = [
        {
          id: "agent",
          agentCode: agent.agentCode,
          dob: null,
          name: "Agent",
          to: [agent.email],
          title: agentSubject,
          content: agentContent,
          embedImgs
        },
        {
          id: "client",
          agentCode: null,
          dob: profile.dob,
          name: "Client",
          to: [profile.email],
          title: clientSubject,
          content: clientContent,
          embedImgs
        }
      ];

      yield put({
        type: ACTION_TYPES[FNA].OPEN_EMAIL,
        emails
      });

      if (_.isFunction(action.callback)) {
        action.callback();
      }
    }
  } catch (e) {
    // TODO need error handling
  }
}

export function* sendEmail(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const profile = yield select(state => state[CLIENT].profile);
    const agent = yield select(state => state[AGENT]);
    const emails = yield select(state => state[FNA].FNAReportEmail.emails);
    const reportOptions = yield select(
      state => state[FNA].FNAReportEmail.reportOptions
    );
    const skipEmailInReport = yield select(
      state => state[FNA].FNAReportEmail.skipEmailInReport
    );
    const authToken = yield select(state => state[LOGIN].authToken);
    const webServiceUrl = yield select(state => state[LOGIN].webServiceUrl);

    const newEmail = [];

    Object.keys(reportOptions).forEach(key => {
      if (reportOptions[key].fnaReport === true) {
        newEmail.push(emails.find(x => x.id === key));
      }
    });
    _.forEach(newEmail, email => {
      email.content += "<img src='cid:axaLogo'>";
    });
    const response = yield call(callServer, {
      url: `needs`,
      data: {
        action: "emailFnaReport",
        emails: newEmail,
        profile,
        agentProfile: agent,
        skipEmailInReport,
        authToken,
        webServiceUrl
      }
    });

    if (response && response.success && _.isFunction(action.callback)) {
      action.callback();

      // TODO: will close email in coming version
      // yield put({
      //   type: ACTION_TYPES[FNA].CLOSE_EMAIL
      // });
    }
  } catch (e) {
    // TODO: need handler error
  }
}

export function* validateGivenName() {
  try {
    const givenNameData = yield select(state => state[FNA].givenName);

    yield put({
      type: ACTION_TYPES[FNA].VALIDATE_TRUSTED_INDIVIDUAL_GIVEN_NAME,
      givenNameData
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateSurname() {
  try {
    const surnameData = yield select(state => state[FNA].surname);

    yield put({
      type: ACTION_TYPES[FNA].VALIDATE_TRUSTED_INDIVIDUAL_SURNAME,
      surnameData
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateName() {
  try {
    const nameData = yield select(state => state[FNA].name);

    yield put({
      type: ACTION_TYPES[FNA].VALIDATE_TRUSTED_INDIVIDUAL_NAME,
      nameData
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateIDDocumentType() {
  try {
    const IDDocumentTypeData = yield select(state => state[FNA].IDDocumentType);

    yield put({
      type: ACTION_TYPES[FNA].VALIDATE_TRUSTED_INDIVIDUAL_ID_DOCUMENT_TYPE,
      IDDocumentTypeData
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateIDDocumentTypeOther() {
  try {
    const IDDocumentTypeOtherData = yield select(
      state => state[FNA].IDDocumentTypeOther
    );
    const IDDocumentTypeData = yield select(state => state[FNA].IDDocumentType);

    yield put({
      type: ACTION_TYPES[FNA].VALIDATE_TRUSTED_ID_DOCUMENT_TYPE_OTHER,
      IDDocumentTypeOtherData,
      IDDocumentTypeData
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateID() {
  try {
    const IDData = yield select(state => state[FNA].ID);
    const IDDocumentTypeData = yield select(state => state[FNA].IDDocumentType);

    yield put({
      type: ACTION_TYPES[FNA].VALIDATE_TRUSTED_INDIVIDUAL_ID,
      IDData,
      IDDocumentTypeData
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateRelationship() {
  try {
    const relationshipData = yield select(state => state[FNA].relationship);

    yield put({
      type: ACTION_TYPES[FNA].VALIDATE_TRUSTED_INDIVIDUAL_RELATIONSHIP,
      relationshipData
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* relationShipOtherSaga() {
  try {
    const relationshipData = yield select(state => state[FNA].relationship);
    const relationshipOtherData = yield select(
      state => state[FNA].relationshipOther
    );

    yield put({
      type: ACTION_TYPES[FNA].VALIDATE_TRUSTED_INDIVIDUAL_RELATIONSHIP_OTHER,
      relationshipOtherData,
      relationshipData
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validatePrefix() {
  try {
    const prefixData = yield select(state => state[FNA].prefix);

    yield put({
      type: ACTION_TYPES[FNA].VALIDATE_TRUSTED_INDIVIDUAL_PREFIX,
      prefixData
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* validateMobileNo() {
  try {
    const mobileNoData = yield select(state => state[FNA].mobileNo);

    yield put({
      type: ACTION_TYPES[FNA].VALIDATE_TRUSTED_INDIVIDUAL_MOBILE_NO,
      mobileNoData
    });
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* initialValidateTrustedIndividualSaga(action) {
  try {
    const fna = yield select(state => state[FNA]);

    yield put({
      type: ACTION_TYPES[FNA].INITIAL_VALIDATE,
      surnameData: fna.surname,
      nameData: fna.name,
      relationshipData: fna.relationship,
      givenNameData: fna.givenName,
      IDData: fna.ID,
      IDDocumentTypeData: fna.IDDocumentType,
      prefixData: fna.prefix,
      mobileNoData: fna.mobileNo,
      relationshipOtherData: fna.relationshipOther
    });

    yield call(action.callback);
  } catch (e) {
    /* TODO need error handling */
  }
}

export function* updatePDA(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const clientProfile = yield select(state => state[CLIENT].profile);

    const serverResponse = yield call(callServer, {
      url: `needs`,
      data: {
        action: "initNeeds",
        cid: clientProfile.cid
      }
    });

    yield put({
      type: ACTION_TYPES[FNA].UPDATE_PDA,
      newPda: serverResponse.pda
    });

    action.callback();
  } catch (e) {
    // TODO
  }
}

export function* savePDA(action) {
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const clientProfile = yield select(state => state[CLIENT].profile);
    const pda = yield select(state => state[FNA].pda);
    const fe = yield select(state => state[FNA].fe);
    const fna = yield select(state => state[FNA].fna);
    const tiInfo =
      _.isEmpty(pda.tiInfo) &&
      _.isEqual(pda.tiInfo, clientProfile.trustedIndividuals)
        ? _.cloneDeep(pda.tiInfo)
        : _.cloneDeep(clientProfile.trustedIndividuals);
    const { confirm, callback } = action;

    const serverResponse = yield call(callServer, {
      url: `needs`,
      data: {
        action: "savePDA",
        cid: clientProfile.cid,
        pda,
        tiInfo,
        confirm
      }
    });

    if (serverResponse && serverResponse.success) {
      const dependantProfilesData = yield select(
        state => state[CLIENT].dependantProfiles
      );
      yield put({
        type: ACTION_TYPES[FNA].SAVE_FNA_COMPLETE,
        feData: serverResponse.fe || fe,
        profileData: serverResponse.profile || clientProfile,
        pdaData: serverResponse.pda,
        dependantProfilesData,
        naData: serverResponse.fna || fna,
        newPda: serverResponse.pda,
        newIsFnaInvalid: !!serverResponse.showFnaInvalidFlag
      });
      yield put({
        type: ACTION_TYPES[FNA].UPDATE_COMPLETED_STEP,
        completedStep: -1
      });
    }

    if (serverResponse && serverResponse.code) {
      yield call(callback, serverResponse.code);
    } else {
      yield call(callback);
    }
  } catch (e) {
    // TODO
  }
}
