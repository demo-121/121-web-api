import { call, select } from "redux-saga/effects";
import { CONFIG } from "../constants/REDUCER_TYPES";

export function* signatureExpiry(action) {
  let response = null;
  try {
    const callServer = yield select(state => state[CONFIG].callServer);
    const { cid, applicationId } = action;

    response = yield call(callServer, {
      url: `invalidation`,
      data: {
        action: "signatureExpiry",
        cid,
        applicationId
      }
    });
  } catch (e) {
    // TODO
    return null;
  }
  return response;
}

export default {};
