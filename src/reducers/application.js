import { combineReducers } from "redux";
import * as _ from "lodash";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import { APPLICATION } from "../constants/REDUCER_TYPES";
/**
 * template
 * @description store the template retrieved from core-api
 * @default {}
 * */
const template = (state = {}, { type, newTemplate }) => {
  switch (type) {
    case ACTION_TYPES[APPLICATION].GET_APPLICATION:
      if (newTemplate) {
        return Object.assign({}, state, newTemplate);
      }
      return state;
    case ACTION_TYPES[APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA:
      return {};
    default:
      return state;
  }
};

/**
 * application
 * @description store the application value retrieved from core-api or updated from app
 * @default {}
 * */
const application = (
  state = {},
  { type, newApplication, newIsMandDocsAllUploaded, newPolicuNumnber }
) => {
  switch (type) {
    case ACTION_TYPES[APPLICATION].GET_APPLICATION:
    case ACTION_TYPES[APPLICATION].UPDATE_PERSONAL_DETAILS_ADDRESS:
    case ACTION_TYPES[APPLICATION].UPDATE_APPLICATION_FORM_VALUES:
    case ACTION_TYPES[APPLICATION].UPDATE_APPLICATION_FORM_PERSONAL_DETAILS:
    case ACTION_TYPES[APPLICATION].GET_APPLICATION_FOR_SUPPORTING_DOCUMENT:
      if (newApplication) {
        return Object.assign({}, state, newApplication);
      }
      return state;
    case ACTION_TYPES[APPLICATION].UPDATE_POLICY_NUMBER:
      if (newPolicuNumnber) {
        return Object.assign(
          {},
          state,
          Object.assign({}, state.application, {
            policyNumber: newPolicuNumnber
          })
        );
      }
      return state;
    case ACTION_TYPES[APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA:
      return {};
    case ACTION_TYPES[APPLICATION].UPDATE_SUPPORTING_DOCUMENT_ERROR_ICON:
      if (
        newIsMandDocsAllUploaded !== undefined &&
        newIsMandDocsAllUploaded !== null
      ) {
        return Object.assign({}, state, {
          isMandDocsAllUploaded: newIsMandDocsAllUploaded
        });
      }
      return state;
    case ACTION_TYPES[APPLICATION].INIT_PAYMENT_PAGE:
    default:
      return state;
  }
};

const initStateComponent = {
  completedStep: -1,
  currentStep: 0,
  selectedSectionKey: "",
  selectedCid: "",
  showPlanDetailsView: false,
  disabled: false,
  valueEdited: false,
  apiPage: "",
  shieldPolicyNumber: [],
  isAlertDelayed: false,
  isCrossAgeAlertShow: false,
  isSignatureExpiryAlertShow: false,
  isDisableEappButton: false
};
/**
 * component
 * @description store the component status in UI
 * @default {}
 * */
const component = (
  state = initStateComponent,
  {
    type,
    newShieldPolicyNumber,
    isPlanDetailsView,
    newSelectedSectionKey,
    newValueEdited,
    applicationStatus,
    newCurrentStep,
    newSelectedCid,
    newCompletedStep,
    newApiPage,
    newIsAlertDelayed,
    newIsCrossAgeAlertShow,
    newIsSignatureExpiryAlertShow,
    newIsDisableEappButton
  }
) => {
  switch (type) {
    case ACTION_TYPES[APPLICATION].IS_DISABLE_EAPP_BUTTON:
      return Object.assign({}, state, {
        isDisableEappButton: newIsDisableEappButton
      });
    case ACTION_TYPES[APPLICATION].UPDATE_SHIELD_POLICY_NUMBER:
      return Object.assign({}, state, {
        shieldPolicyNumber: newShieldPolicyNumber
      });
    case ACTION_TYPES[APPLICATION].UPDATE_API_PAGE:
      return Object.assign({}, state, {
        apiPage: newApiPage
      });
    case ACTION_TYPES[APPLICATION].UPDATE_UI_VIEW_PLAN_DETAILS:
      return Object.assign({}, state, {
        showPlanDetailsView: isPlanDetailsView
      });
    case ACTION_TYPES[APPLICATION].VALUE_EDITED:
      return Object.assign({}, state, {
        valueEdited: newValueEdited
      });

    case ACTION_TYPES[APPLICATION].UPDATE_UI_SELECTED_SECTION_KEY:
      return Object.assign({}, state, {
        selectedSectionKey: newSelectedSectionKey
      });
    case ACTION_TYPES[APPLICATION].UPDATE_EAPP_STEP:
      return Object.assign(
        {},
        state,
        {
          currentStep: newCurrentStep
        },
        _.isNumber(newCompletedStep) ? { completedStep: newCompletedStep } : {}
      );
    case ACTION_TYPES[APPLICATION].UPDATE_APPLICATION_STATUS:
      return Object.assign({}, state, applicationStatus);

    case ACTION_TYPES[APPLICATION].UPDATE_UI_SELECTED_CID:
      return Object.assign({}, state, {
        selectedCid: newSelectedCid
      });
    case ACTION_TYPES[APPLICATION].UPDATE_UI_SHOW_CROSS_AGE_ALERT:
      return Object.assign({}, state, {
        isAlertDelayed: newIsAlertDelayed,
        isCrossAgeAlertShow: newIsCrossAgeAlertShow
      });
    case ACTION_TYPES[APPLICATION].UPDATE_UI_SIGNATURE_EXPIRY_ALERT:
      return Object.assign({}, state, {
        isAlertDelayed: newIsAlertDelayed,
        isSignatureExpiryAlertShow: newIsSignatureExpiryAlertShow
      });
    case ACTION_TYPES[APPLICATION].UPDATE_UI_ALERT_DELAYED:
      return Object.assign({}, state, {
        isAlertDelayed: newIsAlertDelayed
      });
    case ACTION_TYPES[APPLICATION].UPDATE_COMPLETED_STEP:
    case ACTION_TYPES[APPLICATION].VALIDATE_APPLICATION_FORM:
      return Object.assign({}, state, {
        completedStep: newCompletedStep
      });
    case ACTION_TYPES[APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA:
      return initStateComponent;
    default:
      return state;
  }
};

const initStateError = { application: {}, payment: {} };

/**
 * error
 * @description store the error retrieved from web/app UI
 * @default { application: {}, payment: {} }
 * */
const error = (state = initStateError, { type, errorObj }) => {
  switch (type) {
    case ACTION_TYPES[APPLICATION].VALIDATE_APPLICATION_FORM:
      return Object.assign({}, state, errorObj);
    case ACTION_TYPES[APPLICATION]
      .INIT_VALIDATE_APPLICATION_FORM_PERSONAL_DETAILS:
      return Object.assign({}, state, errorObj);
    case ACTION_TYPES[APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA:
      return initStateError;
    default:
      return state;
  }
};

const initStateCrossAge = {
  crossedAge: false,
  allowBackdate: false,
  // For normal product
  proposerStatus: 0,
  insuredStatus: 0,
  // For shield product
  crossedAgeCid: [],
  status: 0
};

/**
 * crossAge
 * @description store the crossAge object retrieved from web/app UI
 * @default {
 *   crossedAge: false,
 *   allowBackdate: false,
 *   proposerStatus: 0,
 *   insuredStatus: 0,
 *   crossedAgeCid: [],
 *   status: 0
 * }
 * */
const crossAge = (state = initStateCrossAge, { type, newCrossAge }) => {
  switch (type) {
    case ACTION_TYPES[APPLICATION].UPDATE_CROSS_AGE:
      return Object.assign(
        {},
        state,
        Object.assign({}, initStateCrossAge, newCrossAge)
      );
    case ACTION_TYPES[APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA:
      return initStateCrossAge;
    default:
      return state;
  }
};

export default combineReducers({
  template,
  application,
  error,
  component,
  crossAge
});
