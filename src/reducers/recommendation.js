import { combineReducers } from "redux";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import { RECOMMENDATION } from "../constants/REDUCER_TYPES";

const initStateRecommendation = {
  chosenList: [],
  notChosenList: []
};
/**
 * recommendation
 * @description store the recommendation data retrieved from core-api and web/app UI
 * @default { chosenList: [], notChosenList: [] }
 * */
const recommendation = (state = initStateRecommendation, action) => {
  switch (action.type) {
    case ACTION_TYPES[RECOMMENDATION].GET_RECOMMENDATION:
    case ACTION_TYPES[RECOMMENDATION].UPDATE_RECOMMENDATION:
    case ACTION_TYPES[RECOMMENDATION].UPDATE_RECOMMENDATION_DATA:
      return Object.assign({}, state, action.recommendation);
    case ACTION_TYPES[RECOMMENDATION].CLOSE_RECOMMENDATION:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return initStateRecommendation;
    default:
      return state;
  }
};

/**
 * budget
 * @description store the budget data retrieved from core-api and web/app UI
 * @default {}
 * */
const budget = (state = {}, action) => {
  switch (action.type) {
    case ACTION_TYPES[RECOMMENDATION].GET_RECOMMENDATION:
    case ACTION_TYPES[RECOMMENDATION].UPDATE_RECOMMENDATION:
    case ACTION_TYPES[RECOMMENDATION].UPDATE_BUDGET_DATA:
      return Object.assign({}, state, action.budget);
    case ACTION_TYPES[RECOMMENDATION].CLOSE_RECOMMENDATION:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return {};
    default:
      return state;
  }
};

/**
 * acceptance
 * @description store the acceptance retrieved from core-api and web/app UI
 * @default []
 * */
const acceptance = (state = [], action) => {
  switch (action.type) {
    case ACTION_TYPES[RECOMMENDATION].GET_RECOMMENDATION:
    case ACTION_TYPES[RECOMMENDATION].UPDATE_RECOMMENDATION:
      return action.acceptance;
    case ACTION_TYPES[RECOMMENDATION].CLOSE_RECOMMENDATION:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return [];
    default:
      return state;
  }
};

const initStateError = {
  recommendation: {},
  budget: {},
  acceptance: {}
};
/**
 * error
 * @description store the error retrieved from web/app UI
 * @default { recommendation: {}, budget: {}, acceptance: {} }
 * */
const error = (state = initStateError, action) => {
  switch (action.type) {
    case ACTION_TYPES[RECOMMENDATION].GET_RECOMMENDATION:
    case ACTION_TYPES[RECOMMENDATION].UPDATE_RECOMMENDATION_ERROR:
    case ACTION_TYPES[RECOMMENDATION].UPDATE_BUDGET_ERROR:
      return Object.assign({}, state, action.error);
    case ACTION_TYPES[RECOMMENDATION].CLOSE_RECOMMENDATION:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return initStateError;
    default:
      return state;
  }
};

const initStateComponent = {
  completedStep: -1,
  currentStep: 0,
  completedSection: { recommendation: {}, budget: false },
  selectedQuotId: "",
  showDetails: false,
  disabled: false
};
/**
 * component
 * @description store the component status in UI
 * @default { completedStep: -1, currentStep: 0, completedSection: { recommendation: {}, budget: false }, showDetails: false, disabled: false }
 * */
const component = (state = initStateComponent, action) => {
  switch (action.type) {
    case ACTION_TYPES[RECOMMENDATION].GET_RECOMMENDATION:
    case ACTION_TYPES[RECOMMENDATION].UPDATE_RECOMMENDATION_COMPONENT:
    case ACTION_TYPES[RECOMMENDATION].UPDATE_RECOMMENDATION:
      return Object.assign({}, state, action.component);
    case ACTION_TYPES[RECOMMENDATION].UPDATE_RECOMMENDATION_SELECTED_QUOT:
      return Object.assign({}, state, {
        selectedQuotId: action.newSelectedQuotId
      });
    case ACTION_TYPES[RECOMMENDATION].UPDATE_RECOMMENDATION_DETAILS:
      return Object.assign({}, state, { showDetails: action.newShowDetails });
    case ACTION_TYPES[RECOMMENDATION].CLOSE_RECOMMENDATION:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return initStateComponent;
    default:
      return state;
  }
};

export default combineReducers({
  recommendation,
  budget,
  acceptance,
  error,
  component
});
