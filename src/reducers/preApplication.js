import { combineReducers } from "redux";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import { PRE_APPLICATION } from "../constants/REDUCER_TYPES";
import FILTER_TYPES from "../constants/FILTER_TYPES";

/**
 * applicationsList
 * @description store the applications list retrieved from core-api
 * @default []
 * */
const applicationsList = (state = [], action) => {
  switch (action.type) {
    case ACTION_TYPES[PRE_APPLICATION].UPDATE_APPLICATION_LIST:
    case ACTION_TYPES[PRE_APPLICATION].DELETE_APPLICATION_LIST:
      return action.applicationsList;
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return [];
    default:
      return state;
  }
};

/**
 * productList
 * @description store the product list, generated from applications list which is retrieved from core-api
 * @default []
 * */
const productList = (state = [], action) => {
  switch (action.type) {
    case ACTION_TYPES[PRE_APPLICATION].UPDATE_APPLICATION_LIST:
    case ACTION_TYPES[PRE_APPLICATION].DELETE_APPLICATION_LIST:
      return action.productList;
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return [];
    default:
      return state;
  }
};

/**
 * pdaMembers
 * @description store the client pda member list retrieved from core-api
 * @default []
 * */
const pdaMembers = (state = [], action) => {
  switch (action.type) {
    case ACTION_TYPES[PRE_APPLICATION].UPDATE_APPLICATION_LIST:
      return action.pdaMembers;
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return [];
    default:
      return state;
  }
};

/**
 * agentChannel
 * @description store the agent channel retrieved from core-api
 * @default ""
 * */
const agentChannel = (state = "", action) => {
  switch (action.type) {
    case ACTION_TYPES[PRE_APPLICATION].UPDATE_APPLICATION_LIST:
      return action.agentChannel;
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return "";
    default:
      return state;
  }
};

const initStateRecommendation = {
  completed: false,
  choiceList: []
};

/**
 * recommendation
 * @description store the information of recommendation retrieved from core-api
 * @default { completed: false, choiceList: [] }
 * */
const recommendation = (state = initStateRecommendation, action) => {
  switch (action.type) {
    case ACTION_TYPES[PRE_APPLICATION].UPDATE_APPLICATION_LIST:
    case ACTION_TYPES[PRE_APPLICATION].DELETE_APPLICATION_LIST:
      return Object.assign({}, state, action.recommendation);
    case ACTION_TYPES[PRE_APPLICATION].UPDATE_RECOMMENDATION_CHOICE_LIST:
      return Object.assign({}, state, {
        completed: action.completed,
        choiceList: action.choiceList
      });
    case ACTION_TYPES[PRE_APPLICATION].UPDATE_RECOMMENDATION_COMPLETED:
      return Object.assign({}, state, {
        completed: action.completed
      });
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return initStateRecommendation;
    default:
      return state;
  }
};

const initStateComponent = {
  selectedBundleId: "",
  selectModeOn: false,
  selectedIdList: [],
  selectedAppListFilter: FILTER_TYPES.APPLICATION_LIST.ALL
};

/**
 * component
 * @description store the information of UI component retrieved from core-api
 * @default {
 *  selectedBundleId: "",
 *  selectModeOn: false,
 *  selectedIdList: [],
 *  selectedAppListFilter: FILTER_TYPES.APPLICATION_LIST.ALL,
 *  isPlanDetailsPage: true
 * }
 * */
const component = (
  state = initStateComponent,
  {
    type,
    selectModeOn,
    selectedIdList,
    selectedAppListFilter,
    selectedBundleId
  }
) => {
  switch (type) {
    case ACTION_TYPES[PRE_APPLICATION].UPDATE_UI_SELECT_MODE:
      return Object.assign(
        {},
        state,
        {
          selectModeOn
        },
        !selectModeOn ? { selectedIdList: [] } : {}
      );
    case ACTION_TYPES[PRE_APPLICATION].UPDATE_UI_SELECTED_LIST:
      return Object.assign({}, state, {
        selectedIdList
      });
    case ACTION_TYPES[PRE_APPLICATION].UPDATE_UI_APPLICATION_LIST_FILTER:
      return Object.assign({}, state, {
        selectedAppListFilter
      });
    case ACTION_TYPES[PRE_APPLICATION].UPDATE_UI_SELECTED_BUNDLE_ID:
      return Object.assign({}, state, {
        selectedBundleId
      });
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return initStateComponent;
    default:
      return state;
  }
};

const initStatePreApplicationEmail = {
  appSumEmailOpen: false,
  emails: [],
  attKeysMap: [],
  tab: "",
  options: []
};

const preApplicationEmail = (
  state = initStatePreApplicationEmail,
  { type, emails, attKeysMap, tab, options }
) => {
  switch (type) {
    case ACTION_TYPES[PRE_APPLICATION].OPEN_EMAIL:
      return Object.assign({}, state, {
        appSumEmailOpen: true,
        attKeysMap,
        emails,
        tab: "agent",
        options
      });
    case ACTION_TYPES[PRE_APPLICATION].TAB_ON_CHANGE:
      return Object.assign({}, state, {
        tab
      });
    case ACTION_TYPES[PRE_APPLICATION].REPORT_ON_CHANGE:
      return Object.assign({}, state, {
        options
      });
    default:
      return state;
  }
};

const initStateMultiClientProfile = {
  hasErrorList: []
};

const multiClientProfile = (
  state = initStateMultiClientProfile,
  { type, newHasErrorList }
) => {
  switch (type) {
    case ACTION_TYPES[PRE_APPLICATION].UPDATE_MULTI_CLIENT_PROFILE_HAS_ERROR:
      return Object.assign({}, state, { hasErrorList: newHasErrorList });
    case ACTION_TYPES[PRE_APPLICATION].CLEAN_MULTI_CLIENT_PROFILE:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return initStateMultiClientProfile;
    default:
      return state;
  }
};

const productInvalidList = (state = [], { type, newProductInvalidList }) => {
  switch (type) {
    case ACTION_TYPES[PRE_APPLICATION].UPDATE_APPLICATION_LIST:
      return newProductInvalidList || [];
    default:
      return state;
  }
};

export default combineReducers({
  applicationsList,
  productList,
  pdaMembers,
  agentChannel,
  recommendation,
  component,
  preApplicationEmail,
  multiClientProfile,
  productInvalidList
});
