import * as _ from "lodash";
import traverse from "traverse";
import { combineReducers } from "redux";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import { FNA, FNA_CKA, FNA_RA } from "../constants/REDUCER_TYPES";
import { TEXT_FIELD } from "../constants/FIELD_TYPES";
import {
  FIPROTECTION,
  CIPROTECTION,
  DIPROTECTION,
  PAPROTECTION,
  PCHEADSTART,
  HCPROTECTION,
  PSGOALS,
  EPLANNING,
  RPLANNING,
  OTHER
} from "../constants/NEEDS";
import { OWNER, SPOUSE, DEPENDANTS } from "../constants/DEPENDANT";
import {
  naDataGetter,
  validateDiProtection,
  validateFiProtection,
  validateCiProtection,
  validateRPlanning,
  validatePcHeadstart,
  validatePaProtection,
  validateHcProtection,
  validateEPlanning,
  validateOther,
  validatePsGoals
} from "../utilities/naUtilities";
import { feDataGetter } from "../utilities/needsUtil";
import {
  validateBudgetAndForceIncome,
  validateCashFlow,
  validateEIP,
  validateMandatory,
  validateNetWorth
} from "../utilities/validation";
import TEXT_STORE, { LANGUAGE_TYPES } from "../locales";

// =============================================================================
// support functions
// =============================================================================
const addAspect = (need, aspects) => {
  let aspectsArr = aspects.split(",");
  const index = aspectsArr.indexOf(need);
  if (index === -1) {
    aspectsArr.push(need);
  }
  aspectsArr = aspectsArr.filter(x => x !== "");
  return aspectsArr.join();
};

const removeAspect = (need, aspects) => {
  const aspectsArr = aspects.split(",");
  const index = aspectsArr.indexOf(need);
  if (index > -1) {
    aspectsArr.splice(index, 1);
  }
  return aspectsArr.join();
};

const genNeedsAnalysisNewData = (state, value) => {
  const newState = _.cloneDeep(state);

  if (
    value.targetProfile !== "owner" &&
    value.targetProfile !== "spouse" &&
    state[value.targetProduct].dependants
  ) {
    // set dependants new data
    let targetObj = {};
    let targetIndex = null;
    state[value.targetProduct].dependants.some((data, index) => {
      if (data.cid === value.targetProfile) {
        targetObj = data;
        targetIndex = index;
        return true;
      }
      return false;
    });
    if (targetObj === {} || targetIndex === null) {
      return state;
    }
    const cloneDependants = _.cloneDeep(state[value.targetProduct].dependants);
    _.set(
      cloneDependants,
      `[${targetIndex}]`,
      Object.assign({}, targetObj, value.targetData)
    );

    const newTargetAspect = _.cloneDeep(state[value.targetProduct]);
    _.set(newTargetAspect, "dependants", cloneDependants);
    _.set(newState, `${value.targetProduct}`, newTargetAspect);
    _.set(newState, "isCompleted", false);
  } else {
    // set owner & spouse new data
    const aspectData = _.get(
      state,
      `${value.targetProduct}.${value.targetProfile}`,
      {}
    );
    const newAspectData = Object.assign({}, aspectData, value.targetData);
    _.set(
      newState,
      `${value.targetProduct}.${value.targetProfile}`,
      newAspectData
    );
    _.set(newState, "isCompleted", false);
  }
  return newState;
};

const feHandler = ({ feData, profileData, pdaData, dependantProfilesData }) => {
  // if pdaData is not ready, skip the initialize
  if (pdaData.isCompleted) {
    const newFEData = _.cloneDeep(feData);

    // owner
    newFEData.owner = feDataGetter({
      profileData,
      dependantProfilesData,
      fe: newFEData,
      relationshipObject: {
        cid: profileData.cid,
        relationship: "owner"
      }
    });

    // spouse
    const spouseData = _.find(
      profileData.dependants,
      dependant => dependant.relationship === "SPO"
    );
    if (pdaData.applicant === "joint" && !_.isEmpty(spouseData)) {
      newFEData.spouse = feDataGetter({
        profileData,
        dependantProfilesData,
        fe: newFEData,
        relationshipObject: {
          cid: spouseData.cid,
          relationship: "SPO"
        }
      });
    }

    // dependants
    if (pdaData.dependants !== "") {
      pdaData.dependants.split(",").forEach(cid => {
        const dependantData = _.find(
          profileData.dependants,
          dependant => dependant.cid === cid
        );

        // filter rule
        if (
          dependantData &&
          (dependantData.relationship === "SON" ||
            dependantData.relationship === "DAU")
        ) {
          const dependants = _.cloneDeep(newFEData.dependants) || [];

          const index = _.findIndex(
            dependants,
            dependant => dependant.cid === dependantData.cid
          );
          const dependantFEData = feDataGetter({
            profileData,
            dependantProfilesData,
            fe: newFEData,
            relationshipObject: dependantData
          });
          if (index !== -1) {
            dependants[index] = dependantFEData;
          } else {
            dependants.push(dependantFEData);
          }

          newFEData.dependants = dependants;
        }
      });
    }

    return newFEData;
  }
  return feData;
};

const nadHandler = ({
  naData,
  pdaData,
  profileData,
  dependantProfilesData,
  feData,
  state
}) => {
  // if feData is not ready, skip the initialize
  if (feData.isCompleted) {
    return naDataGetter({
      naData,
      pdaData,
      profileData,
      dependantProfilesData,
      feData,
      shouldInitProtection: naData.iarRate !== state.iarRate
    });
  }
  return naData;
};
// =============================================================================
// initial states
// =============================================================================
const naState = {};
const isNaErrorState = {};
// =============================================================================
// reducers
// =============================================================================
const pda = (state = {}, { type, newPda }) => {
  switch (type) {
    case ACTION_TYPES[FNA].UPDATE_FNA:
      return Object.assign({}, state, Object.assign({}, newPda));
    case ACTION_TYPES[FNA].UPDATE_PDA:
      return Object.assign({}, state, Object.assign({}, newPda));
    case ACTION_TYPES[FNA].GET_PDA:
      state = newPda;
      return state;
    case ACTION_TYPES[FNA].UPDATE_PDA_APPLICATE:
      return Object.assign({}, state, {
        applicant: newPda ? "single" : "joint"
      });
    case ACTION_TYPES[FNA].UPDATE_PDA_DEPENDANTS:
      return Object.assign({}, state, {
        dependants: newPda
      });
    case ACTION_TYPES[FNA].UPDATE_PDA_ISCOMPLETED:
      return Object.assign({}, state, {
        isCompleted: newPda
      });
    case ACTION_TYPES[FNA].UPDATE_PDA_LAST_STEP_INDEX:
      return Object.assign({}, state, {
        lastStepIndex: newPda
      });
    case ACTION_TYPES[FNA].UPDATE_APPLICANT_HAS_CHANGED:
      return Object.assign({}, state, {
        applicantHasChanged: newPda
      });
    case ACTION_TYPES[FNA].UPDATE_CONSENT_NOTICES_HAS_CHANGED:
      return Object.assign({}, state, {
        consentNoticesHasChanged: newPda
      });
    case ACTION_TYPES[FNA].UPDATE_PDA_OWNERCONSENTMETHOD:
      return Object.assign({}, state, {
        ownerConsentMethod: newPda
      });
    case ACTION_TYPES[FNA].UPDATE_PDA_SPOUSECONSENTMETHOD:
      return Object.assign({}, state, {
        spouseConsentMethod: newPda
      });
    case ACTION_TYPES[FNA].UPDATE_PDA_TRUSTINDIVIDUAL:
      return Object.assign({}, state, {
        trustedIndividual: newPda
      });
    case ACTION_TYPES[FNA].SAVE_FNA_COMPLETE:
      return newPda !== undefined ? newPda : state;
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return {};
    default:
      return state;
  }
};
/**
 * trustedIndividual
 * @description trustedIndividual
 * @default ""
 * */
const trustedIndividual = (state = "", { type, newTrustedIndividual }) => {
  switch (type) {
    case ACTION_TYPES[FNA].LOAD_TRUSTED_INDIVIDUAL:
    case ACTION_TYPES[FNA].TRUSTED_INDIVIDUAL_ON_CHANGE:
      return newTrustedIndividual;
    case ACTION_TYPES[FNA].CLEAN_TRUSTED_INDIVIDUAL_FORM:
      return "";
    default:
      return state;
  }
};

const na = (
  state = naState,
  {
    type,
    value,
    profileData,
    dependantProfilesData,
    naData,
    isNaErrorData,
    pdaData,
    feData,
    fnaData,
    ckaData,
    lastStepIndex,
    completedStep,
    newIarRateNotMatchReason,
    newIsIarRateNotMatchReasonError,
    newCourse,
    newInstitution,
    newStudyPeriodEndYear,
    birthYear,
    newCollectiveInvestment,
    newTransactionType,
    newInsuranceInvestment,
    newInsuranceType,
    newProfession,
    newYears,
    newPassCka,
    newRiskPotentialReturn,
    newAvgAGReturn,
    newSmDroped,
    newAlofLosses,
    newExpInvTime,
    newInvPref,
    newSelfSelectedRiskLevel,
    newSelfRLReasonRP,
    newAssessedRL,
    isValid,
    ckaIsValid,
    raIsValid,
    isCompleted,
    newInit
  }
) => {
  const errorState = {
    hasError: false,
    message: {
      [LANGUAGE_TYPES.ENGLISH]: "",
      [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: ""
    }
  };

  switch (type) {
    case ACTION_TYPES[FNA].INIT_NA_ASPECTS: {
      if (state.completedStep) {
        completedStep = _.cloneDeep(state.completedStep);
      } else {
        completedStep = -1;
      }

      if (value === "") {
        completedStep = -1;
      } else if (completedStep <= 0) {
        completedStep = 0;
      }

      return Object.assign({}, state, {
        aspects: value,
        completedStep,
        lastStepIndex: 0
      });
    }
    case ACTION_TYPES[FNA].UPDATE_FNA: {
      // if feData is not ready, skip the initialize
      if (feData.isCompleted) {
        return naDataGetter({
          naData: fnaData,
          pdaData,
          profileData,
          dependantProfilesData,
          feData
        });
      }
      return fnaData;
    }
    case ACTION_TYPES[FNA].UPDATE_NA:
      return nadHandler({
        naData,
        pdaData,
        profileData,
        dependantProfilesData,
        feData,
        state
      });
    case ACTION_TYPES[FNA].UPDATE_NA_VALID_STATUS: {
      const newNaData = _.cloneDeep(naData);
      const protectionList = [
        "fiProtection",
        "diProtection",
        "ciProtection",
        "rPlanning",
        "pcHeadstart",
        "paProtection",
        "hcProtection",
        "ePlanning",
        "other",
        "psGoals"
      ];
      let analysisIsCompleted = true;
      const updateData = {};

      protectionList.forEach(protection => {
        const protectionError = isNaErrorData[protection];
        let protectionHasError = false;

        Object.keys(protectionError).forEach(dependantKey => {
          const dependantError = protectionError[dependantKey];

          Object.keys(dependantError).forEach(key => {
            const errorData = protectionError[dependantKey][key];

            // some error is array, so need special handling
            if (Array.isArray(errorData)) {
              if (protectionError[dependantKey].goals) {
                // loop goals
                protectionError[dependantKey].goals.forEach(
                  (goal, goalIndex) => {
                    const goalError = errorData[goalIndex];

                    Object.keys(goalError).forEach(goalErrorKey => {
                      protectionHasError =
                        goalError[goalErrorKey].hasError || protectionHasError;

                      // loop goals assets
                      if (goalErrorKey === "assets") {
                        goalError.assets.forEach(goalAsset => {
                          Object.keys(goalAsset).forEach(goalAssetKey => {
                            protectionHasError =
                              goalAsset[goalAssetKey].hasError ||
                              protectionHasError;
                          });
                        });
                      }
                    });
                  }
                );
              }
              if (protectionError[dependantKey].assets) {
                // loop assets
                protectionError[dependantKey].assets.forEach(
                  (asset, assetIndex) => {
                    const assetError = errorData[assetIndex];

                    Object.keys(assetError).forEach(assetErrorKey => {
                      protectionHasError =
                        assetError[assetErrorKey].hasError ||
                        protectionHasError;
                    });
                  }
                );
              }
            } else {
              protectionHasError = errorData.hasError || protectionHasError;
            }
          });
        });

        updateData[protection] = Object.assign({}, newNaData[protection], {
          isValid: !protectionHasError
        });
        analysisIsCompleted = analysisIsCompleted && !protectionHasError;
      });
      if (newNaData.completedStep === 1 && analysisIsCompleted) {
        newNaData.completedStep = 2;
      }
      if (newNaData.completedStep >= 2 && !analysisIsCompleted) {
        newNaData.completedStep = 1;
      }

      /* update productType isValid */
      let productTypeIsValid = true;
      if (newNaData.productType.lastProdType.replace(/,/g, "") !== "") {
        productTypeIsValid = productTypeIsValid && true;
      }
      if (newNaData.productType.prodType.replace(/,/g, "") !== "") {
        productTypeIsValid = productTypeIsValid && true;
      }
      updateData.productType = Object.assign({}, newNaData.productType, {
        isValid: productTypeIsValid
      });

      return Object.assign({}, newNaData, updateData);
    }
    case ACTION_TYPES[FNA].SAVE_FNA_COMPLETE:
      return naData !== undefined
        ? nadHandler({
            naData,
            pdaData,
            profileData,
            dependantProfilesData,
            feData,
            state
          })
        : state;
    case ACTION_TYPES[FNA].UPDATE_NA_NEEDS: {
      if (value.client === OWNER) {
        return Object.assign({}, state, {
          [value.need]: Object.assign({}, state[value.need], {
            owner: Object.assign({}, state[value.need].owner, {
              isActive: value.isActive,
              cid: value.cid,
              init: false
            })
          })
        });
      } else if (value.client === SPOUSE) {
        return Object.assign({}, state, {
          [value.need]: Object.assign({}, state[value.need], {
            spouse: Object.assign({}, state[value.need].spouse, {
              isActive: value.isActive,
              cid: value.cid,
              init: false
            })
          })
        });
      } else if (value.client === DEPENDANTS) {
        const { dependants } = _.cloneDeep(state[value.need]);

        if (dependants) {
          const dependantIndex = dependants.findIndex(x => x.cid === value.cid);

          if (dependantIndex > -1) {
            dependants[dependantIndex].isActive = value.isActive;
          } else {
            dependants.push({
              cid: value.cid,
              isActive: value.isActive,
              init: false
            });
          }
          return Object.assign({}, state, {
            [value.need]: Object.assign({}, state[value.need], {
              dependants
            })
          });
        }

        return Object.assign({}, state, {
          [value.need]: Object.assign({}, state[value.need], {
            dependants: [
              {
                cid: value.cid,
                isActive: value.isActive,
                init: false
              }
            ]
          })
        });
      }
      return state;
    }

    case ACTION_TYPES[FNA].UPDATE_NA_ASPECTS: {
      const aspects = _.cloneDeep(state.aspects);
      let newAspects;

      let isActive = false;

      if (state[value.need]) {
        traverse(state[value.need]).forEach(x => {
          if (x && x.isActive) {
            isActive = true;
          }
        });
      }

      if (isActive === true) {
        newAspects = addAspect(value.need, aspects);
      } else {
        newAspects = removeAspect(value.need, aspects);
      }

      if (newAspects === "") {
        completedStep = -1;
      } else {
        completedStep = 0;
      }

      return Object.assign({}, state, {
        aspects: newAspects,
        preNeedsProductsList: newAspects,
        completedStep,
        isCompleted: false
      });
    }

    case ACTION_TYPES[FNA].IAR_RATE_NOT_MATCH_REASON_ON_CHANGE:
      return Object.assign({}, state, {
        iarRateNotMatchReason: newIarRateNotMatchReason
      });

    case ACTION_TYPES[FNA].VALIDATE_IAR_RATE_NOT_MATCH_REASON:
      return Object.assign({}, state, {
        isIarRateNotMatchReasonError: newIsIarRateNotMatchReasonError,
        completedStep,
        isCompleted: false
      });

    case ACTION_TYPES[FNA].UPDATE_NA_ANALYSIS: {
      return Object.assign({}, state, value);
    }
    case ACTION_TYPES[FNA].INIT_NA_PRIORITY:
    case ACTION_TYPES[FNA].UPDATE_NA_PRIORITY: {
      if (state.completedStep) {
        completedStep = _.cloneDeep(state.completedStep);
      } else {
        completedStep = 2;
      }

      if (_.isEmpty(value.sfAspects)) {
        completedStep = 2;
      }

      if (
        (completedStep <= 3 && !_.isEmpty(value.sfAspects)) ||
        type === ACTION_TYPES[FNA].UPDATE_NA_PRIORITY
      ) {
        completedStep = 3;
      }

      return Object.assign(
        {},
        state,
        _.has(value, "sfAspects")
          ? {
              sfAspects: value.sfAspects
            }
          : {},
        _.has(value, "spAspects")
          ? {
              spAspects: value.spAspects
            }
          : {},
        {
          completedStep,
          isCompleted:
            type === ACTION_TYPES[FNA].INIT_NA_PRIORITY && state.isCompleted,
          lastStepIndex: 3
        }
      );
    }
    case ACTION_TYPES[FNA].UPDATE_NA_COMPLETED_STEP: {
      if (value === "") {
        completedStep = 3;
      } else {
        completedStep = 4;
      }

      return Object.assign({}, state, {
        completedStep,
        lastStepIndex: 4
      });
    }

    case ACTION_TYPES[FNA].UPDATE_NA_PRODUCT_TYPES: {
      if (value === "") {
        isValid = false;
      } else {
        isValid = true;
      }

      return Object.assign({}, state, {
        productType: {
          isValid,
          lastProdType: _.get(state, "productType.lastProdType", value),
          prodType: value
        }
      });
    }

    case ACTION_TYPES[FNA].UPDATE_LAST_STEP_INDEX:
      return Object.assign({}, state, {
        lastStepIndex: value
      });
    case ACTION_TYPES[FNA].INIT_COMPLETED_STEP:
      return Object.assign({}, state, {
        completedStep,
        lastStepIndex
      });
    case ACTION_TYPES[FNA].UPDATE_COMPLETED_STEP:
      return Object.assign({}, state, {
        completedStep
      });
    case ACTION_TYPES[FNA].UPDATE_IS_COMPLETED:
      return Object.assign({}, state, {
        isCompleted
      });
    case ACTION_TYPES[FNA].UPDATE_NA_ANALYSIS_USERDATA:
      return genNeedsAnalysisNewData(state, value);
    case ACTION_TYPES[FNA].NA_ANALYSIS_USERDATA_VALIDATE:
      return Object.assign({}, state, value);
    case ACTION_TYPES[FNA].UPDATE_NA_ANALYSIS_USERDATA_ASSETS: {
      const productList = [
        FIPROTECTION,
        CIPROTECTION,
        DIPROTECTION,
        PAPROTECTION,
        PCHEADSTART,
        HCPROTECTION,
        RPLANNING,
        EPLANNING,
        PSGOALS,
        OTHER
      ];
      const assetKey = value.targetData.assets[0].key;
      let totalValue = 0;
      const productSet = [];
      for (let i = 0; i < productList.length; i += 1) {
        if (state[productList[i]]) {
          if (state[productList[i]][value.targetProfile]) {
            if (state[productList[i]][value.targetProfile].assets) {
              const targetIndex = state[productList[i]][
                value.targetProfile
              ].assets.findIndex(ass => ass.key === assetKey);

              totalValue += parseInt(
                state[productList[i]][value.targetProfile].assets[targetIndex]
                  .calAsset,
                10
              );
              productSet.push(productList[i]);
            }
          }
        }
      }

      productSet.forEach(product => {
        const targetIndex = state[product][
          value.targetProfile
        ].assets.findIndex(ass => ass.key === assetKey);
        state[product][value.targetProfile].assets[
          targetIndex
        ].totalValue = totalValue;
      });
      return state;
    }
    case ACTION_TYPES[FNA].UPDATE_LAST_SELECTED_PRODUCT: {
      return Object.assign({}, state, {
        lastSelectedProduct: value
      });
    }

    case ACTION_TYPES[FNA].RESET_PRODUCT_TYPE_CKA_RA_IS_VALID: {
      let productTypeIsValid = false;
      if (value !== "") {
        productTypeIsValid = true;
      }

      return Object.assign({}, state, {
        productType: {
          isValid: productTypeIsValid,
          lastProdType: value,
          prodType: value
        },
        ckaSection: Object.assign({}, state.ckaSection, {
          isValid: ckaIsValid
        }),
        raSection: Object.assign({}, state.raSection, {
          isValid: raIsValid
        })
      });
    }
    case ACTION_TYPES[FNA_CKA].UPDATE_CKA_OWNER_INIT: {
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            init: newInit
          })
        })
      });
    }
    case ACTION_TYPES[FNA_CKA].UPDATE_CKA_IS_VALID: {
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          isValid
        })
      });
    }

    case ACTION_TYPES[FNA_CKA].INITIAL_VALIDATE: {
      const isCourseError = validateMandatory({
        field: { type: TEXT_FIELD, mandatory: true, disabled: false },
        value: ckaData.course
      });
      let isInstitutionError = validateMandatory({
        field: { type: TEXT_FIELD, mandatory: true, disabled: false },
        value: ckaData.institution
      });
      let isStudyPeriodEndYearError = validateMandatory({
        field: { type: TEXT_FIELD, mandatory: true, disabled: false },
        value: ckaData.studyPeriodEndYear
      });
      if (birthYear > ckaData.studyPeriodEndYear) {
        isStudyPeriodEndYearError = {
          hasError: true,
          message: {
            [LANGUAGE_TYPES.ENGLISH]:
              TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.306"],
            [LANGUAGE_TYPES.TRADITIONAL_CHINESE]:
              TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.306"]
          }
        };
      }
      if (ckaData.course === "notApplicable" || ckaData.course === "") {
        isInstitutionError = errorState;
        isStudyPeriodEndYearError = errorState;
      }
      const isCollectiveInvestmentError = validateMandatory({
        field: { type: TEXT_FIELD, mandatory: true, disabled: false },
        value: ckaData.collectiveInvestment
      });
      let isTransactionTypeError = validateMandatory({
        field: { type: TEXT_FIELD, mandatory: true, disabled: false },
        value: ckaData.transactionType
      });
      if (
        ckaData.collectiveInvestment === "N" ||
        ckaData.collectiveInvestment === ""
      ) {
        isTransactionTypeError = errorState;
      }
      const isInsuranceInvestmentError = validateMandatory({
        field: { type: TEXT_FIELD, mandatory: true, disabled: false },
        value: ckaData.insuranceInvestment
      });
      let isInsuranceTypeError = validateMandatory({
        field: { type: TEXT_FIELD, mandatory: true, disabled: false },
        value: ckaData.insuranceType
      });
      if (
        ckaData.insuranceInvestment === "N" ||
        ckaData.insuranceInvestment === ""
      ) {
        isInsuranceTypeError = errorState;
      }
      const isProfessionError = validateMandatory({
        field: { type: TEXT_FIELD, mandatory: true, disabled: false },
        value: ckaData.profession
      });
      let isYearsError = validateMandatory({
        field: { type: TEXT_FIELD, mandatory: true, disabled: false },
        value: ckaData.years
      });
      if (ckaData.years < 3) {
        isYearsError = {
          hasError: true,
          message: {
            [LANGUAGE_TYPES.ENGLISH]: TEXT_STORE[LANGUAGE_TYPES.ENGLISH][
              "error.405"
            ].replace("{1}", "3"),
            [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: TEXT_STORE[
              LANGUAGE_TYPES.ENGLISH
            ]["error.405"].replace("{1}", "3")
          }
        };
      }
      if (ckaData.profession === "notApplicable" || ckaData.profession === "") {
        isYearsError = errorState;
      }
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            isCourseError,
            isInstitutionError,
            isStudyPeriodEndYearError,
            isCollectiveInvestmentError,
            isTransactionTypeError,
            isInsuranceInvestmentError,
            isInsuranceTypeError,
            isProfessionError,
            isYearsError
          })
        })
      });
    }

    case ACTION_TYPES[FNA_CKA].COURSE_ON_CHANGE: {
      if (newCourse === "notApplicable" || newCourse === "") {
        return Object.assign({}, state, {
          ckaSection: Object.assign({}, state.ckaSection, {
            owner: Object.assign({}, state.ckaSection.owner, {
              course: newCourse,
              isInstitutionError: errorState,
              isStudyPeriodEndYearError: errorState
            })
          })
        });
      }
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            course: newCourse
          })
        })
      });
    }
    case ACTION_TYPES[FNA_CKA].VALIDATE_COURSE:
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            isCourseError: validateMandatory({
              field: { type: TEXT_FIELD, mandatory: true, disabled: false },
              value: newCourse
            })
          })
        })
      });
    case ACTION_TYPES[FNA_CKA].INSTITUTION_ON_CHANGE:
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            institution: newInstitution
          })
        })
      });
    case ACTION_TYPES[FNA_CKA].VALIDATE_INSTITUTION: {
      let errorResult = errorState;

      if (newInstitution.length > 100) {
        errorResult = {
          hasError: true,
          message: {
            [LANGUAGE_TYPES.ENGLISH]: TEXT_STORE[LANGUAGE_TYPES.ENGLISH][
              "error.406"
            ].replace("{1}", "100"),
            [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: TEXT_STORE[
              LANGUAGE_TYPES.ENGLISH
            ]["error.406"].replace("{1}", "100")
          }
        };
      }

      const checkMandatory = validateMandatory({
        field: { type: TEXT_FIELD, mandatory: true, disabled: false },
        value: newInstitution
      });

      if (checkMandatory.hasError) {
        errorResult = checkMandatory;
      }

      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            isInstitutionError: errorResult
          })
        })
      });
    }
    case ACTION_TYPES[FNA_CKA].STUDYPERIODENDYEAR_ON_CHANGE:
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            studyPeriodEndYear: newStudyPeriodEndYear
          })
        })
      });
    case ACTION_TYPES[FNA_CKA].VALIDATE_STUDYPERIODENDYEAR: {
      let errorResult = errorState;

      if (birthYear > newStudyPeriodEndYear) {
        errorResult = {
          hasError: true,
          message: {
            [LANGUAGE_TYPES.ENGLISH]:
              TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.306"],
            [LANGUAGE_TYPES.TRADITIONAL_CHINESE]:
              TEXT_STORE[LANGUAGE_TYPES.ENGLISH]["error.306"]
          }
        };
      }

      const checkMandatory = validateMandatory({
        field: { type: TEXT_FIELD, mandatory: true, disabled: false },
        value: newStudyPeriodEndYear
      });

      if (checkMandatory.hasError) {
        errorResult = checkMandatory;
      }
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            isStudyPeriodEndYearError: errorResult
          })
        })
      });
    }
    case ACTION_TYPES[FNA_CKA].COLLECTIVEINVESTMENT_ON_CHANGE: {
      if (newCollectiveInvestment === "N" || newCollectiveInvestment === "") {
        return Object.assign({}, state, {
          ckaSection: Object.assign({}, state.ckaSection, {
            owner: Object.assign({}, state.ckaSection.owner, {
              collectiveInvestment: newCollectiveInvestment,
              isTransactionTypeError: errorState
            })
          })
        });
      }
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            collectiveInvestment: newCollectiveInvestment
          })
        })
      });
    }
    case ACTION_TYPES[FNA_CKA].VALIDATE_COLLECTIVEINVESTMENT:
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            isCollectiveInvestmentError: validateMandatory({
              field: { type: TEXT_FIELD, mandatory: true, disabled: false },
              value: newCollectiveInvestment
            })
          })
        })
      });
    case ACTION_TYPES[FNA_CKA].TRANSACTIONTYPE_ON_CHANGE:
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            transactionType: newTransactionType
          })
        })
      });
    case ACTION_TYPES[FNA_CKA].VALIDATE_TRANSACTIONTYPE: {
      let errorResult = errorState;

      if (newTransactionType.length > 100) {
        errorResult = {
          hasError: true,
          message: {
            [LANGUAGE_TYPES.ENGLISH]: TEXT_STORE[LANGUAGE_TYPES.ENGLISH][
              "error.406"
            ].replace("{1}", "100"),
            [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: TEXT_STORE[
              LANGUAGE_TYPES.ENGLISH
            ]["error.406"].replace("{1}", "100")
          }
        };
      }

      const checkMandatory = validateMandatory({
        field: { type: TEXT_FIELD, mandatory: true, disabled: false },
        value: newTransactionType
      });

      if (checkMandatory.hasError) {
        errorResult = checkMandatory;
      }

      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            isTransactionTypeError: errorResult
          })
        })
      });
    }
    case ACTION_TYPES[FNA_CKA].INSURANCEINVESTMENT_ON_CHANGE: {
      if (newInsuranceInvestment === "N" || newInsuranceInvestment === "") {
        return Object.assign({}, state, {
          ckaSection: Object.assign({}, state.ckaSection, {
            owner: Object.assign({}, state.ckaSection.owner, {
              insuranceInvestment: newInsuranceInvestment,
              isInsuranceTypeError: errorState
            })
          })
        });
      }
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            insuranceInvestment: newInsuranceInvestment
          })
        })
      });
    }
    case ACTION_TYPES[FNA_CKA].VALIDATE_INSURANCEINVESTMENT:
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            isInsuranceInvestmentError: validateMandatory({
              field: { type: TEXT_FIELD, mandatory: true, disabled: false },
              value: newInsuranceInvestment
            })
          })
        })
      });
    case ACTION_TYPES[FNA_CKA].INSURANCETYPE_ON_CHANGE:
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            insuranceType: newInsuranceType
          })
        })
      });
    case ACTION_TYPES[FNA_CKA].VALIDATE_INSURANCETYPE: {
      let errorResult = errorState;

      if (newInsuranceType.length > 100) {
        errorResult = {
          hasError: true,
          message: {
            [LANGUAGE_TYPES.ENGLISH]: TEXT_STORE[LANGUAGE_TYPES.ENGLISH][
              "error.406"
            ].replace("{1}", "100"),
            [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: TEXT_STORE[
              LANGUAGE_TYPES.ENGLISH
            ]["error.406"].replace("{1}", "100")
          }
        };
      }

      const checkMandatory = validateMandatory({
        field: { type: TEXT_FIELD, mandatory: true, disabled: false },
        value: newInsuranceType
      });

      if (checkMandatory.hasError) {
        errorResult = checkMandatory;
      }
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            isInsuranceTypeError: errorResult
          })
        })
      });
    }
    case ACTION_TYPES[FNA_CKA].PROFESSION_ON_CHANGE: {
      if (newProfession === "notApplicable" || newProfession === "") {
        return Object.assign({}, state, {
          ckaSection: Object.assign({}, state.ckaSection, {
            owner: Object.assign({}, state.ckaSection.owner, {
              profession: newProfession,
              isYearsError: errorState
            })
          })
        });
      }
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            profession: newProfession
          })
        })
      });
    }
    case ACTION_TYPES[FNA_CKA].VALIDATE_PROFESSION:
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            isProfessionError: validateMandatory({
              field: { type: TEXT_FIELD, mandatory: true, disabled: false },
              value: newProfession
            })
          })
        })
      });
    case ACTION_TYPES[FNA_CKA].YEARS_ON_CHANGE: {
      let result;
      const numberValue = parseInt(newYears, 10);
      if (!Number.isNaN(numberValue) && numberValue >= 0) {
        result = numberValue;
      } else {
        result = "";
      }
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            years: result
          })
        })
      });
    }
    case ACTION_TYPES[FNA_CKA].YEARS_ON_BLUR: {
      if (parseInt(newYears, 10) < 3) {
        return Object.assign({}, state, {
          ckaSection: Object.assign({}, state.ckaSection, {
            owner: Object.assign({}, state.ckaSection.owner, {
              years: 3,
              isYearsError: {
                hasError: false,
                message: ""
              }
            })
          })
        });
      }
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            years: parseInt(newYears, 10)
          })
        })
      });
    }
    case ACTION_TYPES[FNA_CKA].VALIDATE_YEARS: {
      let errorResult = errorState;

      if (newYears < 3) {
        errorResult = {
          hasError: true,
          message: {
            [LANGUAGE_TYPES.ENGLISH]: TEXT_STORE[LANGUAGE_TYPES.ENGLISH][
              "error.405"
            ].replace("{1}", "3"),
            [LANGUAGE_TYPES.TRADITIONAL_CHINESE]: TEXT_STORE[
              LANGUAGE_TYPES.ENGLISH
            ]["error.405"].replace("{1}", "3")
          }
        };
      }

      const checkMandatory = validateMandatory({
        field: { type: TEXT_FIELD, mandatory: true, disabled: false },
        value: newYears
      });

      if (checkMandatory.hasError) {
        errorResult = checkMandatory;
      }
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            isYearsError: errorResult
          })
        })
      });
    }

    case ACTION_TYPES[FNA_CKA].UPDATE_PASS_CKA: {
      return Object.assign({}, state, {
        ckaSection: Object.assign({}, state.ckaSection, {
          owner: Object.assign({}, state.ckaSection.owner, {
            passCka: newPassCka
          })
        })
      });
    }

    case ACTION_TYPES[FNA_RA].RISKPOTENTIALRETURN_ON_CHANGE:
      return Object.assign({}, state, {
        raSection: Object.assign({}, state.raSection, {
          owner: Object.assign({}, state.raSection.owner, {
            riskPotentialReturn: newRiskPotentialReturn
          })
        })
      });
    case ACTION_TYPES[FNA_RA].AVGAGRETURN_ON_CHANGE:
      return Object.assign({}, state, {
        raSection: Object.assign({}, state.raSection, {
          owner: Object.assign({}, state.raSection.owner, {
            avgAGReturn: newAvgAGReturn
          })
        })
      });
    case ACTION_TYPES[FNA_RA].SMDROPED_ON_CHANGE:
      return Object.assign({}, state, {
        raSection: Object.assign({}, state.raSection, {
          owner: Object.assign({}, state.raSection.owner, {
            smDroped: newSmDroped
          })
        })
      });
    case ACTION_TYPES[FNA_RA].ALOFLOSSES_ON_CHANGE:
      return Object.assign({}, state, {
        raSection: Object.assign({}, state.raSection, {
          owner: Object.assign({}, state.raSection.owner, {
            alofLosses: newAlofLosses
          })
        })
      });
    case ACTION_TYPES[FNA_RA].EXPINVTIME_ON_CHANGE:
      return Object.assign({}, state, {
        raSection: Object.assign({}, state.raSection, {
          owner: Object.assign({}, state.raSection.owner, {
            expInvTime: newExpInvTime
          })
        })
      });
    case ACTION_TYPES[FNA_RA].INVPREF_ON_CHANGE:
      return Object.assign({}, state, {
        raSection: Object.assign({}, state.raSection, {
          owner: Object.assign({}, state.raSection.owner, {
            invPref: newInvPref
          })
        })
      });
    case ACTION_TYPES[FNA_RA].SELF_SELECTED_RISK_LEVEL_ON_CHANGE:
      return Object.assign({}, state, {
        raSection: Object.assign({}, state.raSection, {
          owner: Object.assign({}, state.raSection.owner, {
            selfSelectedriskLevel: newSelfSelectedRiskLevel
          })
        })
      });
    case ACTION_TYPES[FNA_RA].SELF_RL_REASON_RP_ON_CHANGE:
      return Object.assign({}, state, {
        raSection: Object.assign({}, state.raSection, {
          owner: Object.assign({}, state.raSection.owner, {
            selfRLReasonRP: newSelfRLReasonRP
          })
        })
      });
    case ACTION_TYPES[FNA_RA].UPDATE_RA_IS_VALID: {
      return Object.assign({}, state, {
        raSection: Object.assign({}, state.raSection, {
          isValid
        })
      });
    }
    case ACTION_TYPES[FNA_RA].UPDATE_RA_OWNER_INIT: {
      return Object.assign({}, state, {
        raSection: Object.assign({}, state.raSection, {
          owner: Object.assign({}, state.raSection.owner, {
            init: newInit
          })
        })
      });
    }
    case ACTION_TYPES[FNA_RA].UPDATE_ASSESSED_RL:
      return Object.assign({}, state, {
        raSection: Object.assign({}, state.raSection, {
          owner: Object.assign({}, state.raSection.owner, {
            assessedRL: newAssessedRL
          })
        })
      });

    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return naState;
    default:
      return state;
  }
};

const isNaError = (
  state = isNaErrorState,
  { type, naData, profileData, dependantProfilesData, feData }
) => {
  switch (type) {
    case ACTION_TYPES[FNA].VALIDATE_NA: {
      const spouseKey = Object.keys(dependantProfilesData).find(
        key => dependantProfilesData[key].relationship === "SPO"
      );
      const spouseProfile = dependantProfilesData[spouseKey];

      // validate fiProtection
      const fiProtection = {
        owner: validateFiProtection({
          fiProtectionData: naData.fiProtection.owner,
          dependantProfileData: profileData,
          na: naData,
          fe: feData,
          selectedProfile: "owner"
        })
      };
      if (naData.fiProtection.spouse) {
        fiProtection.spouse = validateFiProtection({
          fiProtectionData: naData.fiProtection.spouse,
          dependantProfileData: spouseProfile,
          na: naData,
          fe: feData,
          selectedProfile: "spouse"
        });
      }

      // validate diProtection
      const diProtection = {
        owner: validateDiProtection(naData.diProtection.owner)
      };
      if (naData.diProtection.spouse) {
        diProtection.spouse = validateDiProtection(naData.diProtection.spouse);
      }
      if (naData.diProtection.dependants) {
        naData.diProtection.dependants.forEach(dependant => {
          diProtection[dependant.cid] = validateDiProtection(dependant);
        });
      }

      // validate ciProtection
      const ciProtection = {
        owner: validateCiProtection({
          ciProtectionData: naData.ciProtection.owner,
          profileData,
          na: naData,
          fe: feData,
          selectedProfile: "owner"
        })
      };
      if (naData.ciProtection.spouse) {
        ciProtection.spouse = validateCiProtection({
          ciProtectionData: naData.ciProtection.spouse,
          profileData,
          na: naData,
          fe: feData,
          selectedProfile: "spouse"
        });
      }

      // validate rPlanning
      const rPlanning = {
        owner: validateRPlanning({
          rPlanningData: naData.rPlanning.owner,
          dependantProfileData: profileData,
          na: naData,
          fe: feData,
          selectedProfile: "owner"
        })
      };
      if (naData.rPlanning.spouse) {
        rPlanning.spouse = validateRPlanning({
          rPlanningData: naData.rPlanning.spouse,
          dependantProfileData: spouseProfile,
          na: naData,
          fe: feData,
          selectedProfile: "spouse"
        });
      }

      // validate pcHeadstart
      const pcHeadstart = {};
      if (naData.pcHeadstart.dependants) {
        naData.pcHeadstart.dependants.forEach(dependant => {
          pcHeadstart[dependant.cid] = validatePcHeadstart(dependant);
        });
      }

      // validate paProtection
      const paProtection = {
        owner: naData.paProtection.owner
          ? validatePaProtection(naData.paProtection.owner)
          : {},
        spouse: naData.fiProtection.spouse
          ? validatePaProtection(naData.paProtection.spouse)
          : {}
      };
      if (naData.paProtection.spouse) {
        paProtection.spouse = validatePaProtection(naData.paProtection.spouse);
      }
      if (naData.paProtection.dependants) {
        naData.paProtection.dependants.forEach(dependant => {
          paProtection[dependant.cid] = validatePaProtection(dependant);
        });
      }

      // validate hcProtection
      const hcProtection = {
        owner: validateHcProtection(naData.hcProtection.owner)
      };
      if (naData.hcProtection.spouse) {
        hcProtection.spouse = validateHcProtection(naData.hcProtection.spouse);
      }
      if (naData.hcProtection.dependants) {
        naData.hcProtection.dependants.forEach(dependant => {
          hcProtection[dependant.cid] = validateHcProtection(dependant);
        });
      }

      // validate ePlanning
      const ePlanning = {};
      if (naData.ePlanning.dependants) {
        naData.ePlanning.dependants.forEach(dependant => {
          ePlanning[dependant.cid] = validateEPlanning({
            ePlanningData: dependant,
            na: naData,
            fe: feData
          });
        });
      }

      // validate other
      const other = {
        owner: validateOther(naData.other.owner)
      };
      if (naData.other.spouse) {
        other.spouse = validateOther(naData.other.spouse);
      }

      // validate psGoals
      const psGoals = {
        owner: validatePsGoals({
          psGoalsData: naData.psGoals.owner,
          na: naData,
          fe: feData,
          selectedProfile: "owner"
        })
      };
      if (naData.psGoals.spouse) {
        psGoals.spouse = validatePsGoals({
          psGoalsData: naData.psGoals.spouse,
          na: naData,
          fe: feData,
          selectedProfile: "spouse"
        });
      }

      return {
        aspects: {},
        roi: {},
        diProtection,
        ciProtection,
        ePlanning,
        fiProtection,
        hcProtection,
        other,
        paProtection,
        pcHeadstart,
        psGoals,
        rPlanning,
        sfAspects: {},
        spAspects: {},
        productType: {},
        ckaSection: {},
        raSection: {}
      };
    }
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return isNaErrorState;
    default:
      return state;
  }
};
/**
 * @description financial evaluation section state
 * */
const fe = (
  state = {},
  { type, feData, profileData, pdaData, dependantProfilesData }
) => {
  switch (type) {
    case ACTION_TYPES[FNA].UPDATE_FNA:
    case ACTION_TYPES[FNA].UPDATE_FE:
      return feHandler({ feData, profileData, pdaData, dependantProfilesData });
    case ACTION_TYPES[FNA].SAVE_FNA_COMPLETE:
      return feData !== undefined
        ? feHandler({ feData, profileData, pdaData, dependantProfilesData })
        : state;
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return {};
    default:
      return state;
  }
};
/**
 * @description fe validation
 * */
const feErrors = (
  state = {},
  { type, pdaData, feData, profileData, dependantProfilesData }
) => {
  switch (type) {
    case ACTION_TYPES[FNA].VALIDATE_FE: {
      // get all validation target
      const validationTargets = [];
      if (feData.owner && feData.owner.cid === profileData.cid) {
        validationTargets.push({
          data: feData.owner,
          relationship: "owner"
        });
      }
      if (pdaData.applicant === "joint" && feData.spouse) {
        validationTargets.push({
          data: feData.spouse,
          relationship: "spouse"
        });
      }
      if (
        pdaData.dependants !== "" &&
        feData.dependants &&
        feData.dependants.length > 0
      ) {
        pdaData.dependants.split(",").forEach(dependantCid => {
          if (
            dependantProfilesData[dependantCid] &&
            (dependantProfilesData[dependantCid].relationship === "SON" ||
              dependantProfilesData[dependantCid].relationship === "DAU")
          ) {
            const dependantIndex = _.findIndex(
              feData.dependants,
              dependant => dependant.cid === dependantCid
            );

            validationTargets.push({
              data: feData.dependants[dependantIndex],
              relationship: "dependant"
            });
          }
        });
      }

      // validate all targets
      const feErrorsObject = {};
      validationTargets.forEach(({ data, relationship }) => {
        let errorObject = {
          isFinish:
            data.init !== false
              ? {
                  hasError: true,
                  message: ""
                }
              : {
                  hasError: false,
                  message: ""
                }
        };

        switch (relationship) {
          case "owner": {
            errorObject = Object.assign(
              errorObject,
              validateNetWorth({ data, relationship }),
              validateEIP({ data, relationship }),
              validateCashFlow({ data, relationship }),
              validateBudgetAndForceIncome(data)
            );
            break;
          }
          case "spouse": {
            errorObject = Object.assign(
              errorObject,
              validateNetWorth({ data, relationship }),
              validateEIP({ data, relationship }),
              validateCashFlow({ data, relationship })
            );
            break;
          }
          case "dependant": {
            errorObject = Object.assign(
              errorObject,
              validateEIP({ data, relationship })
            );
            break;
          }
          default:
            throw new Error(
              `reducer "fna.feErrors" error: unexpected relationship "${relationship}"`
            );
        }

        feErrorsObject[data.cid] = errorObject;
      });
      return feErrorsObject;
    }
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return {};
    default:
      return state;
  }
};

/**
 * @description this reducer is use to save fna report PDF data. It should be
 * noted that we have already add a file header to the base64 data.
 * */
const FNAReport = (state = "", { type, FNAReportData }) => {
  switch (type) {
    case ACTION_TYPES[FNA].SAVE_FNA_REPORT:
      return `data:application/pdf;base64,${FNAReportData}`;
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return "";
    default:
      return state;
  }
};

/**
 * @description this reducer is use to store the FNA Report Email
 * */

const FNAReportEmail = (
  state = {},
  { type, emails, tab, report, isSelected, option, value }
) => {
  switch (type) {
    case ACTION_TYPES[FNA].OPEN_EMAIL:
      return Object.assign({}, state, {
        emails,
        reportOptions: {
          agent: { fnaReport: true },
          client: { fnaReport: true }
        },
        tab: "agent"
      });
    case ACTION_TYPES[FNA].REPORT_ON_CHANGE:
      return Object.assign({}, state, {
        reportOptions: Object.assign({}, state.reportOptions, {
          [tab]: Object.assign({}, state.reportOptions[tab], {
            [report]: !isSelected
          })
        })
      });
    case ACTION_TYPES[FNA].TAB_ON_CHANGE:
      return Object.assign({}, state, {
        tab: option
      });
    case ACTION_TYPES[FNA].UPDATE_SKIP_EMAIL_IN_REPORT:
      return Object.assign({}, state, {
        skipEmailInReport: value
      });
    default:
      return state;
  }
};

/**
 * @description Needs Summary state
 */
const needsSummary = (state = "", { type, needsSummaryData }) => {
  switch (type) {
    case ACTION_TYPES[FNA].UPDATE_FNA:
      return needsSummaryData;
    default:
      return state;
  }
};

/**
 * currentPage
 * @description clientForm currentPage.
 * @default ""
 * */
const currentPage = (state = "", { type, newPage }) => {
  switch (type) {
    case ACTION_TYPES[FNA].UPDATE_CURRENT_PAGE:
      return newPage;
    default:
      return state;
  }
};

const naIsChanged = (state = false, { type, newNaIsChanged }) => {
  switch (type) {
    case ACTION_TYPES[FNA].UPDATE_NA_IS_CHANGED:
      return newNaIsChanged;
    case ACTION_TYPES[FNA].UPDATE_NA_NEEDS:
    case ACTION_TYPES[FNA].UPDATE_NA_ASPECTS:
    case ACTION_TYPES[FNA].UPDATE_NA_PRIORITY:
    case ACTION_TYPES[FNA].UPDATE_NA_PRODUCT_TYPES_COMPLETED_STEP:
    case ACTION_TYPES[FNA_RA].UPDATE_ASSESSED_RL:
    case ACTION_TYPES[FNA_RA].SELF_SELECTED_RISK_LEVEL_ON_CHANGE:
    case ACTION_TYPES[FNA_RA].SELF_RL_REASON_RP_ON_CHANGE:
      return true;
    case ACTION_TYPES[FNA].UPDATE_FNA:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return false;
    default:
      return state;
  }
};

const naIsGloballyChanged = (
  state = false,
  { type, newNaIsGloballyChanged }
) => {
  switch (type) {
    case ACTION_TYPES[FNA].UPDATE_NA_IS_GLOBALLY_CHANGED:
      return newNaIsGloballyChanged;
    case ACTION_TYPES[FNA].UPDATE_NA_NEEDS:
    case ACTION_TYPES[FNA].UPDATE_NA_ASPECTS:
    case ACTION_TYPES[FNA].UPDATE_NA_PRIORITY:
    case ACTION_TYPES[FNA].UPDATE_NA_PRODUCT_TYPES_COMPLETED_STEP:
    case ACTION_TYPES[FNA_RA].UPDATE_ASSESSED_RL:
    case ACTION_TYPES[FNA_RA].SELF_SELECTED_RISK_LEVEL_ON_CHANGE:
    case ACTION_TYPES[FNA_RA].SELF_RL_REASON_RP_ON_CHANGE:
      return true;
    case ACTION_TYPES[FNA].UPDATE_FNA:
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return false;
    default:
      return state;
  }
};

const isFnaInvalid = (state = false, { type, newIsFnaInvalid }) => {
  switch (type) {
    case ACTION_TYPES[FNA].UPDATE_IS_FNA_INVALID:
      return newIsFnaInvalid || false;
    case ACTION_TYPES[FNA].SAVE_FNA_COMPLETE:
      return newIsFnaInvalid !== undefined ? newIsFnaInvalid : state;
    case ACTION_TYPES.root.CLEAN_CLIENT_DATA:
      return false;
    default:
      return state;
  }
};

const fnaPage = (state = "", { type, newPage }) => {
  switch (type) {
    case ACTION_TYPES[FNA].REDIRECT_TO:
      return newPage;
    default:
      return state;
  }
};

export default combineReducers({
  pda,
  na,
  fe,
  feErrors,
  isNaError,
  FNAReport,
  FNAReportEmail,
  needsSummary,
  trustedIndividual,
  currentPage,
  naIsChanged,
  naIsGloballyChanged,
  isFnaInvalid,
  fnaPage
});
