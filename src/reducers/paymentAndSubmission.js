import { combineReducers } from "redux";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import {
  APPLICATION,
  PAYMENT_AND_SUBMISSION
} from "../constants/REDUCER_TYPES";

/**
 * paymentAndSubmissionData
 * @description affect product list filter
 * @default ALL
 * */
const payment = (state = {}, { type, newPayment }) => {
  switch (type) {
    case ACTION_TYPES[PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_AND_SUBMISSION:
      if (newPayment) {
        return Object.assign({}, newPayment);
      }
      return state;
    case ACTION_TYPES[APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA:
      return {};
    default:
      return state;
  }
};

const submission = (state = {}, { type, newSubmission }) => {
  switch (type) {
    case ACTION_TYPES[PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_AND_SUBMISSION:
      if (newSubmission) {
        return Object.assign({}, newSubmission);
      }
      return state;
    case ACTION_TYPES[APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA:
      return {};
    default:
      return state;
  }
};

const error = (state = {}, { type, errorObj }) => {
  switch (type) {
    case ACTION_TYPES[PAYMENT_AND_SUBMISSION].VALIDATE_PAYMENT_AND_SUBMISSION:
      return Object.assign({}, errorObj);
    case ACTION_TYPES[APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA:
      return {};
    default:
      return state;
  }
};

// this store is for shield use only
const template = (state = {}, { type, newTemplate }) => {
  switch (type) {
    case ACTION_TYPES[PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_AND_SUBMISSION:
      if (newTemplate) {
        return Object.assign({}, newTemplate);
      }
      return state;
    case ACTION_TYPES[APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA:
      return {};
    default:
      return state;
  }
};
/**
 * @description this reducer is for iOS only
 * */
const onlinePaymentInitialState = {
  trxStatusRemark: "",
  trxTime: NaN,
  trxNo: "",
  trxStatus: "",
  trxStartTime: NaN,
  trxMethod: "",
  initPayMethod: ""
};
const onlinePaymentStatus = (
  state = onlinePaymentInitialState,
  { type, newStatus }
) => {
  switch (type) {
    case ACTION_TYPES[PAYMENT_AND_SUBMISSION].UPDATE_PAYMENT_STATUS:
      return {
        trxStatusRemark: newStatus.trxStatusRemark,
        trxTime: newStatus.trxTime,
        trxNo: newStatus.trxNo,
        trxStatus: newStatus.trxStatus,
        trxStartTime: newStatus.trxStartTime,
        trxMethod: newStatus.trxMethod,
        initPayMethod: newStatus.initPayMethod
      };
    case ACTION_TYPES[APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA:
      return onlinePaymentInitialState;
    default:
      return state;
  }
};

export default combineReducers({
  payment,
  submission,
  error,
  template,
  onlinePaymentStatus
});
