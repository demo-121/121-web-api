import { combineReducers } from "redux";
import ACTION_TYPES from "../constants/ACTION_TYPES";
import { APPLICATION, SIGNATURE } from "../constants/REDUCER_TYPES";

const initStateSignature = {
  isFaChannel: false,
  selectedPdfIdx: 0,
  signingTabIdx: -1,
  isSigningProcess: [],
  numOfTabs: 0,
  attachments: [],
  attUrls: [],
  agentSignFields: [],
  clientSignFields: []
};

// TODO update commment
/**
 * signature
 * @description signature data
 * @default {}
 * */
const signature = (
  state = initStateSignature,
  { type, newSignature, newSelectedPdfIdx, newSigningTabIdx, newAttachments }
) => {
  switch (type) {
    case ACTION_TYPES[SIGNATURE].UPDATE_SIGNATURE:
      return Object.assign({}, state, newSignature);
    case ACTION_TYPES[SIGNATURE].SIGNATURE_SWITCH_PDF:
      return Object.assign({}, state, { selectedPdfIdx: newSelectedPdfIdx });
    case ACTION_TYPES[SIGNATURE].UPDATE_SIGNED_PDF:
      return Object.assign({}, state, {
        signingTabIdx: newSigningTabIdx,
        attachments: newAttachments
      });
    case ACTION_TYPES[APPLICATION].UPDATE_APPLICATION_BACKDATE:
      return Object.assign({}, state, {
        attachments: newAttachments
      });
    case ACTION_TYPES[APPLICATION].CLEAR_STORE_SELECTED_APPLICATION_DATA:
      return initStateSignature;
    default:
      return state;
  }
};

export default combineReducers({
  signature
});
