import * as clientActions from "./actions/client";
import * as clientFormActions from "./actions/clientForm";
import * as fnaActions from "./actions/fna";
import * as preApplicationActions from "./actions/preApplication";
import * as productsActions from "./actions/products";
import * as quotationActions from "./actions/quotation";
import * as recommendationActions from "./actions/recommendation";
import * as applicationActions from "./actions/application";
import * as TIME_FORMAT from "./constants/TIME_FORMAT";
import * as root from "./actions/root";
import ACTION_TYPES from "./constants/ACTION_TYPES";
import * as FIELD_TYPES from "./constants/FIELD_TYPES";
import FILTER_TYPES from "./constants/FILTER_TYPES";
import PAYMENT_BANK from "./constants/PAYMENT_BANK";
import SYSTEM from "./constants/SYSTEM";
import PROGRESS_TABS from "./constants/PROGRESS_TABS";
import * as NUMBER_FORMAT from "./constants/NUMBER_FORMAT";
import SECTION_KEYS from "./constants/SECTION_KEYS";
import * as clientTypeCheck from "./constants/REDUCER_TYPE_CHECK/client";
import * as clientFormTypeCheck from "./constants/REDUCER_TYPE_CHECK/clientForm";
import * as commonTypeCheck from "./constants/REDUCER_TYPE_CHECK/common";
import optionsMapTypeCheck from "./constants/REDUCER_TYPE_CHECK/optionsMap";
import * as preApplicationTypeCheck from "./constants/REDUCER_TYPE_CHECK/preApplication";
import * as applicationTypeCheck from "./constants/REDUCER_TYPE_CHECK/application";
import * as supportingDocumentTypeCheck from "./constants/REDUCER_TYPE_CHECK/supportingDocument";
import * as productsTypeCheck from "./constants/REDUCER_TYPE_CHECK/products";
import * as proposalTypeCheck from "./constants/REDUCER_TYPE_CHECK/proposal";
import * as quotationTypeCheck from "./constants/REDUCER_TYPE_CHECK/quotation";
import * as recommendationTypeCheck from "./constants/REDUCER_TYPE_CHECK/recommendation";
import * as paymentAndSubmissionTypeCheck from "./constants/REDUCER_TYPE_CHECK/paymentAndSubmission";
import * as signatureTypeCheck from "./constants/REDUCER_TYPE_CHECK/signature";
import * as DOCUMENT_TYPES from "./constants/DOCUMENT_TYPES";
import * as DIALOG_TYPES from "./constants/DIALOG_TYPES";
import * as PRODUCT_TYPES from "./constants/PRODUCT_TYPES";
import * as REDUCER_TYPES from "./constants/REDUCER_TYPES";
import * as configTypeCheck from "./constants/REDUCER_TYPE_CHECK/config";
import * as fnaTypeCheck from "./constants/REDUCER_TYPE_CHECK/fna";
import * as TOLERANCE_LEVEL from "./constants/TOLERANCE_LEVEL";
import * as DEPENDANT from "./constants/DEPENDANT";
import * as NEEDS from "./constants/NEEDS";
import * as USER_TAB_TYPES from "./constants/USER_TAB_TYPES";
import * as ROP_TABLE from "./constants/ROP_TABLE";
import * as DYNAMIC_VIEWS from "./constants/DYNAMIC_VIEWS";
import EAPP from "./constants/EAPP";
import TEXT_STORE, { LANGUAGE_TYPES } from "./locales";
import client from "./reducers/client";
import clientForm from "./reducers/clientForm";
import fna from "./reducers/fna";
import optionsMap from "./reducers/optionsMap";
import preApplication from "./reducers/preApplication";
import products from "./reducers/products";
import proposal from "./reducers/proposal";
import quotation from "./reducers/quotation";
import recommendation from "./reducers/recommendation";
import agent from "./reducers/agent";
import agentUX from "./reducers/agentUX";
import application from "./reducers/application";
import supportingDocument from "./reducers/supportingDocument";
import paymentAndSubmission from "./reducers/paymentAndSubmission";
import signature from "./reducers/signature";
// TODO: will be remove fnaRA in the future version
// import fnaRA from "./reducers/fnaRA";
import saga from "./saga";
import * as common from "./utilities/common";
import * as applicationUtil from "./utilities/application";
import fundChecker from "./utilities/fundChecker";
import getProductId from "./utilities/getProductId";
import * as trigger from "./utilities/trigger";
import * as validation from "./utilities/validation";
import * as dataMapping from "./utilities/dataMapping";
import * as numberFormatter from "./utilities/numberFormatter";
import * as naAnalysis from "./utilities/naAnalysis";
import * as needsUtil from "./utilities/needsUtil";
import * as preApplicationUtil from "./utilities/preApplication";
import * as dynamicApplicationForm from "./utilities/dynamicApplicationForm";
import genProfileMandatory from "./utilities/genProfileMandatory";
import searchClient from "./utilities/searchClient";

export {
  TIME_FORMAT,
  REDUCER_TYPES,
  ACTION_TYPES,
  FILTER_TYPES,
  FIELD_TYPES,
  DOCUMENT_TYPES,
  DIALOG_TYPES,
  PRODUCT_TYPES,
  SECTION_KEYS,
  TOLERANCE_LEVEL,
  DEPENDANT,
  NEEDS,
  SYSTEM,
  PROGRESS_TABS,
  NUMBER_FORMAT,
  saga,
  TEXT_STORE,
  LANGUAGE_TYPES,
  USER_TAB_TYPES,
  ROP_TABLE,
  DYNAMIC_VIEWS,
  EAPP,
  PAYMENT_BANK
};

export const ACTION_LIST = {
  root,
  [REDUCER_TYPES.CLIENT_FORM]: clientFormActions,
  [REDUCER_TYPES.PRE_APPLICATION]: preApplicationActions,
  [REDUCER_TYPES.RECOMMENDATION]: recommendationActions,
  [REDUCER_TYPES.QUOTATION]: quotationActions,
  [REDUCER_TYPES.FNA]: fnaActions,
  [REDUCER_TYPES.PRODUCTS]: productsActions,
  [REDUCER_TYPES.CLIENT]: clientActions,
  [REDUCER_TYPES.APPLICATION]: applicationActions
};

export const reducers = {
  [REDUCER_TYPES.AGENT]: agent,
  [REDUCER_TYPES.AGENTUX]: agentUX,
  [REDUCER_TYPES.CLIENT]: client,
  [REDUCER_TYPES.CLIENT_FORM]: clientForm,
  [REDUCER_TYPES.FNA]: fna,
  // TODO: will be remove fnaRA in the future version
  // [REDUCER_TYPES.FNA_RA]: fnaRA,
  [REDUCER_TYPES.PROPOSAL]: proposal,
  [REDUCER_TYPES.QUOTATION]: quotation,
  [REDUCER_TYPES.RECOMMENDATION]: recommendation,
  [REDUCER_TYPES.PRE_APPLICATION]: preApplication,
  [REDUCER_TYPES.PRODUCTS]: products,
  [REDUCER_TYPES.OPTIONS_MAP]: optionsMap,
  [REDUCER_TYPES.APPLICATION]: application,
  [REDUCER_TYPES.SUPPORTING_DOCUMENT]: supportingDocument,
  [REDUCER_TYPES.PAYMENT_AND_SUBMISSION]: paymentAndSubmission,
  [REDUCER_TYPES.SIGNATURE]: signature
};

export const REDUCER_TYPE_CHECK = {
  [REDUCER_TYPES.CLIENT]: clientTypeCheck,
  [REDUCER_TYPES.PROPOSAL]: proposalTypeCheck,
  [REDUCER_TYPES.QUOTATION]: quotationTypeCheck,
  [REDUCER_TYPES.PRE_APPLICATION]: preApplicationTypeCheck,
  [REDUCER_TYPES.APPLICATION]: applicationTypeCheck,
  [REDUCER_TYPES.RECOMMENDATION]: recommendationTypeCheck,
  [REDUCER_TYPES.PRODUCTS]: productsTypeCheck,
  [REDUCER_TYPES.OPTIONS_MAP]: optionsMapTypeCheck,
  [REDUCER_TYPES.CLIENT_FORM]: clientFormTypeCheck,
  [REDUCER_TYPES.CONFIG]: configTypeCheck,
  [REDUCER_TYPES.FNA]: fnaTypeCheck,
  [REDUCER_TYPES.COMMON]: commonTypeCheck,
  [REDUCER_TYPES.SUPPORTING_DOCUMENT]: supportingDocumentTypeCheck,
  [REDUCER_TYPES.PAYMENT_AND_SUBMISSION]: paymentAndSubmissionTypeCheck,
  [REDUCER_TYPES.SIGNATURE]: signatureTypeCheck
};

export const utilities = {
  common,
  preApplication: preApplicationUtil,
  application: applicationUtil,
  validation,
  getProductId,
  trigger,
  dataMapping,
  numberFormatter,
  naAnalysis,
  needsUtil,
  genProfileMandatory,
  searchClient,
  fundChecker,
  dynamicApplicationForm
};
