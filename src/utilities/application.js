import * as _ from "lodash";
import * as validation from "../utilities/validation";
import { APPLICATION } from "../constants/REDUCER_TYPES";
import { TEXT_FIELD, TEXT_BOX } from "../constants/FIELD_TYPES";
import SECTION_KEYS from "../constants/SECTION_KEYS";

const {
  PERSONAL_DETAILS,
  RESIDENCY,
  INSURABILITY,
  PLAN_DETAILS,
  DECLARATION
} = SECTION_KEYS[APPLICATION];

const sectionMapping = {
  [PERSONAL_DETAILS]: ["personalInfo"],
  [RESIDENCY]: ["residency", "foreigner", "policies"],
  [INSURABILITY]: ["insurability"],
  [DECLARATION]: ["declaration"]
};

function checkBranchInfo({ profile, field, errorObj }) {
  if (profile.branchInfo) {
    field.forEach(fieldName => {
      if (
        !profile.branchInfo[fieldName] ||
        profile.branchInfo[fieldName] === ""
      ) {
        _.set(
          errorObj.application,
          `proposer.personalInfo.${fieldName}`,
          validation.validateMandatory({
            field: {
              type: TEXT_BOX,
              mandatory: true,
              disabled: false
            },
            value: profile.branchInfo[fieldName]
          })
        );
      }
    });
  } else {
    field.forEach(fieldName => {
      _.set(
        errorObj.application,
        `proposer.personalInfo.${fieldName}`,
        validation.validateMandatory({
          field: {
            type: TEXT_BOX,
            mandatory: true,
            disabled: false
          },
          value: null
        })
      );
    });
  }
}

export const checkHasInsurability = ({ application, template }) => {
  const isShield = application.quotation.quotType === "SHIELD";

  /**
   * template -> items[0] (section "Application")
   * -> items[0] (menu in left side)
   * -> items[0] (1st section in three sections)
   * */
  const menuTemplate =
    // Shield case & non-Shield case
    isShield
      ? _.get(template, "items[0]")
      : _.get(template, "items[0].items[0]");
  const menuSections = _.get(menuTemplate, `items[0].items`);
  return (
    _.findIndex(menuSections, section => section.key === INSURABILITY) >= 0
  );
};

export const checkHasPolicyNumber = ({ application, error }) => {
  let policyNumberExist = true;
  if (application.quotation.quotType === "SHIELD") {
    Object.values(application.iCidMapping).forEach(value =>
      value.forEach(insured => {
        if (!insured.policyNumber) {
          policyNumberExist = false;
        }
      })
    );
  } else if (!application.policyNumber) {
    policyNumberExist = false;
  }
  if (error) {
    _.set(error, "applicationForm.policyNumberExist", policyNumberExist);
  }
  return policyNumberExist;
};

/**
 * initApplicationFormPersonalDetailsError
 * @description Only trigger once after enters the Application Form Page
 * @param {object} errorObj - error object from reducer store application.error
 * @param {object} profile - the real user data from reducer store application.application
 * @param {object} targetProfileName - only to init the object fields name
 * */
function initApplicationFormPersonalDetailsError({
  errorObj,
  profile,
  targetProfileName,
  isShield
}) {
  let initFields = {
    personalInfo: [
      "organization",
      "organizationCountry",
      "postalCode",
      "addrBlock",
      "addrStreet",
      "isSameAddr",
      "mAddrCountry",
      "mPostalCode",
      "mAddrBlock",
      "mAddrStreet",
      "branchInfo"
    ]
  };

  if (isShield && targetProfileName.includes("insured")) {
    initFields = {
      personalInfo: ["organization", "branchInfo"]
    };
  }

  let fieldTriggerList = {
    mAddrCountry: "isSameAddr",
    mPostalCode: "isSameAddr",
    mAddrBlock: "isSameAddr",
    mAddrStreet: "isSameAddr",
    branchInfo: ["bankRefId", "branch"]
  };
  if (
    profile.extra.channelName !== "SINGPOST" &&
    targetProfileName === "proposer"
  ) {
    fieldTriggerList = {
      mAddrCountry: "isSameAddr",
      mPostalCode: "isSameAddr",
      mAddrBlock: "isSameAddr",
      mAddrStreet: "isSameAddr"
    };
  }
  const fieldTriggerValues = {
    isSameAddr: "Y"
  };

  // Init the Error Obj
  _.set(errorObj, `application.${targetProfileName}.personalInfo`, {});
  if (
    profile.extra.channelName === "SINGPOST" &&
    targetProfileName === "proposer"
  ) {
    checkBranchInfo({
      profile: profile.personalInfo,
      field: fieldTriggerList.branchInfo,
      errorObj
    });
  }

  if (
    profile.personalInfo.nationality !== "N1" &&
    profile.personalInfo.prStatus === "N"
  ) {
    _.set(
      errorObj,
      `application.${targetProfileName}.personalInfo.passExpDate`,
      validation.validateMandatory({
        field: {
          type: TEXT_BOX,
          mandatory: true,
          disabled: false
        },
        value: profile.personalInfo.passExpDate || null
      })
    );
  }

  Object.entries(initFields).forEach(([formName, formFieldArr]) => {
    Object.keys(profile[formName]).forEach(fieldName => {
      if (formFieldArr.indexOf(fieldName) >= 0) {
        const fieldTriggerFrom = fieldTriggerList[fieldName];
        const fieldTriggerValue = fieldTriggerValues[fieldName];
        if (
          _.isEmpty(fieldTriggerFrom) ||
          profile[formName][fieldTriggerFrom] === fieldTriggerValue
        ) {
          _.set(
            errorObj,
            `application.${targetProfileName}.${formName}.${fieldName}`,
            validation.validateMandatory({
              field: {
                type: TEXT_BOX,
                mandatory: true,
                disabled: false
              },
              value: profile[formName][fieldName]
            })
          );
        }
      }
    });
    if (profile[formName].isSameAddr === "N") {
      Object.keys(fieldTriggerList).forEach(trigerListKey => {
        if (formFieldArr.indexOf(trigerListKey) >= 0) {
          _.set(
            errorObj,
            `application.${targetProfileName}.${formName}.${trigerListKey}`,
            validation.validateMandatory({
              field: {
                type: TEXT_BOX,
                mandatory: true,
                disabled: false
              },
              value: profile[formName][trigerListKey]
            })
          );
        }
      });
    }
  });
}

/**
 * initApplicationFormPersonalDetailsValue
 * @description Only trigger once after enters the Application Form Page
 * @param {object} application - The real application data object from reducer store application.application
 * */
export const initApplicationFormPersonalDetailsValue = ({
  application,
  template
}) => {
  const newApplication = _.cloneDeep(application);

  const proposerValues = _.get(
    newApplication,
    "applicationForm.values.proposer",
    {}
  );
  const insuredValues = _.get(
    newApplication,
    "applicationForm.values.insured",
    []
  );

  if (!_.isEmpty(proposerValues)) {
    if (!_.has(proposerValues, "personalInfo.isSameAddr")) {
      proposerValues.personalInfo.isSameAddr = "Y";
    } else if (
      proposerValues.personalInfo.isSameAddr === "" ||
      !proposerValues.personalInfo.isSameAddr
    ) {
      proposerValues.personalInfo.isSameAddr = "Y";
    }
  }
  if (!_.isEmpty(insuredValues)) {
    _.forEach(insuredValues, insured => {
      if (!_.has(insured, "personalInfo.isSameAddr")) {
        insured.personalInfo.isSameAddr = "Y";
      } else if (
        insured.personalInfo.isSameAddr === "" ||
        !insured.personalInfo.isSameAddr
      ) {
        insured.personalInfo.isSameAddr = "Y";
      }
    });
  }

  if (!_.has(newApplication, "applicationForm.values.checkedMenu")) {
    _.set(newApplication, "applicationForm.values.checkedMenu", [
      PERSONAL_DETAILS
    ]);
  }

  if (!_.has(newApplication, "applicationForm.values.completedMenus")) {
    _.set(newApplication, "applicationForm.values.completedMenus", []);
  }

  const menus = [PERSONAL_DETAILS, RESIDENCY];
  if (
    checkHasInsurability({
      application: newApplication,
      template
    })
  ) {
    menus.push(INSURABILITY);
  }
  menus.push(PLAN_DETAILS);
  menus.push(DECLARATION);

  if (!_.has(newApplication, "applicationForm.values.menus")) {
    _.set(newApplication, "applicationForm.values.menus", menus);
  }

  return newApplication;
};

/**
 * initValidateApplicationPersonalDetailsForm
 * @description Only trigger once after enters the Application Form Page
 * @param {object} application - The real application data object from reducer store application.application
 * @param {object} errorObj - error object from reducer store application.error
 * */
export const initValidateApplicationPersonalDetailsForm = ({
  application,
  errorObj
}) => {
  if (!_.isEmpty(_.get(application, "applicationForm.values"))) {
    Object.entries(application.applicationForm.values).forEach(
      ([key, profile]) => {
        const isShield = application.quotation.quotType === "SHIELD";
        if (key === "proposer") {
          initApplicationFormPersonalDetailsError({
            errorObj,
            profile,
            targetProfileName: key,
            isShield
          });
        } else if (key === "insured") {
          application.applicationForm.values.insured.forEach(
            (insuredProfile, arrayIndex) => {
              initApplicationFormPersonalDetailsError({
                errorObj,
                profile: insuredProfile,
                targetProfileName: `insured[${arrayIndex}]`,
                isShield
              });
            }
          );
        }
      }
    );
  }
};

export const initValidateApplicationFormError = ({
  template,
  rootValues,
  errorObj
}) => {
  const values = _.get(rootValues, "applicationForm.values", {});
  const applicationFormPageIds = ["residency", "insurability", "declaration"];
  const menuTemplate =
    // Shield case & non-Shield case
    _.toUpper(_.get(template, "items[0].type", "")) === "MENU"
      ? _.get(template, "items[0]")
      : _.get(template, "items[0].items[0]");
  const mapping = {
    residency: {
      template: _.get(menuTemplate, `items[0].items[1].items[0]`, {})
    },
    insurability: {
      template: _.get(menuTemplate, `items[0].items[2].items[0]`, {})
    },
    declaration: {
      template: _.get(menuTemplate, `items[2].items[0].items[0]`, {})
    }
  };

  if (_.has(values, "proposer") && !_.isEmpty(values, "proposer")) {
    _.each(applicationFormPageIds, pageId => {
      const pageTemplate = _.get(mapping, `${pageId}.template`);
      const profileTemplate = _.find(
        pageTemplate.items,
        item => item.subType === "proposer"
      );
      if (!_.isUndefined(profileTemplate)) {
        _.each(_.get(profileTemplate, "items"), sectionTemplate => {
          const sectionId = sectionTemplate.id;
          const path = `applicationForm.values.proposer.${sectionId}`;
          if (_.isUndefined(_.get(errorObj, path))) {
            _.set(errorObj, path, {});
          }
          validation.validateApplicationFormTemplate({
            iTemplate: sectionTemplate,
            iValues: _.get(rootValues, path, {}),
            iErrorObj: _.get(errorObj, path),
            rootValues,
            path
          });
        });
      }
    });
  }

  if (_.has(values, "insured") && !_.isEmpty(values, "insured")) {
    _.each(values.insured, (insured, index) => {
      _.each(applicationFormPageIds, pageId => {
        const pageTemplate = _.get(mapping, `${pageId}.template`);
        const profileTemplate = _.get(
          _.filter(pageTemplate.items, item => item.subType === "insured"),
          `[${index}]`
        );
        if (!_.isUndefined(profileTemplate)) {
          _.each(_.get(profileTemplate, "items"), sectionTemplate => {
            const sectionId = sectionTemplate.id;
            const path = `applicationForm.values.insured[${index}].${sectionId}`;
            if (_.isUndefined(_.get(errorObj, path))) {
              _.set(errorObj, path, {});
            }
            validation.validateApplicationFormTemplate({
              iTemplate: sectionTemplate,
              iValues: _.get(rootValues, path, {}),
              iErrorObj: _.get(errorObj, path),
              rootValues,
              path
            });
          });
        }
      });
    });
  }
};

/**
 * validateApplicationFormPersonalDetails
 * @description Trigger each time after input data into the Application Form Page
 * @param {object} validateObj - The rules set from axa-app
 * @param {object} errorObj - error object from reducer store application.error
 * */

export const validateApplicationFormPersonalDetails = ({
  validateObj,
  errorObj
}) => {
  _.set(
    errorObj.application,
    validateObj.field,
    validation.validateMandatory({
      field: {
        type: TEXT_FIELD,
        mandatory: validateObj.mandatory,
        disabled: false
      },
      value: validateObj.value
    })
  );
};

const checkPersonalDetailsProfileHasError = profile => {
  let hasError = false;
  Object.values(profile.personalInfo).some(field => {
    if (field.hasError) {
      hasError = true;
      return true;
    }
    return false;
  });
  return hasError;
};

const checkPersonalDetailsHasError = ({ targetProfile = "", error }) => {
  let hasError = false;
  if (!_.isEmpty(error) && !_.isEmpty(error.application)) {
    Object.values(error.application).forEach(profile => {
      if (profile.personalInfo && ["", "proposer"].includes(targetProfile)) {
        const profileHasError = checkPersonalDetailsProfileHasError(profile);
        hasError = hasError || profileHasError;
      } else if (Array.isArray(profile)) {
        profile.forEach((p, index) => {
          if (["", `insured[${index}]`].includes(targetProfile)) {
            const profileHasError = checkPersonalDetailsProfileHasError(p);
            hasError = hasError || profileHasError;
          }
        });
      }
    });
  }
  return hasError;
};

const checkDynamicFormByProfileHasError = ({ dataKey, profileError }) => {
  let hasError = false;
  if (profileError[dataKey]) {
    Object.values(profileError[dataKey]).some(field => {
      if (field.hasError) {
        hasError = true;
        return true;
      }
      return false;
    });
  }
  return hasError;
};

const checkDynamicFormHasError = ({ dataKey, error, targetProfile = "" }) => {
  let hasError = false;
  const iError = _.get(error, "applicationForm.values", {});

  if (!_.isEmpty(iError)) {
    if (_.has(iError, "proposer") && ["", "proposer"].includes(targetProfile)) {
      const profileError = iError.proposer;
      hasError =
        hasError ||
        checkDynamicFormByProfileHasError({ dataKey, profileError });
    }
    if (_.has(iError, "insured")) {
      _.each(iError.insured, (insured, index) => {
        if (["", `insured[${index}]`].includes(targetProfile)) {
          const profileError = insured;
          hasError =
            hasError ||
            checkDynamicFormByProfileHasError({ dataKey, profileError });
        }
      });
    }
  }
  return hasError;
};

export const checkApplicationFormSectionHasError = ({
  sectionKey,
  targetProfile = "",
  error
}) => {
  if (sectionKey === SECTION_KEYS[APPLICATION].PERSONAL_DETAILS) {
    return checkPersonalDetailsHasError({ targetProfile, error });
  }
  let formHasError = false;
  _.forEach(sectionMapping[sectionKey], dataKey => {
    formHasError =
      formHasError ||
      checkDynamicFormHasError({ dataKey, targetProfile, error });
  });
  return formHasError;
};

export const checkSignatureHasError = application => !application.isFullySigned;

export const checkPaymentHasError = ({ application, error }) => {
  if (!application.payment) {
    return true;
  }

  let hasError = false;
  _.forEach(error, e => {
    if (_.isObject(e)) {
      hasError = hasError || e.hasError;
    }
  });

  return hasError;
};

const checkApplicationFormByProfileHasError = ({
  application,
  error,
  targetProfile
}) => {
  let hasError = false;
  _.forEach(sectionMapping, (dataKey, sectionKey) => {
    const sectionHasError = checkApplicationFormSectionHasError({
      sectionKey,
      targetProfile,
      error
    });
    hasError = hasError || sectionHasError;
  });

  if (targetProfile !== "") {
    _.set(
      application,
      `applicationForm.values.${targetProfile}.extra.isCompleted`,
      !hasError
    );
  }

  return hasError;
};

export const checkApplicationFormHasError = ({ application, error }) => {
  let hasError = false;
  if (!_.isEmpty(_.get(application, "applicationForm.values"))) {
    Object.entries(application.applicationForm.values).forEach(([key]) => {
      if (key === "proposer") {
        const profileHasError = checkApplicationFormByProfileHasError({
          application,
          error,
          targetProfile: key
        });
        hasError = hasError || profileHasError;
      } else if (key === "insured") {
        application.applicationForm.values.insured.forEach(
          (insuredProfile, index) => {
            const profileHasError = checkApplicationFormByProfileHasError({
              application,
              error,
              targetProfile: `insured[${index}]`
            });
            hasError = hasError || profileHasError;
          }
        );
      }
    });
  }
  return hasError;
};

export const checkCanShieldApplicationGoSignature = error => {
  const personalHasError = checkPersonalDetailsHasError({ error });

  let proposerHasError = false;
  _.forEach(sectionMapping, (dataKey, sectionKey) => {
    const sectionHasError = checkApplicationFormSectionHasError({
      sectionKey,
      targetProfile: "proposer",
      error
    });
    proposerHasError = proposerHasError || sectionHasError;
  });

  return !personalHasError && !proposerHasError;
};

export const checkCanShieldApplicationGoPayment = ({ application, error }) => {
  const canGoSignature = checkCanShieldApplicationGoSignature(error);
  return (
    canGoSignature &&
    application.isProposalSigned &&
    application.isAppFormProposerSigned
  );
};

export const updateProfileList = ({ updIds, application }) => {
  const profileList = [];

  const proposerPersonalInfo = _.get(
    application,
    "applicationForm.values.proposer.personalInfo"
  );
  const insuredsArr = _.get(application, "applicationForm.values.insured", []);

  if (updIds.indexOf(_.get(proposerPersonalInfo, "cid") > -1)) {
    profileList.push(proposerPersonalInfo);
  }
  if (insuredsArr.length > 0) {
    insuredsArr.forEach(insured => {
      if (updIds.indexOf(_.get(insured, "personalInfo.cid")) > -1) {
        profileList.push(insured.personalInfo);
      }
    });
  }
  return profileList;
};

export const updateApplicationFormCompletedMenus = ({
  template,
  application,
  error,
  sectionKey,
  isInit
}) => {
  const checkedMenu = _.get(
    application,
    "applicationForm.values.checkedMenu",
    []
  );
  const completedMenus = _.get(
    application,
    "applicationForm.values.completedMenus",
    []
  );
  const hasInsurability = checkHasInsurability({ application, template });

  let sectionHasError =
    sectionKey === PERSONAL_DETAILS
      ? checkPersonalDetailsHasError({ error })
      : checkApplicationFormSectionHasError({ sectionKey, error });
  if (
    (hasInsurability && sectionKey === INSURABILITY) ||
    (!hasInsurability && sectionKey === DECLARATION)
  ) {
    sectionHasError =
      sectionHasError || !checkHasPolicyNumber({ application, error });
  }

  const removeCompletedMenu =
    sectionHasError && completedMenus.includes(sectionKey);
  const addCompletedMenu =
    !sectionHasError && !completedMenus.includes(sectionKey);

  const removeCheckedMenu =
    isInit && sectionHasError && checkedMenu.includes(sectionKey);
  const addCheckedMenu =
    isInit && !sectionHasError && !checkedMenu.includes(sectionKey);

  if (removeCompletedMenu) {
    const index = completedMenus.indexOf(sectionKey);
    completedMenus.splice(index, 1);
  } else if (addCompletedMenu) {
    completedMenus.push(sectionKey);
  }

  if (removeCheckedMenu) {
    const index = checkedMenu.indexOf(sectionKey);
    checkedMenu.splice(index, 1);
  } else if (addCheckedMenu) {
    checkedMenu.push(sectionKey);
  }
};
