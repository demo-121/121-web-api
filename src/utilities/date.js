export function getAttainedAge(targetDate, dob) {
  const birthDate = new Date(dob.getFullYear(), dob.getMonth(), dob.getDate());
  const tDate = new Date(
    targetDate.getFullYear(),
    targetDate.getMonth(),
    targetDate.getDate()
  );

  let age = tDate.getFullYear() - birthDate.getFullYear();
  const m = tDate.getMonth() - birthDate.getMonth();
  const d = tDate.getDate() - birthDate.getDate();
  let monthNum = age * 12 + tDate.getMonth() - birthDate.getMonth();
  const dayNum = Math.round(
    (tDate.getTime() - birthDate.getTime()) / (24 * 3600000)
  );
  if (m < 0 || (m === 0 && d < 0)) {
    age -= 1;
  }
  if (d < 0) {
    monthNum -= 1;
  }
  return {
    year: age,
    month: monthNum,
    day: dayNum
  };
}

/**
 * Get the nearest age on a specific date.
 * e.g. attained age: 19.2 -> nearest age 19
 *      attained age: 19.8 -> nearest age 20
 *
 * @param {Date} targetDate target date, will only use the date part
 * @param {Date} dob date of birth
 */
export function getNearestAge(targetDate, dob) {
  const tYr = targetDate.getFullYear();
  const tDate = new Date(tYr, targetDate.getMonth(), targetDate.getDate());

  const dobYr = dob.getFullYear();
  const dobMonth = dob.getMonth();
  const dobDay = dob.getDate();

  const tyBirthday = new Date(tYr, dobMonth, dobDay);
  const prevDob =
    tDate - tyBirthday >= 0 ? tyBirthday : new Date(tYr - 1, dobMonth, dobDay);
  const nextDob =
    tDate - tyBirthday < 0 ? tyBirthday : new Date(tYr + 1, dobMonth, dobDay);

  const lastDiff = Math.abs(tDate - prevDob);
  const nextDiff = Math.abs(tDate - nextDob);
  const finalDob = lastDiff > nextDiff ? nextDob : prevDob;

  return finalDob.getFullYear() - dobYr;
}

/**
 * Get the age on next birthday for a specific date.
 * e.g. attained age: 19.2 -> nearest age 20
 *      attained age: 19.8 -> nearest age 20
 *
 * @param {Date} targetDate target date, will only use the date part
 * @param {Date} dob date of birth
 */
export function getAgeNextBirthday(targetDate, dob) {
  const tYr = targetDate.getFullYear();
  const tDate = new Date(tYr, targetDate.getMonth(), targetDate.getDate());

  const dobYr = dob.getFullYear();
  const dobMonth = dob.getMonth();
  const dobDay = dob.getDate();

  const tyBirthday = new Date(tYr, dobMonth, dobDay);
  const nextDob =
    tDate - tyBirthday < 0 ? tyBirthday : new Date(tYr + 1, dobMonth, dobDay);

  return nextDob.getFullYear() - dobYr;
}
