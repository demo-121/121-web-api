import * as _ from "lodash";
import { SPO } from "../constants/DEPENDANT";

export const getClientName = ({ profile, dependantProfiles, cid }) => {
  const targetProfile = profile.cid === cid ? profile : dependantProfiles[cid];
  return targetProfile && targetProfile.fullName;
};

export const getSpouseProfile = ({ profile, dependantProfiles }) => {
  const spouse = _.find(
    _.get(profile, "dependants"),
    d => d.relationship === SPO
  );
  return _.get(dependantProfiles, _.get(spouse, "cid"));
};
