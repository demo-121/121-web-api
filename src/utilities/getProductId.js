/* eslint-disable no-underscore-dangle */
export default function getProductId(product) {
  switch (true) {
    case !product:
      break;
    case !!product.id:
      return product.id;
    case !!product._id:
      return product._id;
    default: {
      const idA = product.compCode || "08";
      const idB = product.covCode;
      const idC = product.version ? product.version : "1";

      return `${idA}_product_${idB}_${idC}`;
    }
  }
  throw new Error("can not get product id.");
}
