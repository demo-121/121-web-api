import * as _ from "lodash";
import { getCountryWithCityList } from "./common";
import { getAttainedAge } from "./date";
import { getItemValueFunc, getPathFunc } from "./dynamicApplicationForm";

/**
 * Special handling for Dynamic Condition
 */
const checkTriggerForDynamicCondition = ({
  trigger,
  rootValues,
  valuePath
}) => {
  const { id, value: triggerValue } = trigger;
  const idAddressArr = id.split(",");
  const itemValue = idAddressArr.map(idAddress => {
    if (idAddress.indexOf("@") === 0 && idAddress.indexOf("@spouse") === -1) {
      const idArr = idAddress.slice(1).split(".");
      const path = getPathFunc({ idArr, valuePath });
      return _.get(rootValues, path, "");
    } else if (idAddress.indexOf("#") === 0) {
      const idArr = idAddress.slice(1).split(".");
      const valuePathTemp = valuePath.split(".").splice(0, 2);
      const resultIdArr = valuePathTemp.concat(idArr);
      const path = resultIdArr.join(".");
      return _.get(rootValues, path, "");
    }
    return "";
  });
  let newTriggerValue = triggerValue;
  _.each(itemValue, (value, index) => {
    const reg = new RegExp(`#${index + 1}\\b`, "g");
    newTriggerValue = newTriggerValue.replace(reg, `itemValue[${index}]`);
  });
  return eval(newTriggerValue);
  // return false;
};

const checkTriggerForComparePremium = ({
  trigger,
  rootValues,
  valuePath,
  itemValue
}) => {
  const { ccys, id } = trigger;
  const validRefValues = getItemValueFunc({
    rootValues,
    valuePath,
    id
  });
  const values = _.get(rootValues, valuePath);
  const ccy =
    values.ccy ||
    (validRefValues.extra ? validRefValues.extra.ccy : null) ||
    "SGD";
  let result = false;
  for (let i = 0; i < ccys.length; i += 1) {
    if (ccy === ccys[i].ccy) {
      if (itemValue >= ccys[i].amount) result = true;
      break;
    }
  }
  return result;
};

const checkTrigger = ({ trigger, rootValues, valuePath }) => {
  const { type, id, value: triggerValue } = trigger;
  if (type === "dynamicCondition") {
    return checkTriggerForDynamicCondition({ trigger, rootValues, valuePath });
  }
  // This type trigger does not mean show/hide function
  else if (type === "excludeSG") {
    return true;
  } else if (type === "showIfNotExist") {
    return true;
  }

  const itemValue = getItemValueFunc({ rootValues, valuePath, id });
  if (type === "empty") {
    return !itemValue;
  }
  if (_.isUndefined(itemValue)) {
    return false;
  }
  if (type === "showIfEqual") {
    return _.toUpper(itemValue) === _.toUpper(triggerValue);
  } else if (type === "showIfNotEqual") {
    return _.toUpper(itemValue) !== _.toUpper(triggerValue);
  } else if (type === "showIfExist") {
    const triggerValueArr = _.toUpper(triggerValue).split(",");
    return _.includes(triggerValueArr, _.toUpper(itemValue));
  } else if (type === "notEmpty") {
    return !_.isEmpty(itemValue);
  } else if (type === "showIfSmaller") {
    return _.isNumber(itemValue) && itemValue < triggerValue;
  } else if (type === "showIfGreater") {
    return _.isNumber(itemValue) && itemValue > triggerValue;
  } else if (type === "comparePremium") {
    return checkTriggerForComparePremium({
      trigger,
      rootValues,
      valuePath,
      itemValue
    });
  } else if (type === "showIfWithinSixMonthOld") {
    const days = getAttainedAge(new Date(), new Date(itemValue)).day;
    return days < 184 && days >= 0;
  }
  return true;
};

/**
 * This function is to find whether the component should be displayed or not
 * It depends on the component's template Json's "trigger" field
 */
export const show = ({ template, rootValues, valuePath }) => {
  if (!_.has(template, "trigger")) {
    return true;
  }
  const { trigger } = template;

  // trigger can be array of objects or object
  if (_.isArray(trigger)) {
    if (template.triggerType === "and") {
      return !_.some(
        trigger,
        itemTrigger =>
          !checkTrigger({
            trigger: itemTrigger,
            rootValues,
            valuePath
          })
      );
    }
    return _.some(trigger, itemTrigger =>
      checkTrigger({ trigger: itemTrigger, rootValues, valuePath })
    );
  }
  return checkTrigger({ trigger, rootValues, valuePath });
};

export const setMandatory = ({ template, rootValues, valuePath }) => {
  if (_.has(template, "mandatoryTrigger")) {
    const trigger = _.get(template, "mandatoryTrigger");
    const triggerId = _.get(trigger, "id");
    const triggerValue = _.get(trigger, "value");
    const triggerType = _.toUpper(_.get(trigger, "type"));
    const value = _.get(rootValues, `${valuePath}.${triggerId}`);

    _.set(template, "mandatory", false);
    if (triggerType === _.toUpper("trueIfContains")) {
      if (_.includes(triggerValue, value)) {
        _.set(template, "mandatory", true);
      }
    } else if (triggerType === _.toUpper("trueIfNotContains")) {
      if (!_.includes(triggerValue, value)) {
        _.set(template, "mandatory", true);
      }
    } else if (triggerType === _.toUpper("trueIfEmpty")) {
      if (
        _.isUndefined(value) ||
        value === "" ||
        (value !== "A" && value !== "B")
      ) {
        _.set(template, "mandatory", true);
      } else {
        delete template.mandatory;
      }
    } else if (triggerType === _.toUpper("falseIfContains")) {
      if (_.includes(triggerValue, value)) {
        _.set(template, "mandatory", false);
      } else {
        _.set(template, "mandatory", true);
      }
    }
  }
};

export const setDisable = ({ template, rootValues, valuePath, optionsMap }) => {
  const { disableTrigger } = template;
  if (disableTrigger) {
    const { id: triggerId, value: triggerValue, type } = disableTrigger;
    const triggerType = type.toUpperCase();

    if (triggerType === "AnyFieldMatch".toUpperCase()) {
      const ids = triggerId.split(",");
      template.disabled = _.some(
        ids,
        id =>
          getItemValueFunc({
            rootValues,
            valuePath,
            id
          }) === triggerValue
      );
    } else {
      const value = getItemValueFunc({
        rootValues,
        valuePath,
        id: triggerId
      });

      if (triggerType === "trueIfEqual".toUpperCase()) {
        if (triggerValue === value) {
          template.disabled = true;
        } else {
          template.disabled = false;
        }
      } else if (triggerType === "trueIfContains".toUpperCase()) {
        let checkValue = triggerValue;
        if (_.isString(triggerValue) && triggerValue.toLowerCase() === "city") {
          checkValue = getCountryWithCityList({
            optionsMap,
            optionPath: triggerValue.toLowerCase()
          });
          template.disableTrigger.value = checkValue;
        }

        if (checkValue.indexOf(value) > -1) {
          template.disabled = true;
        } else {
          template.disabled = false;
        }
      } else if (triggerType === "trueIfNotContains".toUpperCase()) {
        let checkValue = triggerValue;
        if (_.isString(triggerValue) && triggerValue.toLowerCase() === "city") {
          checkValue = getCountryWithCityList({
            optionsMap,
            optionPath: triggerValue.toLowerCase()
          });
          template.disableTrigger.value = checkValue;
        }

        if (checkValue.indexOf(value) < 0) {
          template.disabled = true;
        } else {
          template.disabled = false;
        }
      } else if (triggerType === "trueIfNotEqual".toUpperCase()) {
        if (triggerValue !== value) {
          template.disabled = true;
        } else {
          template.disabled = false;
        }
      }
    }
  }
};

export function optionCondition({ value, options, optionsMap, showIfNoValue }) {
  if (showIfNoValue) {
    return true;
  }
  let option;
  if (_.isString(options)) {
    [option] = _.at(optionsMap, `${options}.options`);
  }
  return !!(option && _.filter(option, opt => opt.condition === value).length);
}
