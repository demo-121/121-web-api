import * as _ from "lodash";
import * as validation from "../utilities/validation";
import * as ROP_TABLE from "../constants/ROP_TABLE";

const { REPLACE_POLICY_TYPE_AXA, REPLACE_POLICY_TYPE_OTHER } = ROP_TABLE;

export const getPathFunc = ({ idArr, valuePath }) => {
  const originalIdArr = valuePath.split(".");
  const newIdCount = idArr.length;
  const resultIdArr = [...originalIdArr.slice(0, -newIdCount + 1), ...idArr];
  const result = _.join(resultIdArr, ".");

  return result;
};

export const getItemValueFunc = ({ rootValues, valuePath, id }) => {
  if (id && id.indexOf("/") !== -1) {
    const idToSplit = id.indexOf("@") === 0 ? id.substring(1) : id;
    const idArr = idToSplit.split("/");
    const path = getPathFunc({ idArr, valuePath });
    return _.get(rootValues, path);
  }
  const path = `${valuePath}.${id}`;
  return _.get(rootValues, path);
};

export const updateFieldValue = ({
  iTemplate,
  rootValues,
  path,
  id,
  pathValue
}) => {
  const itemPath = `${path}.${id}`;
  _.set(rootValues, itemPath, pathValue);

  if (_.has(iTemplate, "valueTrigger")) {
    const valueTriggerObj = _.get(iTemplate, "valueTrigger", {});

    if (_.get(valueTriggerObj, "type", "") === "resetTargetValue") {
      const targetIdArr = _.split(_.get(valueTriggerObj, "id", ""), ",");
      const validateArr = _.split(_.get(valueTriggerObj, "value", ""), ",");
      if (pathValue === validateArr[0]) {
        _.each(targetIdArr, targetId => {
          _.set(rootValues, `${path}.${targetId}`, validateArr[1]);
        });
      }
    }
  }
};

export const initValidateApplicationFormError = ({
  template,
  rootValues,
  errorObj
}) => {
  const values = _.get(rootValues, "applicationForm.values", {});
  const applicationFormPageIds = ["residency", "insurability", "declaration"];
  const menuTemplate =
    // Shield case & non-Shield case
    _.toUpper(_.get(template, "items[0].type", "")) === "MENU"
      ? _.get(template, "items[0]")
      : _.get(template, "items[0].items[0]");
  const mapping = {
    residency: {
      template: _.get(menuTemplate, `items[0].items[1].items[0]`, {})
    },
    insurability: {
      template: _.get(menuTemplate, `items[0].items[2].items[0]`, {})
    },
    declaration: {
      template: _.get(menuTemplate, `items[2].items[0].items[0]`, {})
    }
  };

  if (_.has(values, "proposer") && !_.isEmpty(values, "proposer")) {
    _.each(applicationFormPageIds, pageId => {
      const pageTemplate = _.get(mapping, `${pageId}.template`);
      const profileTemplate = _.find(
        pageTemplate.items,
        item => item.subType === "proposer"
      );
      if (!_.isUndefined(profileTemplate)) {
        _.each(_.get(profileTemplate, "items"), sectionTemplate => {
          const sectionId = sectionTemplate.id;
          const path = `applicationForm.values.proposer.${sectionId}`;
          if (_.isUndefined(_.get(errorObj, path))) {
            _.set(errorObj, path, {});
          }
          validation.validateApplicationFormTemplate({
            iTemplate: sectionTemplate,
            iValues: _.get(rootValues, path, {}),
            iErrorObj: _.get(errorObj, path),
            rootValues,
            path
          });
        });
      }
    });
  }

  if (_.has(values, "insured") && !_.isEmpty(values, "insured")) {
    _.each(values.insured, (insured, index) => {
      _.each(applicationFormPageIds, pageId => {
        const pageTemplate = _.get(mapping, `${pageId}.template`);
        const arrProfileTemplate = _.filter(
          pageTemplate.items,
          item => item.subType === "insured"
        );
        if (!_.isUndefined(arrProfileTemplate)) {
          const profileTemplate = arrProfileTemplate[index];
          if (!_.isUndefined(profileTemplate)) {
            _.each(_.get(profileTemplate, "items"), sectionTemplate => {
              const sectionId = sectionTemplate.id;
              const path = `applicationForm.values.insured[${index}].${sectionId}`;
              if (_.isUndefined(_.get(errorObj, path))) {
                _.set(errorObj, path, {});
              }
              validation.validateApplicationFormTemplate({
                iTemplate: sectionTemplate,
                iValues: _.get(rootValues, path, {}),
                iErrorObj: _.get(errorObj, path),
                rootValues,
                path
              });
            });
          }
        }
      });
    });
  }
};

export function calculateRopTable({ rootValues, path, id, pathValue }) {
  if (!_.isString(id)) {
    return;
  }

  if ([REPLACE_POLICY_TYPE_AXA, REPLACE_POLICY_TYPE_OTHER].indexOf(id) > -1) {
    _.set(rootValues, `${path}.${id}`, pathValue);
  } else if (_.isNumber(pathValue)) {
    if (pathValue < 0) {
      return;
    }

    const values = _.get(rootValues, path, {});
    let relatedId;
    let relatedTotalId;

    if (id.indexOf("AXA") > -1) {
      relatedId = _.replace(id, "AXA", "Other");
      relatedTotalId = _.replace(id, "AXA", "");
    } else if (id.indexOf("Other") > -1) {
      relatedId = _.replace(id, "Other", "AXA");
      relatedTotalId = _.replace(id, "Other", "");
    }

    if (id.indexOf("exist") > -1 || id.indexOf("replace") > -1) {
      const relatedTotalValue = _.get(values, relatedTotalId, 0);
      if (pathValue > relatedTotalValue) {
        return;
      }
      _.set(values, id, pathValue);
      if (_.isNumber(relatedTotalValue)) {
        _.set(values, relatedId, relatedTotalValue - pathValue);
      }
    } else if (id.indexOf("pending") > -1) {
      const relatedValue = _.get(values, relatedId, 0);
      _.set(values, id, pathValue);
      if (_.isNumber(relatedValue)) {
        _.set(values, relatedTotalId, relatedValue + pathValue);
      }
    }

    _.set(rootValues, path, values);
  }
}
