import * as _ from "lodash";
import SYSTEM from "../constants/SYSTEM";

const genProductMandatory = () => {
  const mandatory = [
    "nationality",
    "prStatus",
    "dob",
    "gender",
    "isSmoker",
    "industry",
    "occupation",
    "addrCountry",
    "residenceCountry"
  ];
  return mandatory;
};

const genFnaMandatory = isSpouse => {
  const mandatory = [
    "idDocType",
    "idCardNo",
    "title",
    "dob",
    "marital",
    "mobileNo",
    "countryCode",
    "language",
    "education",
    "employStatus",
    "allowance"
  ];
  if (isSpouse) {
    mandatory.push("relationship");
  }
  return mandatory;
};

const genApplicationMandatory = (moduleId, hasFNA, isProposer) => {
  let mandatory = [
    "nameOrder",
    "dob",
    "nationality",
    "residenceCountry",
    "idDocType",
    "idCardNo",
    "marital",
    "isSmoker",
    "industry",
    "occupation",
    "addrCountry",
    "allowance",
    "pass"
  ];
  if (isProposer) {
    mandatory = _.concat(mandatory, ["mobileNo", "mobileNoSection", "email"]);
    // only for agent proposer
    if (hasFNA) {
      mandatory = _.concat(mandatory, ["language", "education"]);
    }
  }
  if (hasFNA) {
    mandatory = _.concat(mandatory, ["title"]);
  }

  return mandatory;
};

export default function genProfileMandatory(
  moduleId,
  hasFNA,
  isProposer,
  isSpouse
) {
  switch (moduleId) {
    case SYSTEM.FEATURES.FNA:
      return genFnaMandatory(isSpouse);
    case SYSTEM.FEATURES.FQ:
    case SYSTEM.FEATURES.QQ:
      return genProductMandatory(moduleId, hasFNA, isProposer, isSpouse);
    case SYSTEM.FEATURES.EAPP:
      return genApplicationMandatory(moduleId, hasFNA, isProposer);
    default:
      return [];
  }
}
